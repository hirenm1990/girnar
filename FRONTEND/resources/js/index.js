$(function() {

    /**
     * Autoload tab based on URL hash
     */
    var tab = window.location.hash.substr(1);
    $('.tabs').find("li[data-tab='#"+tab+"']").click();


    /**
     * Global confirmation dialog for all buttons with class `.confirm-delete`
     */
    $(document).on("click", ".confirm-delete", function() {
        var me = $(this);

        var confirm = $.confirm({
            "title" : "Item Removal",
            "message" : "You are about to remove selected item `<em>forever</em>`. You will not be able to recover afterwise. Are you triple sure on your action?<br>",
            "buttons" : {
                "Yes" : {
                    class: "btn-red",
                    action: function() {
                        window.location.replace(me.attr('href'));
                    }
                },
                "No" : {
                    class: "btn-white"
                }
            }
        });

        return false;
    });


    /**
     * Stop form from autosubmitting on enter key press
     */
    $(document).on("keyup keypress", "form input", function(e) {
        var keyCode = e.KeyCode || e.which;
        if(keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });


    /**
     * Discharge Port auto fillup
     */
    function product_discharge_port_auto_fillup() {
        var opts = [
            $("select[name='port_discharge']").val() + ", " + $("select[name='final_destination']").val(),
            $("select[name='port_discharge2']").val() + ", " + $("select[name='final_destination2']").val(),
            $("select[name='port_discharge3']").val() + ", " + $("select[name='final_destination3']").val()
        ];

        // Remove select plugin instance & re-call
        $.each( $("select[name='product_discharge_port[]']"), function( i, v ) {
            var target = $(this);
            var val = target.val();
            var target2 = target.parent().find(".selectWrap");
            if(target.hasClass(target2.data('id'))) {

                var options = "";
                $.each( opts, function(i, v) {
                    if(v !== "") {
                        if(v == val) {
                            options += "<option value='" + v + "' selected='selected'>" + v + "</option>";
                        } else {
                            options += "<option value='" + v + "'>" + v + "</option>";
                        }
                    }
                });

                target.removeClass(target2.data('id'));
                target2.remove();
                target.show();
                target.html( options );
                target.select();
            }
        });
    }

    function discharge_port_auto_fillup() {
        var opts = [
            $("select[name='port_discharge']").val() + ", " + $("select[name='final_destination']").val(),
            $("select[name='port_discharge2']").val() + ", " + $("select[name='final_destination2']").val(),
            $("select[name='port_discharge3']").val() + ", " + $("select[name='final_destination3']").val()
        ];

        var val = $("select[name='discharge_portA']").val();
        var target = $("select[name='discharge_portA']");
        var target2 = target.parent().find(".selectWrap");
        if(target.hasClass(target2.data('id'))) 
        {
            var options = "";
            $.each( opts, function(i, v) {
                if(v !== "") {
                    if(v == val) {
                        options += "<option value='" + v + "' selected='selected'>" + v + "</option>";
                    } else {
                        options += "<option value='" + v + "'>" + v + "</option>";
                    }
                }
            });

            target.removeClass(target2.data('id'));
            target2.remove();
            target.show();
            target.html( options );
            target.select();
        }

        var val = $("select[name='discharge_portB']").val();
        var target = $("select[name='discharge_portB']");
        var target2 = target.parent().find(".selectWrap");
        if(target.hasClass(target2.data('id'))) 
        {
            var options = "";
            $.each( opts, function(i, v) {
                if(v !== "") {
                    if(v == val) {
                        options += "<option value='" + v + "' selected='selected'>" + v + "</option>";
                    } else {
                        options += "<option value='" + v + "'>" + v + "</option>";
                    }
                }
            });

            target.removeClass(target2.data('id'));
            target2.remove();
            target.show();
            target.html( options );
            target.select();
        }

        var val = $("select[name='discharge_portC']").val();
        var target = $("select[name='discharge_portC']");
        var target2 = target.parent().find(".selectWrap");
        if(target.hasClass(target2.data('id'))) 
        {
            var options = "";
            $.each( opts, function(i, v) {
                if(v !== "") {
                    if(v == val) {
                        options += "<option value='" + v + "' selected='selected'>" + v + "</option>";
                    } else {
                        options += "<option value='" + v + "'>" + v + "</option>";
                    }
                }
            });

            target.removeClass(target2.data('id'));
            target2.remove();
            target.show();
            target.html( options );
            target.select();
        }

        var val = $("select[name='discharge_portD']").val();
        var target = $("select[name='discharge_portD']");
        var target2 = target.parent().find(".selectWrap");
        if(target.hasClass(target2.data('id'))) 
        {
            var options = "";
            $.each( opts, function(i, v) {
                if(v !== "") {
                    if(v == val) {
                        options += "<option value='" + v + "' selected='selected'>" + v + "</option>";
                    } else {
                        options += "<option value='" + v + "'>" + v + "</option>";
                    }
                }
            });

            target.removeClass(target2.data('id'));
            target2.remove();
            target.show();
            target.html( options );
            target.select();
        }
        
    }

    $(document).on("change", "select[name='port_discharge'], select[name='port_discharge2'], select[name='port_discharge3'], select[name='final_destination'], select[name='final_destination2'], select[name='final_destination3']", function() {
        discharge_port_auto_fillup();
        product_discharge_port_auto_fillup();
    });


    /**
     * Expand Sub Contract Row
     */
    $(document).on("click", ".contract-row", function() {
        $(this).next('.sub-contract-row').toggle();
        $(this).find('.toggle-icon i').toggleClass('ti-plus ti-minus');
    });


    /**
     * Update buyer's Address on buyer change
     */
    $(document).on("change", "#importer_select", function() {
        var address = $(this).find(':selected').data('address');
        $("#importer_address").val(address);
    });


    /**
     * Add/Remove new product row
     */
    $(document).on("click", ".add-product-row", function() {
        var blank_product_row = $('#product-row-template').html();

        $.each($(blank_product_row).find("select"), function(i, v) {
            var target = $(this);
            var target2 = target.parent().find(".selectWrap");
            if (target.hasClass(target2.data('id'))) {
                target.removeClass(target2.data('id'));
                target2.remove();
                target.select();
            }
        });

        $(this).parents("tr").before( "<tr>" + blank_product_row + "</tr>" );
        $('.products_table .product_row:last').find("select").select();
    });
    $(document).on("click", ".remove-product-row", function() {
        $(this).parents("tr").remove();
    });

    /**
     * Notifier Party & Consignee address
     */
    $(document).on("change", "#notifier-party", function() {
        if($(this).prop("checked")) {
            $('#notifier-party-address').hide();
        } else {
            $('#notifier-party-address').show();
        }
    });
    $(document).on("change", "#consignee-party", function() {
        if($(this).prop("checked")) {
            $('#consignee-party-address').hide();
        } else {
            $('#consignee-party-address').show();
        }
    });
    $(document).on("click", "#getPrevAddr", function () {

    });


    /**
     * Automatic Amount Calculation : quantity * rate = amount
     */
    $(document).on("change", ".products_table .product_row input[name='quantity[]'], .products_table .product_row input[name='rate[]']", function() {
    	var row = $(this).parents(".product_row");
    	var quantity = row.find("input[name='quantity[]']").val();
    	var rate = row.find("input[name='rate[]']").val();
    	var total_amount = quantity * rate;
    	row.find("input[name='total_amount[]']").val( total_amount.toFixed(2) );
    });


    /**
     * Automatic Calculate total transit
     */
    $(document).on("change", ".etd_origin, .eta_destination", function() {
        var etd = $('.etd_origin').val() + "",
            eta = $('.eta_destination').val() + "";

        var dmy = etd.split('-');
        etd = new Date(dmy[2], dmy[1], dmy[0]);

        var dmy = eta.split('-');
        eta = new Date(dmy[2], dmy[1], dmy[0]);

        var diff = Math.round((eta-etd)/(1000*60*60*24));

        $(".total_transit").val( diff + " days");
    });


    /**
     * Quotation sheet
     */
    $(document).on("click", "#quotation-sheet", function() {
        var url = config.url + "quotationPartial";
        $(".modal.iFrameModal").data("url", url);
        $(".modal.iFrameModal #iFrameIframe").attr("src", url);
        $(".modal.iFrameModal").show();
        return false;
    });


    /**
     * Stuffing Tab
     */
    stuffingContainerCalculate();

    $(document).on("keyup", "#stuffingTab .inputC1,#stuffingTab .inputC2,#stuffingTab .inputC3", function() {
        stuffingContainerCalculate();
    });

    $(document).on("change", ".multipleInvoiceToggle", function() {
        if( $(this).prop('checked') ) {
            $('.multipleInvoiceTable').show();
        } else {
            $('.multipleInvoiceTable').hide();
        }
    });

    $(document).on("click", ".removeMultiInvoiceRow", function() {
        if($(this).parents("tr").is(":first-child")) {
            alert("Can't delete first row. It's required to have atleast one row!!!");
            return false;
        }

        $(this).parents("tr").remove();
    });

    $(document).on("click", ".addMultiInvoiceRow", function() {
        var html = "<tr>" + $(".multipleInvoiceTable table tbody tr:last-child").html() + "</tr>";
        $(".multipleInvoiceTable table tbody").append( html );
        var row = $(".multipleInvoiceTable table tbody tr:last-child");
        //row.find("input[name='m_invoice_no[]']").val( '' );
        //row.find("input[name='m_invoice_date[]']").val( '' );
        //row.find("input[name='m_upto[]']").val( '' );

        row.find(".datepick").removeClass('is-datepick');
        row.find(".datepick").datepick({
            dateFormat: "dd-mm-yyyy",
            showAnim: "",
            //monthsToShow: [2, 3],
            calculateWeek: true,
            onSelect: function() {
                $(this).trigger("change");
            }
        });
    });

    function stuffingContainerCalculate() {
        var total = 0;
        $.each($('#stuffingTab .inputC1'), function(i, v) {
            var inputC1 = $(this).val().replace(/[^0-9\.]/g, '');
            if(inputC1 > 0)
                total += parseFloat( inputC1 );
        });

        $("#stuffingTab .totalC1 input").val( total );

        var total = 0;
        $.each($('#stuffingTab .inputC2'), function() {
            if($(this).val() > 0)
                total += parseFloat($(this).val());
        });

        $("#stuffingTab .totalC2 input").val( total );

        var total = 0;
        $.each($('#stuffingTab .inputC3'), function() {
            if($(this).val() > 0)
                total += parseFloat($(this).val());
        });

        $("#stuffingTab .totalC3 input").val( total );
    }

    $(document).on("keyup", ".productNoOfPackages", function() {
        var flexi = $(".flexiTankToggle").prop("checked");
        if(flexi == true) {
            var c = $(this).data("container");
            $(".C" + c + "C1").val( $(this).val() );
            return false;
        }


        var c = $(this).data("container");
        var total = 0;
        var package_type = "";
        $.each( $(".PC" + c + "C1"), function(i) {
            if($(this).val()) {
                total += parseFloat($(this).val());


                // Get package type
                var me = $(this).val();
                var type = me.split(' ')[1];

                if(package_type == "" && type != "") {
                    package_type = type;
                } else if( package_type != type ) {
                    package_type = "Packages";
                }
            }
        });

        if(!package_type) {
            package_type = "Packages";
        }

        $(".C" + c + "C1").val( total + " " + package_type );
        stuffingContainerCalculate();
    });

    $(document).on("keyup", ".productGrossWeight", function() {
        var c = $(this).data("container");
        var total = 0;
        $.each( $(".PC" + c + "C2"), function(i) {
            if($(this).val()) {
                total += parseFloat($(this).val());
            }
        });

        $(".C" + c + "C2").val( total );
        stuffingContainerCalculate();
    });

    $(document).on("keyup", ".productNetWeight", function() {
        var c = $(this).data("container");
        var total = 0;
        $.each( $(".PC" + c + "C3"), function(i) {
            if($(this).val()) {
                total += parseFloat($(this).val());
            }
        });

        $(".C" + c + "C3").val( total );
        stuffingContainerCalculate();
    });

    // vgm gross_mass 20 
    
    // $(document).on("keyup","gross_net", function(){
    //     $(this).each(function(i){
    //         var index = $(this).data("index");
    //         var netVal = $(".g" + index + "n").val( $(this).val());
    //         gross_Mass_function();
    //     });
    // });



    // $(document).on("keyup","gross_tare", function(){
    //     $(this).each(function(i){
    //         var index = $(this).data("index");
    //         var tareVal = $(".g" + index + "t").val( $(this).val());
    //         gross_Mass_function();
    //     });
    // });

    // $(document).on("keyup","gross_packing", function(){
    //     $(this).each(function(i){
    //         var index = $(this).data("index");
    //         var packVal = $(".g" + index + "p").val( $(this).val());
    //         gross_Mass_function();
    //     });
    // });

    // function gross_Mass_function() {
    //     $(document).on("keyup","gross_packing", function(){
    //         $(this).each(function(i){
    //             var index = $(this).data("index");
    //             $(".g" + index + "a").val($(this).val(netVal + tareVal + packVal));
    //             // consol.log(total);
    //         });
    //     });
    // }

    // // vgm gross_mass 40 
    
    // $(document).on("keyup","grossmass_net", function(){
    //     $(this).each(function(i){
    //         var index = $(this).data("index");
    //         var netVal = $(".g" + index + "gn").val( $(this).val());
    //         grossMass_function();
    //     });
    // });



    // $(document).on("keyup","grossmass_tare", function(){
    //     $(this).each(function(i){
    //         var index = $(this).data("index");
    //         var tareVal = $(".g" + index + "gt").val( $(this).val());
    //         grossMass_function();
    //     });
    // });

    // $(document).on("keyup","grossmass_packing", function(){
    //     $(this).each(function(i){
    //         var index = $(this).data("index");
    //         var packVal = $(".g" + index + "gp").val( $(this).val());
    //         grossMass_function();
    //     });
    // });

    // function grossMass_function() {
    //     $(document).on("keyup","grossmass_packing", function(){
    //         $(this).each(function(i){
    //             var index = $(this).data("index");
    //             $(".g" + index + "ga").val($(this).val(netVal + tareVal + packVal));
    //             // consol.log(total);
    //         });
    //     });
    // }

});
