<?php

class SchemeController extends Controller {

    public function get() {
        return Scheme::orderBy('name')->get();
    }

    public function getById( $id ) {
        return Scheme::find( $id );
    }

    public function create() {
        $request = Request::all();

        $scheme = new Scheme;
        $scheme->name = $request['name'];
        $scheme->detail = $request['detail'];
        $id = $scheme->save();
        return $scheme;
    }

    public function update( $id ) {
        $request = Request::all();

        $scheme = Scheme::find( $id );
        $scheme->name = $request['name'];
        $scheme->detail = $request['detail'];

        $scheme->save();
    }

    public function remove( $id ) {
        Scheme::destroy( $id );
    }

}