<?php

class CompanyController extends Controller {

    public function get() {
        return Company::find( Request::get('_companyid') );
    }

    public function getById( $id ) {
       return Company::find( $id );
    }

    public function update() {
        $request = Request::all();
        $company = Company::find( $request['_companyid'] );
        $company->name = $request['name'];
        $company->address = $request['address'];
        $company->address_short = $request['address_short'];
        $company->phone = $request['phone'];
        $company->phone2 = $request['phone2'];
        $company->website = $request['website'];
        $company->iec_no = $request['iec_no'];
        $company->pan_no = $request['pan_no'];
        $company->gst_no = $request['gst_no'];
        $company->lut_no = $request['lut_no'];
        $company->bank_name = $request['bank_name'];
        $company->bank_address = $request['bank_address'];
        $company->save();
    }

}