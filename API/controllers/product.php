<?php

class ProductController extends Controller {

    public function get() {
        return Product::where('is_active', 1)->orderBy('name')->get();
    }

    public function getById( $id ) {
        return Product::find( $id );
    }

    public function create() {
        $request = Request::all();
        $product = new Product;
        $product->name = $request['name'];
        $product->hs_code = $request['hs_code'];
        $product->shortcode = isset($request['shortcode']) ? $request['shortcode'] : "";
        $id = $product->save();
        return $product;
    }

    public function update( $id ) {
        $request = Request::all();
        
        $product = Product::find( $id );
        $product->name = $request['name'];
        $product->hs_code = $request['hs_code'];
        $product->shortcode = $request['shortcode'];
        $product->dbk_scheme_no = $request['dbk_scheme_no'];
        $product->fob = $request['fob'];
        $product->file_no = $request['file_no'];
        $product->extra1 = $request['extra1'];
        $product->extra2 = $request['extra2'];
        $product->extra3 = $request['extra3'];
        $product->extra4 = $request['extra4'];

        $product->save();
    }

    public function remove( $id ) {
        $product = Product::find( $id );
        $product->is_active = 0;
        $product->save();
        //Product::destroy( $id );
    }

}