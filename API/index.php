<?php

# Load composer autoloader
require_once "vendor/autoload.php";

# Load configuration files
require "config.php";

header('Access-Control-Allow-Origin: *');

# Run router
Router::run();