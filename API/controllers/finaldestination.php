<?php

class FinalDestinationController extends Controller {

    public function get() {
        return FinalDestination::orderBy('name')->get();
    }

    public function getById( $id ) {
        return FinalDestination::find( $id );
    }

    public function create() {
        $request = Request::all();

        $finaldestination = new FinalDestination;
        $finaldestination->name = $request['name'];
        $id = $finaldestination->save();
        return $finaldestination;
    }

    public function update( $id ) {
        $request = Request::all();

        $finaldestination = FinalDestination::find( $id );
        $finaldestination->name = $request['name'];

        $finaldestination->save();
    }

    public function remove( $id ) {
        FinalDestination::destroy( $id );
    }

}