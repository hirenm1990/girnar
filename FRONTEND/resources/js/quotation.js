$(function() {

    // Rerun_times calculator
    var run_times = 0;
    var rerun_times = 0;


    /**
     * Populate Data
     */
    var json_data = $.parseJSON($('#quotation_data').val());
    $.each($('.input'), function(i, v) {
        $(this).html( json_data[i] );
    });


    /**
     * Run on startup
     */
    run();

    /**
     * Selector function
     */
    function _( $selector ) {
        var parsed = $selector.split('.');
        $selector = $("#" + parsed[0] + " ." + parsed[1]);

        var tag = $selector[0].tagName.toLowerCase();

        if(tag == "input") {
            var value= $selector.val();
        } else {
            var value= $selector.html();
        }

        // Set value = 0 if empty string
        if( value == "" ) {
            value = 0;
        }

        value = parseFloat( value );

        return value;
    }


    /**
     * Actual update function
     */
    function update( target ) {
        var value = eval(target.data('value'));

        if(isNaN(value)) {
            target.addClass('queue_for_rerun');
        } else {
            // Convert value to 2 decimal places only if float
            value = (value % 1 === 0) ? value : value.toFixed(2);

            target.html( value );
            target.removeClass('queue_for_rerun');
        }
    }


    /**
     * Main run function
     */
    function run() {
        if(run_times > 5) {
            //console.log("warning: run limit exceed!!!");
            //return false;
        }

        $.each( $("*[data-value]"), function(i, v) {
            update( $(this) );
        });

        run_times++;

        rerun();
    }

    /**
     * Rerun for update remaining queue_for_rerun
     * Will automatically loop again if there is need to rerun again
     */
    function rerun() {
        if(rerun_times > 5) {
            console.log("warning: rerun limit exceed!!!");
            return;
        }

        if($('.queue_for_rerun').length > 0) {
            $.each( $(".queue_for_rerun"), function(i, v) {
                update( $(this) );
            });
            rerun_times++;
        } else {
            return false;
        }

        if($('.queue_for_rerun').length > 0) {
            rerun();
            rerun_times++;
        }
    }

    /**
     * Make .input field editable
     */
    $('.input').attr('contenteditable', true);
    $('.input').keypress(function(e) {
        if(e.keyCode < 48 || e.keyCode > 57) {
            if(e.keyCode != 46) {
                return false;
            }
        }
    });

    /**
     * Re-Run on .input change event
     */
    $(document).on('blur', '.input', function() {
        if($(this).html() == "") {
            $(this).html( '0' );
        }
        run();
    });

    /**
     * Re-calculate using button
     */
    $(document).on('click', '#recalculate', function() {
        console.log( $.now() );
        run();
        console.log( $.now() );
    });

    /**
     * Column Sum Function With _ function included
     */
    function col_sum(target, start, end) {
        var total = 0;

        for(i=start; i <= end; i++) {
            total = total + _(target + i);
        }

        return total;
    }

    $(document).on("click", "#save-quotation-sheet", function() {
        var json_data = [];
        $.each($('.input'), function(i, v) {
            json_data.push($(this).html());
        });

        $.post(
            config.url + "quotation",
            {
                json_data: json_data
            },
            function( response ) {
                console.log( response );
                alert("Quotation Sheet Saved!!!");
            }
        );
    });

});