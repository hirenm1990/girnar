<?php

class ForwarderController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "Forwarder's Register",
            'Forwarders' => API::get('forwarders')
        ];
        $this->render('forwarders/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New Forwarder"
        ];
        $this->render('forwarders/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('forwarder', Request::all());
        $this->redirect('forwarders');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit Forwarder",
            'Forwarder' => API::get('forwarder/' . $id)
        ];
        $this->render('forwarders/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("forwarder/$id", Request::all());
        $this->redirect('forwarders');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('forwarder/' . $id);
        $this->redirect('forwarders');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('forwarders/quickAdd');
    }
    public function quickCreate() {
        echo API::post('forwarder', Request::all(), true);
        exit();
    }
}