<?php

# Set default timezone
date_default_timezone_set("Asia/Kolkata");

# Database configuration
$capsule = new Illuminate\Database\Capsule\Manager;

$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'girnar2',
    'username' => 'root',
    'password' => '',
    'collation' => 'utf8_general_ci',
    'prefix' => ''
]);

$capsule->bootEloquent();

# Load required files
require_once "includes/request.php";
require_once "includes/controller.php";
require_once "includes/router.php";

# Autoload models, middlewares
function autoload_class( $model ) {
	if(file_exists('models/' . strtolower($model) . '.php')) {
		require_once ('models/' . strtolower($model) . '.php');
	}

	if(file_exists('middlewares/' . strtolower($model) . '.php')) {
		require_once ('middlewares/' . strtolower($model) . '.php');
	}
}
spl_autoload_register( 'autoload_class' );

# Class Aliases
class_alias("Illuminate\Database\Eloquent\Model", "Model"); // Eloquent class automatic alias

# Load routes file
require_once "routes.php";

