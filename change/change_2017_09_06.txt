DATABASE:

	ALTER TABLE `ports` ADD `permission_no` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `name`;

------------------------------------------------------------------------------------------------------------------------
Frontend

views/ports/new.html.twig
views/ports/edit.html.twig
views/print/self_sealing.html.twig
views/print/excise_sealing.html.twig
views/print/invoice_cum_packing.html.twig

controller/print.php

------------------------------------------------------------------------------------------------------------------------
API:

controllers/port.php
