<?php

class DocumentController extends Controller {

    public function get( $id ) {
        return Document::find( $id );
    }

    public function upload() {
        $request = Request::all();

        $doc = new Document;
        $doc->company_id = $request['_companyid'];
        $doc->contract_id = $request['contract_id'];
        $doc->filename = $request['name'];
        $doc->filetype = $request['type'];
        $doc->location = $request['location'];
        $doc->extension = $request['extension'];
        $doc->size = $request['size'];
        $doc->save();
    }

    public function remove( $id ) {
        Document::destroy( $id );
    }

}