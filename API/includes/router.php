<?php

class Router {

	public static $routes = [];
	private static $path_prefix = "";
	private static $middleware_prefix = "";

	private static function route( $pattern, $callback, $method = "GET", $middleware = "" ) {
		// Prefix pattern
		$pattern = trim(self::$path_prefix, '/') . '/' . trim($pattern, '/');
		$pattern = trim($pattern, '/');

		// Prepare regex pattern
		$pattern = str_replace("{:any}", "(.*)", $pattern);
		$pattern = preg_replace("/[{].*[}]/U", "([^/]+)", $pattern);
		$pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';

		// Save route
		self::$routes[] = [
			'pattern' => $pattern,
			'callback' => $callback,
			'methods' => explode("|", $method),
			'middleware' => array_filter(explode(",", self::$middleware_prefix . $middleware))
		];
	}

	private static function initialize() {
		// Get url
		$url = (isset($_GET['path'])) ? $_GET['path'] : '';
		$url = rtrim($url, '/');

		// Pick relevant route
		foreach(self::$routes as $route) {
			if(in_array(Request::method(), $route['methods']) && preg_match($route['pattern'], $url, $params)) {

				// Load middleware if any
				foreach($route['middleware'] as $middleware) {
					$middleware_result = call_user_func_array([new $middleware, '__construct'], $params);
					if($middleware_result !== true) {
						return $middleware_result;
					}
				}

				// Clean params
				array_shift($params);

				// Detect if callback is function or controller@action
				if(is_callable($route['callback'])) {
					return call_user_func_array($route['callback'], array_values($params));
				} else {
					$parsed = explode("@", $route['callback']);

					// load controller file
					require_once ('controllers/' . $parsed[0] . '.php');

					// fetch controller & action name
					$controller_name = $parsed[0] . 'Controller'; // Controller naming convention
					$controller = new $controller_name;
					$action = $parsed[1];
					$method = Request::method();

					// Call validation function automatically for post, put & delete request
					if(in_array($method, ['POST', 'PUT', 'DELETE']) && is_callable([$controller, $action.ucfirst($method).'Validate'])) {

						$validate = call_user_func_array([$controller, $action.ucfirst($method).'Validate'], $params);

						if($validate !== true) {
							return $validate;
						}
					}

					// Call actual controller->action
					return call_user_func_array([$controller, $action], $params);
				}
			}
		}

		return NULL;
	}

	public static function group($option, $callback) {
		# Set path prefix
		if(isset($option['path'])) {
			self::$path_prefix = $option['path'];
		}

		# Set middleware prefix
		if(isset($option['middleware'])) {
			self::$middleware_prefix = $option['middleware'] . ",";
		}

		# Call sub functions
		call_user_func_array($callback, array());

		# Clear prefixes
		self::$path_prefix =  "";
		self::$middleware_prefix = "";
	}

	public static function get( $pattern, $callback, $middleware = "" ) {
		self::route( $pattern, $callback, "GET", $middleware );
	}

	public static function post( $pattern, $callback, $middleware = "" ) {
		self::route( $pattern, $callback, "POST", $middleware );
	}

	public static function put( $pattern, $callback, $middleware = "" ) {
		self::route( $pattern, $callback, "PUT", $middleware );
	}

	public static function delete( $pattern, $callback, $middleware = "" ) {
		self::route( $pattern, $callback, "DELETE", $middleware );
	}

	public static function auto( $pattern, $controller ) {
		self::get($pattern . 's', $controller . '@get');
		self::get($pattern . '/{id}', $controller . '@getById');
	    self::post($pattern, $controller . '@create');
	    self::put($pattern . '/{id}', $controller . '@update');
	    self::delete($pattern . '/{id}', $controller . '@remove');
	}

	public static function run() {
		// For API purpose returning JSON formatted code
		$data = self::initialize();
		echo ($data === NULL) ? json_encode(['error' => 'no route found']) : json_encode($data);
	}
}