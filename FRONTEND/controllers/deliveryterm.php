<?php

class DeliveryTermController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "DeliveryTerm's Register",
            'DeliveryTerms' => API::get('deliveryterms')
        ];
        $this->render('deliveryterms/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New DeliveryTerm"
        ];
        $this->render('deliveryterms/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('deliveryterm', Request::all());
        $this->redirect('deliveryterms');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit DeliveryTerm",
            'DeliveryTerm' => API::get('deliveryterm/' . $id)
        ];
        $this->render('deliveryterms/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("deliveryterm/$id", Request::all());
        $this->redirect('deliveryterms');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('deliveryterm/' . $id);
        $this->redirect('deliveryterms');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('deliveryterms/quickAdd');
    }
    public function quickCreate() {
        echo API::post('deliveryterm', Request::all(), true);
        exit();
    }
}