<?php

class CompanyController extends Controller {

    public function index() {
        $data = [
            'Title' => "Product's Register",
            'Company' => API::get('company')
        ];
        $this->render('company/index', $data);
    }

    public function update() {
        API::put("company", Request::all());
        $this->redirect('company');
    }

}