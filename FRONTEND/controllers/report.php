<?php

class ReportController extends Controller {

    public function index() {
        $data = [
            'Title' => 'Reports',
            '_js' => [RESOURCES . '/js/report.js']
        ];
        $this->render('reports/index', $data);
    }

    public function contractGirnarMaster() {
        # Get data for sheet
        $data = API::get('report/contract/girnarmaster');
        // return API::get('report/contract/girnarmaster'); exit();

        # Get report blank sheet
        $inputFileName = ROOT . 'report_templates/contracts_girnar_master.xlsx';
        $filename = "Girnar Master - " . date('d-m-Y');

        # Load sheet
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        # Update sheet
        $baseRow = 4;
        $i = 0;
        foreach($data['contracts'] as $dataRow) {
            // return $dataRow; exit();
            if(!empty($dataRow['shipment']['products'])) {
                foreach($dataRow['shipment']['products'] as $product) {
                    $row = $baseRow + $i;
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

                    $shipment = $dataRow['shipment'];
                    $do = $dataRow['do'];

                    # Container size & Quotation Freight
                    $container_size = "";
                    $quotation_freight = "";
                    if(!empty($shipment['container_size'][0])) {
                        $container_size .= $shipment['container_size'][0];
                        $quotation_freight .= $dataRow['quotation_freight'][0];

                        if(!empty($shipment['container_size'][1])) {
                            $container_size .= ",";
                            $quotation_freight .= " / ";
                        }
                    }
                    if(!empty($shipment['container_size'][1])) {
                        $container_size .= $shipment['container_size'][1] . 'X40';
                        $quotation_freight .= $dataRow['quotation_freight'][1];
                    }

                    # Do Status
                    $do_status = (!empty($do['do_date'])) ? "Y" : "N";

                    # Raw Material Status
                    $rm_status = (!empty($shipment['rawmaterial'])) ? "OK" : "P";

                    # Docs Cutoff
                    $docs_cutoff = (!empty($do['document_cutoff']) && $do['document_cutoff'] != "0000-00-00 00:00:00") ? date("d M", strtotime($do['document_cutoff'])) : "";
                    $si_cutoff = (!empty($do['si_cutoff']) && $do['si_cutoff'] != "0000-00-00 00:00:00") ? date("d M", strtotime($do['si_cutoff'])) : "";
                    $vgm_cutoff = (!empty($do['vgm_cutoff']) && $do['vgm_cutoff'] != "0000-00-00 00:00:00") ? date("d M", strtotime($do['vgm_cutoff'])) : "";

                    $contract_no = ($dataRow['contract_type'] == "multiple") ? $dataRow['contract_no'] .'-'. $shipment['shipment_code'] : $dataRow['contract_no'];


                    $objPHPExcel->getActiveSheet()
                        ->setCellValue('A'.$row, $dataRow['user_id'])
                        ->setCellValue('B'.$row, '')
                        ->setCellValue('C'.$row, $dataRow['surveyor']['name'])
                        ->setCellValue('D'.$row, $product['product']['name'])
                        ->setCellValue('E'.$row, $dataRow['importer']['name'])
                        ->setCellValue('F'.$row, $contract_no)
                        ->setCellValue('G'.$row, $dataRow['final_destination'])
                        ->setCellValue('H'.$row, $container_size)
                        ->setCellValue('I'.$row, $product['notes'])
                        ->setCellValue('I'.$row, $product['notes'])
                        ->setCellValue('J'.$row, $do_status)
                        ->setCellValue('K'.$row, $this->dateformat($do['stuffing_from']))
                        ->setCellValue('L'.$row, $this->dateformat($do['stuffing_to']))
                        ->setCellValue('M'.$row, $this->dateformat($do['etd_origin']))
                        ->setCellValue('N'.$row, $product['quantity'])
                        ->setCellValue('O'.$row, $product['package_type'])
                        ->setCellValue('P'.$row, $rm_status)
                        ->setCellValue('Q'.$row, $rm_status)
                        ->setCellValue('R'.$row, $product['rate'])
                        ->setCellValue('S'.$row, $product['total_amount'])
                        ->setCellValue('T'.$row, $dataRow['payment_terms'])
                        ->setCellValue('U'.$row, $this->dateformat($dataRow['contract_date']))
                        ->setCellValue('V'.$row, $dataRow['dollor_exchange_rate'])
                        ->setCellValue('W'.$row, $quotation_freight)
                        ->setCellValue('X'.$row, $dataRow['port']['name'])
                        ->setCellValue('Y'.$row, $docs_cutoff)
                        ->setCellValue('Z'.$row, $si_cutoff)
                        ->setCellValue('AA'.$row, $vgm_cutoff)
                    ;

                    $i++;
                }
            }
        }

        # Download sheet
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    

    private function dateformat( $value )
    {
        return (!empty($value) && $value != "0000-00-00 00:00:00" && $value != "0000-00-00") ? date("d M", strtotime($value)) : "";
    }

    
    public function contractWeeklySheet()
    {
        $dates = [
            'start_date' => "2016-09-15",
            'end_date' => "2016-10-09"
        ];

        //echo API::get('report/contract/weeklysheet', $dates);exit();
        $data = API::get('report/contract/weeklysheet', $dates);

        $inputFileName = ROOT . 'report_templates/contracts_weekly_report.xlsx';
        $filename = "testFile";

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        # Update Date
        $objPHPExcel->getActiveSheet()->setCellValue('H2', PHPExcel_Shared_Date::PHPToExcel(time()));
        $objPHPExcel->getActiveSheet()->setCellValue('A10', $data['old_buyers']);
        $objPHPExcel->getActiveSheet()->setCellValue('B10', $data['new_buyers']);
        $objPHPExcel->getActiveSheet()->setCellValue('E10', $data['old_buyers']);
        $objPHPExcel->getActiveSheet()->setCellValue('F10', $data['new_buyers']);

        # Insert Rows For Personal Contracts Done
        $baseRow = 6;
        $i = 0;
        foreach($data['personal_contracts'] as $dataRow) {
            if(!empty($dataRow['products'])) {
                foreach($dataRow['products'] as $product) {
                    $row = $baseRow + $i;
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

                    $objPHPExcel->getActiveSheet()
                        ->setCellValue('A'.$row, $dataRow['buyer'])
                        ->setCellValue('B'.$row, $product['name'])
                        ->setCellValue('C'.$row, $product['package'])
                        ->setCellValue('D'.$row, $product['quantity'])
                        ->setCellValue('E'.$row, $dataRow['containers'][0] + $dataRow['containers'][1])
                        ->setCellValue('F'.$row, $dataRow['date'])
                        ->setCellValue('G'.$row, $dataRow['shipment'])
                        ->setCellValue('H'.$row, $product['rate'])
                        ->setCellValue('I'.$row, '=D'.$row.'*H'.$row);

                    $i++;
                }
            }
        }

        $objPHPExcel->getActiveSheet()->removeRow($baseRow-1,1);

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//        header("Content-Type: application/force-download");
//        header("Content-Type: application/octet-stream");
//        header("Content-Type: application/download");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');

        //exit();

//        $data = [
//            'Title' => 'Contracts Weekly Sheet',
//            'contracts' => API::get('report/contract/weeklysheet', $dates)
//        ];
        //$this->render('reports/contractWeeklySheet', $data);
    }

/////////////////////////////////\\ Weekly Report \\///////////////////////////////////////////////

    public function weeklycontractsdone()
    {
        $data = API::get('report/weeklycontracts',Request::all());
        // echo API::get('report/weeklycontracts',Request::all(),true); exit();
        // var_dump($data);
        // exit();
        return $this->render('reports/weeklycontracts',$data);
    }

    public function weeklycontractsdonePOST()
    {
        $data = API::get('report/weeklycontracts',Request::all());
        // echo API::get('report/weeklycontracts',Request::all(),true); exit();
        // return $data; exit();
        // var_dump($data); exit();
        $inputFileName = ROOT . 'report_templates/weeklycontractdonereport.xlsx';
        $filename = "Weekly Contracta Done Report - " . date('d-m-Y');

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        $baseRow = 7;
        $i = 0;
        $j = 1;
        foreach ($data['username'] as $user) {
            $uname = $user['first_name'];
        }
        $total = 0;
        $total1 = 0;
        foreach ($data['contract'] as $contract) {
            $loop = 0;
            foreach ($contract['contract_product'] as $product) {
                if($loop == 0){
                foreach ($contract['shipments'] as $ship) {
                    if($ship['Id'] == $product['shipment_id']){
                        $shipment = $ship['eta'];
                    }
                }
                $total = $total + $product['total_amount'];
                $row = $baseRow + $i;
                $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
                $objPHPExcel->getActiveSheet()
                ->setCellValue('A'.'2','Weekly Report No : '. $data['week'])
                ->setCellValue('A'.'3','Prepared By : '. $uname)
                ->setCellValue('A'.$row,(($i+1) - $j)+1)
                ->setCellValue('B'.$row,$contract['contract_date'])
                ->setCellValue('C'.$row,$contract['importer']['name'])
                ->setCellValue('D'.$row,$product['product']['shortcode'])
                ->setCellValue('E'.$row,$product['package_type'])
                ->setCellValue('F'.$row,$product['quantity'])
                ->setCellValue('G'.$row," ")
                ->setCellValue('H'.$row,$contract['port_discharge'])
                ->setCellValue('I'.$row,$shipment)
                ->setCellValue('J'.$row,$product['rate'])
                ->setCellValue('K'.$row,$product['total_amount'])
                ->setCellValue('K'.'3',"Date : ".date('d-m-Y'))
                ; 
                 break;
                }
                // $loop++;
                // $i++;
            }
            foreach ($contract['contract_product'] as $product) {
                if($loop == 1 or $loop > 1){
                    foreach ($contract['shipments'] as $ship) {
                        if($ship['Id'] == $product['shipment_id']){
                            $shipment = $ship['eta'];
                        }
                    }
                    $total1 = $total1 + $product['total_amount'];
                    $row = $baseRow + $i;
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
                    $objPHPExcel->getActiveSheet()
                    // ->setCellValue('A'.'2','Weekly Report No : '. $data['week'])
                    // ->setCellValue('A'.'3','Prepared By : '. $uname)
                    // ->setCellValue('A'.$row,$i+1)
                    // ->setCellValue('B'.$row,$contract['contract_date'])
                    // ->setCellValue('C'.$row,$contract['importer']['name'])
                    ->setCellValue('D'.$row,$product['product']['shortcode'])
                    ->setCellValue('E'.$row,$product['package_type'])
                    ->setCellValue('F'.$row,$product['quantity'])
                    ->setCellValue('G'.$row," ")
                    ->setCellValue('H'.$row,$contract['port_discharge'])
                    ->setCellValue('I'.$row,$shipment)
                    ->setCellValue('J'.$row,$product['rate'])
                    ->setCellValue('K'.$row,$product['total_amount'])
                    ->setCellValue('K'.($row + 1),$total + $total1)
                    ; 
                $j++;   
                }
                $loop++;
                $i++;
            }
        }
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function weeklydocument()
    {
        $data = API::get('reports/weeklydocument',Request::all());
        // echo API::get('reports/weeklydocument',Request::all(),true); exit();
        // var_dump($data);exit();
        // return $data;
        return $this->render('reports/weeklydocument',$data);
    }

    public function weeklydocumentPOST()
    {
        $data = API::get('reports/weeklydocument',Request::all());
        // return $data; exit();
        $inputFileName = ROOT . 'report_templates/weeklydocumentreport.xlsx';
        $filename = "Weekly Documnet Report - " . date('d-m-Y');

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        $baseRow = 8;
        $i = 0;
        
        foreach ($data['username'] as $user) {
            $uname = $user['first_name'];
        }

        foreach ($data['inProcess'] as $inpro) {
            $row = $baseRow + $i;
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);

            if($inpro['deliveryorder']['etd_origin'] == '0000-00-00 00:00:00'){
                $vess = "";
            }else{
                $vess = date('d-m-Y',strtotime($inpro['deliveryorder']['etd_origin']));
            }

            if($inpro['deliveryorder']['eta_destination'] == '0000-00-00 00:00:00'){
                $etainpro = "";
            }else{
                $etainpro = $inpro['deliveryorder']['eta_destination'];
            }

            if($inpro['deliveryorder']['stuffing_from'] == "0000-00-00 00:00:00"){
                $stuffing = "";
            }else{
                 $stuffing = $inpro['deliveryorder']['stuffing_from'];
            }

            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.'2','Prepared By : '. $uname)
            ->setCellValue('A'.$row,$inpro['comm_invoice_no'])
            ->setCellValue('C'.$row,$inpro['contract']['importer']['name'])
            ->setCellValue('E'.$row,$stuffing)
            ->setCellValue('F'.$row,$vess)
            ->setCellValue('G'.$row,$etainpro)
            ->setCellValue('H'.$row,$inpro['comm_eta'])
            ->setCellValue('H'.'2',date('d-m-Y'))
            ; 
            $i++; 

        }

        $baseRow = 5;
        $i = 0;

        foreach ($data['shipment'] as $ship) {

            $row = $baseRow + $i;
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
            
            if($ship['deliveryorder']['etd_origin'] == '0000-00-00 00:00:00'){
                $vessel = "";
            }else{
                $vessel = date('d-m-Y',strtotime($ship['deliveryorder']['etd_origin']));
            }

            if($ship['deliveryorder']['eta_destination'] == '0000-00-00 00:00:00'){
                $eta = "";
            }else{
                $eta = $ship['deliveryorder']['eta_destination'];
            }

            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.$row,$i+1)
            ->setCellValue('A'.'2','Prepared By : '. $uname)
            ->setCellValue('B'.$row,$ship['comm_invoice_no'])
            ->setCellValue('C'.$row,$ship['contract']['importer']['name'])
            ->setCellValue('D'.$row,$vessel)
            ->setCellValue('E'.$row,$ship['document_ready_on'])
            ->setCellValue('F'.$row,$eta)
            ->setCellValue('G'.$row,$ship['document_status'])
            ->setCellValue('H'.$row,"")
            ; 
            $i++; 
            
            
        }        

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function weeklystuffingReport()
    {   
        $data = API::get('reports/weeklystuffing',Request::all());
        // echo API::get('reports/weeklystuffing',Request::all(),true); exit();
         // var_dump($data);exit();
        // return $data;
        return $this->render('reports/weeklystuffing',$data);
    }

    public function weeklystuffingReportPOST()
    {
        $data = API::get('reports/weeklystuffing',Request::all());
        // echo API::get('reports/weeklystuffing',Request::all(),true); exit();

        $inputFileName = ROOT . 'report_templates/weeklystuffing_report.xlsx';
        $filename = "Weekly Stuffing Report - " . date('d-m-Y');   

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        $baseRow = 6;
        $i = 0;

        foreach ($data['username'] as $user) {
            $uname = $user['first_name'];
        }

        foreach ($data['shipment'] as $ship) {

            $row = $baseRow + $i;
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
            
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.'3','Prepared By :'. $uname)
            ->setCellValue('A'.$row,$i+1)    
            ->setCellValue('A'.$row,$i+1)    
            ->setCellValue('B'.$row,$ship['export_invoice_date']) 
            ->setCellValue('C'.$row,$ship['invoice_no']) 
            ->setCellValue('D'.$row,$ship['contract']['importer']['name'])
            ->setCellValue('E'.$row," ")
            ->setCellValue('E'.'3',"Date : ".date('d-m-Y'))
            ; 
         $i++;
        }

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function weeklyfreight()
    {
        $data = API::get('reports/weeklyfreight',Request::all());
        // echo API::get('reports/weeklyfreight',Request::all(),true); exit();
        // var_dump($data);exit();
        return $this->render('reports/weeklyfreight',$data);
    }

    public function weeklyfreightPOST()
    {
        $data = API::get('reports/weeklyfreight',Request::all());
        // echo API::get('reports/weeklyfreight',Request::all(),true); exit();
        // return ($data['shipment']); exit();
        // var_dump($data); exit();
        $inputFileName = ROOT . 'report_templates/weeklyfreightreport.xlsx';
        $filename = "Weekly Freight Report - " . date('d-m-Y');

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
        foreach ($data['username'] as $user) {
            $uname = $user['first_name'];
        }
        
        $baseRow = 4;
        $i = 0;
        $total = 0;
        $total1 = 0;
        // return $data['shipment']; exit();
        foreach($data['shipment'] as $ship) {
                // return $ship; exit();
                // echo (json_decode($ship['container_size'][0])); exit();
                $buyer = $ship['contract']['importer']['name'];
                $originPort = $ship['contract']['port']['name'];
                // return $ship['Id'];
                // return $ship['quotation_freight'][1]; exit("asdhasadasds");
                
                if(!empty($ship['contract']['port_discharge3']) and !empty($ship['contract']['port_discharge2']) and !empty($ship['contract']['port_discharge'])){
                    $disPort = $ship['contract']['port_discharge'].",".$ship['contract']['port_discharge2'].",".$ship['contract']['port_discharge3'];
                }elseif(!empty($ship['contract']['port_discharge']) and !empty($ship['contract']['port_discharge2']) and empty($ship['contract']['port_discharge'])){
                    $disPort = $ship['contract']['port_discharge'].",".$ship['contract']['port_discharge2'];
                }elseif(!empty($ship['contract']['port_discharge']) and empty($ship['contract']['port_discharge2']) and !empty($ship['contract']['port_discharge'])){
                    $disPort = $ship['contract']['port_discharge'].",".$ship['contract']['port_discharge3'];
                }elseif(empty($ship['contract']['port_discharge']) and !empty($ship['contract']['port_discharge2']) and !empty($ship['contract']['port_discharge'])){
                    $disPort = $ship['contract']['port_discharge2'].",".$ship['contract']['port_discharge3'];
                }
                
                $forwarderName = $ship['deliveryorder']['forwarder_name'];
                // return $ship['container_size'][0]; exit();
                // return $ship['deliveryorder']['total_freight']; exit();
                
                if(empty($ship['deliveryorder']['total_freight'])){
                   $totalfreight = 0;
                }else{
                   $totalfreight = $ship['deliveryorder']['total_freight'];
                }

                if(!empty($ship['container_size'][0])){
                   $diff = ((json_decode($ship['quotation_freight'][0])) - $totalfreight) * (json_decode($ship['container_size'][0]));
                   $total = $total + $diff;
                    // exit("SDASD asgd");
                    $row = $baseRow + $i;
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);  
                  // exit("fhdfk");  
                    $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'.'2','Prepared By :'. $uname)   
                    ->setCellValue('A'.$row,$ship['invoice_no'])
                    ->setCellValue('B'.$row,$ship['contract']['contract_no'])
                    ->setCellValue('C'.$row,$buyer)
                    ->setCellValue('D'.$row,$originPort)
                    ->setCellValue('E'.$row,$disPort)
                    ->setCellValue('F'.$row,json_decode($ship['container_size'][0]))
                    ->setCellValue('G'.$row,"20'")
                    ->setCellValue('H'.$row,$forwarderName)
                    ->setCellValue('I'.$row,$ship['deliveryorder']['total_freight'])
                    ->setCellValue('J'.$row,json_decode($ship['container_size'][0]))
                    ->setCellValue('K'.$row,$diff)
                    ->setCellValue('K'.'2',"Date : ".date('d-m-Y'))
                    ;
                }
                 
                if(!empty($ship['container_size'][1])){
                    $total1 = $total1 + ($ship['quotation_freight'][1] - $totalfreight) * ($ship['container_size'][1]);
                    $row = $baseRow + $i;
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore(($row+1),1);    
                    $objPHPExcel->getActiveSheet()
                    // ->setCellValue('A'.'2','Prepared By :'. $uname)   
                    // ->setCellValue('A'.$row + 1,$ship['invoice_no'])
                    // ->setCellValue('B'.$row + 1,$ship['contract']['contract_no'])
                    // ->setCellValue('C'.$row + 1,$buyer)
                    // ->setCellValue('D'.$row,$originPort)
                    // ->setCellValue('E'.$row,$disPort)
                    ->setCellValue('F'.($row + 1),json_decode($ship['container_size'][1]))
                    ->setCellValue('G'.($row + 1),"40'")
                    ->setCellValue('H'.($row + 1),$forwarderName)
                    ->setCellValue('I'.($row + 1),$ship['deliveryorder']['total_freight'])
                    ->setCellValue('J'.($row + 1),json_decode($ship['quotation_freight'][0]))
                    ->setCellValue('K'.($row + 1),(json_decode($ship['quotation_freight'][1]) - $ship['deliveryorder']['total_freight']) * (json_decode($ship['container_size'][1])))
                    ->setCellValue('J'.($row + 2),"Total :")
                    ->setCellValue('K'.($row + 2),$total + $total1)
                    ;

                    $i++; 

                }
                // exit("sjakdhkjsad");
                // if(!empty($ship['container_size'][1])){
                //     $rowp = $row + 1;
                //     $objPHPExcel->getActiveSheet()
                //     // ->setCellValue('A'.$rowp," ")
                //     // ->setCellValue('B'.$rowp," ")
                //     // ->setCellValue('C'.$rowp," ")
                //     // ->setCellValue('D'.$rowp," ")
                //     // ->setCellValue('E'.$rowp," ")
                //     ->setCellValue('F'.$rowp,$ship['container_size'][1])
                //     ->setCellValue('G'.$rowp,"40'")
                //     ->setCellValue('H'.$rowp,$forwarderName)
                //     ->setCellValue('I'.$rowp,$ship['deliveryorder']['total_freight'])
                //     ->setCellValue('J'.$rowp,$ship['quotation_freight'][0])
                //     ->setCellValue('K'.$rowp,($ship['quotation_freight'][1] - $ship['deliveryorder']['total_freight']) * ($ship['container_size'][1]))
                //     ; 
                // }

                $i++; 

            }


       // exit('555SsaS'); 
            
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function weeklyinvoiceReport()
    {
        $data = API::get('reports/weeklyinvoice',Request::all());
        // echo API::get('reports/weeklyinvoice',Request::all(),true); exit();
        // var_dump($data);exit();
        // return $data;
        return $this->render('reports/weeklyinvoice',$data);   
    }

    public function weeklyinvoiceReportPOST()
    {
        $data = API::get('reports/weeklyinvoice',Request::all());
        // echo API::get('reports/weeklyinvoice',Request::all(),true); exit();
        // var_dump($data); exit();
        // return $data['shipment']['products']; exit();
        $inputFileName = ROOT . 'report_templates/weeklyinvoicereport.xlsx';
        $filename = "Weekly Invoice Report - " . date('d-m-Y');

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        $baseRow = 5;
        $i = 0;
        foreach ($data['username'] as $user) {
            $uname = $user['first_name'];
        }
        $total = 0;
        foreach ($data['shipment'] as $ship) {
            $row = $baseRow + $i;
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
            
            foreach ($ship['products'] as $pro) {
                $product_name = $pro['product']['shortcode'];
                $ans = $pro['total_amount'];
            }

            if(!empty($ship['container_size'][0]) and !empty($ship['container_size'][1])){
                $container = $ship['container_size'][0]."(20')".', '.$ship['container_size'][1]."(40')";
            }elseif(!empty($ship['container_size'][0]) and empty($ship['container_size'][1])){
                $container = $ship['container_size'][0];
            }elseif(!empty($ship['container_size'][1]) and empty($ship['container_size'][0])){
                $container = $ship['container_size'][1]."(40')";
            }
            $objPHPExcel->getActiveSheet()
            ->setCellValue('A'.'2','Prepared By :'. $uname)   
            ->setCellValue('A'.$row,$ship['contract']['importer']['name'])    
            ->setCellValue('B'.$row,$product_name)
            ->setCellValue('C'.$row,$ship['invoice_no'])
            ->setCellValue('D'.$row,$container)
            ->setCellValue('E'.$row,$ship['export_invoice_date'])
            ->setCellValue('F'.$row,$ans)
            ->setCellValue('F'.'2',"Date : ".date('d-m-Y'))
            ->setCellValue('F'.($row + 1),$total = $total + $ans)
            ; 
            $i++;
        }
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function reportformat()
    {
        return $this->render('reports/format');
    }

    public function bookingdetail()
    {
        $inputFileName = ROOT . 'report_templates/bookingdetails.xlsx';
        $filename = "Booking Details - " . date('d-m-Y');

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        // $baseRow = 3;
        // $i = 0;
        //     $row = $baseRow + $i;
        //     $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
        //     $objPHPExcel->getActiveSheet()
        //     ->setCellValue('A'.$row,"")   
        //     ->setCellValue('B'.$row,"")
        //     ->setCellValue('C'.$row,"")
        //     ->setCellValue('D'.$row,"")
        //     ->setCellValue('E'.$row,"")
        //     ->setCellValue('F'.$row,"")
        //     ->setCellValue('G'.$row,"")
        //     ->setCellValue('H'.$row,"")
        //     ->setCellValue('I'.$row,"")
        //     ->setCellValue('J'.$row,"")
        //     ; 
            
      
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function shippingschedule()
    {
        
        $inputFileName = ROOT . 'report_templates/shippingschedule.xlsx';
        $filename = "Shipping Schedule - " . date('d-m-Y');

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        // $baseRow = 3;
        // $i = 0;
        //     $row = $baseRow + $i;
        //     $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
        //     $objPHPExcel->getActiveSheet()
        //     ->setCellValue('A'.$row,"")   
        //     ->setCellValue('B'.$row,"")
        //     ->setCellValue('C'.$row,"")
        //     ->setCellValue('D'.$row,"")
        //     ->setCellValue('E'.$row,"")
        //     ->setCellValue('F'.$row,"")
        //     ->setCellValue('G'.$row,"")
        //     ->setCellValue('H'.$row,"")
        //     ->setCellValue('I'.$row,"")
        //     ->setCellValue('J'.$row,"")
        //     ; 
            
      
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function exportstuffingschedule()
    {
       
        $inputFileName = ROOT . 'report_templates/exportstuffingschedule.xlsx';
        $filename = "Export Stuffing Schedule - " . date('d-m-Y');

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        // $baseRow = 3;
        // $i = 0;
        //     $row = $baseRow + $i;
        //     $objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
        //     $objPHPExcel->getActiveSheet()
        //     ->setCellValue('A'.$row,"")   
        //     ->setCellValue('B'.$row,"")
        //     ->setCellValue('C'.$row,"")
        //     ->setCellValue('D'.$row,"")
        //     ->setCellValue('E'.$row,"")
        //     ->setCellValue('F'.$row,"")
        //     ->setCellValue('G'.$row,"")
        //     ->setCellValue('H'.$row,"")
        //     ->setCellValue('I'.$row,"")
        //     ->setCellValue('J'.$row,"")
        //     ; 
            
      
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }

    public function accountmaster($shipment_id)
    {
        
        $data = API::get('contract/' . $shipment_id); //contract get
        // return $data['shipment'];
        $inputFileName = ROOT . 'report_templates/accountmaster.xlsx';
        $filename = "Account Master - " . date('d-m-Y');

        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        $objPHPExcel->getActiveSheet()->insertNewRowBefore(1,1);
            
        $objPHPExcel->getActiveSheet()
        ->setCellValue('A'.'2',"INVOICE NO")
        ->setCellValue('B'.'2',$data['shipment']['invoice_no'])    
        ->setCellValue('C'.'2',"GRUPO BERHFER")    
        ;

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=$filename.xlsx");
        header("Content-Transfer-Encoding: binary ");
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save('php://output');
    }
}
