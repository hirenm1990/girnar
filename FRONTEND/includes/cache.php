<?php

class Cache {

	public static function set($key, $value) {
		$_SESSION['cache'][$key] = $value;
	}

	public static function get($key) {
		return $_SESSION['cache'][$key];
	}

	public static function _isset($key) {
		return (isset($_SESSION['cache'][$key])) ? true : false;
	}

	public static function _unset($key) {
		unset($_SESSION['cache'][$key]);
	}

	public static function clear() {
		session_destroy();
	}

}