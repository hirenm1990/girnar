<?php

class Vgm extends Model {

    protected $table = 'vgm_details';
    protected $primaryKey = 'Id';

    public function vgmsjson( $value )
    {
    	return json_decode( $value );
    }
}
