<?php

class PortController extends Controller {

    public function get() {
        return Port::where('is_active', 1)->orderBy('name')->get();
    }

    public function getById( $id ) {
        return Port::find( $id );
    }

    public function create() {
        $request = Request::all();
        $port = new Port;
        $port->name = $request['name'];
        $port->permission_no = $request['permission_no'];
        $id = $port->save();
        return $port;
    }

    public function update( $id ) {
        $request = Request::all();
        $port = Port::find( $id );
        $port->name = $request['name'];
        $port->permission_no = $request['permission_no'];
        $port->save();
    }

    public function remove( $id ) {
        $port = Port::find( $id );
        $port->is_active = 0;
        $port->save();

        // Port::destroy( $id );
    }

}