<?php

class Shipment extends Model {

    protected $table = 'contracts_shipment';
    protected $primaryKey = 'Id';

    public function contract() {
        return $this->belongsTo('Contract', 'contract_id', 'Id');
    }

    public function products() {
        return $this->hasMany('ContractProduct', 'shipment_id', 'Id');
    }

    public function port() {
        return $this->hasOne('Port', 'Id', 'port_loading');
    }

    public function country() {
        return $this->hasOne('Country', 'Id', 'final_destination_country');
    }

    public function deliveryorder() {
        return $this->hasOne('DeliveryOrder', 'shipment_id', 'Id');
    }

    public function rawmaterial() {
        return $this->hasMany('RawMaterial', 'shipment_id', 'Id');
    }

    public function schemes() {
        return $this->hasOne('ContractScheme', 'shipment_id', 'Id');
    }

    public function getContainerSizeAttribute( $value ) {
        return json_decode( $value );
    }

    public function getQuotationFreightAttribute( $value ) {
        return json_decode( $value );
    }

    public function vgms()
    {
        return $this->hasOne('Vgm','shipment_id','Id');
    }
    
}