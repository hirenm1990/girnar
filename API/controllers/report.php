<?php

use \Illuminate\Database\DatabaseManager;
use \Illuminate\Database\Connection as DB;

class ReportController extends Controller {

    public function index() {

    }

    public function custom() {
        $query = Request::get( 'query' );

        $data = Contract::hydrateRaw( $query )->toArray();
        return $data;
    }

    public function contractGirnarMaster()
    {
        $dos = DeliveryOrder::orderBy('stuffing_from', 'desc')->get();        

        $contracts = [];
        foreach($dos as $do) {
            $contract = Contract::where('Id', $do->contract_id)->with('surveyor', 'importer', 'port')->first();
            $contract['shipment'] = Shipment::where('Id', $do->shipment_id)->with('products.product', 'rawmaterial')->first();
            $contract['do'] = $do;
            $contracts[] = $contract;
        }

        return [
            'contracts' => $contracts
        ];

        // $contracts = Contract::orderBy()->get();
        // $personal_contracts = [];
        // foreach($contracts as $contract) {
        //     foreach($contract->shipments as $shipment) {
        //         $products = [];
        //         foreach($shipment->products as $product) {
        //             $temp_product = [];
        //             $temp_product['name'] = $product->product->name;
        //             $temp_product['package'] = $product->package_type;
        //             $temp_product['quantity'] = $product->quantity;
        //             $temp_product['rate'] = $product->rate;
        //             $products[] = $temp_product;
        //         }

        //         $temp = [];
        //         $temp['contract_id'] = $contract->contract_no;
        //         $temp['buyer'] = $contract->importer->name;
        //         $temp['products'] = $products;
        //         $temp['containers'] = $shipment->container_size;
        //         $temp['date'] = $contract->contract_date;
        //         $temp['shipment'] = $shipment->eta;
        //         $personal_contracts[] = $temp;
        //     }
        // }
    }

    public function contractWeeklySheet()
    {
        
        $start = Request::get('start_date');
        $end = Request::get('end_date');

        # Personal Contracts
        $contracts = Contract::where('user_id', Request::get('_user'))->where('contract_date', '>=', $start)->where('contract_date', '<=', $end)->get();
        $personal_contracts = [];
        foreach($contracts as $contract) {
            foreach($contract->shipments as $shipment) {
                $products = [];
                foreach($shipment->products as $product) {
                    $temp_product = [];
                    $temp_product['name'] = $product->product->name;
                    $temp_product['package'] = $product->package_type;
                    $temp_product['quantity'] = $product->quantity;
                    $temp_product['rate'] = $product->rate;
                    $products[] = $temp_product;
                }

                $temp = [];
                $temp['contract_id'] = $contract->contract_no;
                $temp['buyer'] = $contract->importer->name;
                $temp['products'] = $products;
                $temp['containers'] = $shipment->container_size;
                $temp['date'] = $contract->contract_date;
                $temp['shipment'] = $shipment->eta;
                $personal_contracts[] = $temp;
            }
        }

        # Total Clients
        $new_buyers = Importer::where('created_at', '>=', $start)->where('created_at', '<=', $end)->count();
        $old_buyers = Importer::where('created_at', '<', $start)->count();

        $return = [
            'personal_contracts' => $personal_contracts,
            'old_buyers' => $old_buyers,
            'new_buyers' => $new_buyers,
        ];
        return $return;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function weeklycontractsdone()
    {
        if(empty(Request::get('fromDate')) and empty(Request::get('endDate'))){
           $endDate = date('Y-m-d');
            $startDate = date('Y-m-d', strtotime('-7 days'));
            $dateBetween = array($startDate,$endDate);
        }else{
            // $startDate = date('Y-m-d',strtotime(Request::get('fromDate')));
            // $endDate =  date('Y-m-d',strtotime(Request::get('toDate')));
            $startDate = date_create(Request::get('fromDate'))->format('Y-m-d');
            $endDate = date_create(Request::get('toDate'))->format('Y-m-d');
            $dateBetween = array($startDate,$endDate);
        }

        $data = [];
        $data['fromDate'] = $startDate;
        $data['toDate'] = $endDate;
        $auth_id = Request::get('_user');
        $data['username'] = User::where('Id',$auth_id)->get();
        $date = new DateTime();
        $data['week'] = $date->format("W");

        $commInvo = Shipment::where('comm_invoice_no','!=',"")->get();
        // $con = count($contract);
        $data['count'] = $con = 1;
        
        // foreach ($contract as $cont) {
        //     $shipments = Shipment::where('contract_id',$cont->Id)->with('deliveryorder')->get();
        //     $tmp = [];
        //     foreach($shipments as $shipment) {
        //         $shipment['contract'] = $cont;
        //         $tmp[] = $shipment;
        //     }
        //     $data['shipment'] = $tmp;
        // }
        $temp = [];
        foreach ($commInvo as $invo) {
            $cid = $invo->contract_id;
            
            $contract = Contract::where('user_id',$auth_id)->where('id',$cid)->whereBetween('contract_date',$dateBetween)->with('importer','shipments','shipments.products', 'contractProduct.product', 'contractProduct')->first();
            
            if(count($contract) > 0){
                $temp[] = $contract;
            }
            
        }
        $data['contract'] = $temp;

        return $data;
    }

    public function weeklydocument()
    {
        if(empty(Request::get('fromDate')) and empty(Request::get('endDate'))){
            $endDate = date('Y-m-d');
            $startDate = date('Y-m-d', strtotime('-7 days'));
            $dateBetween = array($startDate,$endDate);
        }else{
            // $startDate = date('Y-m-d',strtotime(Request::get('fromDate')));
            // $endDate =  date('Y-m-d',strtotime(Request::get('toDate')));
            $startDate = date_create(Request::get('fromDate'))->format('Y-m-d');
            $endDate = date_create(Request::get('toDate'))->format('Y-m-d');
            $dateBetween = array($startDate,$endDate);
        }
        // $dateBetween = [$startDate,$endDate];

        $data = [];
        $data['fromDate'] = $startDate;
        $data['toDate'] = $endDate;
        $auth_id = Request::get('_user');
        $data['username'] = User::where('Id',$auth_id)->get();
        
        $shipment = Shipment::wherebetween('document_ready_on',$dateBetween)->with('deliveryorder','contract.importer')->get();
        
        foreach ($shipment as $ship) {
            if(!empty($ship->document_status)){
                $temp[] = $ship;
                $data['shipment'] = $temp;
            }else{
                $tmp[] = $ship;
                $data['inProcess'] = $tmp;
            }
        }

        
        return $data;
    }

    public function weeklystuffingReport()
    {
        
        if(empty(Request::get('fromDate')) and empty(Request::get('endDate'))){
           $endDate = date('Y-m-d');
            $startDate = date('Y-m-d', strtotime('-7 days'));
            $dateBetween = array($startDate,$endDate);
        }else{
            // $startDate = date('Y-m-d',strtotime(Request::get('fromDate')));
            // $endDate =  date('Y-m-d',strtotime(Request::get('toDate')));
            $startDate = date_create(Request::get('fromDate'))->format('Y-m-d');
            $endDate = date_create(Request::get('toDate'))->format('Y-m-d');
            $dateBetween = array($startDate,$endDate);
        }

        $data = [];
        $data['fromDate'] = $startDate;
        $data['toDate'] = $endDate;
        $auth_id = Request::get('_user');
        $data['username'] = User::where('Id',$auth_id)->get();

        $shipments = Shipment::whereBetween('export_invoice_date',$dateBetween)->with('deliveryorder','contract.importer')->get();
        // $data['shipment'] = $shipments;
        foreach ($shipments as $ship) {
            if(!empty($ship->invoice_no)){
                $tmp[] = $ship;
                $data['shipment'] = $tmp;        
            }
        }
        // $data['shipments'] = $shipments;

        return $data;
    }
    
    public function weeklyfreightReport()
    {
        if(empty(Request::get('fromDate')) and empty(Request::get('endDate'))){
           $endDate = date('Y-m-d');
            $startDate = date('Y-m-d', strtotime('-7 days'));
            $dateBetween = array($startDate,$endDate);
        }else{
            // $startDate = date('Y-m-d',strtotime(Request::get('fromDate')));
            // $endDate =  date('Y-m-d',strtotime(Request::get('toDate')));
            $startDate = date_create(Request::get('fromDate'))->format('Y-m-d');
            $endDate = date_create(Request::get('toDate'))->format('Y-m-d');
            $dateBetween = array($startDate,$endDate);
        }

        $data = [];
        $data['fromDate'] = $startDate;
        $data['toDate'] = $endDate;
        $auth_id = Request::get('_user');
        $data['username'] = User::where('Id',$auth_id)->get();

        // $contract = Contract::whereBetween('contract_date',$dateBetween)->with('importer', 'port', 'surveyor')->get();
        $dos = DeliveryOrder::whereBetween('do_date',$dateBetween)->whereNotNull('total_freight')->get();
        $tmp = [];
        foreach ($dos as $do) {
            
            $shipments = Shipment::where('Id',$do->shipment_id)->with('deliveryorder','contract','contract.importer','contract.port','contract.surveyor')->get();
            
            foreach($shipments as $shipment) {
                if(count($shipment) > 0 and !empty(json_decode($shipment->quotation_freight[0])) and !empty(json_decode($shipment->quotation_freight[1])) and !empty($shipment->quotation_freight)){
                    // $shipment['do'] = $do;
                    $tmp[] = $shipment;
                }
            }
            $data['shipment'] = $tmp;
        }
         return $data; 
    }

    public function weeklyinvoice()
    {
        if(empty(Request::get('fromDate')) and empty(Request::get('endDate'))){
            
            $endDate = date('Y-m-d');
            $startDate = date('Y-m-d', strtotime('-7 days'));
            $dateBetween = array($startDate,$endDate);
        
        }else{
           
            $startDate = date_create(Request::get('fromDate'))->format('Y-m-d');
            $endDate = date_create(Request::get('toDate'))->format('Y-m-d');
            $dateBetween = array($startDate,$endDate);
            
        }

        $data = [];
        $data['fromDate'] = $startDate;
        $data['toDate'] = $endDate;
        $auth_id = Request::get('_user');
        $data['username'] = User::where('Id',$auth_id)->get(); 

        $shipment = Shipment::whereBetween('export_invoice_date',$dateBetween)->with('contract','contract.importer','products','products.product')->get();

        foreach ($shipment as $ship) {
            if(count($ship) > 0){
                $tmp[] = $ship;
                
            }
        }
        $data['shipment'] = $tmp;
        // $data['shipment'] = $shipment;
        // $importer = Importer::all();
        // $data['importer'] = $importer;

        return $data;
    }

    // public function weeklyinvoice()
    // {
    //     if(empty(Request::get('fromDate')) and empty(Request::get('endDate'))){
    //         $endDate = date('Y-m-d');
    //         $startDate = date('Y-m-d', strtotime('-7 days'));
    //         $dateBetween = array($startDate,$endDate);
    //     }else{
    //         $startDate = date_create(Request::get('fromDate'))->format('Y-m-d');
    //         $endDate = date_create(Request::get('toDate'))->format('Y-m-d');
    //         $dateBetween = array($startDate,$endDate);
    //     }
        
    //     $data = [];
    //     $data['fromDate'] = $startDate;
    //     $data['toDate'] = $endDate;
    //     $auth_id = Request::get('_user');
    //     $data['username'] = User::where('Id',$auth_id)->get(); 
    //     $shipment = Shipment::whereBetween('export_invoice_date',$dateBetween)->with('contract','contract.importer','products','products.product')->get();
    //     foreach ($shipment as $ship) {
    //         if(count($ship) > 0){
    //             $tmp[] = $ship;
    //         }
    //     }
    //     $data['shipment'] = $tmp;
    //     return $data;
    // }
    
}