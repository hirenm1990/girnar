<?php

class ForwarderController extends Controller {

    public function get() {
        return Forwarder::where('is_active', 1)->orderBy('name')->get();
    }

    public function getById( $id ) {
        return Forwarder::find( $id );
    }

    public function create() {
        $request = Request::all();

        $forwarder = new Forwarder;
        $forwarder->name = $request['name'];
        $forwarder->contact_person = $request['contact_person'];
        $forwarder->phone = $request['phone'];
        $forwarder->email = $request['email'];
        $id = $forwarder->save();
        return $forwarder;
    }

    public function update( $id ) {
        $request = Request::all();

        $forwarder = Forwarder::find( $id );
        $forwarder->name = $request['name'];
        $forwarder->contact_person = $request['contact_person'];
        $forwarder->phone = $request['phone'];
        $forwarder->email = $request['email'];

        $forwarder->save();
    }

    public function remove( $id ) {
        $forwarder = Forwarder::find( $id );
        $forwarder->is_active = 0;
        $forwarder->save();

        // Forwarder::destroy( $id );
    }

}