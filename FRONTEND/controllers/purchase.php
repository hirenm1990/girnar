<?php

class PurchaseController extends Controller {

    public function index() {
        $data = [
            'Title' => 'Purchase',
            'Products' => API::get('stockproducts'),
            'Vendors' => API::get('stockvendors'),
            'Stocks' => API::get('purchase')
        ];
        
        $this->render('purchase/index', $data);
    }

    # Database operation - Add data to database
    public function store() {
        API::post('purchase', Request::all());
        $this->redirect('purchase');
    }

    public function edit( $id )
    {   
        $data = [
            'Title' => 'Purchase',
            'item' => API::get('purchase/edit/'. $id),
            'Products' => API::get('stockproducts'),
            'Vendors' => API::get('stockvendors'),
        ];
        // return $data['item'];
        $this->render('purchase/edit', $data);
    }

    public function update( $id )
    {
        // return Request::all();
        $data = API::post('purchase/update/'.$id,Request::all());
        // return $data;
        $this->redirect('purchase');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('purchase/' . $id);
        $this->redirect('purchase');
    }

    public function stock() {
        $data = [
            'Title' => 'Current Stock',
            'Stock' => API::get('stock'),
        ];

        $this->render('purchase/stock', $data);
    }

}