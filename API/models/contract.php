<?php

class Contract extends Model {

    protected $table = 'contracts';
    protected $primaryKey = 'Id';

    public function shipments() {
        return $this->hasMany('Shipment', 'contract_id', 'Id');
    }

    public function importer() {
        return $this->hasOne('Importer', 'Id', 'importer_id');
    }

    public function port() {
        return $this->hasOne('Port', 'Id', 'port_loading');
    }

    public function surveyor() {
        return $this->hasOne('Surveyor', 'Id', 'surveyor_id');
    }

    public function country() {
        return $this->hasOne('Country', 'Id', 'final_destination_country');
    }

    public function documents() {
        return $this->hasMany('Document', 'contract_id', 'Id');
    }

    public function getQuotationFreightAttribute( $value ) {
        return json_decode( $value );
    }
    // 
    public function contractProduct()
    {
        return $this->hasMany('ContractProduct','contract_id','Id');
    }

    public function do()
    {
        return $this->hasMany('DeliveryOrder','contract_id','Id');
    }

    // public function company()
    // {
    //     return $this->hasOne('Company','Id','company_id');
    // }
}