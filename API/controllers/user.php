<?php

class UserController extends Controller {

    public function get() {
        return User::all();
    }

    public function getById( $id ) {
        return User::find( $id );
    }

    public function create() {
        $request = Request::all();

        $user = new User;
        $user->username = $request['username'];
        $user->password = $request['password'];
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->save();
    }

    public function remove( $id ) {
        User::destroy( $id );
    }

    public function profile() {
        return User::find( Request::get('_user') );
    }

    public function update() {
        $request = Request::all();

        $user = User::find( $request['_user'] );
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];
        $user->website = $request['website'];
        $user->facebook = $request['facebook'];
        $user->skype = $request['skype'];

        $user->save();
    }

    public function update_restricted_areas( $id ) {
        $request = Request::all();

        $user = User::find( $id );
        $user->restricted = json_encode( $request['restricted'] );

        $user->save();
    }

    public function restricted_areas() {
        $data = User::find( Request::get('_user') );
        return $data['restricted'];
    }

    public function restricted_areasById( $id ) {
        $data = User::find( $id );
        return $data['restricted'];
    }

    public function changepass() {
        $request = Request::all();

        $user = User::find( Request::get('_user') );

        if($request['old_pass'] == $user['password'] && !empty($request['new_pass'])) {
            $user->password = $request['new_pass'];
            $user->save();
            return ['success' => true];
        }

        return ['success' => false, 'message' => 'Invalid password. Try again!'];
    }

}