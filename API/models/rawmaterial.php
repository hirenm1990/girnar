<?php

class RawMaterial extends Model {

    protected $table = 'contracts_rawmaterial';
    protected $primaryKey = 'Id';

    protected $fillable = ['contract_id', 'shipment_id', 'product_id', 'qty'];
}