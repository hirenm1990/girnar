<?php

class CountryController extends Controller {

    public function get() {
        return Country::where('is_active', 1)->orderBy('name')->get();
    }

    public function getById( $id ) {
        return Country::find( $id );
    }

    public function create() {
        $request = Request::all();

        $country = new Country;
        $country->name = $request['name'];
        $country->save();
        return $country;
    }

    public function update( $id ) {
        $request = Request::all();

        $country = Country::find( $id );
        $country->name = $request['name'];

        $country->save();
    }

    public function remove( $id ) {
        $country = Country::find( $id );
        $country->is_active = 0;
        $country->save();

        // Country::destroy( $id );
    }

}