<?php

class StockproductController extends Controller {

    public function get() {
        return Stockproduct::orderBy('name')->get();
    }

    public function getById( $id ) {
        return Stockproduct::find( $id );
    }

    public function create() {
        $request = Request::all();

        $surveyor = new Stockproduct;
        $surveyor->name = $request['name'];
        $id = $surveyor->save();
        return $surveyor;
    }

    public function update( $id ) {
        $request = Request::all();

        $surveyor = Stockproduct::find( $id );
        $surveyor->name = $request['name'];

        $surveyor->save();
    }

    public function remove( $id ) {
        Stockproduct::destroy( $id );
    }

}