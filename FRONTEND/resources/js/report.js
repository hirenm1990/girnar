$(function() {

    $(document).on('click', '#get-report', function() {
        var query = $('#query-text').val();

        $.post(
            config.url + 'api/report/custom',
            {
                query: query
            },
            function(response) {
                var data = "";

                $.each(response[0], function(i, v) {
                    data += "<th>"+i+"</th>";
                });

                for(var i=0;i<response.length;i++) {
                    data += "<tr>";
                    $.each(response[i], function(i, v) {
                        data += "<td>"+v+"</td>";
                    });
                    data += "</tr>";
                }

                $(".target").html( "<table class='table'>" + data + "</table>");
            },
            'json'
        );
    });

});