<?php

class PaymentTermController extends Controller {

    public function get() {
        return PaymentTerm::orderBy('name')->get();
    }

    public function getById( $id ) {
        return PaymentTerm::find( $id );
    }

    public function create() {
        $request = Request::all();

        $paymentterm = new PaymentTerm;
        $paymentterm->name = $request['name'];
        $id = $paymentterm->save();
        return $paymentterm;
    }

    public function update( $id ) {
        $request = Request::all();

        $paymentterm = PaymentTerm::find( $id );
        $paymentterm->name = $request['name'];

        $paymentterm->save();
    }

    public function remove( $id ) {
        PaymentTerm::destroy( $id );
    }

}