<?php

class DischargePortController extends Controller {

    public function get() {
        return DischargePort::orderBy('name')->get();
    }

    public function getById( $id ) {
        return DischargePort::find( $id );
    }

    public function create() {
        $request = Request::all();

        $dischargeport = new DischargePort;
        $dischargeport->name = $request['name'];
        $id = $dischargeport->save();
        return $dischargeport;
    }

    public function update( $id ) {
        $request = Request::all();

        $dischargeport = DischargePort::find( $id );
        $dischargeport->name = $request['name'];

        $dischargeport->save();
    }

    public function remove( $id ) {
        DischargePort::destroy( $id );
    }

}