<?php

class Auth extends Controller {

    public function index( $route, $params ) {
        if(Cache::_isset('token') && Cache::_isset('userid')) {

            $user_restricted_areas = json_decode(API::get('user/restricted_areas'));

            if(!is_array($user_restricted_areas)) {
                $user_restricted_areas = [];
            }

            $method = (isset($_REQUEST['_method'])) ? $_REQUEST['_method'] : $_SERVER['REQUEST_METHOD'];
            $requested_area = strtolower($method) . ':' . $route['callback'];

            if(in_array($requested_area, $user_restricted_areas)) {
                $this->redirect();
            }

            return true;
        }

        $this->redirect('login');
    }

}