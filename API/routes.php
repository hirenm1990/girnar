<?php

// Do user login
Router::get('user/login', function() {
	echo json_encode(Auth::login());
	exit();
});
Router::post('user/login', function() {
    $data = json_decode(file_get_contents("php://input"), true);
    $_REQUEST = array_merge($_REQUEST, $data);

    echo json_encode(Auth::login());
    exit();
});
Router::get('checkLogin', function() {
    $request = $_REQUEST;

    if(isset($request['_token'], $request['_user'])) {
        $data = User::find( $request['_user'] );
        $token = sha1( $data['email'] . $data['username'] . $data['created_at'] );

        if($request['_token'] == $token) {
            return ['success' => true];
        }
    }

    return ['error' => 'Authentication failed!'];
});

Router::group(['middleware' => 'auth'], function() {

    # Autogenerate routes for basic CRUD operations
    Router::auto('importer', 'importer');
    Router::auto('product', 'product');
    Router::auto('scheme', 'scheme');
    Router::auto('surveyor', 'surveyor');
    Router::auto('country', 'country');
    Router::auto('port', 'port');
    Router::auto('forwarder', 'forwarder');
    Router::auto('paymentterm', 'paymentterm');
    Router::auto('deliveryterm', 'deliveryterm');
    Router::auto('dischargeport', 'dischargeport');
    Router::auto('finaldestination', 'finaldestination');
    Router::auto('package', 'package');
    Router::auto('stockproduct', 'stockproduct');
    Router::auto('stockvendor', 'stockvendor');
    // Router::auto('vgm','vgm');

    # Company information
    Router::get('company', 'company@get');
    Router::get('company/{id}', 'company@getById');
    Router::put('company', 'company@update');

    # User profile
    Router::get('user/profile', 'user@profile'); # Get current user's profile
    Router::put('user/profile', 'user@update'); # Update current user profile

    # Change user password
    Router::put('user/changepass', 'user@changepass');

    # Manage user's restricted areas
    Router::get('user/restricted_areas', 'user@restricted_areas');
    Router::get('user/restricted_areas/{id}', 'user@restricted_areasById');
    Router::put('user/restricted_areas/{id}', 'user@update_restricted_areas');

    Router::get('user', 'user@get'); # Get all users
    Router::get('user/{id}', 'user@getById'); # Get user by `Id`
    Router::post('user', 'user@create'); # Add New User
    Router::delete('user/{id}', 'user@remove'); # Remove user

    # Contracts route
    Router::get('contracts', 'contract@get'); # Get all contracts
    Router::get('contracts/shipments', 'contract@getshipments'); # Get all contracts
    Router::get('contract/shipmentlist/{contract_id}', 'contract@getshipmentlist'); # Get all contracts
    Router::get('contracts/filter/{type}', 'contract@get'); # Get all contracts filtered by `Type`
    Router::get('contract/{id}', 'contract@getById'); # Get specific shipment by `Id`
    Router::get('contract/c/{id}', 'contract@getContractById'); # Get specific contract by `Id`

    Router::post('contract', 'contract@create'); # Create new contract
    Router::post('contract/create/shipment', 'contract@createshipment'); # Create new contract

    Router::put('contract/{id}', 'contract@update'); # Update contract table data
    Router::put('shipment/{id}', 'contract@updateshipment'); # Update shipment table data with sub products table

    Router::post('contract/document', 'document@upload'); # Create document entry for contract
    Router::delete('contract/document/{document_id}', 'document@remove'); # Remove document from contract
    Router::get('document/{id}', 'document@get'); # Get document by `Id`

    Router::put('contract/do/{shipment_id}', 'contract@updatedo'); # Delivery Order Update
    Router::put('contract/rm/{shipment_id}', 'contract@updaterm'); # Raw Material Update
    Router::put('contract/scheme/{shipment_id}', 'contract@updatescheme'); # Contract Schemes Update

    Router::delete('contract/remove/{contract_id}', 'contract@remove'); # Remove whole contract
    Router::delete('shipment/remove/{shipment_id}', 'contract@removeshipment'); # Remove selected shipment

    Router::put('shipment/stuffing/{shipment_id}', 'contract@updatestuffing'); # Update Stuffing Tab Container Details
    Router::put('shipment/comminvoice/{shipment_id}', 'contract@updatecomminvoice'); # Update Comm Invoice Tab Details
    
    Router::post('shipment/vgm/{shipment_id}','contract@updatevgm');
    // Router::get('shipment/vgm/{id}','vgm@get');
    # Dashboard Statistics
    Router::get('dashboard/statistics', 'dashboard@statistics');


    # Reports
    Router::get('report/custom', 'report@custom');
    Router::get('report/contract/weeklysheet', 'report@contractWeeklySheet');
    Router::get('report/contract/girnarmaster', 'report@contractGirnarMaster');
    Router::get('report/weeklycontracts', 'report@weeklycontractsdone');
    Router::get('reports/weeklydocument', 'report@weeklydocument');
    Router::get('reports/weeklystuffing', 'report@weeklystuffingReport');
    Router::get('reports/weeklyfreight', 'report@weeklyfreightReport');
    Router::get('reports/weeklyinvoice', 'report@weeklyinvoice');

    # Quotation Sheet
    Router::get('quotation', 'quotation@get');
    Router::put('quotation', 'quotation@update');

    # Purchase
    Router::get('purchase', 'purchase@get');
    Router::post('purchase', 'purchase@store');
    Router::delete('purchase/{id}', 'purchase@remove');
    Router::get('stock', 'purchase@stock');
    Router::get('purchase/edit/{id}', 'purchase@edit');
    Router::post('purchase/update/{id}', 'purchase@update');

    // Router::get('vgm','contract@vgm_get');
    // Router::post('vgm','contract@vgm_store');

});