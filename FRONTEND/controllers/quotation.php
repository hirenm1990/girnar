<?php

class QuotationController extends Controller {

    public function index() {
        $data = [
            'Title' => 'Quotation Sheet',
            'Data' => API::get('quotation'),
            '_js'   => [RESOURCES . '/js/quotation.js']
        ];
        $this->render('quotation/index', $data);
    }

    public function partial()
    {
        $data = [
            'Title' => 'Quotation Sheet',
            'Data' => API::get('quotation'),
            '_js'   => [RESOURCES . '/js/quotation.js']
        ];
        $this->render('quotation/partial', $data);
    }

    public function store()
    {
        if(isset($_POST['json_data'])) {
            API::put("quotation", Request::all());
        }
    }

}