<?php

class PurchaseController extends Controller {

    public function get() {
        return Purchase::with('vendor', 'product')->orderBy('order_date', 'desc')->get();
    }

    public function store() {
        $request = Request::all();

        $purchase = new Purchase;
        $purchase->company_id = $request['_companyid'];
        $purchase->user_id = $request['_user'];
        $purchase->product_id = $request['product_id'];
        $purchase->vendor_id = $request['vendor_id'];
        $purchase->order_date = $this->ymd($request['order_date']);
        $purchase->delivery_date = $this->ymd($request['delivery_date']);
        $purchase->qty = $request['qty'];
        $purchase->amount = $request['amount'];
        $purchase->save();
        return $purchase;
    }

    public function remove( $id ) {
        Purchase::destroy( $id );
    }

    # Current Stock's Sheet
    public function stock() {
        $return = array();

        foreach( Stockproduct::all() as $product ) {
            $purchased = Purchase::where('product_id', $product->Id)->sum('qty');
            $used = RawMaterial::where('product_id', $product->Id)->sum('qty');

            $return[] = [
                'product_id' => $product->Id,
                'product' => $product->name,
                'stock' => $purchased - $used
            ];
        }

        return $return;
    }

    //Purchase 
    public function edit( $id )
    {
        return Purchase::find($id);
    }

    public function update( $id )
    {
        $request = Request::all();

        $purchase = Purchase::find( $id );
        $purchase->company_id = $request['_companyid'];
        $purchase->user_id = $request['_user'];
        $purchase->product_id = $request['product_id'];
        $purchase->vendor_id = $request['vendor_id'];
        $purchase->order_date = $this->ymd($request['order_date']);
        $purchase->delivery_date = $this->ymd($request['delivery_date']);
        $purchase->qty = $request['qty'];
        $purchase->amount = $request['amount'];
        $purchase->save();
        // return $purchase;
    }
}