<?php

class DischargePortController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "DischargePort's Register",
            'DischargePorts' => API::get('dischargeports')
        ];
        $this->render('dischargeports/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New DischargePort"
        ];
        $this->render('dischargeports/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('dischargeport', Request::all());
        $this->redirect('dischargeports');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit DischargePort",
            'DischargePort' => API::get('dischargeport/' . $id)
        ];
        $this->render('dischargeports/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("dischargeport/$id", Request::all());
        $this->redirect('dischargeports');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('dischargeport/' . $id);
        $this->redirect('dischargeports');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('dischargeports/quickAdd');
    }
    public function quickCreate() {
        echo API::post('dischargeport', Request::all(), true);
        exit();
    }
}