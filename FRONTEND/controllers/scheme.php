<?php

class SchemeController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "Scheme's Register",
            'Schemes' => API::get('schemes')
        ];
        $this->render('schemes/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New Scheme"
        ];
        $this->render('schemes/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('scheme', Request::all());
        $this->redirect('schemes');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit Scheme",
            'Scheme' => API::get('scheme/' . $id)
        ];
        $this->render('schemes/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("scheme/$id", Request::all());
        $this->redirect('schemes');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('scheme/' . $id);
        $this->redirect('schemes');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('schemes/quickAdd');
    }
    public function quickCreate() {
        echo API::post('scheme', Request::all(), true);
        exit();
    }
}