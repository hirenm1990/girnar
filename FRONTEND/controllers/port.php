<?php

class PortController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "Port's Register",
            'Ports' => API::get('ports')
        ];
        $this->render('ports/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New Port"
        ];
        $this->render('ports/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('port', Request::all());
        $this->redirect('ports');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit Port",
            'Port' => API::get('port/' . $id)
        ];
        $this->render('ports/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("port/$id", Request::all());
        $this->redirect('ports');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('port/' . $id);
        $this->redirect('ports');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('ports/quickAdd');
    }
    public function quickCreate() {
        echo API::post('port', Request::all(), true);
        exit();
    }
}