<?php

class ImporterController extends Controller {

    public function get() {
        return Importer::where('is_active', 1)->orderBy('name')->get();
    }

    public function getById( $id ) {
        return Importer::find( $id );
    }

    public function create() {
        $request = Request::all();

        $importer = new Importer;
        $importer->name = $request['name'];
        $importer->address = $request['address'];
        $importer->country = $request['country'];
        $importer->phone = $request['phone'];
        $importer->phone2 = $request['phone2'];
        $id = $importer->save();
        return $importer;
    }

    public function update( $id ) {
        $request = Request::all();

        $importer = Importer::find( $id );
        $importer->name = $request['name'];
        $importer->address = $request['address'];
        $importer->country = $request['country'];
        $importer->phone = $request['phone'];
        $importer->phone2 = $request['phone2'];

        $importer->save();
    }

    public function remove( $id ) {
        $importer = Importer::find( $id );
        $importer->is_active = 0;
        $importer->save();

        //Importer::destroy( $id );
    }

}