<?php

class Request {

    public static function all() {
        return $_REQUEST;
    }

    public static function get( $key ) {
        return (isset($_REQUEST[$key])) ? $_REQUEST[$key] : false;
    }

}