<?php

class FinalDestinationController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "FinalDestination's Register",
            'FinalDestinations' => API::get('finaldestinations')
        ];
        $this->render('finaldestinations/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New FinalDestination"
        ];
        $this->render('finaldestinations/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('finaldestination', Request::all());
        $this->redirect('finaldestinations');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit FinalDestination",
            'FinalDestination' => API::get('finaldestination/' . $id)
        ];
        $this->render('finaldestinations/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("finaldestination/$id", Request::all());
        $this->redirect('finaldestinations');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('finaldestination/' . $id);
        $this->redirect('finaldestinations');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('finaldestinations/quickAdd');
    }
    public function quickCreate() {
        echo API::post('finaldestination', Request::all(), true);
        exit();
    }
}