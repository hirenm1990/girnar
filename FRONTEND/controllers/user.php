<?php

class UserController extends Controller {

    public function index() {
        $data = [
            'Title' => 'User Management',
            'Users' => API::get('user')
        ];
        $this->render('user/index', $data);
    }

    public function _new() {
        $data = [
            'Title' => 'Add New User'
        ];
        $this->render('user/new', $data);
    }

    public function create() {
        echo API::post('user', Request::all(), true);
        exit();
        $this->redirect('user/index');
    }

    public function remove( $id ) {
        API::delete('user/' . $id);
        $this->redirect('user/index');
    }

    public function edit( $id ) {
        $haystack = [
            'Contracts Area' => [
                    'Contracts Register' => 'get:contract@index',
                    'Add New Contract' => 'get:contract@create,get:contract@_new',
                    'Edit Contract' => 'get:contract@edit'
                ],
            // 'General Data Entry' => [
            //         'Products' => '',
            //         'Importers' => '',
            //         'Export Schemes' => '',
            //         'Surveyor Companies' => '',
            //         'Countries' => '',
            //         'Ports' => '',
            //     ],
            // 'Reports' => [
            //         'All Reports' => ''
            //     ],
            // 'Settings' => [
            //         'User Management' => '',
            //         'Company Details' => '',
            //         'Application Settings' => '',
            //         'Log Master' => '',
            //         'Backup' => '',
            //     ]
        ];

        $data = [
            'Title' => 'Restrict User',
            'User' => API::get('user/' . $id),
            'Restricted' => json_decode(API::get('user/restricted_areas/' . $id)),
            'Haystack' => $haystack
        ];
        $this->render('user/edit', $data);
    }

    public function profile() {
        $data = [
            'Title' => 'User Profile',
            'User' => API::get('user/profile')
        ];
        $this->render('user/profile', $data);
    }

    public function update() {
        API::put("user/profile", Request::all());
        $this->redirect('user/profile');
    }

    public function updateRestrictedArea( $id ) {
        $restricted = [];
        foreach(Request::get('check') as $restrict) {
            $temp = explode(",", $restrict);
            $restricted = array_merge($restricted, $temp);
        }

        API::put("user/restricted_areas/" . $id, ['restricted' => $restricted]);
        $this->redirect('user/index');
    }

    public function changepass() {
        $data = [
            'Title' => 'Change Password',
            '_js' => [ RESOURCES . '/js/change-pass.js' ]
        ];
        $this->render('user/change-pass', $data);
    }

    public function updatepass() {
        echo json_encode(API::put("user/changepass", Request::all()));
        exit();
    }

}