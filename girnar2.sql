-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2017 at 06:42 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `girnar2`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `Id` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` tinytext NOT NULL,
  `address_short` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `phone2` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `iec_no` varchar(255) NOT NULL,
  `pan_no` varchar(255) NOT NULL,
  `gst_no` varchar(255) NOT NULL,
  `lut_no` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL DEFAULT '',
  `bank_address` tinytext NOT NULL,
  `weighbridge_reg_no` varchar(255) NOT NULL,
  `weighbridge_address` varchar(255) NOT NULL,
  `quotation_data` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`Id`, `name`, `address`, `address_short`, `phone`, `phone2`, `website`, `iec_no`, `pan_no`, `gst_no`, `lut_no`, `bank_name`, `bank_address`, `weighbridge_reg_no`, `weighbridge_address`, `quotation_data`, `created_at`, `updated_at`) VALUES
(1, 'Girnar Industries', 'Opp. Rajdhani weighbridge,\nRajkot Road, Dolatpara, Junagadh(362003),\nGUJARAT, INDIA\nTel. +91 285 2660893', 'Opp. Rajdhani weighbridge, Rajkot Road,\r\nDolatpara, Junagadh(362003),GUJARAT, INDIA', '+91 285 2660893', '', 'www.girnarindustries.com', '2412004315', 'AAIFG8649K', '24AAIFG8649K1ZW', 'v/30-09/mp/LUT/2017-18/1581', 'AXIS BANK', 'Shop No. 1,2,3 Raiji Nagar Shoping Centre,\nN K mehta Road,Motibag,\nJunagadh(362001), Gujarat, INDIA.\nA/C No. 912030004364581, SWIFT Code:- AXISINBB087', '194824-13/06/2016', 'GIRNAR INDUSTRIES ', '[\"310\",\"13250\",\"7500\",\"2500\",\"2000\",\"2000\",\"8000\",\"600\",\"2000\",\"15000\",\"150\",\"1500\",\"1400\",\"0\",\"13250\",\"7500\",\"2500\",\"2000\",\"2000\",\"8000\",\"600\",\"2000\",\"15000\",\"150\",\"1475\",\"1400\",\"0\",\"13250\",\"7000\",\"2500\",\"2000\",\"2000\",\"8000\",\"600\",\"2000\",\"15000\",\"150\",\"9600\",\"13250\",\"7500\",\"2500\",\"2000\",\"2000\",\"8000\",\"600\",\"2000\",\"15000\",\"150\",\"26\",\"1400\",\"13250\",\"7000\",\"2500\",\"4000\",\"2000\",\"8000\",\"600\",\"2000\",\"15000\",\"150\",\"132352\",\"700\",\"1200\",\"0\",\"0\",\"0\",\"20000\",\"13250\",\"7500\",\"2500\",\"800\",\"200\",\"200\",\"2000\",\"8000\",\"0\",\"150\",\"64.4\",\"22\",\"2\",\"1000\",\"1800\"]', '2016-09-03 17:28:46', '2017-07-22 10:34:56');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `Id` int(11) NOT NULL,
  `company_id` int(5) NOT NULL DEFAULT '0',
  `user_id` int(5) NOT NULL,
  `contract_no` varchar(255) DEFAULT NULL,
  `contract_date` date NOT NULL DEFAULT '0000-00-00',
  `importer_id` int(5) NOT NULL,
  `importer_address` tinytext NOT NULL,
  `notifier_party` tinytext NOT NULL,
  `consignee_party` tinytext NOT NULL,
  `purchase_order_no` varchar(255) NOT NULL,
  `invoice_no` varchar(255) NOT NULL DEFAULT '0',
  `invoice_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `surveyor_id` int(5) NOT NULL,
  `surveyor_name` varchar(255) NOT NULL DEFAULT '',
  `port_loading` varchar(255) NOT NULL,
  `port_discharge` varchar(255) NOT NULL,
  `port_discharge2` varchar(255) DEFAULT '',
  `port_discharge3` varchar(255) DEFAULT '',
  `final_destination_country` varchar(255) NOT NULL,
  `final_destination` varchar(255) NOT NULL,
  `final_destination2` varchar(255) DEFAULT '',
  `final_destination3` varchar(255) DEFAULT '',
  `delivery_terms` varchar(255) NOT NULL,
  `payment_terms` varchar(255) NOT NULL,
  `quotation_freight` varchar(255) NOT NULL,
  `dollor_exchange_rate` varchar(255) NOT NULL,
  `currency` varchar(25) NOT NULL DEFAULT 'usd',
  `contract_type` varchar(20) NOT NULL DEFAULT 'single',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`Id`, `company_id`, `user_id`, `contract_no`, `contract_date`, `importer_id`, `importer_address`, `notifier_party`, `consignee_party`, `purchase_order_no`, `invoice_no`, `invoice_date`, `surveyor_id`, `surveyor_name`, `port_loading`, `port_discharge`, `port_discharge2`, `port_discharge3`, `final_destination_country`, `final_destination`, `final_destination2`, `final_destination3`, `delivery_terms`, `payment_terms`, `quotation_freight`, `dollor_exchange_rate`, `currency`, `contract_type`, `created_at`, `updated_at`) VALUES
(8, 0, 3, '343', '2016-12-21', 5, '972/1 3RD FLOOR, VORASUBIN BUILDING\r\nSOI RANA 9 HOSPITAL,RAMA 9 ROAD, BANG KAPI,\r\nBANG KAPI, HUAI KHWANG,BANGKOK 10320 THAILAND', 'KUEHNE+NAGEL, INC.18900 8TH AVESOUTH, SUITE 1300,\r\nSEA TAC,WA 98148\r\nATTN: WENDY THORNTON\r\nTEL: +1 -206-901-9000 EXT 301, FAX: +1-206-901-0977', 'S.A.F.E CHEMICALS LLC\r\n3553 KILGORE PARKWAY, BAYTOWN,TX7753,USA\r\nCONTACT PERSON: MANOJ JAIN\r\nTEL : +1 -206-275-3992 EFAX : +1-419-821-5261', 'STAR ASIA /05-013/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'HOUSTON', '', '', '231', 'HOUSTON', '', '', 'CNF', 'CAD THROUGH BANK', '[\"1620\",\"\"]', '67.95', 'usd', 'single', '2016-12-28 00:19:06', '2016-12-29 19:16:34'),
(11, 0, 3, '373', '2017-01-10', 10, 'P.O. Box: 42448 HAMRIYAH FREE ZONE, SHARJAH - UAE Tel: +971 6 52 60788 Fax: +971 6 52 61799', 'BAALBAKI GROUP SA OFFSHORE\r\nP.O. Box: 42448 HAMRIYAH FREE ZONE, SHARJAH - UAE Tel: +971 6 52 60788 Fax: +971 6 52 61799', 'BAALBAKI GROUP SA OFFSHORE\r\nP.O. Box: 42448 HAMRIYAH FREE ZONE, SHARJAH - UAE Tel: +971 6 52 60788 Fax: +971 6 52 61799', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'sharjah', '', '', '229', 'sharjah', '', '', 'CIF', 'CAD THROUGH BANK', '[\"710\",\"\"]', '68.24', 'usd', 'single', '2017-01-10 21:19:15', '2017-01-10 21:19:47'),
(12, 0, 3, '373', '2017-01-10', 10, 'P.O. Box: 42448 HAMRIYAH FREE ZONE, SHARJAH - UAE Tel: +971 6 52 60788 Fax: +971 6 52 61799', 'BAALBAKI GROUP SA OFFSHORE\nP.O. Box: 42448 HAMRIYAH FREE ZONE, SHARJAH - UAE Tel: +971 6 52 60788 Fax: +971 6 52 61799', 'BAALBAKI GROUP SA OFFSHORE\nP.O. Box: 42448 HAMRIYAH FREE ZONE, SHARJAH - UAE Tel: +971 6 52 60788 Fax: +971 6 52 61799', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'sharjah', '', '', '229', 'sharjah', '', '', 'CIF', 'CAD THROUGH BANK', '[\"710\",\"\"]', '68.24', 'usd', 'multiple', '2017-01-11 01:14:52', '2017-01-11 01:19:28'),
(13, 0, 3, '399', '2017-01-20', 12, 'KOPER, SLOVENIA', 'SOLCHEM D.O.O.\nKOPER, SLOVENIA', 'SOLCHEM D.O.O.\nKOPER, SLOVENIA', 'NN170052', '0', '0000-00-00 00:00:00', 2, '', '2', 'KOPER', '', '', '247', 'KOPER', '', '', 'CIF', '30% ADVANCE & 70% AGAINST DOCS', '[\"1300\",\"\"]', '68.2', 'usd', 'single', '2017-01-22 19:36:27', '2017-01-22 19:36:27'),
(14, 0, 3, '397', '2017-01-19', 13, 'INDUSTRIAL ZONE # 2,\r\nPART 233, 6th OF OCTOBER CITY\r\n', 'EAGLE POLYMERS\nINDUSTRIAL ZONE # 2,\r\nPART 233, 6th OF OCTOBER CITY\r\n', 'EAGLE POLYMERS\nINDUSTRIAL ZONE # 2,\r\nPART 233, 6th OF OCTOBER CITY\r\n', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'ALEXANDRIA OLD PORT', '', '', '248', 'EGYPT', '', '', 'CIF', 'CAD THROUGH BANK', '[\"1110\",\"\"]', '68.22', 'usd', 'single', '2017-01-22 19:51:38', '2017-01-22 19:51:38'),
(15, 0, 3, '398', '2017-01-20', 3, 'KAOHSIUNG\r\nTAIWAN', 'BJ HONG\r\nKAOHSIUNG\r\nTAIWAN', 'BJ HONG\r\nKAOHSIUNG\r\nTAIWAN', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"180\",\"\"]', '68.17', 'usd', 'single', '2017-01-23 00:14:17', '2017-01-23 00:19:42'),
(16, 0, 3, '400', '2017-01-20', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111\r\nMRS. ANA BARBOSA', 'IMP-003/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'SANTOS', '', '', '33', 'SANTOS', '', '', 'CNF', 'CAD THROUGH BANK', '[\"1100\",\"\"]', '68.10', 'usd', 'single', '2017-01-25 17:53:46', '2017-01-25 18:31:27'),
(17, 0, 3, '394', '2017-01-18', 15, 'AVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', 'INTERFAT, S.A.\nAVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', 'INTERFAT, S.A.\nAVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', '12867', '0', '0000-00-00 00:00:00', 2, '', '1', 'BARCELONA', '', '', '204', 'ALEXANDRIA OLD PORT', '', '', 'FOB ', 'CAD THROUGH BANK', '[\"200\",\"\"]', '68.04', 'usd', 'single', '2017-01-25 18:16:49', '2017-01-25 18:16:49'),
(18, 0, 3, '392', '2017-01-18', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1180\",\"\"]', '68.17', 'usd', 'single', '2017-01-25 18:30:40', '2017-01-25 22:13:56'),
(19, 0, 3, '391', '2017-01-16', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\r\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\r\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '13(A+B+C+D)', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', '100% TT PAYMENT', '[\"815\",\"\"]', '68.17', 'usd', 'single', '2017-01-25 22:11:23', '2017-01-25 22:16:51'),
(20, 0, 3, '390', '2017-01-16', 21, '121 ALBERT STREET, FLEET, HAMPSHIRE GU51 3SR\r\nTELEPHONE : 01252 613058 FAX 01252 616374\r\nEMAIL : jon@hampshire-commodities.co.uk\r\n', 'HAMPSHIRE COMMODITIES LIMITED\r\n121 ALBERT STREET, FLEET, HAMPSHIRE GU51 3SR\r\nTELEPHONE : 01252 613058 FAX 01252 616374\r\nEMAIL : jon@hampshire-commodities.co.uk\r\n', 'HAMPSHIRE COMMODITIES LIMITED\r\n121 ALBERT STREET, FLEET, HAMPSHIRE GU51 3SR\r\nTELEPHONE : 01252 613058 FAX 01252 616374\r\nEMAIL : jon@hampshire-commodities.co.uk\r\n', '4187', '0', '0000-00-00 00:00:00', 2, '', '1', 'felixstowe', '', '', '230', 'felixstowe', '', '', 'FOB ', 'CAD THROUGH BANK', '[\"\",\"\"]', '68.17', 'usd', 'single', '2017-01-25 22:43:56', '2017-01-25 23:18:14'),
(22, 0, 3, '375', '2017-01-11', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\r\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\r\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '6(a+b+c)', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', '100% TT PAYMENT', '[\"1430\",\"\"]', '68.17', 'usd', 'single', '2017-01-26 01:41:42', '2017-01-26 16:13:26'),
(23, 0, 3, '362', '2017-01-03', 23, 'GIRONA,34\r\n08400 GRANOLLERS (BARCELONA)\r\nSPAIN', 'ADUANAS LLOBET S.L\r\nAVDA. DRASSANES, 6-8,PLANTA 1, OFIC. 2 Y 3\r\n08001 BARCELONA\r\nTEL : 93.3426017/18 (ATT. SR ALEX SERRANO)', 'TEXTRON TECNICA S.L.\nGIRONA,34\r\n08400 GRANOLLERS (BARCELONA)\r\nSPAIN', '126.989', '0', '0000-00-00 00:00:00', 2, '', '1', 'BARCELONA', '', '', '204', 'BARCELONA', '', '', 'CIF', 'TT 45 DAYS FROM B/L DATE', '[\"\",\"\"]', '', 'usd', 'single', '2017-01-26 16:24:56', '2017-01-26 16:24:56'),
(24, 0, 3, '359', '2017-01-02', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\r\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\r\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '01 ( A+B)', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CASH AGAINST DOCUMENT', '[\"1370\",\"\"]', '68.13', 'usd', 'single', '2017-01-26 16:27:23', '2017-01-26 16:28:14'),
(25, 0, 3, '409', '2017-01-28', 24, '7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', '17SL4MLSY010', '0', '0000-00-00 00:00:00', 2, '', '2', 'SHANGHAI', '', '', '47', 'SHANGHAI', '', '', 'CIF', 'CAD THROUGH BANK', '[\"85\",\"\"]', '68.1', 'usd', 'single', '2017-02-01 17:09:35', '2017-02-01 17:09:35'),
(26, 0, 3, '416', '2017-01-31', 24, '7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\r\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\r\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', '17SL4MLSY014', '0', '0000-00-00 00:00:00', 2, '', '2', 'SHANGHAI', '', '', '47', 'SHANGHAI', '', '', 'CIF', 'CAD THROUGH BANK', '[\"75\",\"\"]', '67.87', 'usd', 'single', '2017-02-01 17:18:26', '2017-02-01 17:22:32'),
(27, 0, 3, '417', '2017-01-31', 24, '7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', '17SL4MLSY014', '0', '0000-00-00 00:00:00', 2, '', '2', 'HUANGPU', '', '', '47', 'HUANGPU', '', '', 'CIF', 'CAD THROUGH BANK', '[\"185\",\"\"]', '67.87', 'usd', 'single', '2017-02-01 17:21:41', '2017-02-01 17:21:41'),
(28, 0, 3, '420', '2017-01-31', 25, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'TO ORDER', '2104', '0', '0000-00-00 00:00:00', 2, '', '1', 'VERACRUZ', '', '', '144', 'MEXICO', '', '', 'CIF', 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '[\"1175\",\"\"]', '67.93', 'usd', 'single', '2017-02-01 17:39:46', '2017-02-01 17:39:46'),
(29, 0, 3, '419', '2017-01-31', 25, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\r\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'TO ORDER', '2103', '0', '0000-00-00 00:00:00', 2, '', '1', 'VERACRUZ', '', '', '144', 'VERACRUZ', '', '', 'CIF', 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '[\"1240\",\"\"]', '67.93', 'usd', 'single', '2017-02-01 17:45:35', '2017-02-01 17:48:32'),
(30, 0, 3, '414', '2017-01-31', 26, '60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', 'CJP CHEMICALS (PTY) LTD\n60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', 'CJP CHEMICALS (PTY) LTD\n60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', '94080', '0', '0000-00-00 00:00:00', 2, '', '2', 'CAPETOWN', '', '', '202', 'CAPETOWN', '', '', 'CFR', '60 DAYS LC FROM BL DATE', '[\"600\",\"\"]', '67.97', 'usd', 'single', '2017-02-01 18:41:26', '2017-02-01 18:41:26'),
(31, 0, 3, '413', '2017-01-31', 27, 'PO BOX 41671. HAMIRIYAH FREE ZONE, PHASE I,\r\nSHARJAH', 'IFFCO CHEMICALS FZE\nPO BOX 41671. HAMIRIYAH FREE ZONE, PHASE I,\r\nSHARJAH', 'IFFCO CHEMICALS FZE\nPO BOX 41671. HAMIRIYAH FREE ZONE, PHASE I,\r\nSHARJAH', '2000211', '0', '0000-00-00 00:00:00', 3, '', '1', 'JEBEL ALI', '', '', '229', 'JEBEL ALI', '', '', 'CIF', '100% ADVANCE PAYMENT', '[\"80\",\"\"]', '68.05', 'usd', 'single', '2017-02-01 18:48:42', '2017-02-01 18:48:42'),
(32, 0, 3, '412', '2017-01-30', 28, 'PO BOX 2571 DAMMAM', 'SAUDI INDUSTRIAL DETERGENTS CO.\r\nPO BOX 2571 DAMMAM', 'SAUDI INDUSTRIAL DETERGENTS CO.\r\nPO BOX 2571 DAMMAM', '', '0', '0000-00-00 00:00:00', 3, '', '1', 'DAMMAM', '', '', '192', 'DAMMAM', '', '', 'CIF', 'CAD THROUGH BANK', '[\"325\",\"\"]', '68.05', 'usd', 'single', '2017-02-01 18:57:22', '2017-02-01 18:58:05'),
(33, 0, 3, '411', '2017-01-30', 29, 'Jesica Road, Campbellfield,\r\nVictoria 3061', 'AUSTRALIAN VINYLS CORPORATION PTY LTD\nJesica Road, Campbellfield,\r\nVictoria 3061', 'AUSTRALIAN VINYLS CORPORATION PTY LTD\nJesica Road, Campbellfield,\r\nVictoria 3061', '81012 OP', '0', '0000-00-00 00:00:00', 2, '', '2', 'SYDNEY', '', '', '16', 'SYDNEY', '', '', 'CIF', '100% TT PAYMENT', '[\"400\",\"\"]', '68.19', 'usd', 'single', '2017-02-01 19:07:09', '2017-02-01 19:07:09'),
(34, 0, 3, '421', '2017-02-01', 30, '129/112 Moo1 Bangkayang, Moung,\r\nANY PORT, INDIA\r\nPathumthani 12000\r\nThailand', 'SIROCKO (THAILAND) CO. LTD.\r\n129/112 Moo1 Bangkayang, Moung,\r\nANY PORT, INDIA\r\nPathumthani 12000\r\nThailand', 'SIROCKO (THAILAND) CO. LTD.\r\n129/112 Moo1 Bangkayang, Moung,\r\nANY PORT, INDIA\r\nPathumthani 12000\r\nThailand', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'BANGKOK', '', '', '217', 'BANGKOK', '', '', 'CIF', '100% ADVANCE PAYMENT', '[\"205\",\"\"]', '67.56', 'usd', 'single', '2017-02-02 17:06:15', '2017-02-02 17:06:59'),
(35, 0, 3, '407', '2017-01-27', 31, '115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1180\",\"\"]', '68.21', 'usd', 'single', '2017-02-02 17:42:48', '2017-02-02 17:42:48'),
(36, 0, 3, '405', '2017-01-25', 32, 'Carretera a GarcÃ­a Km 1.2 S/N\r\nSanta Catarina, N.L.\r\nCP 66350 MÃ©xico\r\nTax ID: LAM951127KF6', 'LUBRICANTES DE AMERICA, S.A. DE C.V.\r\nCarretera a GarcÃ­a Km 1.2 S/N\r\nSanta Catarina, N.L.\r\nCP 66350 MÃ©xico\r\nTax ID: LAM951127KF6', 'LUBRICANTES DE AMERICA, S.A. DE C.V.\r\nCarretera a GarcÃ­a Km 1.2 S/N\r\nSanta Catarina, N.L.\r\nCP 66350 MÃ©xico\r\nTax ID: LAM951127KF6', '4500110854', '0', '0000-00-00 00:00:00', 2, '', '2', 'ALTAMIRA', '', '', '144', 'ALTAMIRA', '', '', 'CIF', '100% TT PAYMENT', '[\"1275\",\"\"]', '68.14', 'usd', 'single', '2017-02-02 17:51:09', '2017-02-02 17:53:17'),
(37, 0, 3, '403', '2017-01-25', 33, 'NO.41, ALLEY 1, LANE420, KUANG-FU S.RD.,\r\nTAIPEI, TAIWAN R.O.C. 10695', 'PATECH FINE CHEMICALS CO., LTD.\nNO.41, ALLEY 1, LANE420, KUANG-FU S.RD.,\r\nTAIPEI, TAIWAN R.O.C. 10695', 'PATECH FINE CHEMICALS CO., LTD.\nNO.41, ALLEY 1, LANE420, KUANG-FU S.RD.,\r\nTAIPEI, TAIWAN R.O.C. 10695', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CIF', 'LC AT SIGHT', '[\"\",\"350\"]', '68.16', 'usd', 'single', '2017-02-02 17:59:53', '2017-02-02 17:59:53'),
(38, 0, 3, '402', '2017-01-24', 34, '905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'EGYPOLYMERS FOR CHEMICALS\r\nABO RAWASH INDUSTRIAL AREA\r\nGIZA, EGYPT\r\nTel: +201 019 676 764 / +201 009 815 151', 'EGYPOLYMERS FOR CHEMICALS\r\nABO RAWASH INDUSTRIAL AREA\r\nGIZA, EGYPT\r\nTel: +201 019 676 764 / +201 009 815 151', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'ALEXANDRIA', '', '', '248', 'ALEXANDRIA', '', '', 'CIF', 'DP AT SIGHT(AGAINST SCAN DOCS)', '[\"1150\",\"\"]', '68.17', 'usd', 'single', '2017-02-02 18:10:34', '2017-02-02 18:10:34'),
(39, 0, 3, '422', '2017-02-02', 31, '115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1275\",\"\"]', '67.48', 'usd', 'single', '2017-02-02 22:10:24', '2017-02-02 22:10:24'),
(40, 0, 3, '396', '2017-01-19', 35, 'NO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', 'YU YUAN TECHNOLOGY CO., LTD \nNO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', 'YU YUAN TECHNOLOGY CO., LTD \nNO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"180\",\"\"]', '68.22', 'usd', 'single', '2017-02-02 22:27:23', '2017-02-02 22:27:23'),
(41, 0, 3, '395', '2017-01-19', 36, '2F, NO 58, CHANGPING E, 3RD BEITUN DISTRICT,\r\nTAICHUNG CITY 406, TAIWAN R.O.C.', 'LVLIN BIOTECH CO LTD.\n2F, NO 58, CHANGPING E, 3RD BEITUN DISTRICT,\r\nTAICHUNG CITY 406, TAIWAN R.O.C.', 'LVLIN BIOTECH CO LTD.\n2F, NO 58, CHANGPING E, 3RD BEITUN DISTRICT,\r\nTAICHUNG CITY 406, TAIWAN R.O.C.', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"165\",\"\"]', '68.22', 'usd', 'single', '2017-02-02 22:36:35', '2017-02-02 22:36:35'),
(42, 0, 3, '388', '2017-01-16', 36, '2F, NO 58, CHANGPING E, 3RD BEITUN DISTRICT,\r\nTAICHUNG CITY 406, TAIWAN R.O.C.', 'LVLIN BIOTECH CO LTD.\n2F, NO 58, CHANGPING E, 3RD BEITUN DISTRICT,\r\nTAICHUNG CITY 406, TAIWAN R.O.C.', 'LVLIN BIOTECH CO LTD.\n2F, NO 58, CHANGPING E, 3RD BEITUN DISTRICT,\r\nTAICHUNG CITY 406, TAIWAN R.O.C.', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"165\",\"\"]', '68.18', 'usd', 'single', '2017-02-02 22:40:17', '2017-02-02 22:40:17'),
(43, 0, 3, '383', '2017-01-13', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"175\",\"\"]', '68.25', 'usd', 'single', '2017-02-02 22:47:58', '2017-02-02 22:47:58'),
(44, 0, 3, '380', '2017-01-12', 38, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"810\",\"\"]', '68.05', 'usd', 'single', '2017-02-02 22:56:28', '2017-02-02 22:56:28'),
(45, 0, 3, '379', '2017-01-12', 39, '12F No.695 MingCheng 3rd Road\r\nKaohsiung 804, Taiwan\r\nTel: +886 7 553-8738 Fax: +886 7 553-6867', 'TELENOLOGY INTERNATIONAL CO., LTD\n12F No.695 MingCheng 3rd Road\r\nKaohsiung 804, Taiwan\r\nTel: +886 7 553-8738 Fax: +886 7 553-6867', 'TELENOLOGY INTERNATIONAL CO., LTD\n12F No.695 MingCheng 3rd Road\r\nKaohsiung 804, Taiwan\r\nTel: +886 7 553-8738 Fax: +886 7 553-6867', 'J1051112-1', '0', '0000-00-00 00:00:00', 2, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CIF', '100% ADVANCE PAYMENT', '[\"\",\"275\"]', '68.26', 'usd', 'single', '2017-02-02 23:01:30', '2017-02-02 23:01:30'),
(46, 0, 3, '368', '2017-01-06', 40, '28-19, Bunseong-ro 5412eon-gi3, Gi0hae-si,\r\nGyeongsangna0-do, 50820, Korea\r\nTe3 : +82 55 322 9986 Fax : +82 55 322 9989', 'DONGEUN CO., LTD\n28-19, Bunseong-ro 5412eon-gi3, Gi0hae-si,\r\nGyeongsangna0-do, 50820, Korea\r\nTe3 : +82 55 322 9986 Fax : +82 55 322 9989', 'DONGEUN CO., LTD\n28-19, Bunseong-ro 5412eon-gi3, Gi0hae-si,\r\nGyeongsangna0-do, 50820, Korea\r\nTe3 : +82 55 322 9986 Fax : +82 55 322 9989', 'PO170106-1', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"\",\"300\"]', '67.98', 'usd', 'single', '2017-02-02 23:07:39', '2017-02-02 23:07:39'),
(47, 0, 3, '365', '2017-01-06', 41, 'NO.600. TING-HO SEC., TOU YUAN RD., FANG YUAN TOWNSHIP,\r\nCHANGHUA COUNTY 528, TAIWAN, R.O.C.', 'KING NUNG YEOU ENTERPRISE CO., LTD\nNO.600. TING-HO SEC., TOU YUAN RD., FANG YUAN TOWNSHIP,\r\nCHANGHUA COUNTY 528, TAIWAN, R.O.C.', 'KING NUNG YEOU ENTERPRISE CO., LTD\nNO.600. TING-HO SEC., TOU YUAN RD., FANG YUAN TOWNSHIP,\r\nCHANGHUA COUNTY 528, TAIWAN, R.O.C.', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"190\",\"\"]', '67.82', 'usd', 'single', '2017-02-02 23:10:53', '2017-02-02 23:10:53'),
(48, 0, 3, '357', '2017-01-02', 42, '68-1, 9 LING, BAO TANQ TSUN, GUAN IN SHIANG,\r\nTAO YUAN, TAIWAN, R.O.C.', 'TAI YEH CHEMICAL INDUSTRY CO., LTD.\n68-1, 9 LING, BAO TANQ TSUN, GUAN IN SHIANG,\r\nTAO YUAN, TAIWAN, R.O.C.', 'TAI YEH CHEMICAL INDUSTRY CO., LTD.\n68-1, 9 LING, BAO TANQ TSUN, GUAN IN SHIANG,\r\nTAO YUAN, TAIWAN, R.O.C.', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"200\",\"\"]', '67.92', 'usd', 'single', '2017-02-02 23:15:58', '2017-02-02 23:15:58'),
(49, 0, 3, '344', '2016-12-21', 43, '5-1, 1-CHOME, AZUCHIMACHI, CHUOKU, OSAKA,\r\nJAPAN\r\nTEL: +81 06 6262 2701', 'SHOEI YAKUHIN CO.,LTD.\n5-1, 1-CHOME, AZUCHIMACHI, CHUOKU, OSAKA,\r\nJAPAN\r\nTEL: +81 06 6262 2701', 'SHOEI YAKUHIN CO.,LTD.\n5-1, 1-CHOME, AZUCHIMACHI, CHUOKU, OSAKA,\r\nJAPAN\r\nTEL: +81 06 6262 2701', 'GI-122116', '0', '0000-00-00 00:00:00', 2, '', '2', 'OSAKA', '', '', '112', 'OSAKA', '', '', 'CIF', '100% TT PAYMENT', '[\"160\",\"\"]', '67.87', 'usd', 'single', '2017-02-02 23:28:34', '2017-02-02 23:28:34'),
(50, 0, 3, '327', '2016-12-13', 44, '4th FLOOR, NO.72, NINGPO WEST STREET,\r\nTAIPEI 10075, TAIWAN', 'FWUSOW INDUSTRY CO., LTD\n4th FLOOR, NO.72, NINGPO WEST STREET,\r\nTAIPEI 10075, TAIWAN', 'FWUSOW INDUSTRY CO., LTD\n4th FLOOR, NO.72, NINGPO WEST STREET,\r\nTAIPEI 10075, TAIWAN', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"75\",\"\"]', '67.45', 'usd', 'single', '2017-02-02 23:32:07', '2017-02-02 23:32:07'),
(51, 0, 3, '408', '2017-01-27', 45, 'VIA DELLA LIRICA 11\r\n48124 RAVENNA RA', 'EVERKEM SRL \nVIA DELLA LIRICA 11\r\n48124 RAVENNA RA', 'EVERKEM SRL \nVIA DELLA LIRICA 11\r\n48124 RAVENNA RA', '75', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', '100% TT PAYMENT', '[\"1070\",\"\"]', '68.19', 'usd', 'single', '2017-02-03 02:03:34', '2017-02-03 02:03:34'),
(52, 0, 3, '410', '2017-01-28', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '32', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CASH AGAINST DOCUMENT', '[\"1620\",\"\"]', '68.10', 'usd', 'single', '2017-02-03 02:06:29', '2017-02-03 02:06:29'),
(53, 0, 3, '418', '2017-01-31', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '36A+B+C+D', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', '', '[\"\",\"\"]', '', 'usd', 'single', '2017-02-03 02:08:24', '2017-02-03 02:08:24'),
(54, 0, 3, '415', '2017-02-02', 46, 'ADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\nTel. : +98 41 35544872  \r\n Fax : +98 41 35538475', 'TADBIR GOSTAR ZAMAN\nADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\nTel. : +98 41 35544872  \r\n Fax : +98 41 35538475', 'TADBIR GOSTAR ZAMAN\nADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\nTel. : +98 41 35544872  \r\n Fax : +98 41 35538475', '', '0', '0000-00-00 00:00:00', 5, '', '1', 'BANDARABBAS', '', '', '104', 'BANDAR ABBAS', '', '', 'CFR', '100% ADVANCE PAYMENT', '[\"180\",\"\"]', '67.85', 'usd', 'single', '2017-02-03 02:15:46', '2017-02-03 02:15:46'),
(55, 0, 3, '425', '2017-02-02', 49, 'P.O. BOX 96.\r\n5750 AB DEURNE\r\nTHE NETHERLANDS', 'CASTOR INTERNATIONAL BV\nP.O. BOX 96.\r\n5750 AB DEURNE\r\nTHE NETHERLANDS', 'CASTOR INTERNATIONAL BV\nP.O. BOX 96.\r\n5750 AB DEURNE\r\nTHE NETHERLANDS', '50004545', '0', '0000-00-00 00:00:00', 2, '', '1', 'ROTTERDAM', '', '', '157', 'ROTTERDAM', '', '', 'CFR', 'CAD', '[\"1270\",\"\"]', '67.37', 'usd', 'single', '2017-02-04 16:54:13', '2017-02-04 16:54:13'),
(56, 0, 3, '423', '2017-02-03', 48, '9901 I-1O West Suite 800,\r\nSan Antonio TX 78230 USA\r\nPhone 210 558 2886', 'CHARLOTTE CHEMICAL INTERNATIONAL, S.A. de C.V.\r\nHomero No. 432â€“PH, Col. Polanco\r\n11560 Mexico, Mexico\r\nTAX ID: CCI960516RQ2 Ph: +52 55 52036226', 'LESCHACO MEXICANA, S.A. de C.V.\r\nBLVD. Manuel Avila Camacho No.88, Lomas De Chapultepec,\r\n11000 Mexico, D.F., Mexico TAX ID : LME980116R15\r\nTel. : +52 55 5955 0000 Fax : +52 55 5955 3246', '4657', '0', '0000-00-00 00:00:00', 2, '', '2', 'VERACRUZ', '', '', '144', 'VERACRUZ', '', '', 'CIF', 'CAD THROUGH BANK', '[\"1235\",\"\"]', '67.37', 'usd', 'single', '2017-02-04 22:47:56', '2017-02-04 22:47:56'),
(57, 0, 3, '424', '2017-02-03', 48, '9901 I-1O West Suite 800,\r\nSan Antonio TX 78230 USA\r\nPhone 210 558 2886', 'CHARLOTTE CHEMICAL INTERNATIONAL, S.A. de C.V.\r\nHomero No. 432â€“PH, Col. Polanco\r\n11560 Mexico, Mexico\r\nTAX ID: CCI960516RQ2 Ph: +52 55 52036226', 'LESCHACO MEXICANA, S.A. de C.V.\r\nBLVD. Manuel Avila Camacho No.88, Lomas De Chapultepec,\r\n11000 Mexico, D.F., Mexico \r\nTAX ID : LME980116R15\r\nTel. : +52 55 5955 0000 \r\nFax : +52 55 5955 3246', '4658', '0', '0000-00-00 00:00:00', 2, '', '2', 'ALTAMIRA', '', '', '144', 'ALTAMIRA', '', '', 'CIF', 'CAD THROUGH BANK', '[\"1235\",\"\"]', '67.37', 'usd', 'single', '2017-02-04 22:51:03', '2017-02-04 22:51:03'),
(58, 0, 3, '426', '2017-02-03', 50, '69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', 'CHEMICAL-LAND TRADING CO. LTD\n69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', 'CHEMICAL-LAND TRADING CO. LTD\n69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"132\",\"\"]', '674', 'usd', 'single', '2017-02-04 22:54:20', '2017-02-04 22:54:20'),
(59, 0, 3, '428', '2017-02-06', 51, '301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'INCHEON', '', '', '3', 'INCHEON', '', '', 'CIF', 'LC AT SIGHT', '[\"175\",\"\"]', '67.2', 'usd', 'single', '2017-02-06 18:42:51', '2017-02-06 18:42:51'),
(60, 0, 3, '429', '2017-02-06', 52, 'No.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"170\",\"\"]', '67.18', 'usd', 'single', '2017-02-07 17:16:16', '2017-02-07 17:16:16'),
(61, 0, 3, '430', '2017-02-07', 53, 'FYC BLDG,2-1-5,MINATO CHUO-KU,FUKUOKA\r\nJAPAN, POSTCODE :810-0075', 'FYC CO LTD\nFYC BLDG,2-1-5,MINATO CHUO-KU,FUKUOKA\r\nJAPAN, POSTCODE :810-0075', 'FYC CO LTD\nFYC BLDG,2-1-5,MINATO CHUO-KU,FUKUOKA\r\nJAPAN, POSTCODE :810-0075', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'SHIBUSHI', '', '', '112', 'SHIBUSHI', '', '', 'CFR', '100% TT PAYMENT', '[\"470\",\"\"]', '67.33', 'usd', 'single', '2017-02-07 17:21:11', '2017-02-07 17:21:11'),
(62, 0, 3, '432', '2017-02-07', 54, 'TD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\nTEL: +603-2289 0632 FAX: +603-2289 0636', 'CAHAYA LAGENDA RESOURCES SDN. BHD.\nTD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\nTEL: +603-2289 0632 FAX: +603-2289 0636', 'CAHAYA LAGENDA RESOURCES SDN. BHD.\nTD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\nTEL: +603-2289 0632 FAX: +603-2289 0636', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'PORT KELANG', '', '', '135', 'PORT KELANG', '', '', 'CNF', '100% TT PAYMENT', '[\"152\",\"\"]', '67.42', 'usd', 'single', '2017-02-10 01:02:22', '2017-02-10 01:02:22'),
(63, 0, 3, '433', '2017-02-07', 55, '1785-6, CHEONGWON RO, WONGOK MYEON,\r\nANSEONG SI, GYEONGGI DO, SEOUL,\r\nSOUTH KOREA', 'DONG YANG OIL CHEMICAL CO. LTD.\n1785-6, CHEONGWON RO, WONGOK MYEON,\r\nANSEONG SI, GYEONGGI DO, SEOUL,\r\nSOUTH KOREA', 'DONG YANG OIL CHEMICAL CO. LTD.\n1785-6, CHEONGWON RO, WONGOK MYEON,\r\nANSEONG SI, GYEONGGI DO, SEOUL,\r\nSOUTH KOREA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"155\",\"\"]', '67.42', 'usd', 'single', '2017-02-10 01:06:27', '2017-02-10 01:06:27'),
(64, 0, 3, '435', '2017-02-10', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', '100% TT PAYMENT', '[\"165\",\"\"]', '66.9', 'usd', 'single', '2017-02-10 16:55:52', '2017-02-10 16:55:52'),
(65, 0, 3, '427', '2017-02-04', 56, 'RUA GIL STEIN FERREIRA,357 , SALA 703,\r\n 88 301-210 CENTRO, ITAJAI-SC,\r\nCNPJ 60.874.724/0005-10', 'INDUSTRIA QUIMICA ANASTACIO S/A\nRUA GIL STEIN FERREIRA,357 , SALA 703,\r\n 88 301-210 CENTRO, ITAJAI-SC,\r\nCNPJ 60.874.724/0005-10', 'KOMPORT COMERCIAL IMPORTADORA S.A\r\nAV. MAURO RAMOS, 1450-7áµ’ . ANDAR-CENTRO\r\n88020-302-FLORIANOPOLIS-SC-BRASIL\r\nCNPJ: 07.409.820/0001-80\r\nPHONE : +55 482106-4821   FAX: +55 48 2106-4848', '12.400/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'ITAJAI / NAVEGANTS', '', '', '33', 'ITAJAI / NAVEGANTS', '', '', 'CIF', '100% TT PAYMENT', '[\"\",\"40\"]', '67.31', 'usd', 'single', '2017-02-11 16:32:08', '2017-02-11 16:32:08'),
(66, 0, 3, '434', '2017-02-07', 56, 'RUA GIL STEIN FERREIRA,357 , SALA 703,\r\n 88 301-210 CENTRO, ITAJAI-SC,\r\nCNPJ 60.874.724/0005-10', 'INDUSTRIA QUIMICA ANASTACIO S/A\nRUA GIL STEIN FERREIRA,357 , SALA 703,\r\n 88 301-210 CENTRO, ITAJAI-SC,\r\nCNPJ 60.874.724/0005-10', 'KOMPORT COMERCIAL IMPORTADORA S.A\r\nAV. MAURO RAMOS, 1450-7áµ’ . ANDAR-CENTRO\r\n88020-302-FLORIANOPOLIS-SC-BRASIL\r\nCNPJ: 07.409.820/0001-80\r\nPHONE : +55 482106-4821   FAX: +55 48 2106-4848', '12.400/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'ITAJAI / NAVEGANTS', '', '', '33', 'ITAJAI / NAVEGANTS', '', '', 'CIF', '100% TT PAYMENT', '[\"\",\"40\"]', '67.40', 'usd', 'single', '2017-02-11 16:35:21', '2017-02-11 16:35:21'),
(67, 0, 3, '436', '2017-02-10', 57, 'NO. 26, JALAN PUTERI 5/16, BANDAR PUTERI,\r\n47100 PUCHONG, SELANGOR DARUL EHSAN, MALAYSIA\r\nTEL: +603-80624478 FAX: +603-80616876', 'STRONG CARGO GLOBAL SDN BHD.\r\nB-05-06, 3 TWO SQUARE\r\nNO.2, JALAN 19/1, 46300 PETALING JAYA,\r\nT(\': 603-7954 8096 F#x: +603-7954 8067', 'MFRP ENGINEERING SDN BHD\nNO. 26, JALAN PUTERI 5/16, BANDAR PUTERI,\r\n47100 PUCHONG, SELANGOR DARUL EHSAN, MALAYSIA\r\nTEL: +603-80624478 FAX: +603-80616876', '2017-7446', '0', '0000-00-00 00:00:00', 2, '', '1', 'PORT KELANG', '', '', '135', 'PORT KELANG', '', '', 'CIF', '50% ADVANCE + 50% AGAINST SCAN BL', '[\"795\",\"\"]', '66.95', 'usd', 'single', '2017-02-14 22:31:12', '2017-02-14 22:31:12'),
(68, 0, 3, '437', '2017-02-10', 58, '3.BLDG ., 7A DOROZHNAYA ST. ,\r\nMOSCOW, RUSSIA 117545\r\nTEL : +7 (963) 682-99-44', 'OOO \"TD HALMEK\"\n3.BLDG ., 7A DOROZHNAYA ST. ,\r\nMOSCOW, RUSSIA 117545\r\nTEL : +7 (963) 682-99-44', 'OOO \"TD HALMEK\"\n3.BLDG ., 7A DOROZHNAYA ST. ,\r\nMOSCOW, RUSSIA 117545\r\nTEL : +7 (963) 682-99-44', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1470\",\"\"]', '67.00', 'usd', 'single', '2017-02-14 22:40:20', '2017-02-14 22:40:20'),
(69, 0, 3, '438', '2017-02-14', 59, 'RM51, 5TH FLOOR, BRITANNIA HOUSE, JALAN CATOR,\r\nBANDAR SERI BEGAWAN BS 8811,NEGARA BRUNEI DARUSSALAM\r\nTEL:+886-6-2531668 FAX: +886-6-2421613', 'BRILLIANT INDUSTRY COMPANY LIMITED\r\nROAD NO.1 DONG AN INDUSTRIAL PARK,BINH HOA VILLAGE,\r\nTHUAN AN DISTRICT, BINH DUONG PROVINCE, VIETNAM\r\nTEL: +84 650-378-2591<3 FAX: +84 650-378-2594', 'BRILLIANT INDUSTRY COMPANY LIMITED\r\nROAD NO.1 DONG AN INDUSTRIAL PARK,BINH HOA VILLAGE,\r\nTHUAN AN DISTRICT, BINH DUONG PROVINCE, VIETNAM\r\nTEL: +84 650-378-2591<3 FAX: +84 650-378-2594', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'HCMC(CAT LAI)', '', '', '238', 'HCMC(CAT LAI)', '', '', 'CIF', '100% TT PAYMENT', '[\"170\",\"\"]', '66.95', 'usd', 'single', '2017-02-14 22:44:53', '2017-02-14 22:44:53'),
(70, 0, 3, '439', '2017-02-14', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'KAOHSIUNG', 'TAICHUNG', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"170\",\"\"]', '66.95', 'usd', 'single', '2017-02-14 22:49:57', '2017-02-14 22:49:57'),
(71, 0, 3, '442', '2017-02-20', 60, 'BSCIS I/E, Sagarika Road\r\nBlock-A, P.O. Customs Academy, Chittagong-4219\r\nTel: +880-31-750060/750446 \r\nFAX: +880-31-750061', 'LUBRICANTS ASIA LTD.\nBSCIS I/E, Sagarika Road\r\nBlock-A, P.O. Customs Academy, Chittagong-4219\r\nTel: +880-31-750060/750446 \r\nFAX: +880-31-750061', 'LUBRICANTS ASIA LTD.\nBSCIS I/E, Sagarika Road\r\nBlock-A, P.O. Customs Academy, Chittagong-4219\r\nTel: +880-31-750060/750446 \r\nFAX: +880-31-750061', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'CHITTAGONG', '', '', '21', 'CHITTAGONG', '', '', 'CFR', 'LC AT SIGHT', '[\"700\",\"\"]', '67.00', 'usd', 'single', '2017-02-21 17:53:36', '2017-02-21 17:53:36'),
(72, 0, 3, '443', '2017-02-20', 61, 'NO.7-1, LN. 520, DAREN RD., LUZHU DIST.,\r\nKAOHSIUNG CITY 821, TAIWAN (R.O.C.)', 'GO-FENG INDUSTRY CO., LTD\nNO.7-1, LN. 520, DAREN RD., LUZHU DIST.,\r\nKAOHSIUNG CITY 821, TAIWAN (R.O.C.)', 'GO-FENG INDUSTRY CO., LTD\nNO.7-1, LN. 520, DAREN RD., LUZHU DIST.,\r\nKAOHSIUNG CITY 821, TAIWAN (R.O.C.)', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"155\",\"\"]', '67.07', 'usd', 'single', '2017-02-21 17:55:44', '2017-02-21 17:55:44'),
(73, 0, 3, '444', '2017-02-21', 50, '69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', 'CHEMICAL-LAND TRADING CO. LTD\n69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', 'CHEMICAL-LAND TRADING CO. LTD\n69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"150\",\"\"]', '67.00', 'usd', 'single', '2017-02-21 23:04:30', '2017-02-21 23:04:30'),
(74, 0, 3, '445', '2017-02-21', 50, '69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', 'CHEMICAL-LAND TRADING CO. LTD\n69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', 'CHEMICAL-LAND TRADING CO. LTD\n69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"150\",\"\"]', '67.00', 'usd', 'single', '2017-02-21 23:05:39', '2017-02-21 23:05:39'),
(75, 0, 3, '446', '2017-02-22', 62, '12th Fl., Orakarn Bldg., 26/42 Soi Chidlom,Ploenchit Rd.,\r\nLumpini, Patumvan, Bangkok 10330, Thailand\r\nTel : +66 2254 1490-7', 'THAI CASTOR OIL INDUSTRIES CO., LTD\r\n12th Fl., Orakarn Bldg., 26/42 Soi Chidlom,Ploenchit Rd.,\r\nLumpini, Patumvan, Bangkok 10330, Thailand\r\nTel : +66 2254 1490-7', 'THAI CASTOR OIL INDUSTRIES CO., LTD\r\n12th Fl., Orakarn Bldg., 26/42 Soi Chidlom,Ploenchit Rd.,\r\nLumpini, Patumvan, Bangkok 10330, Thailand\r\nTel : +66 2254 1490-7', 'TCOPO-14/17', '0', '0000-00-00 00:00:00', 2, '', '2', 'BANGKOK', '', '', '217', 'BANGKOK', '', '', 'CFR', 'LC AT SIGHT', '[\"200\",\"\"]', '67.07', 'usd', 'single', '2017-02-22 23:15:54', '2017-02-22 23:23:07'),
(76, 0, 3, '447', '2017-02-22', 51, '301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'INCHEON', '', '', '3', 'INCHEON', '', '', 'CIF', 'LC AT SIGHT', '[\"215\",\"\"]', '66.95', 'usd', 'single', '2017-02-22 23:19:03', '2017-02-22 23:19:03'),
(77, 0, 3, '448', '2017-02-22', 63, '5F, No.3, Alley 14, Lane 151, Yuan Shan Rd.,\r\nChung Ho Dist., New Taipei City 235, Taiwan\r\nTel : +886-2-2226-5160\r\nFax: +886-2-2226-9687', 'HORN HAUR ENTERPRISE CO., LTD.\n5F, No.3, Alley 14, Lane 151, Yuan Shan Rd.,\r\nChung Ho Dist., New Taipei City 235, Taiwan\r\nTel : +886-2-2226-5160\r\nFax: +886-2-2226-9687', 'HORN HAUR ENTERPRISE CO., LTD.\n5F, No.3, Alley 14, Lane 151, Yuan Shan Rd.,\r\nChung Ho Dist., New Taipei City 235, Taiwan\r\nTel : +886-2-2226-5160\r\nFax: +886-2-2226-9687', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'KEELUNG', '', '', '2', 'KEELUNG', '', '', 'CIF', '100% ADVANCE PAYMENT', '[\"225\",\"\"]', '66.98', 'usd', 'single', '2017-02-22 23:22:48', '2017-02-22 23:22:48'),
(78, 0, 3, '431', '2017-02-07', 64, 'INDUSTRIAL AREA, BUILDING NO-52, BLOCK-4\r\nSTREET-7, SHUAIBAH, P.O.BOX 9748\r\nAHMADI 61008, KUWAIT', 'KUWAIT LUBE OIL COMPANY\nINDUSTRIAL AREA, BUILDING NO-52, BLOCK-4\r\nSTREET-7, SHUAIBAH, P.O.BOX 9748\r\nAHMADI 61008, KUWAIT', 'KUWAIT LUBE OIL COMPANY\nINDUSTRIAL AREA, BUILDING NO-52, BLOCK-4\r\nSTREET-7, SHUAIBAH, P.O.BOX 9748\r\nAHMADI 61008, KUWAIT', '', '0', '0000-00-00 00:00:00', 2, '', '', 'SHUWAIKH PORT', '', '', '120', 'SHUWAIKH', '', '', 'CIF', 'CAD THROUGH BANK', '[\"5\",\"\"]', '67.37', 'usd', 'single', '2017-02-24 17:10:15', '2017-02-24 17:10:15'),
(79, 0, 3, '440', '2017-02-14', 65, 'NO. 6 OIL INSTALLATION AREA, KHEMARI\r\nKARACHI - 75620 PAKISTAN', 'PAK GREASE MFG CO (PVT) LTD\nNO. 6 OIL INSTALLATION AREA, KHEMARI\r\nKARACHI - 75620 PAKISTAN', 'PAK GREASE MFG CO (PVT) LTD\nNO. 6 OIL INSTALLATION AREA, KHEMARI\r\nKARACHI - 75620 PAKISTAN', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'KARACHI', '', '', '169', 'KARACHI', '', '', 'CFR', 'LC AT SIGHT', '[\"150\",\"\"]', '66.97', 'usd', 'single', '2017-02-24 17:18:53', '2017-02-24 17:18:53'),
(80, 0, 3, '451', '2017-02-13', 24, '7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\r\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\r\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'HUANGPU', '', '', '47', 'HUANGPU', '', '', 'CIF', 'CAD THROUGH BANK', '[\"330\",\"\"]', '67.05', 'usd', 'single', '2017-02-24 17:28:50', '2017-02-24 18:06:01'),
(81, 0, 3, '452', '2017-02-23', 66, 'KEHATIE 1\r\n33880 LEMPAALA\r\nFINLAND', 'KIILTO OY\nKEHATIE 1\r\n33880 LEMPAALA\r\nFINLAND', 'KIILTO OY\nKEHATIE 1\r\n33880 LEMPAALA\r\nFINLAND', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'LEMPAALA', '', '', '74', 'LEMPAALA', '', '', 'DAP', 'CAD THROUGH BANK 60 DAYS', '[\"3600\",\"\"]', '70.7', 'euro', 'single', '2017-02-24 18:20:01', '2017-02-24 18:20:01'),
(82, 0, 3, '441', '2017-02-17', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '63', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', '100% TT PAYMENT', '[\"800\",\"\"]', '67', 'usd', 'single', '2017-02-24 18:45:31', '2017-02-24 18:45:31'),
(83, 0, 3, '449', '2017-02-22', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '67', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', '100% TT PAYMENT', '[\"1800\",\"\"]', '67', 'usd', 'single', '2017-02-24 18:48:48', '2017-02-24 18:48:48'),
(84, 0, 3, '450', '2017-02-22', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '68', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', '100% TT PAYMENT', '[\"1950\",\"\"]', '67', 'usd', 'single', '2017-02-24 19:05:58', '2017-02-24 19:05:58'),
(85, 0, 3, '457', '2017-02-24', 47, 'ADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\nTel. : +98 41 35544872  \r\n Fax : +98 41 35538475', 'TADBIR GOSTAR ZAMAN\nADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\nTel. : +98 41 35544872  \r\n Fax : +98 41 35538475', 'TADBIR GOSTAR ZAMAN\nADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\nTel. : +98 41 35544872  \r\n Fax : +98 41 35538475', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'BANDARABBAS', '', '', '104', 'BANDAR ABBAS', '', '', 'CFR', '100% TT PAYMENT', '[\"200\",\"\"]', '66.81', 'usd', 'single', '2017-02-24 19:10:02', '2017-02-24 19:10:02'),
(86, 0, 3, '453', '2017-02-23', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"130\",\"\"]', '66.95', 'usd', 'single', '2017-02-24 22:30:05', '2017-02-24 22:30:05'),
(87, 0, 3, '454', '2017-02-23', 67, '303 Kamaladi, Kathmandu Nepal\r\nCustomers VAT/PAN Number: 300020748\r\nTel: + 977 1 4240363', 'MB PETROLUBE INDUSTRIES PVT. LTD.\n303 Kamaladi, Kathmandu Nepal\r\nCustomers VAT/PAN Number: 300020748\r\nTel: + 977 1 4240363', 'MB PETROLUBE INDUSTRIES PVT. LTD.\n303 Kamaladi, Kathmandu Nepal\r\nCustomers VAT/PAN Number: 300020748\r\nTel: + 977 1 4240363', '', '0', '0000-00-00 00:00:00', 2, '', '3', 'EX -WORKS', '', '', '156', 'KATHMANDU', '', '', 'EX -WORKS', '100% TT PAYMENT', '[\"\",\"\"]', '', 'usd', 'single', '2017-02-24 22:34:45', '2017-02-24 22:34:45'),
(88, 0, 3, '455', '2017-02-24', 52, 'No.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"155\",\"\"]', '66.85', 'usd', 'single', '2017-02-24 22:37:45', '2017-02-24 22:37:45'),
(89, 0, 3, '456', '2017-02-24', 52, 'No.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"155\",\"\"]', '66.85', 'usd', 'single', '2017-02-24 22:40:55', '2017-02-24 22:40:55'),
(90, 0, 3, '458', '2017-02-24', 68, '1427 W. 86th Street Suite 365\r\nIndianapolis, IN 46260\r\nTel : +1 -317-852-1986', 'A.A. Pedro RamÃ³n PurÃ³n Acevedo\r\nCalle Ballena 56, Edificio 56-B, Col. Fondeport\r\nManzanillo, Colima CP 28219 TAX-ID DAC911011F57\r\nDalia Sana5ria< dsana5ria@grupo-castaneda.com>', 'Pochteca Materia Primas SA de CV\r\nManuel Reyes Veramendi No.6 Col San Migue Chapultepec, CP. 1 1850\r\nDel. Miguel HidalgoM ;<ico DFR FC: PMP950301S62\r\nVeronica Jaimes<vjaimes@pochteca.com.m<>', 'PCT757', '0', '0000-00-00 00:00:00', 2, '', '1', 'MANZANILLO', '', '', '231', 'MANZANILLO', '', '', 'CIF', '100% ADVANCE PAYMENT', '[\"1800\",\"\"]', '67.07', 'usd', 'single', '2017-02-24 22:46:29', '2017-02-24 22:46:29'),
(91, 0, 3, '459', '2017-02-24', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"150\",\"\"]', '66.7', 'usd', 'single', '2017-02-24 22:49:33', '2017-02-24 22:49:33'),
(92, 0, 3, '461', '2017-02-27', 31, '115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'FOB ', '100% TT PAYMENT', '[\"0\",\"\"]', '66.75', 'usd', 'single', '2017-02-28 22:54:32', '2017-02-28 22:54:32'),
(93, 0, 3, '462', '2017-02-28', 69, 'NO.29, LANE 180, CHIEN CHENG RD.,\r\nMIN SHENG TSUN, FANG YUAN HSIANG, \r\nCHANGHUA HSIEN, TAIWAN', 'CHURWAN YU CO., LTD.\r\nNO.29, LANE 180, CHIEN CHENG RD.,\r\nMIN SHENG TSUN, FANG YUAN HSIANG, \r\nCHANGHUA HSIEN, TAIWAN', 'CHURWAN YU CO., LTD.\r\nNO.29, LANE 180, CHIEN CHENG RD.,\r\nMIN SHENG TSUN, FANG YUAN HSIANG, \r\nCHANGHUA HSIEN, TAIWAN', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"155\",\"\"]', '66.85', 'usd', 'single', '2017-03-01 23:23:18', '2017-03-01 23:24:59'),
(94, 0, 3, '463', '2017-03-01', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"155\",\"\"]', '66.85', 'usd', 'single', '2017-03-01 23:24:40', '2017-03-01 23:24:40'),
(95, 0, 3, '466', '2017-03-02', 70, 'Av. Presidente Eduardo Frei Montalva 9431\r\nQuilicura, Santiago, Chile - 8710004\r\nVAT NÂ°: 84.083.400-K\r\nContact person: Jose Miralles', 'MADESA S.A.\nAv. Presidente Eduardo Frei Montalva 9431\r\nQuilicura, Santiago, Chile - 8710004\r\nVAT NÂ°: 84.083.400-K\r\nContact person: Jose Miralles', 'MADESA S.A.\nAv. Presidente Eduardo Frei Montalva 9431\r\nQuilicura, Santiago, Chile - 8710004\r\nVAT NÂ°: 84.083.400-K\r\nContact person: Jose Miralles', '020016', '0', '0000-00-00 00:00:00', 2, '', '2', 'SAN-ANTONIO', '', '', '46', 'SAN-ANTONIO', '', '', 'CFR', '100% TT PAYMENT', '[\"1450\",\"\"]', '66.84', 'usd', 'single', '2017-03-03 16:28:44', '2017-03-03 16:28:44'),
(96, 0, 3, '468', '2017-03-02', 71, '1 Guy Gibson Avenue\r\nAeroton 2013, Gauteng, South Africa\r\nTel : (+27) 11 248 0000 Fax: (+27) 494 1115', 'EPIC FOODS (PTY) LTD\n1 Guy Gibson Avenue\r\nAeroton 2013, Gauteng, South Africa\r\nTel : (+27) 11 248 0000 Fax: (+27) 494 1115', 'EPIC FOODS (PTY) LTD\n1 Guy Gibson Avenue\r\nAeroton 2013, Gauteng, South Africa\r\nTel : (+27) 11 248 0000 Fax: (+27) 494 1115', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'DURBAN', '', '', '202', 'DURBAN', '', '', 'CIF', 'LC AT SIGHT', '[\"690\",\"\"]', '66.8', 'usd', 'single', '2017-03-03 16:34:23', '2017-03-03 16:34:23');
INSERT INTO `contracts` (`Id`, `company_id`, `user_id`, `contract_no`, `contract_date`, `importer_id`, `importer_address`, `notifier_party`, `consignee_party`, `purchase_order_no`, `invoice_no`, `invoice_date`, `surveyor_id`, `surveyor_name`, `port_loading`, `port_discharge`, `port_discharge2`, `port_discharge3`, `final_destination_country`, `final_destination`, `final_destination2`, `final_destination3`, `delivery_terms`, `payment_terms`, `quotation_freight`, `dollor_exchange_rate`, `currency`, `contract_type`, `created_at`, `updated_at`) VALUES
(97, 0, 3, '469', '2017-03-02', 72, 'Noviy Arbat str., h. 21 office 1806\r\nMoscow 119019, Russia\r\nTel :+7-495-789-8399, 739-5457', 'ZAO \"Ruskhimset\"\r\nNoviy Arbat str., h. 21 office 1806\r\nMoscow 119019, Russia\r\nTel :+7-495-789-8399, 739-5457', 'ZAO \"Ruskhimset\"\r\nNoviy Arbat str., h. 21 office 1806\r\nMoscow 119019, Russia\r\nTel :+7-495-789-8399, 739-5457', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1560\",\"\"]', '66.8', 'usd', 'single', '2017-03-03 22:43:10', '2017-03-03 22:43:51'),
(98, 0, 3, '465', '2017-03-01', 15, 'AVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', 'INTERFAT, S.A.\nAVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', 'INTERFAT, S.A.\nAVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', '12960', '0', '0000-00-00 00:00:00', 2, '', '1', 'BARCELONA', '', '', '204', 'SPAIN', '', '', 'FOB ', 'CAD THROUGH BANK', '[\"100\",\"\"]', '66.90', 'usd', 'single', '2017-03-04 16:22:12', '2017-03-04 16:22:12'),
(99, 0, 3, '464', '2017-03-01', 24, '7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING, 100045, CHINA', 'SINOCHEM PLASTICS CO., LTD.\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING, 100045, CHINA', 'SINOCHEM PLASTICS CO., LTD.\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING, 100045, CHINA', '17SL4MLSY025', '0', '0000-00-00 00:00:00', 2, '', '2', 'SHANGHAI', '', '', '47', 'SHANGHAI', '', '', 'CIF', 'CAD THROUGH BANK', '[\"200\",\"\"]', '66.87', 'usd', 'single', '2017-03-04 16:39:06', '2017-03-04 16:39:06'),
(100, 0, 3, '467', '2017-03-02', 24, '7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', '17SL4MLSY036', '0', '0000-00-00 00:00:00', 2, '', '2', 'SHANGHAI', '', '', '47', 'SHANGHAI', '', '', 'CIF', 'CAD THROUGH BANK', '[\"190\",\"\"]', '66.76', 'usd', 'single', '2017-03-04 16:44:52', '2017-03-04 16:44:52'),
(101, 0, 3, '478', '2017-03-10', 73, 'GRAHA STR 2nd FLOOR, JI. AMPERA RAYA NO.11\r\nRAGUNAN - PASAR MINGGU, JAKARTA SELATION 12550', 'PT. BALMER LAWRIE INDONESIA\nGRAHA STR 2nd FLOOR, JI. AMPERA RAYA NO.11\r\nRAGUNAN - PASAR MINGGU, JAKARTA SELATION 12550', 'PT. BALMER LAWRIE INDONESIA\nGRAHA STR 2nd FLOOR, JI. AMPERA RAYA NO.11\r\nRAGUNAN - PASAR MINGGU, JAKARTA SELATION 12550', '315/BLI-POI/3/17', '0', '0000-00-00 00:00:00', 2, '', '2', 'JAKARTA', '', '', '1', 'JAKARTA', '', '', 'CIF', 'TT 45 DAYS FROM B/L DATE', '[\"330\",\"\"]', '66.72', 'usd', 'single', '2017-03-11 06:45:22', '2017-03-11 06:45:22'),
(102, 0, 3, '470', '2017-03-08', 51, '301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'INCHEON', '', '', '3', 'INCHEON', '', '', 'CIF', 'LC AT SIGHT', '[\"220\",\"\"]', '66.65', 'usd', 'single', '2017-03-16 23:49:02', '2017-03-16 23:49:02'),
(103, 0, 3, '471', '2017-03-10', 25, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', '2106', '0', '0000-00-00 00:00:00', 2, '', '1', 'VERACRUZ', '', '', '144', 'VERACRUZ', '', '', 'CIF', 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '[\"1725\",\"\"]', '66.8', 'usd', 'single', '2017-03-16 23:52:47', '2017-03-16 23:52:47'),
(104, 0, 3, '472', '2017-03-10', 74, '10F., NO.50, SEC. 1, XINSHENG S. RD.,\r\nTAIPEI, TAIWAN(R.O.C.)\r\nTEL : 886-2-23937111', 'PAN ASIA CHEMICAL CORPORATION\n10F., NO.50, SEC. 1, XINSHENG S. RD.,\r\nTAIPEI, TAIWAN(R.O.C.)\r\nTEL : 886-2-23937111', 'PAN ASIA CHEMICAL CORPORATION\n10F., NO.50, SEC. 1, XINSHENG S. RD.,\r\nTAIPEI, TAIWAN(R.O.C.)\r\nTEL : 886-2-23937111', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CIF', 'LC AT SIGHT', '[\"175\",\"\"]', '66.8', 'usd', 'single', '2017-03-16 23:58:09', '2017-03-16 23:58:09'),
(105, 0, 3, '473', '2017-03-10', 75, '24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', 'THE PEKAY GROUP (PTY) LTD.\n24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', 'THE PEKAY GROUP (PTY) LTD.\n24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'JOHANNESBURG', '', '', '202', 'JOHANNESBURG', '', '', 'CFR', 'CAD THROUGH BANK', '[\"1300\",\"\"]', '66.75', 'usd', 'single', '2017-03-17 00:04:50', '2017-03-17 00:04:50'),
(106, 0, 3, '474', '2017-03-10', 75, '24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', 'THE PEKAY GROUP (PTY) LTD.\n24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', 'THE PEKAY GROUP (PTY) LTD.\n24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'DURBAN', '', '', '202', 'DURBAN', '', '', 'CFR', 'CAD THROUGH BANK', '[\"850\",\"\"]', '66.75', 'usd', 'single', '2017-03-17 00:06:54', '2017-03-17 00:06:54'),
(107, 0, 3, '475', '2017-03-10', 75, '24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', 'THE PEKAY GROUP (PTY) LTD.\n24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', 'THE PEKAY GROUP (PTY) LTD.\n24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'CAPETOWN', '', '', '202', 'CAPETOWN', '', '', 'CFR', 'CAD THROUGH BANK', '[\"850\",\"\"]', '66.75', 'usd', 'single', '2017-03-17 00:08:28', '2017-03-17 00:08:28'),
(108, 0, 3, '476', '2017-03-10', 38, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1700\",\"\"]', '66.73', 'usd', 'single', '2017-03-17 00:10:09', '2017-03-17 00:10:09'),
(109, 0, 3, '477', '2017-03-10', 40, '28-19, Bunseong-ro 5412eon-gi3, Gi0hae-si,\r\nGyeongsangna0-do, 50820, Korea\r\nTe3 : +82 55 322 9986 Fax : +82 55 322 9989', 'DONGEUN CO., LTD\n28-19, Bunseong-ro 5412eon-gi3, Gi0hae-si,\r\nGyeongsangna0-do, 50820, Korea\r\nTe3 : +82 55 322 9986 Fax : +82 55 322 9989', 'DONGEUN CO., LTD\n28-19, Bunseong-ro 5412eon-gi3, Gi0hae-si,\r\nGyeongsangna0-do, 50820, Korea\r\nTe3 : +82 55 322 9986 Fax : +82 55 322 9989', 'DE170310', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"\",\"300\"]', '66.8', 'usd', 'single', '2017-03-17 00:13:14', '2017-03-17 00:13:14'),
(110, 0, 3, '479', '2017-03-11', 31, '115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1650\",\"\"]', '66.7', 'usd', 'single', '2017-03-17 00:14:55', '2017-03-17 00:14:55'),
(111, 0, 3, '482', '2017-03-14', 76, 'Office 610, Wooree Venture Town II, 82-29, 3-Ga Mullae-dong,\r\nYeongdeungpo-gu, Seoul, Republic of Korea (#150-836)\r\nTEL : +82-2-2068-9312, \r\nFAX : +82-2-2068-8761,', 'HUBE GLOBAL CO., LTD\nOffice 610, Wooree Venture Town II, 82-29, 3-Ga Mullae-dong,\r\nYeongdeungpo-gu, Seoul, Republic of Korea (#150-836)\r\nTEL : +82-2-2068-9312, \r\nFAX : +82-2-2068-8761,', 'HUBE GLOBAL CO., LTD\nOffice 610, Wooree Venture Town II, 82-29, 3-Ga Mullae-dong,\r\nYeongdeungpo-gu, Seoul, Republic of Korea (#150-836)\r\nTEL : +82-2-2068-9312, \r\nFAX : +82-2-2068-8761,', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'INCHEON', '', '', '3', 'INCHEON', '', '', 'CIF', 'LC AT SIGHT', '[\"180\",\"\"]', '66.5', 'usd', 'single', '2017-03-17 00:18:20', '2017-03-17 00:18:20'),
(112, 0, 3, '483', '2017-03-14', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'KEELUNG', '', '', '2', 'KEELUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"180\",\"\"]', '66.15', 'usd', 'single', '2017-03-17 00:22:22', '2017-03-17 00:22:22'),
(113, 0, 3, '484', '2017-03-14', 38, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"2625\",\"\"]', '66.15', 'usd', 'single', '2017-03-17 00:25:31', '2017-03-17 00:25:31'),
(114, 0, 3, '485', '2017-03-14', 77, '3 SHENTON WAY, #09-08 SHENTON HOUSE\r\nSINGAPORE 068805\r\nTEL : (65) 62240036 FAX : (65) 62230611', 'TRADECOM SERVICES PVT LTD\r\n3 SHENTON WAY, #09-08 SHENTON HOUSE\r\nSINGAPORE 068805\r\nTEL : (65) 62240036 FAX : (65) 62230611', 'TRADECOM SERVICES PVT LTD\r\n3 SHENTON WAY, #09-08 SHENTON HOUSE\r\nSINGAPORE 068805\r\nTEL : (65) 62240036 FAX : (65) 62230611', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'MANILA', '', '', '176', 'MANILA', '', '', 'CIF', 'TT 30DAYS FROM BL DATE', '[\"\",\"350\"]', '65.53', 'usd', 'single', '2017-03-17 00:28:52', '2017-03-17 00:34:21'),
(115, 0, 3, '486', '2017-03-15', 78, 'Kowon Building 20-19\r\nYangjae-Dong,Seocho-ku Seoul,Korea\r\nTel : +82-2-578-6181 Fax : +82-2-578-6692', 'KOWON EVERCHEM CORPORATION\nKowon Building 20-19\r\nYangjae-Dong,Seocho-ku Seoul,Korea\r\nTel : +82-2-578-6181 Fax : +82-2-578-6692', 'KOWON EVERCHEM CORPORATION\nKowon Building 20-19\r\nYangjae-Dong,Seocho-ku Seoul,Korea\r\nTel : +82-2-578-6181 Fax : +82-2-578-6692', 'KGI-1701', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"210\",\"\"]', '65.63', 'usd', 'single', '2017-03-17 00:33:47', '2017-03-17 00:33:47'),
(116, 0, 3, '487', '2017-03-15', 52, 'No.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"145\",\"\"]', '65.55', 'usd', 'single', '2017-03-17 00:37:04', '2017-03-17 00:37:04'),
(117, 0, 3, '490', '2017-03-16', 54, 'TD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\nTEL: +603-2289 0632 FAX: +603-2289 0636', 'CAHAYA LAGENDA RESOURCES SDN. BHD.\nTD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\nTEL: +603-2289 0632 FAX: +603-2289 0636', 'CAHAYA LAGENDA RESOURCES SDN. BHD.\nTD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\nTEL: +603-2289 0632 FAX: +603-2289 0636', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'PORT KELANG', '', '', '135', 'PORT KELANG', '', '', 'CNF', '100% TT PAYMENT', '[\"150\",\"\"]', '65.35', 'usd', 'single', '2017-03-17 00:38:34', '2017-03-17 00:38:34'),
(118, 0, 3, '488', '2017-03-15', 15, 'AVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', 'ADUANAS LLOBET S.L\r\nAVDA. DRASSANES, 6-8,PLANTA 1, OFIC. 2 Y 3\r\n08001 BARCELONA\r\nTEL : 93.3426017/18 (ATT. SR ALEX SERRANO)', 'INTERFAT, S.A.\nAVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', '127.190', '0', '0000-00-00 00:00:00', 2, '', '', 'BARCELONA', '', '', '204', 'SPAIN', '', '', 'CIF', 'TT 45 DAYS FROM B/L DATE', '[\"100\",\"\"]', '65.6', 'usd', 'single', '2017-03-18 00:03:27', '2017-03-18 00:03:27'),
(119, 0, 3, '492', '2017-03-17', 79, '3-a, Stroitelnaya str., Berdyansk\r\nUkraine, 71100\r\nTel.: +38 06153 60701', 'LLCâ€œRU SIE AGRINOLâ€\n3-a, Stroitelnaya str., Berdyansk\r\nUkraine, 71100\r\nTel.: +38 06153 60701', 'LLCâ€œRU SIE AGRINOLâ€\n3-a, Stroitelnaya str., Berdyansk\r\nUkraine, 71100\r\nTel.: +38 06153 60701', '160816~4', '0', '0000-00-00 00:00:00', 3, '', '1', 'ODESSA', '', '', '228', 'ODESSA', '', '', 'CIF', '100% TT PAYMENT', '[\"1685\",\"\"]', '65.45', 'usd', 'single', '2017-03-18 01:27:19', '2017-03-18 01:27:19'),
(120, 0, 3, '480', '2017-03-11', 80, '14 SELIM HASSAN STREET - EL SHATBY\r\nALEXANDRIA - EGYPT', 'EL KAYAR IMPORT, EXPORT & COMMERCIAL AGENCIES CORP\n14 SELIM HASSAN STREET - EL SHATBY\r\nALEXANDRIA - EGYPT', 'EL KAYAR IMPORT, EXPORT & COMMERCIAL AGENCIES CORP\n14 SELIM HASSAN STREET - EL SHATBY\r\nALEXANDRIA - EGYPT', 'CO/1524', '0', '0000-00-00 00:00:00', 2, '', '2', 'ALEXANDRIA OLD PORT', '', '', '65', 'ALEXANDRIA OLD PORT', '', '', 'CIF', 'CAD THROUGH BANK', '[\"1300\",\"\"]', '66.75', 'usd', 'single', '2017-03-19 16:30:57', '2017-03-19 16:30:57'),
(121, 0, 3, '481', '2017-03-13', 81, 'P.O. BOX 424\r\n13005 SAFAT\r\nKUWAIT', 'AL SHARHAN INDUSTRIES\nP.O. BOX 424\r\n13005 SAFAT\r\nKUWAIT', 'AL SHARHAN INDUSTRIES\nP.O. BOX 424\r\n13005 SAFAT\r\nKUWAIT', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'SHUWAIKH', '', '', '120', 'SHUWAIKH', '', '', 'CIF', 'CAD THROUGH BANK', '[\"525\",\"\"]', '66.53', 'usd', 'single', '2017-03-19 16:37:57', '2017-03-19 16:37:57'),
(122, 0, 3, '489', '2017-03-15', 82, '1-B GARDEN BLOCK, GARDEN TOWN\r\nLAHORE, PAKISTAN', 'SUFI SOAP & CHEMICAL INDUSTRIES (PVT) LTD\n1-B GARDEN BLOCK, GARDEN TOWN\r\nLAHORE, PAKISTAN', 'SUFI SOAP & CHEMICAL INDUSTRIES (PVT) LTD\n1-B GARDEN BLOCK, GARDEN TOWN\r\nLAHORE, PAKISTAN', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'KARACHI', '', '', '169', 'KARACHI', '', '', 'C&F', 'LC AT SIGHT', '[\"95\",\"\"]', '65.67', 'usd', 'single', '2017-03-19 16:43:01', '2017-03-19 16:43:01'),
(123, 0, 3, '491', '2017-03-17', 83, 'HANDIAN INDUSTRIAL ZONE, ZOUPING\r\nSHANDONG 256200 CHINA', 'TIANXING BIOTECHNOLOGY CO., LTD\nHANDIAN INDUSTRIAL ZONE, ZOUPING\r\nSHANDONG 256200 CHINA', 'TIANXING BIOTECHNOLOGY CO., LTD\nHANDIAN INDUSTRIAL ZONE, ZOUPING\r\nSHANDONG 256200 CHINA', 'CO20170318', '0', '0000-00-00 00:00:00', 2, '', '2', 'SHANGHAI', '', '', '47', 'SHANGHAI', '', '', 'CNF', 'LC AT SIGHT', '[\"200\",\"\"]', '65.54', 'usd', 'single', '2017-03-19 16:49:24', '2017-03-19 16:49:24'),
(124, 0, 3, '494', '2017-03-18', 84, 'ALBERTI 101-(1722) MERLO - PCIA. DE BS. AS.\r\nREPUBLICA ARGENTINA', 'FLEXA ADHESIVOS Y RECUBRIMIENTOS S. R. L.\nALBERTI 101-(1722) MERLO - PCIA. DE BS. AS.\r\nREPUBLICA ARGENTINA', 'FLEXA ADHESIVOS Y RECUBRIMIENTOS S. R. L.\nALBERTI 101-(1722) MERLO - PCIA. DE BS. AS.\r\nREPUBLICA ARGENTINA', '00004186', '0', '0000-00-00 00:00:00', 2, '', '1', 'BUENOS AIRES', '', '', '13', 'BUENOS AIRES', '', '', 'FOB ', '30% ADVANCE & 70% AGAINST DOCS', '[\"\",\"\"]', '65.42', 'usd', 'single', '2017-03-19 16:53:16', '2017-03-19 16:53:16'),
(125, 0, 3, '493', '2017-03-18', 38, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"2600\",\"\"]', '65.45', 'usd', 'single', '2017-03-23 16:52:52', '2017-03-23 16:52:52'),
(126, 0, 3, '496', '2017-03-20', 51, '301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'INCHEON', '', '', '3', 'INCHEON', '', '', 'CIF', 'LC AT SIGHT', '[\"250\",\"\"]', '65.38', 'usd', 'single', '2017-03-23 16:55:33', '2017-03-23 16:55:33'),
(127, 0, 3, '497', '2017-03-20', 69, 'NO.29, LANE 180, CHIEN CHENG RD.,\r\nMIN SHENG TSUN, FANG YUAN HSIANG, \r\nCHANGHUA HSIEN, TAIWAN', 'CHURWAN YU CO., LTD.\nNO.29, LANE 180, CHIEN CHENG RD.,\r\nMIN SHENG TSUN, FANG YUAN HSIANG, \r\nCHANGHUA HSIEN, TAIWAN', 'CHURWAN YU CO., LTD.\nNO.29, LANE 180, CHIEN CHENG RD.,\r\nMIN SHENG TSUN, FANG YUAN HSIANG, \r\nCHANGHUA HSIEN, TAIWAN', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"200\",\"\"]', '65.4', 'usd', 'single', '2017-03-23 16:58:48', '2017-03-23 16:58:48'),
(128, 0, 3, '498', '2017-03-20', 31, '115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1760\",\"\"]', '65.4', 'usd', 'single', '2017-03-23 17:01:10', '2017-03-23 17:01:10'),
(129, 0, 3, '499', '2017-03-22', 25, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', '2107', '0', '0000-00-00 00:00:00', 2, '', '5', 'VERACRUZ', '', '', '144', 'VERACRUZ', '', '', 'CIF', 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '[\"1775\",\"\"]', '65.4', 'usd', 'single', '2017-03-23 17:03:27', '2017-03-23 17:03:27'),
(130, 0, 3, '500', '2017-03-22', 25, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', '2108', '0', '0000-00-00 00:00:00', 2, '', '5', 'VERACRUZ', '', '', '144', 'VERACRUZ', '', '', 'CIF', 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '[\"1775\",\"\"]', '65.4', 'usd', 'single', '2017-03-23 17:05:33', '2017-03-23 17:05:33'),
(131, 0, 3, '501', '2017-03-22', 25, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', '2109', '0', '0000-00-00 00:00:00', 2, '', '5', 'VERACRUZ', '', '', '144', 'VERACRUZ', '', '', 'CIF', 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '[\"1775\",\"\"]', '65.4', 'usd', 'single', '2017-03-23 17:07:13', '2017-03-23 17:07:13'),
(132, 0, 3, '506', '2017-03-23', 38, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', '', '100% TT PAYMENT', '[\"1700\",\"\"]', '65.45', 'usd', 'single', '2017-03-23 17:09:12', '2017-03-23 17:09:12'),
(133, 0, 3, '505', '2017-03-22', 13, 'INDUSTRIAL ZONE # 2,\r\nPART 233, 6th OF OCTOBER CITY\r\n', 'EAGLE POLYMERS\nINDUSTRIAL ZONE # 2,\r\nPART 233, 6th OF OCTOBER CITY\r\n', 'EAGLE POLYMERS\nINDUSTRIAL ZONE # 2,\r\nPART 233, 6th OF OCTOBER CITY\r\n', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'ALEXANDRIA', '', '', '65', 'ALEXANDRIA', '', '', 'CIF', 'CAD THROUGH BANK', '[\"1370\",\"\"]', '65.23', 'usd', 'single', '2017-03-24 18:55:46', '2017-03-24 18:55:46'),
(134, 0, 3, '516', '2017-03-31', 85, '216 BALCATTA ROAD, BALCATTA\r\nWA 6021 AUSTRALIA', 'AUSTRALIAN MUD COMPANY\n216 BALCATTA ROAD, BALCATTA\r\nWA 6021 AUSTRALIA', 'AUSTRALIAN MUD COMPANY\n216 BALCATTA ROAD, BALCATTA\r\nWA 6021 AUSTRALIA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'FREEMANTLE', '', '', '16', 'FREEMANTLE', '', '', 'CIF', 'CAD THROUGH BANK', '[\"750\",\"\"]', '64.78', 'usd', 'single', '2017-04-05 02:17:38', '2017-04-05 02:17:38'),
(135, 0, 3, '521', '2017-03-31', 86, '7th ST. VINH LOC INDUSTRIAL PARK\r\nBINH CHANH DIS., HO CHI MINH CITY,\r\nVIETNAM', 'CHOLIMEX FOODS JOINT STOCK COMPANY\n7th ST. VINH LOC INDUSTRIAL PARK\r\nBINH CHANH DIS., HO CHI MINH CITY,\r\nVIETNAM', 'CHOLIMEX FOODS JOINT STOCK COMPANY\n7th ST. VINH LOC INDUSTRIAL PARK\r\nBINH CHANH DIS., HO CHI MINH CITY,\r\nVIETNAM', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'HO CHI MINH', '', '', '238', 'HO CHI MINH', '', '', 'FOB PIPAVAV', 'LC SIGHT 30 DAYS', '[\"\",\"\"]', '64.84', 'usd', 'single', '2017-04-05 02:23:10', '2017-04-05 02:23:10'),
(136, 0, 3, '502', '2017-03-21', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '79', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CAD THROUGH BANK', '[\"2100\",\"\"]', '65.28', 'usd', 'single', '2017-04-06 18:52:52', '2017-04-06 18:52:52'),
(137, 0, 3, '511', '2017-03-27', 87, 'NO.282, FU HSING RD., IA LIAO VILL.,\r\nLU JWU HSIANG, KAOHSIUNG, TAIWAN R.O.C.', 'THREE JACK FEED INDUSTRY CO., LTD.\nNO.282, FU HSING RD., IA LIAO VILL.,\r\nLU JWU HSIANG, KAOHSIUNG, TAIWAN R.O.C.', 'THREE JACK FEED INDUSTRY CO., LTD.\nNO.282, FU HSING RD., IA LIAO VILL.,\r\nLU JWU HSIANG, KAOHSIUNG, TAIWAN R.O.C.', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"175\",\"\"]', '65.08', 'usd', 'single', '2017-04-06 19:00:34', '2017-04-06 19:00:34'),
(138, 0, 3, '512', '2017-03-28', 51, '301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"175\",\"\"]', '65.05', 'usd', 'single', '2017-04-06 19:02:46', '2017-04-06 19:02:46'),
(139, 0, 3, '508', '2017-03-22', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '80', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CAD', '[\"2100\",\"\"]', '65.48', 'usd', 'single', '2017-04-06 19:04:27', '2017-04-06 19:04:27'),
(140, 0, 3, '514', '2017-03-31', 59, 'RM51, 5TH FLOOR, BRITANNIA HOUSE, JALAN CATOR,\r\nBANDAR SERI BEGAWAN BS 8811,NEGARA BRUNEI DARUSSALAM\r\nTEL:+886-6-2531668 FAX: +886-6-2421613', 'GUANGXI SINOTUNG TRADING CO.,LTD.\r\nROOM 0, UNIT 1, BUILDING NO. 2, LIJINGHAOTINGNO. 3-1, ZHUXI ROAD, NANNING, GUANGXI, CHINA\r\n', 'GUANGXI SINOTUNG TRADING CO.,LTD.\r\nROOM 0, UNIT 1, BUILDING NO. 2, LIJINGHAOTINGNO. 3-1, ZHUXI ROAD, NANNING, GUANGXI, CHINA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'HUANGPU', '', '', '47', 'HUANGPU', '', '', 'CIF', '100% TT PAYMENT', '[\"290\",\"\"]', '64.94', 'usd', 'single', '2017-04-06 19:06:47', '2017-04-06 19:06:47'),
(141, 0, 3, '513', '2017-03-29', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '84', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CAD', '[\"2080\",\"\"]', '64.94', 'usd', 'single', '2017-04-06 19:07:02', '2017-04-06 19:07:02'),
(142, 0, 3, '503', '2017-03-21', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-', 'IMP-012/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'SANTOS', '', '', '33', 'SANTOS', '', '', 'CIF', 'CAD THROUGH BANK', '[\"1210\",\"\"]', '65.28', 'usd', 'single', '2017-04-06 19:15:50', '2017-04-06 19:15:50'),
(143, 0, 3, '507', '2017-03-22', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-', 'IMP-014/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'SANTOS', '', '', '33', 'SANTOS', '', '', 'C&F', 'CAD THROUGH BANK', '[\"1000\",\"\"]', '65.48', 'usd', 'single', '2017-04-06 19:19:34', '2017-04-06 19:19:34'),
(144, 0, 3, '522', '2017-03-31', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-', 'IMP-016/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'SANTOS', '', '', '33', 'SANTOS', '', '', 'C&F', 'CAD THROUGH BANK', '[\"1140\",\"\"]', '64.84', 'usd', 'single', '2017-04-06 19:22:40', '2017-04-06 19:22:40'),
(145, 0, 3, '517', '2017-04-06', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"160\",\"\"]', '64.82', 'usd', 'single', '2017-04-06 19:24:21', '2017-04-06 19:24:21'),
(146, 0, 3, '510', '2017-03-22', 56, 'RUA GIL STEIN FERREIRA,357 , SALA 703,\r\n 88 301-210 CENTRO, ITAJAI-SC,\r\nCNPJ 60.874.724/0005-10', 'INDUSTRIA QUIMICA ANASTACIO S/A\nRUA GIL STEIN FERREIRA,357 , SALA 703,\r\n 88 301-210 CENTRO, ITAJAI-SC,\r\nCNPJ 60.874.724/0005-10', 'KOMPORT COMERCIAL IMPORTADORA S.A\r\nAV. MAURO RAMOS, 1450-7áµ’ . ANDAR-CENTRO\r\n88020-302-FLORIANOPOLIS-SC-BRASIL\r\nCNPJ: 07.409.820/0001-80\r\nPHONE : +55 482106-4821 FAX: +55 48 2106-4848', '12.731/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'ITAJAI / NAVEGANTS', '', '', '33', 'ITAJAI / NAVEGANTS', '', '', 'CIF', '100% TT PAYMENT', '[\"\",\"2600\"]', '65.40', 'usd', 'single', '2017-04-06 19:27:30', '2017-04-06 19:27:30'),
(147, 0, 3, '504', '2017-03-22', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '', '[\"2100\",\"\"]', '65.28', 'usd', 'single', '2017-04-06 22:25:50', '2017-04-06 22:25:50'),
(148, 0, 3, '509', '2017-03-24', 88, 'VIA CARAVAGGI - 24040 LEVATE (BG)\r\nITALY\r\nTEL. +39 035596000 FAX. : +39.035594400', 'SABO SPA\nVIA CARAVAGGI - 24040 LEVATE (BG)\r\nITALY\r\nTEL. +39 035596000 FAX. : +39.035594400', 'SABO SPA\nVIA CARAVAGGI - 24040 LEVATE (BG)\r\nITALY\r\nTEL. +39 035596000 FAX. : +39.035594400', 'ODA1711309', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CAD', '[\"2170\",\"\"]', '70.5', 'euro', 'single', '2017-04-06 22:57:40', '2017-04-06 22:57:40'),
(149, 0, 3, '518', '2017-03-31', 61, 'NO.7-1, LN. 520, DAREN RD., LUZHU DIST.,\r\nKAOHSIUNG CITY 821, TAIWAN (R.O.C.)', 'GO-FENG INDUSTRY CO., LTD\nNO.7-1, LN. 520, DAREN RD., LUZHU DIST.,\r\nKAOHSIUNG CITY 821, TAIWAN (R.O.C.)', 'GO-FENG INDUSTRY CO., LTD\nNO.7-1, LN. 520, DAREN RD., LUZHU DIST.,\r\nKAOHSIUNG CITY 821, TAIWAN (R.O.C.)', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"160\",\"\"]', '64.844', 'usd', 'single', '2017-04-06 23:06:40', '2017-04-06 23:06:40'),
(150, 0, 3, '519', '2017-03-31', 35, 'NO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', 'YU YUAN TECHNOLOGY CO., LTD \nNO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', 'YU YUAN TECHNOLOGY CO., LTD \nNO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"160\",\"\"]', '64.844', 'usd', 'single', '2017-04-06 23:13:50', '2017-04-06 23:13:50'),
(151, 0, 3, '523', '2017-03-31', 50, '69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', 'CHEMICAL-LAND TRADING CO. LTD\n69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', 'CHEMICAL-LAND TRADING CO. LTD\n69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CIF', 'LC AT SIGHT', '[\"200\",\"\"]', '64.849', 'usd', 'single', '2017-04-06 23:21:00', '2017-04-06 23:21:00'),
(152, 0, 3, '524', '2017-03-31', 30, '129/112 Moo1 Bangkayang, Moung,\r\nANY PORT, INDIA\r\nPathumthani 12000', 'SIROCKO (THAILAND) CO. LTD.\n129/112 Moo1 Bangkayang, Moung,\r\nANY PORT, INDIA\r\nPathumthani 12000', 'SIROCKO (THAILAND) CO. LTD.\n129/112 Moo1 Bangkayang, Moung,\r\nANY PORT, INDIA\r\nPathumthani 12000', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'BANGKOK', '', '', '217', 'BANGKOK', '', '', 'CIF', '100% ADVANCE PAYMENT', '[\"250\",\"\"]', '64.849', 'usd', 'single', '2017-04-06 23:25:50', '2017-04-06 23:25:50'),
(153, 0, 3, '515', '2017-03-30', 90, 'ATLANTIDA, 21 , 102a\r\n08930 SANT ADRIA DE BESOS\r\nBARCELONA SPAIN', 'BILTREC S.A.,\nATLANTIDA, 21 , 102a\r\n08930 SANT ADRIA DE BESOS\r\nBARCELONA SPAIN', 'BILTREC S.A.,\nATLANTIDA, 21 , 102a\r\n08930 SANT ADRIA DE BESOS\r\nBARCELONA SPAIN', 'I-10733/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'BARCELONA', '', '', '204', 'BARCELONA', '', '', 'CIF', 'CAD', '[\"2160\",\"\"]', '64.90', 'usd', 'single', '2017-04-06 23:27:10', '2017-04-06 23:27:10'),
(154, 0, 3, '525', '2017-03-31', 25, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', '2110', '0', '0000-00-00 00:00:00', 2, '', '2', 'VERACRUZ', '', '', '144', 'VERACRUZ', '', '', 'CIF', 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '[\"1520\",\"\"]', '64.849', 'usd', 'single', '2017-04-06 23:28:05', '2017-04-06 23:28:05'),
(155, 0, 3, '001', '2017-04-03', 91, '(553, Sungnae-dong) 7, Olympic-ro 48-gil,\r\nGangdong-gu, Seoul, Korea', 'N H TRADING\n(553, Sungnae-dong) 7, Olympic-ro 48-gil,\r\nGangdong-gu, Seoul, Korea', 'N H TRADING\n(553, Sungnae-dong) 7, Olympic-ro 48-gil,\r\nGangdong-gu, Seoul, Korea', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'BUSAN', '', '', '3', 'BUSAN', '', '', 'CFR', 'LC AT SIGHT', '[\"\",\"360\"]', '65.03', 'usd', 'single', '2017-04-06 23:33:46', '2017-04-06 23:33:46'),
(156, 0, 3, '003', '2017-04-04', 28, 'PO BOX 2571 DAMMAM', 'SAUDI INDUSTRIAL DETERGENTS CO.\r\nPO BOX 2571 DAMMAM', 'SAUDI INDUSTRIAL DETERGENTS CO.\r\nPO BOX 2571 DAMMAM', 'MFG02802', '0', '0000-00-00 00:00:00', 3, '', '2', 'DAMMAM', '', '', '192', 'DAMMAM', '', '', 'CIF', 'CAD THROUGH BANK', '[\"550\",\"\"]', '65', 'usd', 'single', '2017-04-06 23:36:09', '2017-04-06 23:36:42'),
(157, 0, 3, '006', '2017-04-06', 92, 'Warehouse RA08BB04, JAFZA, UAE\nTel: +9714 880 8272\nFax: +9714 8808 273', 'QATAR DETERGENTS COMPANY W.L.L.\r\nP.O. Box 6983 DOHA â€“ QATAR\r\nTel : +974 4460 0600 / 4423 8777\r\nFax : +974 4460 1921 / 4423 8700', 'QATAR DETERGENTS COMPANY W.L.L.\r\nP.O. Box 6983 DOHA â€“ QATAR\r\nTel : +974 4460 0600 / 4423 8777\r\nFax : +974 4460 1921 / 4423 8700', '2017-055', '0', '0000-00-00 00:00:00', 2, '', '2', 'DOHA', '', '', '181', 'DOHA', '', '', 'CIF', '100% TT PAYMENT', '[\"525\",\"\"]', '65', 'usd', 'single', '2017-04-06 23:43:22', '2017-04-06 23:43:22'),
(158, 0, 3, '007', '2017-04-06', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"155\",\"\"]', '64.82', 'usd', 'single', '2017-04-06 23:45:59', '2017-04-06 23:45:59'),
(159, 0, 3, '008', '2017-04-06', 93, '133 Cecil Street, #9-01A, Keck Seng Tower,\r\nSingapore 069535\r\nTel : +65 6224 8871 Fax : +65 6224 8082', 'VVF SINGAPORE PTE LTD\n133 Cecil Street, #9-01A, Keck Seng Tower,\r\nSingapore 069535\r\nTel : +65 6224 8871 Fax : +65 6224 8082', 'VVF SINGAPORE PTE LTD\n133 Cecil Street, #9-01A, Keck Seng Tower,\r\nSingapore 069535\r\nTel : +65 6224 8871 Fax : +65 6224 8082', 'PO17522', '0', '0000-00-00 00:00:00', 2, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CIF', '100% TT PAYMENT', '[\"160\",\"\"]', '64.85', 'usd', 'single', '2017-04-11 16:28:02', '2017-04-11 16:28:02'),
(160, 0, 3, '010', '2017-04-07', 34, '905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'EGYPOLYMERS FOR CHEMICALS\r\nABO RAWASH INDUSTRIAL AREA\r\nGIZA, EGYPT\r\nTel: +201 019 676 764 / +201 009 815 151', 'EGYPOLYMERS FOR CHEMICALS\r\nABO RAWASH INDUSTRIAL AREA\r\nGIZA, EGYPT\r\nTel: +201 019 676 764 / +201 009 815 151', 'EGY-20170401', '0', '0000-00-00 00:00:00', 2, '', '2', 'ALEXANDRIA OLD PORT', '', '', '248', 'ALEXANDRIA OLD PORT', '', '', 'CIF', '100% TT PAYMENT', '[\"1410\",\"\"]', '64.32', 'usd', 'single', '2017-04-11 16:33:50', '2017-04-11 16:33:50'),
(161, 0, 3, '013', '2017-04-10', 38, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1725\",\"\"]', '64.45', 'usd', 'single', '2017-04-11 16:37:24', '2017-04-11 16:37:24'),
(162, 0, 3, '4', '2017-04-05', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '87', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CAD', '[\"2120\",\"\"]', '65', 'usd', 'single', '2017-04-11 16:40:05', '2017-04-11 16:40:05'),
(163, 0, 3, '5', '2017-04-05', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '88', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CAD', '[\"2070\",\"\"]', '65', 'usd', 'single', '2017-04-11 16:43:02', '2017-04-11 16:43:02'),
(164, 0, 3, '12', '2017-04-09', 5, '972/1 3RD FLOOR, VORASUBIN BUILDING\r\nSOI RANA 9 HOSPITAL,RAMA 9 ROAD, BANG KAPI,\r\nBANG KAPI, HUAI KHWANG,BANGKOK 10320 THAILAND', 'KUEHNE+NAGEL, INC.18900 8TH AVE SOUTH,SUITE 1300,\r\nSEA TAC, WA 98148, \r\nATTN : WENDY THORNTON,\r\nTEL : +1-206-901-9000 EXT 301,    FAX : +1 -206-901-0977', 'S.A.F.E. CHEMICALS LLC\r\n3553 KILGORE PARKWAY,BAYTOWN,TX7753,USA.\r\nCONTACT PERSON : MANOJ JAIN\r\nTEL : +1 -206-275-3992, EFAX :+1-419-821-5261', 'STAR ASIA /05-024/17', '0', '0000-00-00 00:00:00', 2, '', '1', 'HOUSTON', '', '', '231', 'HOUSTON', '', '', 'CNF', 'CAD THROUGH BANK', '[\"3050\",\"\"]', '64.15', 'usd', 'single', '2017-04-11 16:48:59', '2017-04-11 16:48:59'),
(166, 0, 3, '2', '2017-04-03', 95, 'NO 25, TONG XING STREET, ROOM 3208, 3209\r\nWORLD TRADE CENTRE, DALIAN, ZIP CODE : 116001\r\n', 'SYNCHEM INTERNATIONAL CO., LTD\nNO 25, TONG XING STREET, ROOM 3208, 3209\r\nWORLD TRADE CENTRE, DALIAN, ZIP CODE : 116001\r\n', 'SYNCHEM INTERNATIONAL CO., LTD\nNO 25, TONG XING STREET, ROOM 3208, 3209\r\nWORLD TRADE CENTRE, DALIAN, ZIP CODE : 116001\r\n', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'LIANYUNGANG', '', '', '47', 'LIANYUNGANG', '', '', 'CIF', 'CAD THROUGH BANK', '[\"260\",\"\"]', '64.85', 'usd', 'single', '2017-04-16 17:47:37', '2017-04-16 17:47:37'),
(167, 0, 3, '21', '2017-04-14', 12, 'KOPER, SLOVENIA', 'SOLCHEM D.O.O.\nKOPER, SLOVENIA', 'SOLCHEM D.O.O.\nKOPER, SLOVENIA', 'NN170337', '0', '0000-00-00 00:00:00', 2, '', '1', 'KOPER', '', '', '199', 'KOPER', '', '', 'CIF', '30% ADVANCE & 70% AGAINST DOCS', '[\"1800\",\"\"]', '64.40', 'usd', 'single', '2017-04-16 17:50:52', '2017-04-16 17:50:52'),
(168, 0, 3, '016', '2017-04-13', 51, '301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'INCHEON', '', '', '3', 'INCHEON', '', '', 'CIF', 'LC AT SIGHT', '[\"200\",\"\"]', '64.37', 'usd', 'single', '2017-04-17 17:04:44', '2017-04-17 17:04:44'),
(169, 0, 3, '17', '2017-04-13', 96, 'Box 187\r\n265 22 Ã…STORP\r\nSWEDEN', 'UTS LLC\r\nLeninsky pr., 140 â€œLâ€,\r\nSt.Petersburg, 198216, Russia\r\nSergey Be11ubov.27 812 7031 035', 'UTS LLC\r\nLeninsky pr., 140 â€œLâ€,\r\nSt.Petersburg, 198216, Russia', '695914', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1725\",\"\"]', '64.34', 'usd', 'single', '2017-04-17 17:08:51', '2017-04-17 17:08:51'),
(170, 0, 3, '18', '2017-04-13', 98, '1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', 'CHANG HSING BIOTECHNOLOGY CO., LTD.\n1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', 'CHANG HSING BIOTECHNOLOGY CO., LTD.\n1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"145\",\"\"]', '67.37', 'usd', 'single', '2017-04-17 17:12:08', '2017-04-17 17:12:08'),
(171, 0, 3, '19', '2017-04-13', 99, 'NO.160, LI-KANG RD., CHUN-LIN VILLAGE,\r\nLI-KANG HSIANG, PING-TUNG HSIEN,\r\nTAIWAN(R.O.C.)', 'CHEONG-MU ENTERPRISE CO., LTD.\nNO.160, LI-KANG RD., CHUN-LIN VILLAGE,\r\nLI-KANG HSIANG, PING-TUNG HSIEN,\r\nTAIWAN(R.O.C.)', 'CHEONG-MU ENTERPRISE CO., LTD.\nNO.160, LI-KANG RD., CHUN-LIN VILLAGE,\r\nLI-KANG HSIANG, PING-TUNG HSIEN,\r\nTAIWAN(R.O.C.)', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', '100% TT PAYMENT', '[\"145\",\"\"]', '64.37', 'usd', 'single', '2017-04-17 17:14:16', '2017-04-17 17:14:16'),
(172, 0, 3, '20', '2017-04-14', 37, '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '0', '0000-00-00 00:00:00', 5, '', '2', 'KAOHSIUNG', 'TAICHUNG', '', '2', 'KAOHSIUNG', 'TAICHUNG', '', 'CFR', 'LC AT SIGHT', '[\"145\",\"\"]', '64.55', 'usd', 'single', '2017-04-17 17:19:26', '2017-04-17 17:19:26'),
(173, 0, 3, '22', '2017-04-18', 38, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1725\",\"\"]', '64.5', 'usd', 'single', '2017-04-21 22:06:41', '2017-04-21 22:06:41'),
(174, 0, 3, '25', '2017-04-19', 31, '115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', 'NEFTECHEMEX LLC\n115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1660\",\"\"]', '64.55', 'usd', 'single', '2017-04-21 22:08:03', '2017-04-21 22:08:03'),
(175, 0, 3, '12', '2017-04-24', 5, '972/1 3RD FLOOR, VORASUBIN BUILDING\r\nSOI RANA 9 HOSPITAL,RAMA 9 ROAD, BANG KAPI,\r\nBANG KAPI, HUAI KHWANG,BANGKOK 10320 THAILAND', 'KUEHNE+NAGEL, INC.18900 8TH AVE SOUTH,SUITE 1300,\r\nSEA TAC, WA 98148, \r\nATTN : WENDY THORNTON,\r\nTEL : +1-206-901-9000 EXT 301,    FAX : +1 -206-901-0977', 'S.A.F.E. CHEMICALS LLC\r\n3553 KILGORE PARKWAY,BAYTOWN,TX7753,USA.\r\n CONTACT PERSON : MANOJ JAIN\r\n TEL : +1 -206-275-3992, EFAX :+1-419-821-5261', 'STKOTH17-026', '0', '0000-00-00 00:00:00', 2, '', '1', 'HOUSTON', '', '', '231', 'HOUSTON', '', '', 'CNF', 'CAD THROUGH BANK', '[\"3050\",\"\"]', '64.15', 'usd', 'single', '2017-04-24 17:05:14', '2017-04-24 17:05:14'),
(176, 0, 3, '14', '2017-04-24', 22, '3FL, NO 296, SEC 1, CHUNG SHAN ROAD SHULIN\r\nTAIWAN 23842\r\n', 'CHIN MING TRADING CO,.LTD\n3FL, NO 296, SEC 1, CHUNG SHAN ROAD SHULIN\r\nTAIWAN 23842\r\n', 'CHIN MING TRADING CO,.LTD\n3FL, NO 296, SEC 1, CHUNG SHAN ROAD SHULIN\r\nTAIWAN 23842\r\n', 'CMG-6523 2CC', '0', '0000-00-00 00:00:00', 2, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CIF', '100% TT PAYMENT', '[\"\",\"\"]', '64.3', 'usd', 'single', '2017-04-24 17:26:42', '2017-04-24 17:26:42'),
(177, 0, 3, '15', '2017-04-24', 49, 'P.O. BOX 96.\r\n5750 AB DEURNE\r\nTHE NETHERLANDS', 'CASTOR INTERNATIONAL BV\nP.O. BOX 96.\r\n5750 AB DEURNE\r\nTHE NETHERLANDS', 'CASTOR INTERNATIONAL BV\nP.O. BOX 96.\r\n5750 AB DEURNE\r\nTHE NETHERLANDS', '50004624 ', '0', '0000-00-00 00:00:00', 2, '', '1', 'ROTTERDAM', '', '', '83', 'ROTTERDAM', '', '', 'CNF', '100% TT PAYMENT', '[\"1860\",\"\"]', '64.55', 'usd', 'single', '2017-04-24 17:45:26', '2017-04-24 17:45:26'),
(178, 0, 3, '23', '2017-04-19', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '101', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CIF', 'CAD', '[\"\",\"\"]', '64.52', 'usd', 'single', '2017-04-24 18:04:30', '2017-04-24 18:04:30'),
(179, 0, 3, '26', '2017-04-20', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '102', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '108', 'GENOVA', '', '', 'CNF', 'CAD', '[\"2370\",\"\"]', '64.55', 'usd', 'single', '2017-04-24 18:15:18', '2017-04-24 18:15:18'),
(180, 0, 3, '28', '2017-04-24', 100, 'ASSENEDESTRAAT 2\r\n9940 ERTVELDE\r\nBELGIUM', 'OLEON N.V.\nASSENEDESTRAAT 2\r\n9940 ERTVELDE\r\nBELGIUM', 'OLEON N.V.\nASSENEDESTRAAT 2\r\n9940 ERTVELDE\r\nBELGIUM', 'CS80', '0', '0000-00-00 00:00:00', 2, '', '1', 'ANTWERP', '', '', '24', 'ANTWERP', '', '', 'CIF', 'CAD THROUGH BANK 30DAYS FROM B/L DATE', '[\"2880 & 2180\",\"\"]', '64.55', 'usd', 'single', '2017-04-24 18:45:17', '2017-04-24 18:45:17'),
(181, 0, 3, '29', '2017-04-24', 5, '972/1 3RD FLOOR, VORASUBIN BUILDING\r\nSOI RANA 9 HOSPITAL,RAMA 9 ROAD, BANG KAPI,\r\nBANG KAPI, HUAI KHWANG,BANGKOK 10320 THAILAND', 'KUEHNE+NAGEL, INC.18900 8TH AVE SOUTH,SUITE 1300,\r\nSEA TAC, WA 98148,\r\nATTN : WENDY THORNTON,\r\nTEL : +1-206-901-9000 EXT 301, FAX : +1 -206-901-0977', 'S.A.F.E. CHEMICALS LLC\r\n3553 KILGORE PARKWAY,BAYTOWN,TX7753,USA.\r\nCONTACT PERSON : MANOJ JAIN\r\nTEL : +1 -206-275-3992, EFAX :+1-419-821-5261', 'STKOTH17-032', '0', '0000-00-00 00:00:00', 2, '', '1', 'HOUSTON', '', '', '231', 'HOUSTON', '', '', 'CNF', 'CAD THROUGH BANK', '[\"3130\",\"\"]', '64.55', 'usd', 'single', '2017-04-24 19:07:06', '2017-04-24 19:07:06'),
(182, 0, 3, '30', '2017-04-20', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1800\",\"\"]', '64.6', 'usd', 'single', '2017-04-25 23:53:43', '2017-04-25 23:53:43'),
(183, 0, 3, '30', '2017-04-20', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ST.PETERSBURG', '', '', '249', 'ST.PETERSBURG', '', '', 'CIF', '100% TT PAYMENT', '[\"1800\",\"\"]', '64.6', 'usd', 'single', '2017-04-25 23:54:50', '2017-04-25 23:54:50');
INSERT INTO `contracts` (`Id`, `company_id`, `user_id`, `contract_no`, `contract_date`, `importer_id`, `importer_address`, `notifier_party`, `consignee_party`, `purchase_order_no`, `invoice_no`, `invoice_date`, `surveyor_id`, `surveyor_name`, `port_loading`, `port_discharge`, `port_discharge2`, `port_discharge3`, `final_destination_country`, `final_destination`, `final_destination2`, `final_destination3`, `delivery_terms`, `payment_terms`, `quotation_freight`, `dollor_exchange_rate`, `currency`, `contract_type`, `created_at`, `updated_at`) VALUES
(184, 0, 3, '35', '2017-04-24', 20, '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', 'LIBA CHEM\n20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'GENOVA', '', '', '', 'GENOVA', '', '', '', 'CAD', '[\"2260\",\"\"]', '64.45', 'usd', 'single', '2017-04-25 23:57:04', '2017-04-25 23:57:04'),
(185, 0, 3, '031', '2017-04-22', 101, '1st Elshiek Rehan, Inside Panasonic Building, \r\n9th Floor, Abdeen, Cairo, Egypt', 'EL NABILA GROUP COMPANY\n1st Elshiek Rehan, Inside Panasonic Building, \r\n9th Floor, Abdeen, Cairo, Egypt', 'EL NABILA GROUP COMPANY\n1st Elshiek Rehan, Inside Panasonic Building, \r\n9th Floor, Abdeen, Cairo, Egypt', '', '0', '0000-00-00 00:00:00', 5, '', '1', 'ALEXANDRIA', '', '', '248', 'ALEXANDRIA', '', '', 'CIF', '100% ADVANCE PAYMENT', '[\"70\",\"\"]', '64.6', 'usd', 'single', '2017-04-27 18:56:11', '2017-04-27 18:56:11'),
(186, 0, 3, '032', '2017-04-24', 51, '301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', 'SAMYANG FINE CHEMICAL CO.,LTD.\n301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', '', '0', '0000-00-00 00:00:00', 2, '', '2', 'INCHEON', '', '', '3', 'INCHEON', '', '', 'CIF', 'LC AT SIGHT', '[\"235\",\"\"]', '64.5', 'usd', 'single', '2017-04-27 18:57:52', '2017-04-27 18:57:52'),
(187, 0, 3, '033', '2017-04-24', 102, 'BOULEVARD INDUSTRIAL, 120\r\nB-7700 BELGIUM\r\nVAT NUMBER : BE - 471.546.593', 'VANDEPUTTE OLEOCHEMICALS SA\nBOULEVARD INDUSTRIAL, 120\r\nB-7700 BELGIUM\r\nVAT NUMBER : BE - 471.546.593', 'VANDEPUTTE OLEOCHEMICALS SA\nBOULEVARD INDUSTRIAL, 120\r\nB-7700 BELGIUM\r\nVAT NUMBER : BE - 471.546.593', '', '0', '0000-00-00 00:00:00', 2, '', '1', 'ANTWERP', '', '', '24', 'ANTWERP', '', '', 'CIF', '100% TT PAYMENT', '[\"1475\",\"\"]', '64.5', 'usd', 'single', '2017-04-27 19:01:55', '2017-04-27 19:01:55'),
(188, 0, 3, '034', '2017-04-24', 27, 'PO BOX 41671. HAMIRIYAH FREE ZONE, PHASE I,\r\nSHARJAH', 'IFFCO CHEMICALS FZE\nPO BOX 41671. HAMIRIYAH FREE ZONE, PHASE I,\r\nSHARJAH', 'IFFCO CHEMICALS FZE\nPO BOX 41671. HAMIRIYAH FREE ZONE, PHASE I,\r\nSHARJAH', '2000224', '0', '0000-00-00 00:00:00', 2, '', '2', 'JEBEL ALI', '', '', '229', 'JEBEL ALI', '', '', 'CIF', '100% ADVANCE PAYMENT', '[\"210\",\"\"]', '64.5', 'usd', 'single', '2017-04-27 19:04:31', '2017-04-27 19:04:31'),
(189, 0, 3, '036', '2017-04-25', 42, '68-1, 9 LING, BAO TANQ TSUN, GUAN IN SHIANG,\r\nTAO YUAN, TAIWAN, R.O.C.', 'TAI YEH CHEMICAL INDUSTRY CO., LTD.\n68-1, 9 LING, BAO TANQ TSUN, GUAN IN SHIANG,\r\nTAO YUAN, TAIWAN, R.O.C.', 'TAI YEH CHEMICAL INDUSTRY CO., LTD.\n68-1, 9 LING, BAO TANQ TSUN, GUAN IN SHIANG,\r\nTAO YUAN, TAIWAN, R.O.C.', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'TAICHUNG', '', '', '2', 'TAICHUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"150\",\"\"]', '64.3', 'usd', 'single', '2017-04-27 19:09:48', '2017-04-27 19:09:48'),
(190, 0, 3, '037', '2017-04-26', 52, 'No.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.\nNo.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', '', '0', '0000-00-00 00:00:00', 4, '', '2', 'KAOHSIUNG', '', '', '2', 'KAOHSIUNG', '', '', 'CFR', 'LC AT SIGHT', '[\"130\",\"\"]', '64.3', 'usd', 'single', '2017-04-27 19:11:26', '2017-04-27 19:11:26'),
(191, 0, 3, '038', '2017-04-26', 26, '60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', 'CJP CHEMICALS (PTY) LTD\n60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', 'CJP CHEMICALS (PTY) LTD\n60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', '94513', '0', '0000-00-00 00:00:00', 2, '', '2', 'DURBAN', '', '', '202', 'DURBAN', '', '', 'CFR', '60 DAYS LC FROM BL DATE', '[\"700\",\"\"]', '64.3', 'usd', 'single', '2017-04-27 19:12:57', '2017-04-27 19:12:57'),
(192, 0, 3, '040', '2017-04-27', 25, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', '2113', '0', '0000-00-00 00:00:00', 2, '', '2', 'VERACRUZ', '', '', '144', 'VERACRUZ', '', '', 'CIF', 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '[\"1555\",\"\"]', '64.1', 'usd', 'single', '2017-04-27 19:14:37', '2017-04-27 19:14:37'),
(194, 0, 1, 'rg', '2017-05-30', 34, '905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', '', '0', '0000-00-00 00:00:00', 2, '', '5', 'ALEXANDRIA', '', '', '4', '', '', '', 'CIF', '30% ADVANCE & 70% AGAINST DOCS', '[\"\",\"\"]', '', 'usd', 'single', '2017-05-30 03:58:26', '2017-05-30 03:58:26'),
(195, 0, 1, 'rg', '2017-05-30', 34, '905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', '', '0', '0000-00-00 00:00:00', 2, '', '5', 'ALEXANDRIA', '', '', '4', '', '', '', 'CIF', '30% ADVANCE & 70% AGAINST DOCS', '[\"\",\"\"]', '', 'usd', 'single', '2017-05-30 04:00:36', '2017-05-30 04:00:36'),
(196, 0, 1, 'rg', '2017-05-30', 34, '905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', '', '0', '0000-00-00 00:00:00', 2, '', '5', 'ALEXANDRIA', '', '', '4', '', '', '', 'CIF', '30% ADVANCE & 70% AGAINST DOCS', '[\"\",\"\"]', '', 'usd', 'single', '2017-05-30 04:00:49', '2017-05-30 04:00:49'),
(197, 0, 1, 'rg', '2017-05-30', 34, '905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', '', '0', '0000-00-00 00:00:00', 2, '', '5', 'ALEXANDRIA', '', '', '4', '', '', '', 'CIF', '30% ADVANCE & 70% AGAINST DOCS', '[\"\",\"\"]', '', 'usd', 'single', '2017-05-30 04:00:49', '2017-05-30 04:00:49'),
(203, 0, 1, 'rf125', '2017-07-05', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'sadasdasd', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'CFR', '100% TT PAYMENT', '[\"\",\"\",\"\",\"\",\"\",\"\"]', '152', 'usd', 'single', '2017-07-05 08:34:45', '2017-07-05 08:34:45'),
(204, 0, 1, 'rf125', '2017-07-05', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'sadasdasd', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'CFR', '100% TT PAYMENT', '[\"\",\"\",\"\",\"\",\"\",\"\"]', '152', 'usd', 'single', '2017-07-05 08:35:17', '2017-07-05 08:35:17'),
(205, 0, 1, 'rf125', '2017-07-05', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'sadasdasd', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'CFR', '100% TT PAYMENT', '[\"\",\"\",\"\",\"\",\"\",\"\"]', '152', 'usd', 'single', '2017-07-05 08:39:32', '2017-07-05 08:39:32'),
(206, 0, 1, 'rf125', '2017-07-05', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'sadasdasd', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'CFR', '100% TT PAYMENT', '[\"\",\"\",\"\",\"\",\"\",\"\"]', '152', 'usd', 'single', '2017-07-05 08:42:00', '2017-07-05 08:42:00'),
(207, 0, 1, 'rf125', '2017-07-05', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'sadasdasd', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'CFR', '100% TT PAYMENT', '[\"\",\"\",\"\",\"\",\"\",\"\"]', '152', 'usd', 'single', '2017-07-05 08:51:21', '2017-07-05 08:51:21'),
(208, 0, 1, 'rf125', '2017-07-05', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'sadasdasd', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'CFR', '100% TT PAYMENT', '[\"\",\"\",\"\",\"\",\"\",\"\"]', '152', 'usd', 'single', '2017-07-05 08:51:58', '2017-07-05 08:51:58'),
(209, 0, 1, 'rf125', '2017-07-05', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'sadasdasd', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'CFR', '100% TT PAYMENT', '[\"\",\"\",\"\",\"\",\"\",\"\"]', '152', 'usd', 'single', '2017-07-05 09:00:14', '2017-07-05 09:00:14'),
(210, 0, 1, 'rf125', '2017-07-05', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'sadasdasd', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'CFR', '100% TT PAYMENT', '[\"\",\"\",\"\",\"\",\"\",\"\"]', '152', 'usd', 'single', '2017-07-05 09:15:49', '2017-07-05 09:15:49'),
(236, 0, 1, '111', '2017-07-06', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'test', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA', '', '', 'C&F', '100% ADVANCE PAYMENT', 'null', '65', 'usd', 'single', '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(237, 0, 1, '111', '2017-07-06', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'test', '0', '0000-00-00 00:00:00', 5, '', '5', 'ALEXANDRIA OLD PORT', '', '', '5', 'ALEXANDRIA OLD PORT', '', '', 'C&F', '100% ADVANCE PAYMENT', 'null', '65', 'usd', 'single', '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(250, 0, 1, '', '2017-07-07', 34, '905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', 'AFRIGLOBAL COMMODITIES DMCC\r\n905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', '', '0', '0000-00-00 00:00:00', 2, '', '5', 'ALEXANDRIA', '', '', '4', '', '', '', 'CIF', '30% ADVANCE & 70% AGAINST DOCS', '', '', 'usd', 'multiple', '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(270, 0, 1, '1272017_con', '2017-07-12', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '1272017_con', '0', '0000-00-00 00:00:00', 2, '', '3', 'ALEXANDRIA OLD PORT', 'ALEXANDRIA OLD PORT', '', '5', 'ALEXANDRIA OLD PORT', 'ALEXANDRIA OLD PORT', '', 'CNF', '100% TT PAYMENT', '', '60', 'usd', 'multiple', '2017-07-12 09:41:48', '2017-07-12 09:41:48'),
(271, 0, 1, '12-7-17_Con', '2017-07-12', 66, 'KEHATIE 1\r\n33880 LEMPAALA\r\nFINLAND', 'KIILTO OY\r\nKEHATIE 1\r\n33880 LEMPAALA\r\nFINLAND', 'KIILTO OY\r\nKEHATIE 1\r\n33880 LEMPAALA\r\nFINLAND', '12-7-17_Con', '0', '0000-00-00 00:00:00', 2, '', '5', 'ALEXANDRIA', 'ALEXANDRIA OLD PORT', 'ANTWERP', '21', 'ALEXANDRIA', 'ALTAMIRA', 'BANDAR ABBAS', 'CNF', '50% ADVANCE + 50% AGAINST SCAN BL', '', '25', 'usd', 'multiple', '2017-07-12 10:33:04', '2017-07-12 10:33:04'),
(272, 0, 1, '1372017_con', '2017-07-13', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '1372017_con', '0', '0000-00-00 00:00:00', 2, '', '5', 'ALEXANDRIA OLD PORT', 'ALTAMIRA', 'ANTWERP', '4', 'ALEXANDRIA OLD PORT', 'ALEXANDRIA', 'BANDAR ABBAS', 'CFR', '60 DAYS LC FROM BL DATE', 'null', '65', 'usd', 'single', '2017-07-12 11:13:51', '2017-07-14 07:46:45'),
(274, 0, 1, '274_test', '2017-07-19', 81, 'P.O. BOX 424\r\n13005 SAFAT\r\nKUWAIT', 'AL SHARHAN INDUSTRIES\r\nP.O. BOX 424\r\n13005 SAFAT\r\nKUWAIT', 'AL SHARHAN INDUSTRIES\r\nP.O. BOX 424\r\n13005 SAFAT\r\nKUWAIT', '123', '0', '0000-00-00 00:00:00', 2, '', '5', 'ALEXANDRIA OLD PORT', 'ANTWERP', 'BARCELONA', '9', 'ALTAMIRA', 'BANDAR ABBAS', 'BARCELONA', 'FOB MUNDRA', 'CAD THROUGH BANK', 'null', '67.212', 'usd', 'multiple', '2017-07-19 05:20:42', '2017-08-10 07:09:21'),
(279, 0, 1, 'test', '2017-08-21', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '123', '0', '0000-00-00 00:00:00', 2, '', '3', 'ALEXANDRIA', '', '', '4', 'ALEXANDRIA OLD PORT', '', '', 'C&F', '100% TT PAYMENT', '', '65.20', 'usd', 'multiple', '2017-08-21 08:05:40', '2017-08-21 08:05:40'),
(284, 0, 1, '12312312', '2017-09-04', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '123123', '0', '0000-00-00 00:00:00', 2, '', '1', 'BARCELONA', '', '', '36', 'LIANYUNGANG', '', '', 'EX -WORKS', '50% ADVANCE + 50% AGAINST SCAN BL', '', '52', 'usd', 'multiple', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(285, 0, 1, '12312312', '2017-09-04', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '123123', '0', '0000-00-00 00:00:00', 2, '', '1', 'BARCELONA', '', '', '36', 'LIANYUNGANG', '', '', 'EX -WORKS', '50% ADVANCE + 50% AGAINST SCAN BL', '', '52', 'usd', 'multiple', '2017-09-04 05:58:03', '2017-09-04 05:58:03'),
(286, 0, 1, '12312312', '2017-09-04', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '123123', '0', '0000-00-00 00:00:00', 2, '', '1', 'BARCELONA', '', '', '36', 'LIANYUNGANG', '', '', 'EX -WORKS', '50% ADVANCE + 50% AGAINST SCAN BL', '', '52', 'usd', 'multiple', '2017-09-04 05:59:00', '2017-09-04 05:59:00'),
(288, 0, 1, 'qwe', '2017-09-04', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'wqe', '0', '0000-00-00 00:00:00', 2, '', '3', 'ALEXANDRIA', '', '', '6', 'ALEXANDRIA', '', '', 'CNF', '30% ADVANCE & 70% AGAINST DOCS', '', '', 'usd', 'single', '2017-09-04 06:17:21', '2017-09-04 06:17:21'),
(289, 0, 1, 'weqqwe', '2017-09-04', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'qweqwe', '0', '0000-00-00 00:00:00', 4, '', '3', 'ALEXANDRIA OLD PORT', '', '', '4', 'ALTAMIRA', '', '', 'EX -WORKS', '100% ADVANCE PAYMENT', '', '50', 'usd', 'multiple', '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(290, 0, 1, 'wqewqewq', '2017-09-04', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'wqewqewq', '0', '0000-00-00 00:00:00', 2, '', '6', 'ALEXANDRIA OLD PORT', '', '', '6', 'ALTAMIRA', '', '', 'EX -WORKS', '100% TT PAYMENT', '', '67', 'usd', 'multiple', '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(291, 0, 1, 'wqewqewq', '2017-09-04', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'wqewqewq', '0', '0000-00-00 00:00:00', 2, '', '6', 'ALEXANDRIA', '', '', '6', 'BANDAR ABBAS', '', '', 'EX -WORKS', '100% TT PAYMENT', '', '67', 'usd', 'multiple', '2017-09-04 06:24:35', '2017-09-04 06:24:35'),
(292, 0, 1, 'testdsf', '2017-09-04', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', '1232', '0', '0000-00-00 00:00:00', 2, '', '6', 'ALEXANDRIA OLD PORT', '', '', '5', 'ALTAMIRA', '', '', 'CNF', '100% ADVANCE PAYMENT', '', '60', 'usd', 'multiple', '2017-09-04 06:27:58', '2017-09-04 06:27:58'),
(293, 0, 1, 'qwewqe', '2017-09-04', 19, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '12312', '0', '0000-00-00 00:00:00', 4, '', '6', 'ALEXANDRIA OLD PORT', '', '', '20', 'BARCELONA', '', '', 'CIF', '100% ADVANCE PAYMENT', '', '50', 'usd', 'multiple', '2017-09-04 06:30:38', '2017-09-04 06:30:38'),
(294, 0, 1, 'sadasdas', '2017-09-04', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', '12321', '0', '0000-00-00 00:00:00', 2, '', '3', 'ALEXANDRIA OLD PORT', '', '', '7', 'ALEXANDRIA', '', '', 'CNF', '100% TT PAYMENT', '', '50', 'usd', 'multiple', '2017-09-04 06:34:56', '2017-09-04 06:34:56'),
(295, 0, 1, 'sadsad', '2017-09-04', 14, 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA\r\nESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55', '2132', '0', '0000-00-00 00:00:00', 2, '', '3', 'BANDARABBAS', '', '', '6', 'DOHA', '', '', 'DAP', '30% ADVANCE & 70% AGAINST DOCS', '', '65', 'usd', 'multiple', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(296, 0, 1, 'dasdas', '2017-09-04', 97, '1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', 'CHANG HSING BIOTECHNOLOGY CO., LTD.\r\n1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', 'CHANG HSING BIOTECHNOLOGY CO., LTD.\r\n1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', '1223', '0', '0000-00-00 00:00:00', 2, '', '1', 'BANGKOK', 'ANTWERP', '', '18', 'DOHA', 'BARCELONA', '', 'CIF', '30% ADVANCE & 70% AGAINST DOCS', '', '85', 'usd', 'multiple', '2017-09-04 06:47:58', '2017-09-06 10:54:03');

-- --------------------------------------------------------

--
-- Table structure for table `contracts_do`
--

CREATE TABLE `contracts_do` (
  `Id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `shipment_id` int(11) NOT NULL,
  `do_no` varchar(255) DEFAULT '',
  `do_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `do_exp_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `forwarder_id` int(5) DEFAULT NULL,
  `forwarder_name` varchar(255) DEFAULT NULL,
  `shipment_line` varchar(255) DEFAULT NULL,
  `freight` varchar(255) DEFAULT NULL,
  `total_freight` varchar(255) DEFAULT NULL,
  `thc` varchar(255) DEFAULT NULL,
  `blc` varchar(255) DEFAULT NULL,
  `vessel_name` varchar(255) DEFAULT NULL,
  `voyage_no` varchar(255) DEFAULT NULL,
  `eta_origin` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `etd_origin` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `eta_destination` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `eta_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_transit` varchar(255) DEFAULT NULL,
  `vgm_cutoff` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `container_gate_open` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `gate_cutoff` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `document_cutoff` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `si_cutoff` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `trans_port` varchar(255) DEFAULT '["","","",""]',
  `trans_vessel_name` varchar(255) DEFAULT '["","","",""]',
  `trans_eta` varchar(255) DEFAULT '["","","",""]',
  `trans_etd` varchar(255) DEFAULT '["","","",""]',
  `stuffing_from` date NOT NULL DEFAULT '0000-00-00',
  `stuffing_to` date NOT NULL DEFAULT '0000-00-00',
  `do_port_loading` varchar(255) DEFAULT NULL,
  `do_port_discharge` varchar(255) DEFAULT NULL,
  `do_quotation_freight` varchar(255) DEFAULT NULL,
  `do_container_size` varchar(255) DEFAULT NULL,
  `total_detention_days` varchar(255) DEFAULT NULL,
  `total_demurrage_days` varchar(255) DEFAULT NULL,
  `change_eta_destination_port` varchar(255) DEFAULT '0000-00-00',
  `change_total_transit_time` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contracts_do`
--

INSERT INTO `contracts_do` (`Id`, `contract_id`, `shipment_id`, `do_no`, `do_date`, `do_exp_date`, `forwarder_id`, `forwarder_name`, `shipment_line`, `freight`, `total_freight`, `thc`, `blc`, `vessel_name`, `voyage_no`, `eta_origin`, `etd_origin`, `eta_destination`, `eta_date`, `total_transit`, `vgm_cutoff`, `container_gate_open`, `gate_cutoff`, `document_cutoff`, `si_cutoff`, `trans_port`, `trans_vessel_name`, `trans_eta`, `trans_etd`, `stuffing_from`, `stuffing_to`, `do_port_loading`, `do_port_discharge`, `do_quotation_freight`, `do_container_size`, `total_detention_days`, `total_demurrage_days`, `change_eta_destination_port`, `change_total_transit_time`, `created_at`, `updated_at`) VALUES
(46, 8, 8, '', '2016-12-20 07:00:00', '2016-12-22 07:00:00', 3, NULL, 'UASC', '$810+$25 ENS+INR 450 SEAL CHARGE', '123', '7450', '2450', 'CAM CGM THAMES', '045W', '2017-01-02 07:00:00', '2017-01-03 07:00:00', '2017-02-04 07:00:00', '0000-00-00 00:00:00', '29 days', '0000-00-00 00:00:00', '2016-12-27 00:43:00', '2017-01-01 00:43:00', '2017-01-01 00:43:00', '2017-01-01 00:43:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2016-12-28 00:19:06', '2016-12-28 00:44:39'),
(49, 11, 11, '', '2017-01-10 07:00:00', '2017-01-12 07:00:00', 3, NULL, 'EMIRATES', '$ 700', '4564', '9000', '3000', 'MAERSK INDUS', '1702', '2017-01-15 07:00:00', '2017-01-16 07:00:00', '2017-01-24 07:00:00', '0000-00-00 00:00:00', '8 days', '2017-01-12 21:21:00', '2017-01-09 21:21:00', '2017-01-12 21:22:00', '2017-01-12 21:22:00', '2017-01-12 21:22:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-01-09', '2017-01-11', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2017-01-10 21:19:15', '2017-01-10 21:50:53'),
(50, 12, 12, '', '2017-01-10 07:00:00', '2017-01-12 07:00:00', 3, NULL, 'EMIRATES', '650', '650', '7800', '2500', 'EMIRATES', '123', '2017-01-15 07:00:00', '2017-01-16 07:00:00', '2017-01-26 07:00:00', '0000-00-00 00:00:00', '10 days', '2017-01-13 01:24:00', '2017-01-10 01:24:00', '2017-01-13 01:24:00', '2017-01-13 01:24:00', '2017-01-13 01:24:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-01-10', '2017-01-11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-11 01:14:52', '2017-01-11 01:25:08'),
(51, 12, 13, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-11 01:19:28', '2017-01-11 01:19:28'),
(52, 12, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-11 01:19:53', '2017-01-11 01:19:53'),
(53, 13, 15, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-22 19:36:27', '2017-01-22 19:36:27'),
(54, 14, 16, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-22 19:51:38', '2017-01-22 19:51:38'),
(55, 15, 17, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-23 00:14:17', '2017-01-23 00:14:17'),
(56, 16, 18, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-25 17:53:46', '2017-01-25 17:53:46'),
(57, 17, 19, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-25 18:16:49', '2017-01-25 18:16:49'),
(58, 18, 20, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-25 18:30:40', '2017-01-25 18:30:40'),
(59, 19, 21, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-25 22:11:23', '2017-01-25 22:11:23'),
(60, 20, 22, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-25 22:43:56', '2017-01-25 22:43:56'),
(62, 22, 24, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-26 01:41:42', '2017-01-26 01:41:42'),
(63, 23, 25, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-26 16:24:56', '2017-01-26 16:24:56'),
(64, 24, 26, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-01-26 16:27:23', '2017-01-26 16:27:23'),
(65, 25, 27, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 17:09:35', '2017-02-01 17:09:35'),
(66, 26, 28, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 17:18:26', '2017-02-01 17:18:26'),
(67, 27, 29, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 17:21:41', '2017-02-01 17:21:41'),
(68, 28, 30, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 17:39:46', '2017-02-01 17:39:46'),
(69, 29, 31, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 17:45:35', '2017-02-01 17:45:35'),
(70, 30, 32, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 18:41:26', '2017-02-01 18:41:26'),
(71, 31, 33, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 18:48:42', '2017-02-01 18:48:42'),
(72, 32, 34, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 18:57:22', '2017-02-01 18:57:22'),
(73, 33, 35, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-01 19:07:09', '2017-02-01 19:07:09'),
(74, 34, 36, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 17:06:15', '2017-02-02 17:06:15'),
(75, 35, 37, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 17:42:48', '2017-02-02 17:42:48'),
(76, 36, 38, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 17:51:09', '2017-02-02 17:51:09'),
(77, 37, 39, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 17:59:53', '2017-02-02 17:59:53'),
(78, 38, 40, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 18:10:34', '2017-02-02 18:10:34'),
(79, 39, 41, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 22:10:24', '2017-02-02 22:10:24'),
(80, 40, 42, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 22:27:23', '2017-02-02 22:27:23'),
(81, 41, 43, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 22:36:35', '2017-02-02 22:36:35'),
(82, 42, 44, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 22:40:17', '2017-02-02 22:40:17'),
(83, 43, 45, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 22:47:58', '2017-02-02 22:47:58'),
(84, 44, 46, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 22:56:28', '2017-02-02 22:56:28'),
(85, 45, 47, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 23:01:30', '2017-02-02 23:01:30'),
(86, 46, 48, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 23:07:39', '2017-02-02 23:07:39'),
(87, 47, 49, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 23:10:53', '2017-02-02 23:10:53'),
(88, 48, 50, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 23:15:58', '2017-02-02 23:15:58'),
(89, 49, 51, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 23:28:34', '2017-02-02 23:28:34'),
(90, 50, 52, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-02 23:32:07', '2017-02-02 23:32:07'),
(91, 51, 53, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-03 02:03:34', '2017-02-03 02:03:34'),
(92, 52, 54, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-03 02:06:29', '2017-02-03 02:06:29'),
(93, 53, 55, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-03 02:08:24', '2017-02-03 02:08:24'),
(94, 54, 56, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-03 02:15:46', '2017-02-03 02:15:46'),
(95, 55, 57, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-04 16:54:13', '2017-02-04 16:54:13'),
(96, 56, 58, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-04 22:47:56', '2017-02-04 22:47:56'),
(97, 57, 59, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-04 22:51:03', '2017-02-04 22:51:03'),
(98, 58, 60, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-04 22:54:20', '2017-02-04 22:54:20'),
(99, 59, 61, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-06 18:42:51', '2017-02-06 18:42:51'),
(100, 60, 62, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-07 17:16:16', '2017-02-07 17:16:16'),
(101, 61, 63, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-07 17:21:11', '2017-02-07 17:21:11'),
(102, 62, 64, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-10 01:02:22', '2017-02-10 01:02:22'),
(103, 63, 65, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-10 01:06:27', '2017-02-10 01:06:27'),
(104, 64, 66, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-10 16:55:52', '2017-02-10 16:55:52'),
(105, 65, 67, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-11 16:32:08', '2017-02-11 16:32:08'),
(106, 66, 68, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-11 16:35:21', '2017-02-11 16:35:21'),
(107, 67, 69, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-14 22:31:12', '2017-02-14 22:31:12'),
(108, 68, 70, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-14 22:40:20', '2017-02-14 22:40:20'),
(109, 69, 71, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-14 22:44:53', '2017-02-14 22:44:53'),
(110, 70, 72, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-14 22:49:57', '2017-02-14 22:49:57'),
(111, 71, 73, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-21 17:53:36', '2017-02-21 17:53:36'),
(112, 72, 74, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-21 17:55:44', '2017-02-21 17:55:44'),
(113, 73, 75, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-21 23:04:30', '2017-02-21 23:04:30'),
(114, 74, 76, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-21 23:05:39', '2017-02-21 23:05:39'),
(115, 75, 77, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-22 23:15:54', '2017-02-22 23:15:54'),
(116, 76, 78, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-22 23:19:03', '2017-02-22 23:19:03'),
(117, 77, 79, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-22 23:22:48', '2017-02-22 23:22:48'),
(118, 78, 80, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 17:10:15', '2017-02-24 17:10:15'),
(119, 79, 81, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 17:18:53', '2017-02-24 17:18:53'),
(120, 80, 82, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 17:28:50', '2017-02-24 17:28:50'),
(121, 81, 83, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 18:20:01', '2017-02-24 18:20:01'),
(122, 82, 84, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 18:45:31', '2017-02-24 18:45:31'),
(123, 83, 85, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 18:48:48', '2017-02-24 18:48:48'),
(124, 84, 86, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 19:05:58', '2017-02-24 19:05:58'),
(125, 85, 87, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 19:10:02', '2017-02-24 19:10:02'),
(126, 86, 88, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 22:30:05', '2017-02-24 22:30:05'),
(127, 87, 89, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 22:34:45', '2017-02-24 22:34:45'),
(128, 88, 90, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 22:37:45', '2017-02-24 22:37:45'),
(129, 89, 91, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 22:40:55', '2017-02-24 22:40:55'),
(130, 90, 92, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 22:46:29', '2017-02-24 22:46:29'),
(131, 91, 93, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-24 22:49:33', '2017-02-24 22:49:33'),
(132, 92, 94, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-28 22:54:32', '2017-02-28 22:54:32'),
(133, 93, 95, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-01 23:23:18', '2017-03-01 23:23:18'),
(134, 94, 96, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-01 23:24:40', '2017-03-01 23:24:40'),
(135, 95, 97, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-03 16:28:44', '2017-03-03 16:28:44'),
(136, 96, 98, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-03 16:34:23', '2017-03-03 16:34:23'),
(137, 97, 99, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-03 22:43:10', '2017-03-03 22:43:10'),
(138, 98, 100, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-04 16:22:12', '2017-03-04 16:22:12'),
(139, 99, 101, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-04 16:39:06', '2017-03-04 16:39:06');
INSERT INTO `contracts_do` (`Id`, `contract_id`, `shipment_id`, `do_no`, `do_date`, `do_exp_date`, `forwarder_id`, `forwarder_name`, `shipment_line`, `freight`, `total_freight`, `thc`, `blc`, `vessel_name`, `voyage_no`, `eta_origin`, `etd_origin`, `eta_destination`, `eta_date`, `total_transit`, `vgm_cutoff`, `container_gate_open`, `gate_cutoff`, `document_cutoff`, `si_cutoff`, `trans_port`, `trans_vessel_name`, `trans_eta`, `trans_etd`, `stuffing_from`, `stuffing_to`, `do_port_loading`, `do_port_discharge`, `do_quotation_freight`, `do_container_size`, `total_detention_days`, `total_demurrage_days`, `change_eta_destination_port`, `change_total_transit_time`, `created_at`, `updated_at`) VALUES
(140, 100, 102, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-04 16:44:52', '2017-03-04 16:44:52'),
(141, 101, 103, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-11 06:45:22', '2017-03-11 06:45:22'),
(142, 102, 104, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-16 23:49:02', '2017-03-16 23:49:02'),
(143, 103, 105, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-16 23:52:47', '2017-03-16 23:52:47'),
(144, 104, 106, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-16 23:58:09', '2017-03-16 23:58:09'),
(145, 105, 107, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:04:50', '2017-03-17 00:04:50'),
(146, 106, 108, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:06:54', '2017-03-17 00:06:54'),
(147, 107, 109, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:08:28', '2017-03-17 00:08:28'),
(148, 108, 110, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:10:09', '2017-03-17 00:10:09'),
(149, 109, 111, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:13:14', '2017-03-17 00:13:14'),
(150, 110, 112, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:14:55', '2017-03-17 00:14:55'),
(151, 111, 113, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:18:20', '2017-03-17 00:18:20'),
(152, 112, 114, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:22:22', '2017-03-17 00:22:22'),
(153, 113, 115, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:25:31', '2017-03-17 00:25:31'),
(154, 114, 116, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:28:52', '2017-03-17 00:28:52'),
(155, 115, 117, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:33:47', '2017-03-17 00:33:47'),
(156, 116, 118, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:37:04', '2017-03-17 00:37:04'),
(157, 117, 119, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-17 00:38:34', '2017-03-17 00:38:34'),
(158, 118, 120, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-18 00:03:27', '2017-03-18 00:03:27'),
(159, 119, 121, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-18 01:27:19', '2017-03-18 01:27:19'),
(160, 120, 122, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-19 16:30:57', '2017-03-19 16:30:57'),
(161, 121, 123, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-19 16:37:57', '2017-03-19 16:37:57'),
(162, 122, 124, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-19 16:43:01', '2017-03-19 16:43:01'),
(163, 123, 125, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-19 16:49:24', '2017-03-19 16:49:24'),
(164, 124, 126, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-19 16:53:16', '2017-03-19 16:53:16'),
(165, 125, 127, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-23 16:52:52', '2017-03-23 16:52:52'),
(166, 126, 128, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-23 16:55:33', '2017-03-23 16:55:33'),
(167, 127, 129, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-23 16:58:48', '2017-03-23 16:58:48'),
(168, 128, 130, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-23 17:01:10', '2017-03-23 17:01:10'),
(169, 129, 131, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-23 17:03:27', '2017-03-23 17:03:27'),
(170, 130, 132, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-23 17:05:33', '2017-03-23 17:05:33'),
(171, 131, 133, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-23 17:07:13', '2017-03-23 17:07:13'),
(172, 132, 134, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-23 17:09:12', '2017-03-23 17:09:12'),
(173, 133, 135, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-24 18:55:46', '2017-03-24 18:55:46'),
(174, 134, 136, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-05 02:17:38', '2017-04-05 02:17:38'),
(175, 135, 137, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-05 02:23:10', '2017-04-05 02:23:10'),
(176, 136, 138, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 18:52:52', '2017-04-06 18:52:52'),
(177, 137, 139, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:00:34', '2017-04-06 19:00:34'),
(178, 138, 140, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:02:46', '2017-04-06 19:02:46'),
(179, 139, 141, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:04:27', '2017-04-06 19:04:27'),
(180, 140, 142, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:06:47', '2017-04-06 19:06:47'),
(181, 141, 143, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:07:02', '2017-04-06 19:07:02'),
(182, 142, 144, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:15:50', '2017-04-06 19:15:50'),
(183, 143, 145, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:19:34', '2017-04-06 19:19:34'),
(184, 144, 146, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:22:40', '2017-04-06 19:22:40'),
(185, 145, 147, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:24:21', '2017-04-06 19:24:21'),
(186, 146, 148, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 19:27:30', '2017-04-06 19:27:30'),
(187, 147, 149, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 22:25:50', '2017-04-06 22:25:50'),
(188, 148, 150, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 22:57:40', '2017-04-06 22:57:40'),
(189, 149, 151, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:06:40', '2017-04-06 23:06:40'),
(190, 150, 152, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:13:50', '2017-04-06 23:13:50'),
(191, 151, 153, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:21:00', '2017-04-06 23:21:00'),
(192, 152, 154, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:25:50', '2017-04-06 23:25:50'),
(193, 153, 155, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:27:10', '2017-04-06 23:27:10'),
(194, 154, 156, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:28:05', '2017-04-06 23:28:05'),
(195, 155, 157, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:33:46', '2017-04-06 23:33:46'),
(196, 156, 158, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:36:09', '2017-04-06 23:36:09'),
(197, 157, 159, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:43:22', '2017-04-06 23:43:22'),
(198, 158, 160, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-06 23:45:59', '2017-04-06 23:45:59'),
(199, 159, 161, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-11 16:28:02', '2017-04-11 16:28:02'),
(200, 160, 162, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-11 16:33:50', '2017-04-11 16:33:50'),
(201, 161, 163, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-11 16:37:24', '2017-04-11 16:37:24'),
(202, 162, 164, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-11 16:40:05', '2017-04-11 16:40:05'),
(203, 163, 165, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-11 16:43:02', '2017-04-11 16:43:02'),
(204, 164, 166, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-11 16:48:59', '2017-04-11 16:48:59'),
(206, 166, 168, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-16 17:47:37', '2017-04-16 17:47:37'),
(207, 167, 169, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-16 17:50:52', '2017-04-16 17:50:52'),
(208, 168, 170, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-17 17:04:44', '2017-04-17 17:04:44'),
(209, 169, 171, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-17 17:08:51', '2017-04-17 17:08:51'),
(210, 170, 172, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-17 17:12:08', '2017-04-17 17:12:08'),
(211, 171, 173, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-17 17:14:16', '2017-04-17 17:14:16'),
(212, 172, 174, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-17 17:19:26', '2017-04-17 17:19:26'),
(213, 173, 175, '', '2017-04-20 07:00:00', '2017-04-29 07:00:00', 4, NULL, 'NYK', '$1569', '456', '7800', '2500', 'TEMPANOS_7313', '', '2017-05-04 07:00:00', '2017-05-04 07:00:00', '2017-05-30 07:00:00', '0000-00-00 00:00:00', '26 days', '2017-04-29 19:06:00', '2017-04-27 19:06:00', '2017-04-29 19:06:00', '2017-04-29 19:06:00', '2017-04-29 19:06:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-21 22:06:41', '2017-04-27 19:08:27'),
(214, 174, 176, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-21 22:08:03', '2017-04-21 22:08:03'),
(215, 175, 177, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-24 17:05:14', '2017-04-24 17:05:14'),
(216, 176, 178, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-24 17:26:42', '2017-04-24 17:26:42'),
(217, 177, 179, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-24 17:45:26', '2017-04-24 17:45:26'),
(218, 178, 180, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-24 18:04:30', '2017-04-24 18:04:30'),
(219, 179, 181, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-24 18:15:18', '2017-04-24 18:15:18'),
(220, 180, 182, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', '5', 'ALEXANDRIA OLD PORT', '[\"3\",\"5\"]', '[\"4\",\"7\"]', '', '', '0000-00-00', '', '2017-04-24 18:45:17', '2017-06-16 10:34:11'),
(221, 181, 183, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-24 19:07:06', '2017-04-24 19:07:06'),
(222, 182, 184, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-25 23:54:15', '2017-04-25 23:54:15'),
(223, 183, 185, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-25 23:54:50', '2017-04-25 23:54:50'),
(224, 184, 186, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-25 23:57:04', '2017-04-25 23:57:04'),
(225, 185, 187, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-27 18:56:11', '2017-04-27 18:56:11'),
(226, 186, 188, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-27 18:57:52', '2017-04-27 18:57:52'),
(227, 187, 189, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-27 19:01:55', '2017-04-27 19:01:55'),
(228, 188, 190, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-27 19:04:31', '2017-04-27 19:04:31'),
(229, 189, 191, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-27 19:09:48', '2017-04-27 19:09:48'),
(230, 190, 192, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-27 19:11:26', '2017-04-27 19:11:26'),
(231, 191, 193, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-27 19:12:57', '2017-04-27 19:12:57');
INSERT INTO `contracts_do` (`Id`, `contract_id`, `shipment_id`, `do_no`, `do_date`, `do_exp_date`, `forwarder_id`, `forwarder_name`, `shipment_line`, `freight`, `total_freight`, `thc`, `blc`, `vessel_name`, `voyage_no`, `eta_origin`, `etd_origin`, `eta_destination`, `eta_date`, `total_transit`, `vgm_cutoff`, `container_gate_open`, `gate_cutoff`, `document_cutoff`, `si_cutoff`, `trans_port`, `trans_vessel_name`, `trans_eta`, `trans_etd`, `stuffing_from`, `stuffing_to`, `do_port_loading`, `do_port_discharge`, `do_quotation_freight`, `do_container_size`, `total_detention_days`, `total_demurrage_days`, `change_eta_destination_port`, `change_total_transit_time`, `created_at`, `updated_at`) VALUES
(232, 192, 194, 'gsag', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, NULL, '', '123', '123', '', '', '', '', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '0 days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, '', '', '2017-08-12', '', '2017-04-27 19:14:37', '2017-08-12 04:35:01'),
(244, 236, 238, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(245, 236, 239, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(246, 236, 240, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(247, 236, 241, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(248, 237, 242, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(249, 237, 243, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(250, 237, 244, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(251, 237, 245, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(252, 238, 246, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:19:22', '2017-07-06 04:19:22'),
(253, 238, 247, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:19:22', '2017-07-06 04:19:22'),
(254, 238, 248, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:19:23', '2017-07-06 04:19:23'),
(255, 238, 249, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:19:23', '2017-07-06 04:19:23'),
(256, 239, 250, '', '2017-07-25 18:30:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '123', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:22:13', '2017-07-06 04:22:13'),
(257, 239, 251, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:22:13', '2017-07-06 04:22:13'),
(258, 239, 252, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:22:14', '2017-07-06 04:22:14'),
(259, 239, 253, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 04:22:14', '2017-07-06 04:22:14'),
(264, 241, 258, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 05:38:22', '2017-07-06 05:38:22'),
(265, 241, 259, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 05:38:22', '2017-07-06 05:38:22'),
(266, 241, 260, '', '2017-07-24 18:30:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 05:38:22', '2017-07-06 05:38:22'),
(267, 241, 261, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-06 05:38:22', '2017-07-06 05:38:22'),
(272, 243, 266, '', '2017-07-24 18:30:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:11:43', '2017-07-07 04:11:43'),
(273, 243, 267, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:11:43', '2017-07-07 04:11:43'),
(274, 243, 268, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:11:43', '2017-07-07 04:11:43'),
(275, 243, 269, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:11:43', '2017-07-07 04:11:43'),
(276, 244, 270, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:19:30', '2017-07-07 04:19:30'),
(277, 244, 271, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:19:30', '2017-07-07 04:19:30'),
(278, 244, 272, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:19:30', '2017-07-07 04:19:30'),
(279, 244, 273, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:19:30', '2017-07-07 04:19:30'),
(280, 245, 274, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(281, 245, 275, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(282, 245, 276, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(283, 245, 277, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(284, 246, 278, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 05:20:32', '2017-07-07 05:20:32'),
(285, 246, 279, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(286, 246, 280, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(287, 246, 281, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(288, 247, 282, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(289, 247, 283, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(290, 247, 284, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(291, 247, 285, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 05:32:21', '2017-07-07 05:32:21'),
(292, 248, 286, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 06:31:18', '2017-07-07 06:31:18'),
(293, 249, 287, 'sad', '2017-07-06 18:30:00', '2017-07-06 18:30:00', 3, NULL, 'dsad', 'sad', '123', 'sadsd', 'sadsa', 'sdadsa', '', '2017-07-06 18:30:00', '2017-07-06 18:30:00', '2017-07-06 18:30:00', '0000-00-00 00:00:00', '0 days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-07-07 08:37:00', '2017-07-07 08:37:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-07-07', '2017-07-07', NULL, NULL, NULL, NULL, '2', '2', '2017-07-07', 'sdsaddas', '2017-07-07 06:32:58', '2017-07-07 08:38:10'),
(294, 250, 288, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(295, 250, 289, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(296, 250, 290, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(297, 250, 291, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-07 07:21:18', '2017-07-07 07:21:18'),
(298, 251, 292, 'aSas', '2017-07-06 18:30:00', '2017-07-06 18:30:00', 4, NULL, 'AS', 'sAS', '12312', 'SAsa', 'asa', '123sad', '', '2017-07-06 18:30:00', '2017-07-06 18:30:00', '2017-07-06 18:30:00', '0000-00-00 00:00:00', '0 days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-07-07 09:01:00', '2017-07-07 09:01:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-07-07', '2017-07-07', NULL, NULL, NULL, NULL, 'aSas', 'asAS', '2017-07-07', 'ASas', '2017-07-07 09:00:39', '2017-07-07 09:01:28'),
(299, 252, 293, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 04:25:00', '2017-07-08 04:25:00'),
(300, 253, 294, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 04:25:29', '2017-07-08 04:25:29'),
(301, 254, 295, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 04:27:18', '2017-07-08 04:27:18'),
(302, 255, 296, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(303, 255, 297, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(304, 255, 298, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(305, 255, 299, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(310, 257, 304, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(311, 257, 305, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(312, 257, 306, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(313, 258, 307, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(314, 258, 308, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(315, 258, 309, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(316, 258, 310, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:51:24', '2017-07-08 05:51:24'),
(317, 259, 311, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:59:17', '2017-07-08 05:59:17'),
(318, 259, 312, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 05:59:17', '2017-07-08 05:59:17'),
(319, 260, 313, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(320, 260, 314, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(321, 260, 315, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(322, 260, 316, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(323, 261, 317, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:44:47', '2017-07-08 06:44:47'),
(324, 261, 318, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:44:48', '2017-07-08 06:44:48'),
(325, 261, 319, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:44:48', '2017-07-08 06:44:48'),
(326, 261, 320, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:44:48', '2017-07-08 06:44:48'),
(331, 263, 325, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:49:30', '2017-07-08 06:49:30'),
(332, 263, 326, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 06:49:30', '2017-07-08 06:49:30'),
(333, 264, 327, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(334, 264, 328, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(335, 264, 329, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(336, 264, 330, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 07:29:40', '2017-07-08 07:29:40'),
(337, 12, 331, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 07:41:38', '2017-07-08 07:41:38'),
(338, 265, 332, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, NULL, '2', '123', '123', '', '', '', '', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '0 days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-08-12', '2017-08-14', NULL, NULL, NULL, NULL, '', '', '0000-00-00', '', '2017-07-08 07:45:49', '2017-08-12 05:03:55'),
(339, 265, 333, '12', '2017-08-11 18:30:00', '2017-08-11 18:30:00', 4, NULL, 'H12', '123', '123', '452', '4542', '1352125', '', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '2017-08-11 18:30:00', '0 days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-12 09:36:00', '2017-08-12 09:36:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-08-12', '2017-08-20', NULL, NULL, NULL, NULL, '', '', '0000-00-00', '', '2017-07-08 07:45:49', '2017-08-12 09:36:26'),
(340, 265, 334, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 07:45:50', '2017-07-08 07:45:50'),
(341, 265, 335, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 07:45:50', '2017-07-08 07:45:50'),
(342, 266, 336, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(343, 266, 337, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(344, 266, 338, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(345, 267, 339, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-08 08:04:22', '2017-07-08 08:04:22');
INSERT INTO `contracts_do` (`Id`, `contract_id`, `shipment_id`, `do_no`, `do_date`, `do_exp_date`, `forwarder_id`, `forwarder_name`, `shipment_line`, `freight`, `total_freight`, `thc`, `blc`, `vessel_name`, `voyage_no`, `eta_origin`, `etd_origin`, `eta_destination`, `eta_date`, `total_transit`, `vgm_cutoff`, `container_gate_open`, `gate_cutoff`, `document_cutoff`, `si_cutoff`, `trans_port`, `trans_vessel_name`, `trans_eta`, `trans_etd`, `stuffing_from`, `stuffing_to`, `do_port_loading`, `do_port_discharge`, `do_quotation_freight`, `do_container_size`, `total_detention_days`, `total_demurrage_days`, `change_eta_destination_port`, `change_total_transit_time`, `created_at`, `updated_at`) VALUES
(346, 267, 340, 'sadsad', '2017-07-07 18:30:00', '2017-07-07 18:30:00', 4, NULL, 'sad', 'sdsad', '', 'sadasd', 'sad', 'sadasd', '', '2017-07-07 18:30:00', '2017-07-07 18:30:00', '2017-07-07 18:30:00', '0000-00-00 00:00:00', '0 days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-16 11:04:00', '2017-07-08 08:05:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-07-08', '2017-07-08', NULL, NULL, NULL, NULL, 'sadasd', 'asdasd', '2017-07-08', 'sdasadsad', '2017-07-08 08:04:22', '2017-08-16 11:04:27'),
(347, 268, 341, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-11 10:02:45', '2017-07-11 10:02:45'),
(348, 268, 342, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-11 10:02:45', '2017-07-11 10:02:45'),
(350, 270, 344, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-12 09:41:48', '2017-07-12 09:41:48'),
(351, 270, 345, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-12 09:41:49', '2017-07-12 09:41:49'),
(352, 270, 346, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-12 09:41:49', '2017-07-12 09:41:49'),
(353, 270, 347, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-12 09:41:49', '2017-07-12 09:41:49'),
(354, 271, 348, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-12 10:33:04', '2017-07-12 10:33:04'),
(355, 271, 349, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, NULL, '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-25 18:30:00', 'NaN days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, '', '', '0000-00-00', '', '2017-07-12 10:33:05', '2017-08-26 06:52:45'),
(356, 272, 350, 'as', '2017-07-13 18:30:00', '2017-07-13 18:30:00', 4, NULL, 'As', '3213', '123', 'as', 'SA', 'saAS', '', '2017-07-02 18:30:00', '2017-07-08 18:30:00', '2017-07-04 18:30:00', '0000-00-00 00:00:00', '-4 days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-07-14 07:47:00', '2017-07-14 07:47:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-07-14', '2017-07-15', NULL, NULL, NULL, NULL, 'As', 'as', '2017-07-21', 'AsaSas', '2017-07-12 11:13:51', '2017-07-14 07:47:31'),
(357, 273, 351, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-17 04:05:54', '2017-07-17 04:05:54'),
(358, 274, 352, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, NULL, '', '', '12', '', '', '15242421', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-10 18:30:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, '', '', '0000-00-00', '', '2017-07-19 05:20:43', '2017-08-11 04:09:47'),
(359, 274, 353, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '121', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-19 05:20:43', '2017-07-19 05:20:43'),
(360, 274, 354, '', '2017-07-22 18:30:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '15', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-19 05:20:43', '2017-07-19 05:20:43'),
(361, 274, 355, '', '2017-07-23 18:30:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '454', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-07-19 05:20:43', '2017-07-19 05:20:43'),
(368, 279, 362, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, NULL, '', '', '', '', '', '23423', '', '2017-08-02 18:30:00', '2017-08-02 18:30:00', '2017-08-03 18:30:00', '0000-00-00 00:00:00', '1 days', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '2017-08-03', '2017-08-03', NULL, NULL, NULL, NULL, '3424', '324', '0000-00-00', '', '2017-08-03 05:44:02', '2017-08-03 05:47:06'),
(369, 279, 363, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(370, 279, 364, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(371, 279, 365, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(390, 279, 346, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-21 08:05:40', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-08-21 08:05:40', '2017-08-21 08:05:40'),
(391, 279, 347, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-21 08:05:40', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-08-21 08:05:40', '2017-08-21 08:05:40'),
(402, 284, 358, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 05:51:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(403, 284, 359, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 05:51:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(404, 284, 360, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 05:51:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(405, 284, 361, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 05:51:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(406, 286, 362, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 05:59:00', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 05:59:00', '2017-09-04 05:59:00'),
(407, 286, 363, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 05:59:01', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(408, 286, 364, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 05:59:01', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(409, 286, 365, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 05:59:01', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(414, 288, 370, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:17:22', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:17:22', '2017-09-04 06:17:22'),
(415, 289, 371, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:19:48', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(416, 289, 372, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:19:48', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(417, 290, 373, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:22:30', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(418, 290, 374, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:22:30', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(419, 291, 375, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:24:35', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:24:35', '2017-09-04 06:24:35'),
(420, 291, 376, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:24:36', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:24:36', '2017-09-04 06:24:36'),
(421, 292, 377, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:27:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:27:58', '2017-09-04 06:27:58'),
(422, 292, 378, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:27:59', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:27:59', '2017-09-04 06:27:59'),
(423, 292, 379, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:27:59', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:27:59', '2017-09-04 06:27:59'),
(424, 293, 380, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:30:38', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:30:38', '2017-09-04 06:30:38'),
(425, 293, 381, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:30:39', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(426, 293, 382, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:30:39', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(427, 293, 383, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:30:39', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(428, 294, 384, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:34:57', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(429, 294, 385, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:34:57', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(430, 294, 386, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:34:57', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(431, 294, 387, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:34:57', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(432, 295, 388, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:38:37', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(433, 295, 389, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:38:37', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(434, 295, 390, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:38:37', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(435, 295, 391, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:38:37', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(436, 296, 392, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:47:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(437, 296, 393, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:47:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(438, 296, 394, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:47:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(439, 296, 395, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-04 06:47:58', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '[\"\",\"\",\"\",\"\"]', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '2017-09-04 06:47:58', '2017-09-04 06:47:58');

-- --------------------------------------------------------

--
-- Table structure for table `contracts_docs`
--

CREATE TABLE `contracts_docs` (
  `Id` int(11) NOT NULL,
  `company_id` int(5) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filetype` varchar(20) NOT NULL,
  `extension` varchar(255) NOT NULL DEFAULT '',
  `size` varchar(255) NOT NULL DEFAULT '',
  `location` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contracts_docs`
--

INSERT INTO `contracts_docs` (`Id`, `company_id`, `contract_id`, `filename`, `filetype`, `extension`, `size`, `location`, `created_at`, `updated_at`) VALUES
(1, 1, 8, 'PO. stkoth17-013.pdf', 'application/pdf', 'pdf', '281157', '2016/12/C8 (586255327a902) PO. stkoth17-013.pdf', '2016-12-28 00:19:06', '2016-12-28 00:19:06'),
(2, 1, 8, 'Castor oil  Pictogram .xlsx', 'application/vnd.open', 'xlsx', '19598', '2016/12/C8 (5862588500fb2) Castor oil  Pictogram .xlsx', '2016-12-28 00:33:17', '2016-12-28 00:33:17'),
(3, 1, 8, 'costing .pdf', 'application/pdf', 'pdf', '34195', '2016/12/C8 (5862588566996) costing .pdf', '2016-12-28 00:33:17', '2016-12-28 00:33:17'),
(4, 1, 8, 'PI_343_STAR ASIA _COFSG_.pdf', 'application/pdf', 'pdf', '132041', '2016/12/C8 (5862589179715) PI_343_STAR ASIA _COFSG_.pdf', '2016-12-28 00:33:29', '2016-12-28 00:33:29'),
(6, 1, 11, 'PI_337_BAALBAKI GROUP_COFSG_09.01.2017.pdf', 'application/pdf', 'pdf', '135934', '2017/01/C11 (5874a00b72383) PI_337_BAALBAKI GROUP_COFSG_09.01.2017.pdf', '2017-01-10 21:19:15', '2017-01-10 21:19:15'),
(7, 1, 12, 'PI_337_BAALBAKI GROUP_COFSG_09.01.2017.pdf', 'application/pdf', 'pdf', '135934', '2017/01/C12 (5874d74466272) PI_337_BAALBAKI GROUP_COFSG_09.01.2017.pdf', '2017-01-11 01:14:52', '2017-01-11 01:14:52'),
(8, 1, 15, 'B J HONG_398.pdf', 'application/pdf', 'pdf', '236993', '2017/01/C15 (58849b1160357) B J HONG_398.pdf', '2017-01-23 00:14:17', '2017-01-23 00:14:17'),
(9, 1, 16, 'FSG Purchase conditions documentary and banking instructions (2).docx', 'application/vnd.open', 'docx', '16697', '2017/01/C16 (588836624b0eb) FSG Purchase conditions documentary and banking instructions (2).docx', '2017-01-25 17:53:46', '2017-01-25 17:53:46'),
(10, 1, 28, 'Scan.pdf', 'application/pdf', 'pdf', '641245', '2017/02/C28 (58916d9a1d87e) Scan.pdf', '2017-02-01 17:39:46', '2017-02-01 17:39:46'),
(11, 1, 29, 'Scan0002.pdf', 'application/pdf', 'pdf', '291433', '2017/02/C29 (58916ef72c378) Scan0002.pdf', '2017-02-01 17:45:35', '2017-02-01 17:45:35'),
(12, 1, 30, '94080.pdf', 'application/pdf', 'pdf', '62564', '2017/02/C30 (58917c0e04a84) 94080.pdf', '2017-02-01 18:41:26', '2017-02-01 18:41:26'),
(13, 1, 31, '2000211.pdf', 'application/pdf', 'pdf', '570696', '2017/02/C31 (58917dc24585a) 2000211.pdf', '2017-02-01 18:48:42', '2017-02-01 18:48:42'),
(14, 1, 33, '81012 OP HCO and H12 NSW-signed.pdf', 'application/pdf', 'pdf', '214550', '2017/02/C33 (58918215e03bb) 81012 OP HCO and H12 NSW-signed.pdf', '2017-02-01 19:07:09', '2017-02-01 19:07:09'),
(15, 1, 36, 'OC 4500110854.pdf', 'application/pdf', 'pdf', '20693', '2017/02/C36 (5892c1c5e6ebf) OC 4500110854.pdf', '2017-02-02 17:51:09', '2017-02-02 17:51:09'),
(16, 1, 38, 'SIGNED PI GIRNAR EGY.pdf', 'application/pdf', 'pdf', '79461', '2017/02/C38 (5892c65257c21) SIGNED PI GIRNAR EGY.pdf', '2017-02-02 18:10:34', '2017-02-02 18:10:34'),
(17, 1, 40, 'XF021contractS.pdf', 'application/pdf', 'pdf', '612225', '2017/02/C40 (58930283a49e0) XF021contractS.pdf', '2017-02-02 22:27:23', '2017-02-02 22:27:23'),
(18, 1, 41, 'XF020contractS.pdf', 'application/pdf', 'pdf', '659444', '2017/02/C41 (589304ab0eef7) XF020contractS.pdf', '2017-02-02 22:36:35', '2017-02-02 22:36:35'),
(19, 1, 42, 'XF018contractS.pdf', 'application/pdf', 'pdf', '612916', '2017/02/C42 (589305896fb8b) XF018contractS.pdf', '2017-02-02 22:40:17', '2017-02-02 22:40:17'),
(20, 1, 43, 'B J HONG_383.pdf', 'application/pdf', 'pdf', '238420', '2017/02/C43 (5893075691e0d) B J HONG_383.pdf', '2017-02-02 22:47:58', '2017-02-02 22:47:58'),
(21, 1, 44, 'OOO NORTEX_380.pdf', 'application/pdf', 'pdf', '179753', '2017/02/C44 (5893095496ad2) OOO NORTEX_380.pdf', '2017-02-02 22:56:28', '2017-02-02 22:56:28'),
(22, 1, 45, 'PO J1051112-1-signed.pdf', 'application/pdf', 'pdf', '326937', '2017/02/C45 (58930a82ade3b) PO J1051112-1-signed.pdf', '2017-02-02 23:01:30', '2017-02-02 23:01:30'),
(23, 1, 46, 'Girnar.pdf', 'application/pdf', 'pdf', '514509', '2017/02/C46 (58930bf3a7d5c) Girnar.pdf', '2017-02-02 23:07:39', '2017-02-02 23:07:39'),
(24, 1, 47, 'XF007contractS.pdf', 'application/pdf', 'pdf', '643886', '2017/02/C47 (58930cb55cbd4) XF007contractS.pdf', '2017-02-02 23:10:53', '2017-02-02 23:10:53'),
(25, 1, 48, 'XF006contractS.pdf', 'application/pdf', 'pdf', '721582', '2017/02/C48 (58930de61a93f) XF006contractS.pdf', '2017-02-02 23:15:58', '2017-02-02 23:15:58'),
(26, 1, 49, 'PurchasingOrder_GI122116-signed.pdf', 'application/pdf', 'pdf', '195440', '2017/02/C49 (589310da72db9) PurchasingOrder_GI122116-signed.pdf', '2017-02-02 23:28:34', '2017-02-02 23:28:34'),
(27, 1, 56, 'doc07267120170202164301.pdf', 'application/pdf', 'pdf', '1698384', '2017/02/C56 (5895aa54d44c2) doc07267120170202164301.pdf', '2017-02-04 22:47:59', '2017-02-04 22:47:59'),
(28, 1, 57, 'doc07267120170202164301.pdf', 'application/pdf', 'pdf', '1656966', '2017/02/C57 (5895ab0fcb2bf) doc07267120170202164301.pdf', '2017-02-04 22:51:05', '2017-02-04 22:51:05'),
(29, 1, 67, 'GIRNAR PO 2017-7446.pdf', 'application/pdf', 'pdf', '431828', '2017/02/C67 (58a2d5685e816) GIRNAR PO 2017-7446.pdf', '2017-02-14 22:31:12', '2017-02-14 22:31:12'),
(30, 1, 75, 'TCOPO-14-17-signed.pdf', 'application/pdf', 'pdf', '776115', '2017/02/C75 (58ad6be243b33) TCOPO-14-17-signed.pdf', '2017-02-22 23:15:54', '2017-02-22 23:15:54'),
(31, 1, 90, 'PO_PCT757_from_OmniChem_136_LLC_10076.pdf', 'application/pdf', 'pdf', '358133', '2017/02/C90 (58b007fd1d313) PO_PCT757_from_OmniChem_136_LLC_10076.pdf', '2017-02-24 22:46:31', '2017-02-24 22:46:31'),
(32, 1, 95, 'OC Girnar.pdf', 'application/pdf', 'pdf', '95121', '2017/03/C95 (58b8e9f45a00c) OC Girnar.pdf', '2017-03-03 16:28:44', '2017-03-03 16:28:44'),
(33, 1, 103, 'Scan.pdf', 'application/pdf', 'pdf', '626115', '2017/03/C103 (58ca758712975) Scan.pdf', '2017-03-16 23:52:47', '2017-03-16 23:52:47'),
(34, 1, 109, '20170310173626.pdf', 'application/pdf', 'pdf', '254678', '2017/03/C109 (58ca7a52663af) 20170310173626.pdf', '2017-03-17 00:13:14', '2017-03-17 00:13:14'),
(35, 1, 115, 'KGI-1701-signed.pdf', 'application/pdf', 'pdf', '254653', '2017/03/C115 (58ca7f23587cc) KGI-1701-signed.pdf', '2017-03-17 00:33:47', '2017-03-17 00:33:47'),
(36, 1, 119, 'CA4.pdf', 'application/pdf', 'pdf', '130870', '2017/03/C119 (58cbdd2f50d30) CA4.pdf', '2017-03-18 01:27:19', '2017-03-18 01:27:19'),
(37, 1, 129, '2107.pdf', 'application/pdf', 'pdf', '621178', '2017/03/C129 (58d35017ee693) 2107.pdf', '2017-03-23 17:03:28', '2017-03-23 17:03:28'),
(38, 1, 130, '2108.pdf', 'application/pdf', 'pdf', '280128', '2017/03/C130 (58d3509597b08) 2108.pdf', '2017-03-23 17:05:33', '2017-03-23 17:05:33'),
(39, 1, 131, '2109.pdf', 'application/pdf', 'pdf', '632033', '2017/03/C131 (58d350fa00158) 2109.pdf', '2017-03-23 17:07:14', '2017-03-23 17:07:14'),
(40, 1, 154, 'Scan0007.pdf', 'application/pdf', 'pdf', '289511', '2017/04/C154 (58e61f3df0f36) Scan0007.pdf', '2017-04-06 23:28:06', '2017-04-06 23:28:06'),
(41, 1, 156, 'P.Order # 2802.pdf', 'application/pdf', 'pdf', '373345', '2017/04/C156 (58e6212200ad5) P.Order # 2802.pdf', '2017-04-06 23:36:10', '2017-04-06 23:36:10'),
(42, 1, 157, 'PO-Castor Oil.pdf', 'application/pdf', 'pdf', '494125', '2017/04/C157 (58e622d2f3b30) PO-Castor Oil.pdf', '2017-04-06 23:43:23', '2017-04-06 23:43:23'),
(43, 1, 159, 'PO17522 - GIRNAR.signed.pdf', 'application/pdf', 'pdf', '195035', '2017/04/C159 (58ec544ae6672) PO17522 - GIRNAR.signed.pdf', '2017-04-11 16:28:03', '2017-04-11 16:28:03'),
(44, 1, 160, 'PO EGY-20170401.pdf', 'application/pdf', 'pdf', '104700', '2017/04/C160 (58ec55a6ea2b8) PO EGY-20170401.pdf', '2017-04-11 16:33:50', '2017-04-11 16:33:50'),
(45, 1, 164, 'PO. stkoth17-026.pdf', 'application/pdf', 'pdf', '281831', '2017/04/C164 (58ec59335c524) PO. stkoth17-026.pdf', '2017-04-11 16:48:59', '2017-04-11 16:48:59'),
(46, 1, 169, '695914 Order SSA-120417-013 Girnar.pdf', 'application/pdf', 'pdf', '24169', '2017/04/C169 (58f446db29817) 695914 Order SSA-120417-013 Girnar.pdf', '2017-04-17 17:08:51', '2017-04-17 17:08:51'),
(47, 1, 188, '2000224.pdf', 'application/pdf', 'pdf', '603492', '2017/04/C188 (590190f704e43) 2000224.pdf', '2017-04-27 19:04:31', '2017-04-27 19:04:31'),
(48, 1, 191, '94513.pdf', 'application/pdf', 'pdf', '62563', '2017/04/C191 (590192f1a919c) 94513.pdf', '2017-04-27 19:12:57', '2017-04-27 19:12:57'),
(49, 1, 192, '2113.pdf', 'application/pdf', 'pdf', '1147361', '2017/04/C192 (5901935514da2) 2113.pdf', '2017-04-27 19:14:37', '2017-04-27 19:14:37');

-- --------------------------------------------------------

--
-- Table structure for table `contracts_product`
--

CREATE TABLE `contracts_product` (
  `Id` bigint(20) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `shipment_id` int(11) NOT NULL,
  `product_id` int(5) NOT NULL,
  `comm_invoice_name` varchar(255) NOT NULL,
  `product_type` varchar(255) DEFAULT NULL,
  `package_type` varchar(255) DEFAULT NULL,
  `specification` tinytext,
  `quantity` varchar(255) NOT NULL,
  `gross_quantity` varchar(255) DEFAULT NULL,
  `rate` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `discharge_port` varchar(255) DEFAULT '',
  `total_gw` varchar(255) DEFAULT '0',
  `total_nw` varchar(255) DEFAULT '0',
  `total_packages` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contracts_product`
--

INSERT INTO `contracts_product` (`Id`, `contract_id`, `shipment_id`, `product_id`, `comm_invoice_name`, `product_type`, `package_type`, `specification`, `quantity`, `gross_quantity`, `rate`, `total_amount`, `discharge_port`, `total_gw`, `total_nw`, `total_packages`, `created_at`, `updated_at`) VALUES
(1, 8, 8, 4, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATA SHEET', '22', NULL, '1255', '27610.00', '', '0', '0', NULL, '2016-12-29 19:15:58', '2016-12-29 19:15:58'),
(2, 12, 12, 2, '', '', 'FLEXI TANK(BLBD)', '', '132', NULL, '1224', '161568.00', '', '0', '0', NULL, '2017-01-11 01:14:52', '2017-01-11 01:14:52'),
(3, 13, 15, 5, '', '', 'BULK IN FLEXI TANK  (TLBD)', '', '22', NULL, '1455', '32010.00', '', '0', '0', NULL, '2017-01-22 19:36:27', '2017-01-22 19:36:27'),
(4, 14, 16, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', '', '132', NULL, '1275', '168300.00', '', '0', '0', NULL, '2017-01-22 19:51:38', '2017-01-22 19:51:38'),
(5, 16, 18, 2, '', '', 'FLEXI TANK(BLBD)', 'COLOR ON 5Â¼â€ LOVIBOND : 20 Y, 2 R MAX F.F.A. : 1.00 % MAX MOISTURE : 0.25 % MAX IODINE VALUE : 82-90 SAPONIFICATION VALUE : 177 MIN. HYDROXYL VALUE : 160 MIN.', '330', NULL, '1270', '419100.00', '', '0', '0', NULL, '2017-01-25 17:53:46', '2017-01-25 17:53:46'),
(6, 17, 19, 2, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1248', '27456.00', '', '0', '0', NULL, '2017-01-25 18:16:49', '2017-01-25 18:16:49'),
(7, 18, 20, 4, '', '', ' IN NEW MS DRUMS 225KGS NET', '', '18', NULL, '1455', '26190.00', '', '0', '0', NULL, '2017-01-25 18:30:40', '2017-01-25 18:30:40'),
(8, 19, 21, 2, '', '', 'BULK IN \"ISO\" TANK', '', '88', NULL, '1285', '113080.00', '', '0', '0', NULL, '2017-01-25 22:11:23', '2017-01-25 22:11:23'),
(10, 22, 24, 2, '', '', 'BULK IN \"ISO\" TANK', '', '66', NULL, '1265', '83490.00', '', '0', '0', NULL, '2017-01-26 01:41:42', '2017-01-26 01:41:42'),
(11, 23, 25, 2, '', '', 'FLEXI TANK(BLBD)', '', '22', NULL, '1250', '27500.00', '', '0', '0', NULL, '2017-01-26 16:24:56', '2017-01-26 16:24:56'),
(12, 24, 26, 2, '', '', 'BULK IN \"ISO\" TANK', '', '44', NULL, '1215', '53460.00', '', '0', '0', NULL, '2017-01-26 16:27:23', '2017-01-26 16:27:23'),
(13, 25, 27, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', '', '176', NULL, '1203', '211728.00', '', '0', '0', '0 ', '2017-02-01 17:09:35', '2017-07-22 08:00:48'),
(14, 26, 28, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', '', '44', NULL, '1206', '53064.00', '', '0', '0', NULL, '2017-02-01 17:18:26', '2017-02-01 17:18:26'),
(15, 27, 29, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', '', '44', NULL, '1211', '53284.00', '', '0', '0', NULL, '2017-02-01 17:21:41', '2017-02-01 17:21:41'),
(16, 28, 30, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1318', '28996.00', '', '0', '0', NULL, '2017-02-01 17:39:46', '2017-02-01 17:39:46'),
(17, 29, 31, 7, '', '', '25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '6', NULL, '1710', '10260.00', '', '0', '0', NULL, '2017-02-01 17:45:35', '2017-02-01 17:45:35'),
(18, 29, 31, 8, '', '', '25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '12', NULL, '1468', '17616.00', '', '0', '0', NULL, '2017-02-01 17:45:35', '2017-02-01 17:45:35'),
(20, 31, 33, 5, '', '', '200KG DRUMS PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1436.25', '22980.00', '', '0', '0', NULL, '2017-02-01 18:48:42', '2017-02-01 18:48:42'),
(21, 32, 34, 2, '', '', 'IN 225KG DRUMS PALLETIZED', 'AS PER DATASHEET', '36', NULL, '1380', '49680.00', '', '0', '0', NULL, '2017-02-01 18:58:45', '2017-02-01 18:58:45'),
(24, 34, 36, 2, '', '', '200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1346', '21536.00', '', '0', '0', NULL, '2017-02-02 17:06:15', '2017-02-02 17:06:15'),
(25, 35, 37, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '144', NULL, '1360', '195840.00', '', '0', '0', NULL, '2017-02-02 17:42:48', '2017-02-02 17:42:48'),
(26, 36, 38, 8, '', '', 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER DATASHEET', '20', NULL, '1459', '29180.00', '', '0', '0', NULL, '2017-02-02 17:51:09', '2017-02-02 17:51:09'),
(27, 36, 38, 11, '', '', 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER DATASHEET', '12', NULL, '1690', '20280.00', '', '0', '0', NULL, '2017-02-02 17:51:09', '2017-02-02 17:51:09'),
(28, 37, 39, 9, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '24', NULL, '1370', '32880.00', '', '0', '0', NULL, '2017-02-02 17:59:53', '2017-02-02 17:59:53'),
(29, 38, 40, 2, '', '', 'IN 200KG DRUMS PALLETIZED', 'AS PER DATASHEET', '32', NULL, '1425', '45600.00', '', '0', '0', NULL, '2017-02-02 18:10:34', '2017-02-02 18:10:34'),
(30, 39, 41, 2, '', '', 'IN FLEXI TANK(TOP LOADING BOTTON DISCHARGE) WITH HEATING PAD', 'AS PER DATASHEET', '44', NULL, '1300', '57200.00', '', '0', '0', NULL, '2017-02-02 22:10:24', '2017-02-02 22:10:24'),
(31, 40, 42, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '300', NULL, '74', '22200.00', '', '0', '0', NULL, '2017-02-02 22:27:23', '2017-02-02 22:27:23'),
(32, 41, 43, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '500', NULL, '74', '37000.00', '', '0', '0', NULL, '2017-02-02 22:36:35', '2017-02-02 22:36:35'),
(33, 42, 44, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '1000', NULL, '74', '74000.00', '', '0', '0', NULL, '2017-02-02 22:40:17', '2017-02-02 22:40:17'),
(34, 43, 45, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '300', NULL, '72', '21600.00', '', '0', '0', NULL, '2017-02-02 22:47:58', '2017-02-02 22:47:58'),
(35, 44, 46, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '36', NULL, '1395', '50220.00', '', '0', '0', NULL, '2017-02-02 22:56:28', '2017-02-02 22:56:28'),
(36, 45, 47, 9, '', '', 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER MENTIONED IN PI', '20', NULL, '1397', '27940.00', '', '0', '0', NULL, '2017-02-02 23:02:53', '2017-02-02 23:02:53'),
(37, 46, 48, 13, '', '', 'IN 500KG JUMBO BAGS PALLETIZED', 'AS PER DATASHEET', '22', NULL, '1580', '34760.00', '', '0', '0', NULL, '2017-02-02 23:07:39', '2017-02-02 23:07:39'),
(38, 47, 49, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '500', NULL, '74', '37000.00', '', '0', '0', NULL, '2017-02-02 23:10:53', '2017-02-02 23:10:53'),
(39, 48, 50, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '700', NULL, '75', '52500.00', '', '0', '0', NULL, '2017-02-02 23:15:58', '2017-02-02 23:15:58'),
(40, 49, 51, 10, '', '', 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER DATASHEET', '14', NULL, '1590', '22260.00', '', '0', '0', '0 ', '2017-02-02 23:28:34', '2017-07-24 04:20:14'),
(41, 50, 52, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '500', NULL, '77', '38500.00', '', '0', '0', NULL, '2017-02-02 23:32:07', '2017-02-02 23:32:07'),
(43, 52, 54, 2, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1275', '28050.00', '', '0', '0', NULL, '2017-02-03 02:06:29', '2017-02-03 02:06:29'),
(44, 53, 55, 2, '', '', 'BULK IN \"ISO\" TANK', '', '88', NULL, '1272', '111936.00', '', '0', '0', NULL, '2017-02-03 02:08:24', '2017-02-03 02:08:24'),
(45, 55, 57, 2, '', '', 'BULK IN \"ISO\" TANK', '', '44', NULL, '1275', '56100.00', '', '0', '0', NULL, '2017-02-04 16:54:13', '2017-02-04 16:54:13'),
(46, 56, 58, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1285', '28270.00', '', '0', '0', NULL, '2017-02-04 22:47:56', '2017-02-04 22:47:56'),
(47, 57, 59, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1285', '28270.00', '', '0', '0', NULL, '2017-02-04 22:51:03', '2017-02-04 22:51:03'),
(48, 58, 60, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '32', NULL, '1336', '42752.00', '', '0', '0', NULL, '2017-02-04 22:54:20', '2017-02-04 22:54:20'),
(49, 59, 61, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1348', '21568.00', '', '0', '0', NULL, '2017-02-06 18:42:51', '2017-02-06 18:42:51'),
(50, 60, 62, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '220', NULL, '77', '16940.00', '', '0', '0', NULL, '2017-02-07 17:16:16', '2017-02-07 17:16:16'),
(51, 61, 63, 12, '', '', 'IN 850KG JUMBO BAGS NON-PALLETIZED', 'AS PER DATASHEET', '17', NULL, '136', '2312.00', '', '0', '0', NULL, '2017-02-07 17:21:11', '2017-02-07 17:21:11'),
(52, 62, 64, 12, '', '', '40KG PP BAGS', 'AS PER DATASHEET', '108', NULL, '101', '10908.00', '', '0', '0', NULL, '2017-02-10 01:02:22', '2017-02-10 01:02:22'),
(53, 63, 65, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', 'AS PER MENTIONED IN PI', '80', NULL, '1235', '98800.00', '', '0', '0', NULL, '2017-02-10 01:06:27', '2017-02-10 01:06:27'),
(54, 64, 66, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '154', NULL, '75', '11550.00', '', '0', '0', NULL, '2017-02-10 16:55:52', '2017-02-10 16:55:52'),
(55, 65, 67, 14, '', '', 'IN 25KG PAPER BAGS PALLETIZED', '', '26.6', NULL, '1730', '46018.00', '', '0', '0', NULL, '2017-02-11 16:32:08', '2017-02-11 16:32:08'),
(56, 66, 68, 14, '', '', 'IN 25KG PAPER BAGS PALLETIZED', '', '26.6', NULL, '1745', '46417.00', '', '0', '0', NULL, '2017-02-11 16:35:21', '2017-02-11 16:35:21'),
(57, 67, 69, 2, '', '', 'IN 200KG DRUMS PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1375', '22000.00', 'PORT KELANG', '0', '0', NULL, '2017-02-14 22:31:38', '2017-02-14 22:31:38'),
(58, 68, 70, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '54', NULL, '1410', '76140.00', 'ST.PETERSBURG', '0', '0', NULL, '2017-02-14 22:40:38', '2017-02-14 22:40:38'),
(59, 69, 71, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1345', '21520.00', 'HCMC(CAT LAI)', '0', '0', NULL, '2017-02-14 22:44:53', '2017-02-14 22:44:53'),
(60, 70, 72, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '100', NULL, '75', '7500.00', 'KAOHSIUNG', '0', '0', NULL, '2017-02-14 22:49:57', '2017-02-14 22:49:57'),
(61, 70, 72, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '200', NULL, '75', '15000.00', 'TAICHUNG', '0', '0', NULL, '2017-02-14 22:49:57', '2017-02-14 22:49:57'),
(62, 71, 73, 8, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '10', NULL, '1384', '13840.00', 'CHITTAGONG, CHITTAGONG', '0', '0', NULL, '2017-02-21 17:53:36', '2017-02-21 17:53:36'),
(63, 71, 73, 11, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '8', NULL, '1608', '12864.00', 'CHITTAGONG, CHITTAGONG', '0', '0', NULL, '2017-02-21 17:53:36', '2017-02-21 17:53:36'),
(64, 72, 74, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '300', NULL, '78', '23400.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-02-21 17:55:44', '2017-02-21 17:55:44'),
(65, 73, 75, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '32', NULL, '1349', '43168.00', 'BUSAN, BUSAN', '0', '0', NULL, '2017-02-21 23:04:30', '2017-02-21 23:04:30'),
(66, 74, 76, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '32', NULL, '1355', '43360.00', 'BUSAN, BUSAN', '0', '0', NULL, '2017-02-21 23:05:39', '2017-02-21 23:05:39'),
(67, 75, 77, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', 'AS PER DATASHEET', '110', NULL, '1225', '134750.00', 'BANGKOK, BANGKOK', '0', '0', NULL, '2017-02-22 23:15:54', '2017-02-22 23:15:54'),
(68, 76, 78, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1355', '21680.00', 'INCHEON, INCHEON', '0', '0', NULL, '2017-02-22 23:19:03', '2017-02-22 23:19:03'),
(69, 77, 79, 6, '', '', '200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '32', NULL, '1380', '44160.00', 'KEELUNG, KEELUNG', '0', '0', NULL, '2017-02-22 23:22:48', '2017-02-22 23:22:48'),
(70, 78, 80, 8, '', '', 'IN 25KG PAPER BAGS PALLETIZED', '', '80', NULL, '1345', '107600.00', 'SHUWAIKH PORT, SHUWAIKH', '0', '0', NULL, '2017-02-24 17:10:15', '2017-02-24 17:10:15'),
(71, 79, 81, 9, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', '', '35', NULL, '1345', '47075.00', 'KARACHI, KARACHI', '0', '0', NULL, '2017-02-24 17:18:53', '2017-02-24 17:18:53'),
(72, 80, 82, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', '', '44', NULL, '1233', '54252.00', 'HUANGPU, HUANGPU', '0', '0', NULL, '2017-02-24 17:28:50', '2017-02-24 17:28:50'),
(73, 81, 83, 2, '', '', 'BULK IN \"ISO\" TANK', '', '110', NULL, '1335', '146850.00', 'LEMPAALA, LEMPAALA', '0', '0', NULL, '2017-02-24 18:20:01', '2017-02-24 18:20:01'),
(74, 82, 84, 4, '', '', 'BULK IN \"ISO\" TANK', '', '44', NULL, '1304', '57376.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-02-24 18:45:31', '2017-02-24 18:45:31'),
(75, 83, 85, 15, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1540', '33880.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-02-24 18:48:48', '2017-02-24 18:48:48'),
(76, 84, 86, 2, '', '', 'BULK IN \"ISO\" TANK', '', '44', NULL, '1305', '57420.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-02-24 19:05:58', '2017-02-24 19:05:58'),
(77, 85, 87, 8, '', '', '25KG PAPER BAGS NON-PALLETIZED', '', '30', NULL, '92000', '2760000.00', 'BANDARABBAS, BANDAR ABBAS', '0', '0', NULL, '2017-02-24 19:10:02', '2017-02-24 19:10:02'),
(78, 85, 87, 7, '', '', '25KG PAPER BAGS NON-PALLETIZED', '', '4', NULL, '107000', '428000.00', 'BANDARABBAS, BANDAR ABBAS', '0', '0', NULL, '2017-02-24 19:10:02', '2017-02-24 19:10:02'),
(79, 86, 88, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '440', NULL, '74', '32560.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-02-24 22:30:05', '2017-02-24 22:30:05'),
(80, 87, 89, 8, '', '', 'IN 50KG PP BAGS NON-PALLETIZED', 'AS PER DATASHEET', '1.15', NULL, '87500', '100625.00', 'EX -WORKS, KATHMANDU', '0', '0', NULL, '2017-02-24 22:34:45', '2017-02-24 22:34:45'),
(81, 87, 89, 11, '', '', 'IN 50KG PP BAGS NON-PALLETIZED', 'AS PER DATASHEET', '3.85', NULL, '102500', '394625.00', 'EX -WORKS, KATHMANDU', '0', '0', NULL, '2017-02-24 22:34:45', '2017-02-24 22:34:45'),
(82, 88, 90, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '200', NULL, '75', '15000.00', '', '0', '0', NULL, '2017-02-24 22:37:45', '2017-02-24 22:37:45'),
(83, 89, 91, 16, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '200', NULL, '80', '16000.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-02-24 22:40:55', '2017-02-24 22:40:55'),
(84, 90, 92, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1305', '28710.00', 'MANZANILLO, MANZANILLO', '0', '0', NULL, '2017-02-24 22:46:29', '2017-02-24 22:46:29'),
(85, 91, 93, 17, '', '', 'IN BULK IN CONTAINER', '40% PROTEIN', '220', NULL, '293', '64460.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-02-24 22:49:33', '2017-02-24 22:49:33'),
(86, 92, 94, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1270', '27940.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-02-28 22:54:32', '2017-02-28 22:54:32'),
(87, 93, 95, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '300', NULL, '78', '23400.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-03-01 23:23:18', '2017-03-01 23:23:18'),
(88, 94, 96, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '440', NULL, '75', '33000.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-03-01 23:24:40', '2017-03-01 23:24:40'),
(89, 95, 97, 2, '', '', 'IN 950KG NEW IBC TANK', 'AS PER DATASHEET', '17.1', NULL, '1495', '25564.50', 'SAN-ANTONIO, SAN-ANTONIO', '0', '0', NULL, '2017-03-03 16:28:56', '2017-03-03 16:28:56'),
(90, 96, 98, 15, '', '', 'IN 225KG NEW HDPE DRUMS NON-PALLETIZED', 'AS PER GIVEN BY BUYER', '36', NULL, '1680', '60480.00', 'DURBAN, DURBAN', '0', '0', NULL, '2017-03-03 16:34:23', '2017-03-03 16:34:23'),
(91, 97, 99, 11, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '17', NULL, '1720', '29240.00', '', '0', '0', NULL, '2017-03-03 22:44:27', '2017-03-03 22:44:27'),
(92, 98, 100, 2, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1272', '27984.00', 'BARCELONA, SPAIN', '0', '0', NULL, '2017-03-04 16:22:12', '2017-03-04 16:22:12'),
(93, 99, 101, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', '', '44', NULL, '1264', '55616.00', 'SHANGHAI, SHANGHAI', '0', '0', NULL, '2017-03-04 16:39:06', '2017-03-04 16:39:06'),
(94, 100, 102, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', '', '132', NULL, '1278', '168696.00', 'SHANGHAI, SHANGHAI', '0', '0', NULL, '2017-03-04 16:44:52', '2017-03-04 16:44:52'),
(95, 101, 103, 7, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', '', '13.5', NULL, '1725', '23287.50', 'JAKARTA, JAKARTA', '0', '0', NULL, '2017-03-11 06:45:22', '2017-03-11 06:45:22'),
(96, 101, 103, 8, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', '', '3.5', NULL, '1485', '5197.50', 'JAKARTA, JAKARTA', '0', '0', NULL, '2017-03-11 06:45:22', '2017-03-11 06:45:22'),
(97, 102, 104, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1482', '23712.00', 'INCHEON, INCHEON', '0', '0', NULL, '2017-03-16 23:49:02', '2017-03-16 23:49:02'),
(98, 103, 105, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1478', '32516.00', 'VERACRUZ, VERACRUZ', '0', '0', NULL, '2017-03-16 23:52:47', '2017-03-16 23:52:47'),
(99, 104, 106, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER GIVEN BY BUYER', '32', NULL, '1470', '47040.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-03-16 23:58:09', '2017-03-16 23:58:09'),
(100, 105, 107, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1405', '30910.00', 'JOHANNESBURG, JOHANNESBURG', '0', '0', NULL, '2017-03-17 00:04:50', '2017-03-17 00:04:50'),
(101, 106, 108, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1385', '30470.00', 'DURBAN, DURBAN', '0', '0', NULL, '2017-03-17 00:06:54', '2017-03-17 00:06:54'),
(102, 107, 109, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1385', '30470.00', 'CAPETOWN, CAPETOWN', '0', '0', NULL, '2017-03-17 00:08:28', '2017-03-17 00:08:28'),
(103, 108, 110, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '36', NULL, '1545', '55620.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-03-17 00:10:09', '2017-03-17 00:10:09'),
(104, 109, 111, 13, '', '', 'IN 500KG JUMBO BAGS PALLETIZED', 'AS PER DATASHEET', '22', NULL, '1751', '38522.00', 'BUSAN, BUSAN', '0', '0', NULL, '2017-03-17 00:13:14', '2017-03-17 00:13:14'),
(105, 110, 112, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '72', NULL, '1540', '110880.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-03-17 00:14:55', '2017-03-17 00:14:55'),
(106, 111, 113, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '8', NULL, '1476', '11808.00', 'INCHEON, INCHEON', '0', '0', NULL, '2017-03-17 00:18:20', '2017-03-17 00:18:20'),
(107, 111, 113, 10, '', '', 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER DATASHEET', '8', NULL, '1740', '13920.00', 'INCHEON, INCHEON', '0', '0', NULL, '2017-03-17 00:18:20', '2017-03-17 00:18:20'),
(108, 112, 114, 12, '', '', 'BULK IN CONTAINER', 'AS PER DATASHEET', '440', '440', '81', '35640.00', 'KEELUNG, KEELUNG', '0', '0', NULL, '2017-03-17 00:22:22', '2017-06-04 05:24:42'),
(109, 113, 115, 2, '', '', 'BULK IN \"ISO\" TANK', 'AS PER DATASHEET', '22', NULL, '1515', '33330.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-03-17 00:25:31', '2017-03-17 00:25:31'),
(110, 114, 116, 11, '', '', 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER DATASHEET', '25', NULL, '1750', '43750.00', 'MANILA, MANILA', '0', '0', NULL, '2017-03-17 00:28:52', '2017-03-17 00:28:52'),
(111, 115, 117, 13, '', '', 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER DATASHEET', '14', NULL, '1815', '25410.00', 'BUSAN, BUSAN', '0', '0', NULL, '2017-03-17 00:33:47', '2017-03-17 00:33:47'),
(112, 116, 118, 16, '', '', 'BULK IN CONTAINER', 'AS PER DATASHEET', '200', NULL, '87', '17400.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-03-17 00:37:04', '2017-03-17 00:37:04'),
(113, 117, 119, 16, '', '', '40KG PP BAGS', 'AS PER DATASHEET', '140', NULL, '98', '13720.00', 'PORT KELANG, PORT KELANG', '0', '0', NULL, '2017-03-17 00:38:34', '2017-03-17 00:38:34'),
(114, 118, 120, 2, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1532', '33704.00', 'BARCELONA, SPAIN', '0', '0', NULL, '2017-03-18 00:03:27', '2017-03-18 00:03:27'),
(115, 119, 121, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '44', NULL, '1550', '68200.00', 'ODESSA, ODESSA', '0', '0', NULL, '2017-03-18 01:27:19', '2017-03-18 01:27:19'),
(116, 120, 122, 2, '', '', '200KG DRUMS NON-PALLETIZED', '', '16', NULL, '1532', '24512.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-03-19 16:30:57', '2017-03-19 16:30:57'),
(117, 121, 123, 2, '', '', '200KG DRUMS PALLETIZED', '', '16', NULL, '1530', '24480.00', 'SHUWAIKH, SHUWAIKH', '0', '0', NULL, '2017-03-19 16:37:57', '2017-03-19 16:37:57'),
(118, 122, 124, 2, '', '', '200KG DRUMS NON-PALLETIZED', '', '16', NULL, '1527', '24432.00', 'KARACHI, KARACHI', '0', '0', NULL, '2017-03-19 16:43:01', '2017-03-19 16:43:01'),
(119, 123, 125, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', '', '110', NULL, '1455', '160050.00', 'SHANGHAI, SHANGHAI', '0', '0', NULL, '2017-03-19 16:49:24', '2017-03-19 16:49:24'),
(120, 124, 126, 2, '', '', '200KG DRUMS PALLETIZED', '', '24', NULL, '1536', '36864.00', 'BUENOS AIRES, BUENOS AIRES', '0', '0', NULL, '2017-03-19 16:53:16', '2017-03-19 16:53:16'),
(121, 125, 127, 2, '', '', 'BULK IN \"ISO\" TANK', 'AS PER DATASHEET', '22', NULL, '1590', '34980.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-03-23 16:52:52', '2017-03-23 16:52:52'),
(122, 126, 128, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1620', '25920.00', 'INCHEON, INCHEON', '0', '0', NULL, '2017-03-23 16:55:33', '2017-03-23 16:55:33'),
(123, 127, 129, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '200', NULL, '83', '16600.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-03-23 16:58:48', '2017-03-23 16:58:48'),
(124, 128, 130, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '144', NULL, '1690', '243360.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-03-23 17:01:10', '2017-03-23 17:01:10'),
(125, 129, 131, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1622', '35684.00', 'VERACRUZ, VERACRUZ', '0', '0', NULL, '2017-03-23 17:03:27', '2017-03-23 17:03:27'),
(126, 130, 132, 11, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '17', NULL, '2045', '34765.00', 'VERACRUZ, VERACRUZ', '0', '0', NULL, '2017-03-23 17:05:33', '2017-03-23 17:05:33'),
(128, 132, 134, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '36', NULL, '1780', '64080.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-03-23 17:09:12', '2017-03-23 17:09:12'),
(129, 133, 135, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', '', '88', NULL, '1556', '136928.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-03-24 18:55:46', '2017-03-24 18:55:46'),
(130, 54, 56, 7, '', '', '25KG PAPER BAGS NON-PALLETIZED', '', '4', NULL, '107000', '428000.00', 'BANDARABBAS, BANDAR ABBAS', '0', '0', NULL, '2017-03-28 23:44:46', '2017-03-28 23:44:46'),
(131, 54, 56, 8, '', '', '25KG PAPER BAGS NON-PALLETIZED', '', '30', NULL, '91000', '2730000.00', 'BANDARABBAS, BANDAR ABBAS', '0', '0', NULL, '2017-03-28 23:44:46', '2017-03-28 23:44:46'),
(132, 134, 136, 2, '', '', 'IN 950KG NEW IBC TANK', '', '17.1', NULL, '1808', '30916.80', 'FREEMANTLE, FREEMANTLE', '0', '0', NULL, '2017-04-05 02:17:38', '2017-04-05 02:17:38'),
(133, 135, 137, 18, '', '', 'IN 50KG PP BAGS NON-PALLETIZED', '', '44', NULL, '320', '14080.00', 'HO CHI MINH, HO CHI MINH', '0', '0', NULL, '2017-04-05 02:23:10', '2017-04-05 02:23:10'),
(134, 136, 138, 2, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1575', '34650.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-06 18:52:52', '2017-04-06 18:52:52'),
(135, 137, 139, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '200', NULL, '85', '17000.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-04-06 19:00:34', '2017-04-06 19:00:34'),
(136, 138, 140, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1715', '27440.00', 'BUSAN, BUSAN', '0', '0', NULL, '2017-04-06 19:02:46', '2017-04-06 19:02:46'),
(137, 139, 141, 6, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1650', '36300.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-06 19:04:27', '2017-04-06 19:04:27'),
(138, 140, 142, 2, '', '', 'BULK IN FLEXITANK(BLBD)', 'AS PER DATASHEET', '66', NULL, '1597', '105402.00', 'HUANGPU, HUANGPU', '0', '0', NULL, '2017-04-06 19:06:47', '2017-04-06 19:06:47'),
(139, 141, 143, 2, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1670', '36740.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-06 19:07:02', '2017-04-06 19:07:02'),
(140, 142, 144, 2, '', '', 'BULK IN FLEXITANK(BLBD)', '', '330', NULL, '1550', '511500.00', 'SANTOS, SANTOS', '0', '0', NULL, '2017-04-06 19:15:50', '2017-04-06 19:15:50'),
(141, 143, 145, 2, '', '', 'BULK IN FLEXITANK(BLBD)', '', '440', NULL, '1624', '714560.00', 'SANTOS, SANTOS', '0', '0', NULL, '2017-04-06 19:19:34', '2017-04-06 19:19:34'),
(142, 144, 146, 2, '', '', 'BULK IN FLEXITANK(BLBD)', '', '132', NULL, '1650', '217800.00', 'SANTOS, SANTOS', '0', '0', NULL, '2017-04-06 19:22:40', '2017-04-06 19:22:40'),
(143, 145, 147, 12, '', '', 'IN 30KG PP BAGS', 'AS PER DATASHEET', '286', NULL, '86', '24596.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-04-06 19:24:21', '2017-04-06 19:24:21'),
(144, 146, 148, 14, '', '', 'IN 25KG PAPER BAGS PALLETIZED', '', '53.2', NULL, '2115', '112518.00', 'ITAJAI / NAVEGANTS, ITAJAI / NAVEGANTS', '0', '0', NULL, '2017-04-06 19:27:30', '2017-04-06 19:27:30'),
(147, 148, 150, 2, '', '', 'BULK IN \"ISO\" TANK', '', '264', NULL, '1600', '422400.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-06 22:57:40', '2017-04-06 22:57:40'),
(148, 149, 151, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '300', NULL, '82', '24600.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-04-06 23:06:40', '2017-04-06 23:06:40'),
(149, 150, 152, 12, 'INDIAN CASTOR SEED EXTRACTION MEAL - 23069027', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '300', '300', '81', '24300.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-04-06 23:13:50', '2017-08-30 08:27:57'),
(150, 151, 153, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '32', NULL, '1735', '55520.00', 'BUSAN, BUSAN', '0', '0', NULL, '2017-04-06 23:21:00', '2017-04-06 23:21:00'),
(151, 152, 154, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1725', '27600.00', 'BANGKOK, BANGKOK', '0', '0', NULL, '2017-04-06 23:25:50', '2017-04-06 23:25:50'),
(152, 153, 155, 2, '', '', 'BULK IN \"ISO\" TANK', '', '22', NULL, '1690', '37180.00', 'BARCELONA, BARCELONA', '0', '0', NULL, '2017-04-06 23:27:10', '2017-04-06 23:27:10'),
(153, 154, 156, 7, '', '', 'IN 25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '17', NULL, '2106', '35802.00', 'VERACRUZ, VERACRUZ', '0', '0', NULL, '2017-04-06 23:28:05', '2017-04-06 23:28:05'),
(154, 155, 157, 19, '', '', 'IN 850KG JUMBO BAGS NON-PALLETIZED', 'N P K : 8 1 1', '136', NULL, '182', '24752.00', 'BUSAN, BUSAN', '0', '0', NULL, '2017-04-06 23:33:46', '2017-04-06 23:33:46'),
(155, 156, 158, 2, '', '', 'IN 225KG DRUMS PALLETIZED', 'AS PER DATASHEET', '36', NULL, '1780', '64080.00', 'DAMMAM, DAMMAM', '0', '0', NULL, '2017-04-06 23:36:09', '2017-04-06 23:36:09'),
(156, 157, 159, 2, '', '', 'IN 225KG DRUMS PALLETIZED', 'AS PER DATASHEET', '18', NULL, '1870', '33660.00', 'DOHA, DOHA', '0', '0', NULL, '2017-04-06 23:43:22', '2017-04-06 23:43:22'),
(157, 158, 160, 12, '', '', 'IN 30KG PP BAGS', 'AS PER DATASHEET', '66', NULL, '86', '5676.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-04-06 23:45:59', '2017-04-06 23:45:59'),
(158, 159, 161, 15, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '2025', '32400.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-04-11 16:28:02', '2017-04-11 16:28:02'),
(159, 160, 162, 2, '', '', 'IN 200KG DRUMS PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1900', '30400.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-04-11 16:33:50', '2017-04-11 16:33:50'),
(160, 161, 163, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '18', NULL, '1850', '33300.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-04-11 16:37:24', '2017-04-11 16:37:24'),
(161, 162, 164, 2, '', '', '', '', '22', NULL, '1655', '36410.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-11 16:40:05', '2017-04-11 16:40:05'),
(162, 163, 165, 2, '', '', '', '', '22', NULL, '1760', '38720.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-11 16:43:02', '2017-04-11 16:43:02'),
(163, 164, 166, 4, '', '', 'BULK IN FLEXI TANK  (TLBD)', '', '44', NULL, '1790', '78760.00', 'HOUSTON, HOUSTON', '0', '0', NULL, '2017-04-11 16:48:59', '2017-04-11 16:48:59'),
(165, 166, 168, 2, '', '', 'BULK IN FLEXI TANK (TLTD)', '', '110', NULL, '1630', '179300.00', 'LIANYUNGANG, LIANYUNGANG', '0', '0', NULL, '2017-04-16 17:47:37', '2017-04-16 17:47:37'),
(166, 167, 169, 2, '', '', 'BULK IN FLEXI TANK  (TLBD)', '', '44', NULL, '1680', '73920.00', 'KOPER, KOPER', '0', '0', NULL, '2017-04-16 17:50:52', '2017-04-16 17:50:52'),
(167, 168, 170, 2, '', '', 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1746', '27936.00', 'INCHEON, INCHEON', '0', '0', NULL, '2017-04-17 17:04:44', '2017-04-17 17:04:44'),
(168, 169, 171, 2, '', '', 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '18', NULL, '1847', '33246.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-04-17 17:08:51', '2017-04-17 17:08:51'),
(170, 171, 173, 12, '', '', 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '220', NULL, '78', '17160.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-04-17 17:14:16', '2017-04-17 17:14:16'),
(174, 173, 175, 2, '', NULL, 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '36', NULL, '1775', '63900.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-04-21 22:06:41', '2017-04-21 22:06:41'),
(176, 175, 177, 4, '', NULL, 'BULK IN FLEXI TANK  (TLBD)', '', '44', NULL, '1790', '78760.00', 'HOUSTON, HOUSTON', '0', '0', NULL, '2017-04-24 17:05:14', '2017-04-24 17:05:14'),
(177, 176, 178, 20, '', NULL, 'IN 50KG PP BAGS NON-PALLETIZED', '4 % OIL CAKE', '40', NULL, '173', '6920.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-04-24 17:26:42', '2017-04-24 17:26:42'),
(178, 177, 179, 2, '', NULL, 'BULK IN \"ISO\" TANK', '', '66', NULL, '1679', '110814.00', 'ROTTERDAM, ROTTERDAM', '0', '0', NULL, '2017-04-24 17:45:26', '2017-04-24 17:45:26'),
(179, 178, 180, 2, '', NULL, 'BULK IN \"ISO\" TANK', '', '22', NULL, '1665', '36630.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-24 18:04:30', '2017-04-24 18:04:30'),
(180, 179, 181, 2, '', NULL, '', '', '44', NULL, '1689', '74316.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-24 18:15:18', '2017-04-24 18:15:18'),
(183, 181, 183, 4, '', NULL, 'BULK IN FLEXI TANK  (TLBD)', '', '22', NULL, '1765', '38830.00', 'HOUSTON, HOUSTON', '0', '0', NULL, '2017-04-24 19:07:06', '2017-04-24 19:07:06'),
(184, 182, 184, 7, '', NULL, '25KG PAPER BAGS NON-PALLETIZED', '', '17', NULL, '2105', '35785.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-04-25 23:53:43', '2017-04-25 23:53:43'),
(185, 183, 185, 7, '', NULL, '25KG PAPER BAGS NON-PALLETIZED', '', '17', NULL, '2105', '35785.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-04-25 23:54:50', '2017-04-25 23:54:50'),
(186, 184, 186, 2, '', NULL, 'BULK IN \"ISO\" TANK', '', '44', NULL, '1675', '73700.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-04-25 23:57:04', '2017-04-25 23:57:04'),
(187, 185, 187, 8, '', NULL, 'IN 25KG PAPER BAGS NON-PALLETIZED', 'AS PER DATASHEET', '1', NULL, '1900', '1900.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-04-27 18:56:11', '2017-04-27 18:56:11'),
(188, 186, 188, 2, '', NULL, 'IN 200KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '32', NULL, '1705', '54560.00', 'INCHEON, INCHEON', '0', '0', NULL, '2017-04-27 18:57:52', '2017-04-27 18:57:52'),
(189, 187, 189, 2, '', NULL, 'BULK IN \"ISO\" TANK', 'AS PER DATASHEET', '264', NULL, '1630', '430320.00', 'ANTWERP, ANTWERP', '0', '0', NULL, '2017-04-27 19:01:55', '2017-04-27 19:01:55'),
(190, 188, 190, 5, '', NULL, 'IN 200KG DRUMS PALLETIZED', 'AS PER DATASHEET', '16', NULL, '1920', '30720.00', 'JEBEL ALI, JEBEL ALI', '0', '0', NULL, '2017-04-27 19:04:31', '2017-04-27 19:04:31'),
(191, 189, 191, 12, '', NULL, 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '500', NULL, '76', '38000.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-04-27 19:09:48', '2017-04-27 19:09:48'),
(192, 190, 192, 12, '', NULL, 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '300', NULL, '76', '22800.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-04-27 19:11:26', '2017-04-27 19:11:26'),
(193, 191, 193, 2, '', NULL, 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '36', NULL, '1730', '62280.00', 'DURBAN, DURBAN', '0', '0', NULL, '2017-04-27 19:12:57', '2017-04-27 19:12:57'),
(195, 147, 149, 2, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '9', NULL, '1739', '15651.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-06-04 13:03:00', '2017-06-04 13:03:00'),
(196, 147, 149, 11, '', NULL, '25KG PAPER BAGS NON-PALLETIZED', '', '9', NULL, '1999', '17991.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-06-04 13:03:00', '2017-06-04 13:03:00'),
(197, 131, 133, 2, '', NULL, 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1622', '35684.00', 'VERACRUZ, VERACRUZ', '0.612', '0.605', '150 Drums', '2017-06-06 04:25:24', '2017-06-14 09:31:23'),
(198, 131, 133, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', '', '10', NULL, '456', '4560.00', '', '0.875', '0.875', '50 Packages', '2017-06-06 04:25:24', '2017-06-14 09:31:23'),
(199, 180, 182, 15, '', NULL, 'BULK IN \"ISO\" TANK', '', '22', NULL, '1995', '43890.00', 'ANTWERP, ANTWERP', '0.8', '0.8', '510 ', '2017-06-19 15:58:14', '2017-06-19 17:08:42'),
(200, 180, 182, 15, '', NULL, 'BULK IN \"ISO\" TANK', '', '22', NULL, '2009', '44198.00', 'ANTWERP, ANTWERP', '0.15', '0.15', '25 Packages', '2017-06-19 15:58:14', '2017-06-19 17:08:42'),
(201, 210, 206, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '120', NULL, '2012', '241440.00', '', '0', '0', NULL, '2017-07-05 09:15:49', '2017-07-05 09:15:49'),
(202, 213, 209, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'hgdh', '111', NULL, '111', '12321.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-05 09:34:29', '2017-07-05 09:34:29'),
(203, 214, 210, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'hgdh', '111', NULL, '111', '12321.00', NULL, '0', '0', NULL, '2017-07-05 09:35:37', '2017-07-05 09:35:37'),
(204, 215, 211, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'hgdh', '111', NULL, '111', '12321.00', NULL, '0', '0', NULL, '2017-07-05 09:36:29', '2017-07-05 09:36:29'),
(205, 216, 212, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'hgdh', '111', NULL, '111', '12321.00', '', '0', '0', NULL, '2017-07-05 09:37:06', '2017-07-05 09:37:06'),
(206, 217, 213, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '222', '222', NULL, '22', '4884.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:41:21', '2017-07-05 09:41:21'),
(207, 217, 213, 13, '', NULL, '200KG DRUMS NON-PALLETIZED', NULL, '222', NULL, '222', '49284.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:41:21', '2017-07-05 09:41:21'),
(208, 217, 213, 13, '', NULL, '200KG DRUMS PALLETIZED', NULL, '222', NULL, '222', '49284.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:41:21', '2017-07-05 09:41:21'),
(209, 217, 213, 11, '', NULL, '200KG DRUMS PALLETIZED', NULL, '222', NULL, '222', '49284.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:41:21', '2017-07-05 09:41:21'),
(210, 217, 213, 11, '', NULL, '200KG DRUMS PALLETIZED', NULL, '222', NULL, '222', '49284.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:41:21', '2017-07-05 09:41:21'),
(211, 218, 214, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '222', '222', NULL, '22', '4884.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:48:59', '2017-07-05 09:48:59'),
(212, 218, 214, 13, '', NULL, '200KG DRUMS NON-PALLETIZED', NULL, '222', NULL, '222', '49284.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:48:59', '2017-07-05 09:48:59'),
(213, 218, 214, 13, '', NULL, '200KG DRUMS PALLETIZED', NULL, '222', NULL, '222', '49284.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:48:59', '2017-07-05 09:48:59'),
(214, 218, 214, 11, '', NULL, '200KG DRUMS PALLETIZED', NULL, '222', NULL, '222', '49284.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:48:59', '2017-07-05 09:48:59'),
(215, 218, 214, 11, '', NULL, '200KG DRUMS PALLETIZED', NULL, '222', NULL, '222', '49284.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 09:48:59', '2017-07-05 09:48:59'),
(216, 219, 215, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '222', '222', NULL, '22', '4884.00', NULL, '0', '0', NULL, '2017-07-05 09:49:49', '2017-07-05 09:49:49'),
(217, 219, 215, 11, '', NULL, '200KG DRUMS PALLETIZED', NULL, '222', NULL, '222', '49284.00', NULL, '0', '0', NULL, '2017-07-05 09:49:49', '2017-07-05 09:49:49'),
(218, 220, 216, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '222', '222', NULL, '22', '4884.00', '', '0', '0', NULL, '2017-07-05 09:54:10', '2017-07-05 09:54:10'),
(219, 220, 216, 11, '', NULL, '200KG DRUMS PALLETIZED', NULL, '999', NULL, '222', '221778.00', '', '0', '0', NULL, '2017-07-05 09:54:10', '2017-07-05 09:54:10'),
(220, 221, 217, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '111', '111', NULL, '111', '12321.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 10:01:42', '2017-07-05 10:01:42'),
(221, 221, 217, 20, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '111', '111', NULL, '111', '12321.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 10:01:42', '2017-07-05 10:01:42'),
(222, 222, 218, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '111', '111', '111', '111', '12321.00', NULL, '0', '0', NULL, '2017-07-05 10:03:02', '2017-07-07 07:40:54'),
(223, 222, 218, 20, '', NULL, '200KG DRUMS NON-PALLETIZED', '111', '111', '111', '111', '12321.00', NULL, '0', '0', NULL, '2017-07-05 10:03:02', '2017-07-07 07:40:54'),
(224, 223, 219, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', '77', '77', NULL, '77', '5929.00', 'ANTWERP, ALTAMIRA', '0', '0', NULL, '2017-07-05 10:07:24', '2017-07-05 10:07:24'),
(225, 223, 219, 5, '', NULL, '200KG DRUMS PALLETIZED', '77', '77', NULL, '77', '5929.00', 'ANTWERP, ALTAMIRA', '0', '0', NULL, '2017-07-05 10:07:24', '2017-07-05 10:07:24'),
(226, 223, 219, 11, '', NULL, '225KG DRUMS NON-PALLETIZED', '77', '77', NULL, '77', '5929.00', 'ANTWERP, ALTAMIRA', '0', '0', NULL, '2017-07-05 10:07:24', '2017-07-05 10:07:24'),
(227, 223, 219, 12, '', NULL, '25KG PAPER BAGS NON-PALLETIZED', '77', '777', NULL, '77', '59829.00', 'ANTWERP, ALTAMIRA', '0', '0', NULL, '2017-07-05 10:07:24', '2017-07-05 10:07:24'),
(228, 224, 220, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', '77', '77', NULL, '77', '5929.00', NULL, '0', '0', NULL, '2017-07-05 10:10:06', '2017-07-05 10:10:06'),
(229, 224, 220, 5, '', NULL, '200KG DRUMS PALLETIZED', '77', '77', NULL, '77', '5929.00', NULL, '0', '0', NULL, '2017-07-05 10:10:06', '2017-07-05 10:10:06'),
(230, 224, 220, 11, '', NULL, '225KG DRUMS NON-PALLETIZED', '77', '77', NULL, '77', '5929.00', NULL, '0', '0', NULL, '2017-07-05 10:10:06', '2017-07-05 10:10:06'),
(231, 224, 220, 12, '', NULL, '25KG PAPER BAGS NON-PALLETIZED', '77', '777', NULL, '77', '59829.00', NULL, '0', '0', NULL, '2017-07-05 10:10:06', '2017-07-05 10:10:06'),
(232, 225, 221, 20, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '88', '88', NULL, '88', '7744.00', '', '0', '0', NULL, '2017-07-05 10:18:17', '2017-07-05 10:18:17'),
(233, 226, 222, 20, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '88', '88', NULL, '88', '7744.00', '', '0', '0', NULL, '2017-07-05 10:21:31', '2017-07-05 10:21:31'),
(234, 227, 223, 14, '', NULL, '225KG DRUMS PALLETIZZED', '222', '222', NULL, '22', '4884.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 10:38:46', '2017-07-05 10:38:46'),
(235, 228, 224, 14, '', NULL, '225KG DRUMS PALLETIZZED', '222', '222', NULL, '22', '4884.00', NULL, '0', '0', NULL, '2017-07-05 10:44:29', '2017-07-05 10:44:29'),
(236, 229, 225, 14, '', NULL, '225KG DRUMS PALLETIZZED', '222', '222', NULL, '22', '4884.00', '', '0', '0', NULL, '2017-07-05 10:45:23', '2017-07-05 10:45:23'),
(238, 231, 227, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '12', '12', NULL, '12', '144.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 10:48:05', '2017-07-05 10:48:05'),
(239, 231, 227, 20, '', NULL, '225KG DRUMS NON-PALLETIZED', '1', '1', NULL, '1', '1.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-05 10:48:05', '2017-07-05 10:48:05'),
(249, 245, 274, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', 'dfgdfg', '250', NULL, '250', '62500.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(250, 245, 275, 13, '', NULL, '200KG DRUMS NON-PALLETIZED', 'dfgdf', '250', NULL, '250250', '62562500.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(251, 245, 276, 11, '', NULL, '200KG DRUMS PALLETIZED', 'gdfgdf', '250', NULL, '250', '62500.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(252, 245, 276, 11, '', NULL, '225KG DRUMS NON-PALLETIZED', 'gdfg', '250', NULL, '250', '62500.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(253, 245, 277, 11, '', NULL, '225KG DRUMS PALLETIZZED', 'fdgdf', '250', NULL, '250', '62500.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(254, 246, 278, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test_duplicate', '200', NULL, '200', '40000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 05:20:32', '2017-07-07 05:20:32'),
(255, 246, 279, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test_duplicate', '200', NULL, '200', '40000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(256, 246, 280, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test_duplicate', '200', NULL, '200', '40000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(257, 246, 281, 10, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test_duplicate', '200', NULL, '200', '40000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(258, 247, 282, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'duplicate', '200', NULL, '500', '100000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(259, 247, 282, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'duplicate', '200', NULL, '500', '100000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(260, 247, 283, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'duplicate', '200', NULL, '500', '100000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(261, 247, 283, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'duplicate', '200', NULL, '500', '100000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(262, 249, 287, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdf', '250', NULL, '250', '62500.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-07 06:32:58', '2017-07-07 06:32:58'),
(263, 249, 287, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', '', '250', NULL, '250', '62500.00', '', '0', '0', NULL, '2017-07-07 06:32:58', '2017-07-07 06:32:58'),
(264, 250, 288, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsdfsf', '260', NULL, '254', '66040.00', 'ALEXANDRIA, ', '0', '0', NULL, '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(265, 250, 288, 20, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdf', '260', NULL, '254', '66040.00', 'ALEXANDRIA, ', '0', '0', NULL, '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(266, 250, 289, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsfdsf', '260', NULL, '254', '66040.00', 'ALEXANDRIA, ', '0', '0', NULL, '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(267, 250, 290, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdf', '260', NULL, '254', '66040.00', 'ALEXANDRIA, ', '0', '0', NULL, '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(268, 250, 291, 10, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdf', '260', NULL, '254', '66040.00', 'ALEXANDRIA, ', '0', '0', NULL, '2017-07-07 07:21:18', '2017-07-07 07:21:18'),
(269, 251, 292, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'wqewqewqe', '123', NULL, '123', '15129.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '0', '0', NULL, '2017-07-07 09:00:39', '2017-07-07 09:00:39'),
(270, 251, 292, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '123', NULL, '123', '15129.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '0', '0', NULL, '2017-07-07 09:00:39', '2017-07-07 09:00:39'),
(271, 252, 293, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'weq', '25', NULL, '30', '750.00', '', '0', '0', NULL, '2017-07-08 04:25:00', '2017-07-08 04:25:00'),
(273, 254, 295, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '20', NULL, '20', '400.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-07-08 04:27:18', '2017-07-08 04:27:18'),
(274, 255, 296, 13, '', NULL, '200KG DRUMS NON-PALLETIZED', 'dsadasd', '25', NULL, '25', '625.00', 'ALEXANDRIA, ALTAMIRA', '0', '0', NULL, '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(275, 255, 297, 10, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'asd', '25', NULL, '25', '625.00', 'ALEXANDRIA, ALTAMIRA', '0', '0', NULL, '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(276, 255, 298, 10, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'asdsa', '60', NULL, '20', '1200.00', 'ALEXANDRIA, ALTAMIRA', '0', '0', NULL, '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(277, 255, 299, 20, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsad', '40', NULL, '210', '8400.00', 'ALEXANDRIA, ALTAMIRA', '0', '0', NULL, '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(282, 257, 304, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsasdas', '250', NULL, '251', '62750.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(283, 257, 304, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsadas', '250', NULL, '250', '62500.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(284, 257, 305, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dasdasd', '250', NULL, '250', '62500.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(285, 257, 305, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'asdasd', '250', NULL, '250', '62500.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(286, 258, 307, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsfdsf', '520', NULL, '423', '219960.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(287, 258, 307, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfdsf', '520', NULL, '234', '121680.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(288, 258, 308, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsfsdf', '520', NULL, '234', '121680.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(289, 258, 309, 10, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdf', '520', NULL, '234', '121680.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(290, 258, 310, 20, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdf', '520', NULL, '234', '121680.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-07-08 05:51:24', '2017-07-08 05:51:24'),
(291, 259, 311, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdfsdf', '256', NULL, '3123', '799488.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 05:59:17', '2017-07-08 05:59:17'),
(292, 259, 311, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdfsdf', '256', NULL, '3123', '799488.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 05:59:17', '2017-07-08 05:59:17'),
(293, 259, 312, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsfsdfsdf', '256', '256', '3123.2', '799539.20', 'ANTWERP, BARCELONA', '0', '0', NULL, '2017-07-08 05:59:17', '2017-07-17 10:58:58'),
(294, 259, 312, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'dsfsdfsdf', '256', '256', '3123', '799488.00', 'ANTWERP, BARCELONA', '0', '0', NULL, '2017-07-08 05:59:17', '2017-07-17 10:58:58'),
(295, 259, 312, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdfsdf', '256', '256', '3123', '799488.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 05:59:17', '2017-07-17 10:58:58'),
(296, 260, 313, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', 'sadasd', '800', NULL, '800', '640000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(297, 260, 313, 2, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '800', NULL, '800', '640000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(298, 260, 314, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdsads', '800', NULL, '800', '640000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(299, 260, 315, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '800', NULL, '880', '704000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(300, 260, 316, 4, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'adsad', '800', NULL, '800', '640000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(301, 261, 317, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', 'sadasd', '800', NULL, '800', '640000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:44:47', '2017-07-08 06:44:47'),
(302, 261, 317, 2, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '800', NULL, '312', '249600.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:44:47', '2017-07-08 06:44:47'),
(303, 261, 318, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdsads', '800', NULL, '800', '640000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:44:48', '2017-07-08 06:44:48'),
(304, 261, 319, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '800', NULL, '800', '704000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:44:48', '2017-07-08 06:44:48');
INSERT INTO `contracts_product` (`Id`, `contract_id`, `shipment_id`, `product_id`, `comm_invoice_name`, `product_type`, `package_type`, `specification`, `quantity`, `gross_quantity`, `rate`, `total_amount`, `discharge_port`, `total_gw`, `total_nw`, `total_packages`, `created_at`, `updated_at`) VALUES
(305, 261, 320, 4, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'adsad', '800', NULL, '800', '640000.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:44:48', '2017-07-08 06:44:48'),
(311, 263, 325, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', 'test_duplicate', '243', '243', '234', '56862.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:49:30', '2017-07-08 07:10:23'),
(312, 263, 325, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test_duplicate', '234', '234', '324', '75816.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:49:30', '2017-07-08 07:10:23'),
(313, 263, 326, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test_duplicate', '234', NULL, '324', '75816.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:49:30', '2017-07-08 06:49:30'),
(314, 263, 326, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test_duplicate', '234', NULL, '43', '10062.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 06:49:30', '2017-07-08 06:49:30'),
(315, 264, 327, 7, '', NULL, '', 'dasdasd', '3242', NULL, '4234', '13726628.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(316, 264, 328, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '423', NULL, '32432', '13718736.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(317, 264, 329, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sad', '423', NULL, '324', '137052.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(318, 264, 330, 10, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sad', '423', '423', '4324', '1829052.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 07:29:39', '2017-07-08 07:31:15'),
(319, 264, 330, 20, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sad', '423', '423', '234', '98982.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 07:29:39', '2017-07-08 07:31:15'),
(320, 265, 332, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '123', NULL, '123', '15129.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 07:45:49', '2017-07-08 07:45:49'),
(324, 265, 335, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sadsad', '123', NULL, '123', '15129.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-07-08 07:45:50', '2017-07-08 07:45:50'),
(327, 266, 336, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdfsdf', '234', NULL, '324', '75816.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(328, 266, 337, 7, '12HYDROXY STEARIC ACID - 29157040', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdf', '234', '234', '234', '54756.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-08 08:02:03', '2017-07-29 06:42:15'),
(329, 266, 337, 13, '12HYDROXY STEARIC ACID - BLEACHED - 38231990', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdfsd', '234', '234', '324', '75816.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-08 08:02:03', '2017-07-29 06:42:15'),
(330, 266, 338, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'fsdfsdfsdf', '234', NULL, '234', '54756.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(331, 266, 338, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'sdfsdfsdf', '234', NULL, '234', '54756.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(332, 267, 339, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'asadsad', '243', '243', '234', '56862.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-08 08:04:22', '2017-07-11 09:33:06'),
(333, 267, 339, 13, '', NULL, '200KG DRUMS NON-PALLETIZED', 'sadsadd', '234', '234', '234', '54756.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-08 08:04:22', '2017-07-11 09:33:06'),
(334, 267, 339, 11, '', NULL, '200KG DRUMS PALLETIZED', 'sadasdsad', '234', '234', '234', '54756.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-08 08:04:22', '2017-07-11 09:33:06'),
(337, 268, 341, 7, '', NULL, '200KG DRUMS NON-PALLETIZED', 'Today', '321', '321', '213', '68373.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-11 10:02:44', '2017-07-11 10:03:10'),
(338, 268, 341, 11, '', NULL, '200KG DRUMS NON-PALLETIZED', 'Today', '123', '123', '23', '2829.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-11 10:02:44', '2017-07-11 10:03:10'),
(339, 268, 341, 10, '', NULL, '200KG DRUMS PALLETIZED', 'Today', '213123', '213123', '213', '45395199.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-07-11 10:02:45', '2017-07-11 10:03:10'),
(340, 268, 341, 4, '', NULL, '225KG DRUMS NON-PALLETIZED', 'Today', '123', '123', '213', '26199.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', '456456', '2017-07-11 10:02:45', '2017-07-11 10:03:10'),
(348, 270, 347, 10, '', NULL, '225KG DRUMS NON-PALLETIZED', '1272017_con_D', '999', NULL, '999', '998001.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', '456456', '2017-07-12 09:41:49', '2017-07-12 09:41:49'),
(356, 272, 350, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '1372017_con', '100', '100', '100', '10000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', '456456', '2017-07-12 11:15:07', '2017-07-13 07:31:47'),
(357, 272, 350, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '1372017_con', '200', '200', '200', '40000.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0', '0', '456546', '2017-07-12 11:15:07', '2017-07-13 07:31:47'),
(358, 272, 350, 8, '', NULL, 'IN 200KG DRUMS NON-PALLETIZED', '1372017_con', '300', '300', '300', '90000.00', '', '0', '0', '54654', '2017-07-12 11:15:07', '2017-07-13 07:31:47'),
(365, 274, 355, 20, 'CASTOR  CAKE - 23069027', NULL, ' IN NEW MS DRUMS 225KGS NET', 'comm_invoice_name', '250', '250', '250', '62500.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-07-19 05:20:43', '2017-07-27 04:48:41'),
(366, 273, 351, 7, '12HYDROXY STEARIC ACID - 29157040', NULL, ' IN NEW MS DRUMS 225KGS NET', '24234', '234334', '234334', '234', '54834156.00', 'ALTAMIRA, BANGKOK', '0', '0', NULL, '2017-07-21 05:31:32', '2017-07-21 05:35:32'),
(367, 273, 351, 11, '12HYDROXY STEARIC ACID - NORMAL - 29157040', NULL, ' IN NEW MS DRUMS 225KGS NET', '2344', '234', '234', '234', '54756.00', 'ALTAMIRA, BANGKOK', '0', '0', NULL, '2017-07-21 05:31:32', '2017-07-21 05:35:32'),
(369, 30, 32, 2, '', NULL, '225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '18', NULL, '1350', '24300.00', 'CAPETOWN, CAPETOWN', '0', '0', NULL, '2017-07-21 10:52:10', '2017-07-21 10:52:10'),
(372, 172, 174, 12, '', NULL, 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '110', NULL, '75', '8250.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-07-22 04:13:46', '2017-07-22 04:13:46'),
(373, 172, 174, 12, '', NULL, 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '110', NULL, '75', '8250.00', 'TAICHUNG, TAICHUNG', '0', '0', NULL, '2017-07-22 04:13:46', '2017-07-22 04:13:46'),
(374, 170, 172, 12, '', NULL, 'IN BULK IN CONTAINER', 'AS PER DATASHEET', '500', NULL, '75', '37500.00', 'KAOHSIUNG, KAOHSIUNG', '0', '0', NULL, '2017-07-22 04:15:44', '2017-07-22 04:15:44'),
(376, 174, 176, 2, '', NULL, 'IN 225KG DRUMS NON-PALLETIZED', 'AS PER DATASHEET', '72', NULL, '1760', '126720.00', 'ST.PETERSBURG, ST.PETERSBURG', '0', '0', NULL, '2017-07-22 04:26:39', '2017-07-22 04:26:39'),
(379, 253, 294, 7, '12HYDROXY STEARIC ACID - 29157040', NULL, ' IN NEW MS DRUMS 225KGS NET', 'weq', '25', '25', '30', '750.00', 'BARCELONA, ALEXANDRIA OLD PORT', '0', '0', '0 ', '2017-07-22 07:07:55', '2017-07-29 06:42:52'),
(380, 20, 22, 2, '', NULL, 'BULK IN \"ISO\" TANK', '', '44', NULL, '1225', '53900.00', 'felixstowe, felixstowe', '0', '0', '0 ', '2017-07-22 10:58:55', '2017-07-22 10:59:15'),
(381, 33, 35, 9, '', NULL, 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER DATASHEET', '14', NULL, '1380', '19320.00', 'SYDNEY, SYDNEY', '0', '0', '0 ', '2017-07-24 04:21:16', '2017-07-24 04:21:41'),
(382, 33, 35, 10, '', NULL, 'IN 25KG PAPER BAGS PALLETIZED', 'AS PER DATASHEET', '2', NULL, '1600', '3200.00', 'SYDNEY, SYDNEY', '0', '0', '0 ', '2017-07-24 04:21:16', '2017-07-24 04:21:41'),
(383, 274, 353, 11, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'comm_invoice_name', '250', NULL, '250', '62500.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-07-27 06:56:06', '2017-07-27 06:56:06'),
(397, 51, 53, 9, '', NULL, 'IN 25KG PAPER BAGS PALLETIZED', '', '16', NULL, '1435', '22960.00', 'GENOVA, GENOVA', '0', '0', NULL, '2017-07-31 06:28:47', '2017-07-31 06:28:47'),
(403, 267, 340, 11, '12HYDROXY STEARIC ACID - NORMAL - 29157040', NULL, '225KG DRUMS NON-PALLETIZED', 'asdasd', '234', '234', '234', '54756.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0.246', '0.246', '246 ', '2017-08-03 05:10:10', '2017-08-21 07:00:06'),
(404, 267, 340, 7, '12HYDROXY STEARIC ACID - 29157040', NULL, '225KG DRUMS PALLETIZZED', 'asdsdas', '234', '234', '234', '54756.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA OLD PORT', '0.246', '0.246', '246 ', '2017-08-03 05:10:10', '2017-08-21 07:00:06'),
(405, 279, 362, 7, '12HYDROXY STEARIC ACID - 29157040', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test', '12', '12', '12', '144.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-08-03 05:44:02', '2017-08-03 05:45:47'),
(406, 279, 363, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test', '12', NULL, '12', '144.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(407, 279, 364, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test', '12', NULL, '12', '144.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(408, 279, 365, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test', '12', NULL, '12', '144.00', 'ALTAMIRA, ALTAMIRA', '0', '0', NULL, '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(417, 274, 354, 10, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'comm_invoice_name', '250', NULL, '250', '62500.00', 'ANTWERP, BANDAR ABBAS', '0', '0', NULL, '2017-08-10 07:38:35', '2017-08-10 07:38:35'),
(424, 274, 352, 7, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'comm_invoice_name', '250', NULL, '250', '62500.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-08-11 05:24:46', '2017-08-11 05:24:46'),
(425, 274, 352, 13, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'comm_invoice_name', '250', NULL, '250', '62500.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-08-11 05:24:46', '2017-08-11 05:24:46'),
(430, 192, 194, 2, '', NULL, 'BULK IN FLEXI TANK  (TLBD)', 'AS PER DATASHEET', '22', NULL, '1753', '38566.00', 'VERACRUZ, VERACRUZ', '1.4', '6', '150 ', '2017-08-12 09:20:35', '2017-08-12 09:33:44'),
(461, 279, 347, 9, '', NULL, '200KG DRUMS NON-PALLETIZED', 'test', '2', NULL, '300', '600.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-08-21 08:05:40', '2017-08-21 08:05:40'),
(462, 279, 347, 20, '', NULL, '200KG DRUMS NON-PALLETIZED', 'test', '2', NULL, '400', '800.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0', '0', NULL, '2017-08-21 08:05:40', '2017-08-21 08:05:40'),
(471, 279, 346, 15, 'RICINOLEIC ACID - 29161990', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test', '2', '2', '100', '200.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0.4', '0.6', '6 ', '2017-08-21 09:06:12', '2017-08-30 04:50:05'),
(472, 279, 346, 16, 'INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM - 23069027', NULL, '225KG DRUMS PALLETIZZED', 'test', '4', '4', '200', '800.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0.6', '0.8', '8 ', '2017-08-21 09:06:12', '2017-08-30 04:50:05'),
(473, 279, 346, 10, '12HYDROXY STEARIC ACID - STANDARD - 29157040', NULL, '40KG PP BAGS', 'test', '6', '6', '300', '1800.00', 'ALEXANDRIA, ALEXANDRIA OLD PORT', '0.8', '1', '11 ', '2017-08-21 09:06:12', '2017-08-30 04:50:05'),
(493, 284, 358, 6, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(494, 284, 359, 9, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(495, 284, 360, 19, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(496, 284, 361, 16, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(497, 284, 361, 16, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(498, 286, 362, 6, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'test', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:59:00', '2017-09-04 05:59:00'),
(499, 286, 363, 9, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(500, 286, 364, 19, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(501, 286, 365, 16, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(502, 286, 365, 16, '', NULL, ' IN NEW MS DRUMS 225KGS NET', '', '2', NULL, '200', '400.00', 'BARCELONA, LIANYUNGANG', '0', '0', NULL, '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(513, 288, 370, 15, '', NULL, 'IN 25KG PAPER BAGS NON-PALLETIZED', 'shipment_a_specificatoppn', '2', NULL, '200', '400.00', 'ALEXANDRIA, ALEXANDRIA', '0', '0', NULL, '2017-09-04 06:17:22', '2017-09-04 06:17:22'),
(514, 289, 371, 12, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'ship_a_spe', '2', NULL, '200', '400.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(515, 289, 372, 2, '', NULL, ' IN NEW MS DRUMS 225KGS NET', 'ship_b_spe', '3', NULL, '300', '900.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(516, 290, 373, 14, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci_A', '2', NULL, '200', '400.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(517, 290, 374, 14, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci_B', '3', NULL, '300', '900.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(518, 291, 375, 14, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci_A', '2', NULL, '200', '400.00', 'ALEXANDRIA, BANDAR ABBAS', '0', '0', NULL, '2017-09-04 06:24:35', '2017-09-04 06:24:35'),
(519, 292, 377, 14, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci', '1', NULL, '100', '100.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-09-04 06:27:58', '2017-09-04 06:27:58'),
(520, 292, 378, 14, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci', '2', NULL, '200', '400.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-09-04 06:27:58', '2017-09-04 06:27:58'),
(521, 292, 379, 14, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci', '3', NULL, '300', '900.00', 'ALEXANDRIA OLD PORT, ALTAMIRA', '0', '0', NULL, '2017-09-04 06:27:59', '2017-09-04 06:27:59'),
(522, 293, 380, 15, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci', '1', NULL, '100', '100.00', 'ALEXANDRIA OLD PORT, BARCELONA', '0', '0', NULL, '2017-09-04 06:30:38', '2017-09-04 06:30:38'),
(523, 293, 381, 15, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci', '2', NULL, '200', '400.00', 'ALEXANDRIA OLD PORT, BARCELONA', '0', '0', NULL, '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(524, 293, 382, 15, '', NULL, '200KG DRUMS NON-PALLETIZED', 'speci', '3', NULL, '300', '900.00', 'ALEXANDRIA OLD PORT, BARCELONA', '0', '0', NULL, '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(525, 293, 383, 15, '', NULL, '200KG DRUMS PALLETIZED', 'speci', '4', NULL, '400', '1600.00', 'ALEXANDRIA OLD PORT, BARCELONA', '0', '0', NULL, '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(526, 294, 384, 15, '', NULL, '200KG DRUMS NON-PALLETIZED', 'specification', '1', NULL, '100', '100.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '0', '0', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(527, 294, 384, 14, '', NULL, '200KG DRUMS NON-PALLETIZED', 'specification', '2', NULL, '200', '400.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '0', '0', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(528, 294, 385, 15, '', NULL, '200KG DRUMS PALLETIZED', 'specification', '3', NULL, '300', '900.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '0', '0', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(529, 294, 386, 15, '', NULL, 'BULK IN FLEXI TANK (TLTD)', 'specification', '4', NULL, '400', '1600.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '0', '0', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(530, 294, 387, 15, '', NULL, '200KG DRUMS NON-PALLETIZED', 'specification', '5', NULL, '500', '2500.00', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '0', '0', NULL, '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(535, 295, 390, 15, '', NULL, '200KG DRUMS PALLETIZED', '	Specification', '5', NULL, '500', '2500.00', 'BANDARABBAS, DOHA', '0', '0', NULL, '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(536, 295, 390, 14, '', NULL, '25KG PAPER BAGS NON-PALLETIZED', '	Specification', '6', NULL, '600', '3600.00', '', '0', '0', NULL, '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(537, 295, 391, 15, '', NULL, 'IN 30KG PP BAGS', '	Specification', '7', NULL, '700', '4900.00', '', '0', '0', NULL, '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(538, 295, 391, 14, '', NULL, 'IN 950KG NEW IBC TANK', '	Specification', '8', NULL, '800', '6400.00', '', '0', '0', NULL, '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(539, 295, 388, 15, '', NULL, '200KG DRUMS NON-PALLETIZED', 'Specification', '1', NULL, '100', '100.00', 'BANDARABBAS, DOHA', '0', '0', NULL, '2017-09-04 06:40:14', '2017-09-04 06:40:14'),
(540, 295, 388, 14, '', NULL, '200KG DRUMS PALLETIZED', 'Specification', '2', NULL, '200', '400.00', 'BANDARABBAS, DOHA', '0', '0', NULL, '2017-09-04 06:40:14', '2017-09-04 06:40:14'),
(541, 295, 388, 20, '', NULL, 'BULK IN \"ISO\" TANK', 'Specification', '3', NULL, '300', '900.00', '', '0', '0', NULL, '2017-09-04 06:40:14', '2017-09-04 06:40:14'),
(545, 295, 389, 15, '', NULL, '225KG DRUMS PALLETIZZED', 'Specification', '3', NULL, '300', '900.00', 'BANDARABBAS, DOHA', '0', '0', NULL, '2017-09-04 06:41:30', '2017-09-04 06:41:30'),
(546, 295, 389, 14, '', NULL, '40KG PP BAGS', 'Specification', '4', NULL, '400', '1600.00', 'BANDARABBAS, DOHA', '0', '0', NULL, '2017-09-04 06:41:30', '2017-09-04 06:41:30'),
(547, 295, 389, 18, '', NULL, '200KG DRUMS NON-PALLETIZED', 'Specification', '5', NULL, '500', '2500.00', 'BANDARABBAS, DOHA', '0', '0', NULL, '2017-09-04 06:41:30', '2017-09-04 06:41:30'),
(552, 296, 394, 14, '', NULL, '200KG DRUMS NON-PALLETIZED', 'Specification', '5', NULL, '500', '2500.00', 'BANGKOK, DOHA', '0', '0', NULL, '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(553, 296, 394, 17, '', NULL, '225KG DRUMS NON-PALLETIZED', 'Specification', '6', NULL, '600', '3600.00', 'BANGKOK, DOHA', '0', '0', NULL, '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(557, 296, 395, 14, '', NULL, '225KG DRUMS PALLETIZZED', 'Specification', '7', NULL, '700', '4900.00', 'BANGKOK, DOHA', '0', '0', NULL, '2017-09-04 06:49:39', '2017-09-04 06:49:39'),
(558, 296, 395, 15, '', NULL, 'BULK IN \"ISO\" TANK', 'Specification', '8', NULL, '800', '6400.00', 'BANGKOK, DOHA', '0', '0', NULL, '2017-09-04 06:49:39', '2017-09-04 06:49:39'),
(565, 296, 393, 31, '', NULL, '225KG DRUMS NON-PALLETIZED', 'Specification', '3', NULL, '300', '900.00', 'BANGKOK, DOHA', '0', '0', NULL, '2017-09-04 07:17:32', '2017-09-04 07:17:32'),
(566, 296, 393, 18, '', NULL, 'abc_packing', 'Specification', '4', NULL, '400', '1600.00', 'BANGKOK, DOHA', '0', '0', NULL, '2017-09-04 07:17:32', '2017-09-04 07:17:32'),
(567, 296, 393, 30, '', NULL, 'BULK IN CONTAINER', 'Specification', '5', NULL, '500', '2500.00', '', '0', '0', NULL, '2017-09-04 07:17:32', '2017-09-04 07:17:32'),
(578, 296, 392, 17, '', NULL, '200KG DRUMS NON-PALLETIZED', 'Specification', '1', NULL, '100', '100.00', 'BANGKOK, DOHA', '0.21', '0.21', '0 ', '2017-09-06 11:12:45', '2017-09-06 11:13:47'),
(579, 296, 392, 5, '', NULL, '200KG DRUMS PALLETIZED', 'Specification', '2', NULL, '200', '400.00', 'BANGKOK, DOHA', '0.218', '0.228', '0 ', '2017-09-06 11:12:45', '2017-09-06 11:13:47');

-- --------------------------------------------------------

--
-- Table structure for table `contracts_rawmaterial`
--

CREATE TABLE `contracts_rawmaterial` (
  `Id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `shipment_id` int(11) NOT NULL,
  `order_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `delivery_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `product_id` int(5) DEFAULT NULL,
  `qty` int(5) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contracts_rawmaterial`
--

INSERT INTO `contracts_rawmaterial` (`Id`, `contract_id`, `shipment_id`, `order_date`, `delivery_date`, `product_id`, `qty`, `created_at`, `updated_at`) VALUES
(1, 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-01-07 00:05:12', '2017-01-07 00:05:12'),
(2, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-01-11 01:40:59', '2017-01-11 01:40:59'),
(3, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 2, '2017-01-11 01:40:59', '2017-01-11 01:40:59'),
(4, 12, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 20, '2017-01-11 02:01:23', '2017-01-11 02:01:23'),
(5, 12, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 1, '2017-01-11 02:01:23', '2017-01-11 02:01:23'),
(6, 12, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-01-11 02:01:23', '2017-01-11 02:01:23'),
(7, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(8, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(9, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(10, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 6, '2017-01-23 01:03:14', '2017-01-25 21:23:59'),
(11, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(12, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(13, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(14, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(15, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(16, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(17, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(18, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(19, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(20, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(21, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(22, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-01-23 01:03:14', '2017-01-23 01:03:14'),
(23, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(24, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(25, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(26, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(27, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(28, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(29, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(30, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(31, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(32, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(33, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(34, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(35, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(36, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(37, 12, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 2, '2017-01-25 17:53:54', '2017-01-25 17:53:54'),
(38, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(39, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(40, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(41, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 1, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(42, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(43, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(44, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(45, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(46, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(47, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(48, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(49, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(50, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(51, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(52, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(53, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(54, 13, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-01-25 17:56:56', '2017-01-25 17:56:56'),
(55, 14, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-01-25 18:06:26', '2017-01-25 18:06:26'),
(56, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(57, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(58, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 160, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(59, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(60, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(61, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(62, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(63, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(64, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(65, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(66, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(67, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(68, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(69, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(70, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(71, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(72, 58, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:03:48', '2017-02-05 23:03:48'),
(73, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(74, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(75, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(76, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 1, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(77, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(78, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(79, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(80, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(81, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(82, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(83, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(84, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(85, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(86, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(87, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(88, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(89, 57, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:05:21', '2017-02-05 23:05:21'),
(90, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(91, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(92, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(93, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 1, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(94, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(95, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(96, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(97, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(98, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(99, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(100, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(101, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(102, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(103, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(104, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(105, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(106, 56, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:06:20', '2017-02-05 23:06:20'),
(107, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(108, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(109, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(110, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(111, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(112, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(113, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(114, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 16, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(115, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(116, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 640, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(117, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(118, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(119, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(120, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(121, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(122, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(123, 51, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:12:43', '2017-02-05 23:12:43'),
(124, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(125, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(126, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(127, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(128, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(129, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(130, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(131, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(132, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(133, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(134, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(135, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 1360, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(136, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(137, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(138, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(139, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(140, 54, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:15:27', '2017-02-05 23:15:27'),
(141, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(142, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(143, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(144, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(145, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(146, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(147, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 16, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(148, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(149, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(150, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(151, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 560, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(152, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(153, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(154, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(155, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(156, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(157, 49, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:17:45', '2017-02-05 23:17:45'),
(158, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(159, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(160, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(161, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(162, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(163, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(164, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(165, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(166, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(167, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(168, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(169, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(170, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(171, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(172, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 44, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(173, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 22, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(174, 46, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:19:13', '2017-02-05 23:19:13'),
(175, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(176, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(177, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(178, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(179, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(180, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(181, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 20, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(182, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(183, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(184, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(185, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(186, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 800, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(187, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(188, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(189, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(190, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(191, 45, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:22:26', '2017-02-05 23:22:26'),
(192, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(193, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(194, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(195, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(196, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 160, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(197, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(198, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(199, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(200, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(201, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(202, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(203, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(204, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(205, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(206, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(207, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(208, 44, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:23:47', '2017-02-05 23:23:47'),
(209, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(210, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(211, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(212, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 2, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(213, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(214, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(215, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(216, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(217, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(218, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(219, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(220, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(221, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(222, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(223, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(224, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(225, 39, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:28:04', '2017-02-05 23:28:04'),
(226, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(227, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(228, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 160, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(229, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(230, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(231, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(232, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 40, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(233, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(234, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(235, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(236, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(237, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(238, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(239, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(240, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(241, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(242, 38, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:29:34', '2017-02-05 23:29:34'),
(243, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(244, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(245, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(246, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(247, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(248, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(249, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(250, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(251, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(252, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 960, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(253, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(254, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(255, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(256, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(257, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(258, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(259, 37, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:44:08', '2017-02-05 23:44:08'),
(260, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(261, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(262, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(263, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(264, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(265, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(266, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(267, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 32, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(268, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(269, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 800, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(270, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 480, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(271, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(272, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(273, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(274, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(275, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(276, 36, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:50:32', '2017-02-05 23:50:32'),
(277, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(278, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(279, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(280, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(281, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 320, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(282, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(283, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(284, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(285, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(286, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(287, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(288, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(289, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(290, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(291, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(292, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(293, 35, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:52:18', '2017-02-05 23:52:18'),
(294, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(295, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(296, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 80, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(297, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(298, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(299, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(300, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 20, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(301, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(302, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(303, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(304, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(305, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(306, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(307, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(308, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(309, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(310, 34, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:53:47', '2017-02-05 23:53:47'),
(311, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(312, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(313, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(314, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(315, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(316, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(317, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(318, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(319, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(320, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 560, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(321, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 80, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(322, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(323, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(324, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(325, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(326, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(327, 33, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-05 23:56:09', '2017-02-05 23:56:09'),
(328, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(329, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(330, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(331, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(332, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 80, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(333, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(334, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(335, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(336, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(337, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(338, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(339, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(340, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(341, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(342, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(343, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(344, 30, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-06 00:20:00', '2017-02-06 00:20:00'),
(345, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(346, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(347, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(348, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(349, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(350, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(351, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(352, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(353, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(354, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 480, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(355, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 280, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(356, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(357, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(358, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(359, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(360, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(361, 29, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-06 00:22:46', '2017-02-06 00:22:46'),
(362, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(363, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(364, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(365, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 1, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(366, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(367, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(368, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(369, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(370, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(371, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(372, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(373, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(374, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(375, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(376, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(377, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(378, 28, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-02-06 00:23:53', '2017-02-06 00:23:53'),
(379, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 0, '2017-08-10 17:36:17', '2017-08-10 17:36:17'),
(380, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4, 0, '2017-08-10 17:36:17', '2017-08-10 17:36:17'),
(381, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 0, '2017-08-10 17:36:17', '2017-08-10 17:36:17'),
(382, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6, 0, '2017-08-10 17:36:17', '2017-08-10 17:36:17'),
(383, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7, 0, '2017-08-10 17:36:17', '2017-08-10 17:36:17'),
(384, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 8, 0, '2017-08-10 17:36:17', '2017-08-10 17:36:17'),
(385, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(386, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 10, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(387, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(388, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(389, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 13, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(390, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 14, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(391, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 15, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(392, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 16, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(393, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 17, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(394, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 18, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18'),
(395, 271, 348, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 19, 0, '2017-08-10 17:36:18', '2017-08-10 17:36:18');

-- --------------------------------------------------------

--
-- Table structure for table `contracts_scheme`
--

CREATE TABLE `contracts_scheme` (
  `Id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `shipment_id` int(11) NOT NULL,
  `schemes` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contracts_scheme`
--

INSERT INTO `contracts_scheme` (`Id`, `contract_id`, `shipment_id`, `schemes`, `created_at`, `updated_at`) VALUES
(8, 8, 8, '', '2016-12-28 00:19:06', '2016-12-28 00:19:06'),
(11, 11, 11, '', '2017-01-10 21:19:15', '2017-01-10 21:19:15'),
(12, 12, 12, '', '2017-01-11 01:14:52', '2017-01-11 19:08:05'),
(13, 12, 13, '', '2017-01-11 01:19:28', '2017-01-11 01:19:28'),
(14, 12, 14, '', '2017-01-11 01:19:53', '2017-01-11 01:19:53'),
(15, 13, 15, '', '2017-01-22 19:36:27', '2017-01-22 19:36:27'),
(16, 14, 16, '', '2017-01-22 19:51:38', '2017-01-22 19:51:38'),
(17, 15, 17, '', '2017-01-23 00:14:17', '2017-01-23 00:14:17'),
(18, 16, 18, '', '2017-01-25 17:53:46', '2017-01-25 17:53:46'),
(19, 17, 19, '', '2017-01-25 18:16:49', '2017-01-25 18:16:49'),
(20, 18, 20, '', '2017-01-25 18:30:40', '2017-01-25 18:30:40'),
(21, 19, 21, '', '2017-01-25 22:11:23', '2017-01-25 22:11:23'),
(22, 20, 22, '', '2017-01-25 22:43:56', '2017-01-25 22:43:56'),
(24, 22, 24, '', '2017-01-26 01:41:42', '2017-01-26 01:41:42'),
(25, 23, 25, '', '2017-01-26 16:24:56', '2017-01-26 16:24:56'),
(26, 24, 26, '', '2017-01-26 16:27:23', '2017-01-26 16:27:23'),
(27, 25, 27, '', '2017-02-01 17:09:35', '2017-02-01 17:09:35'),
(28, 26, 28, '', '2017-02-01 17:18:26', '2017-02-01 17:18:26'),
(29, 27, 29, '', '2017-02-01 17:21:41', '2017-02-01 17:21:41'),
(30, 28, 30, '', '2017-02-01 17:39:46', '2017-02-01 17:39:46'),
(31, 29, 31, '', '2017-02-01 17:45:35', '2017-02-01 17:45:35'),
(32, 30, 32, '', '2017-02-01 18:41:26', '2017-02-01 18:41:26'),
(33, 31, 33, '', '2017-02-01 18:48:42', '2017-02-01 18:48:42'),
(34, 32, 34, '', '2017-02-01 18:57:22', '2017-02-01 18:57:22'),
(35, 33, 35, '', '2017-02-01 19:07:09', '2017-02-01 19:07:09'),
(36, 34, 36, '', '2017-02-02 17:06:15', '2017-02-02 17:06:15'),
(37, 35, 37, '', '2017-02-02 17:42:48', '2017-02-02 17:42:48'),
(38, 36, 38, '', '2017-02-02 17:51:09', '2017-02-02 17:51:09'),
(39, 37, 39, '', '2017-02-02 17:59:53', '2017-02-02 17:59:53'),
(40, 38, 40, '', '2017-02-02 18:10:34', '2017-02-02 18:10:34'),
(41, 39, 41, '', '2017-02-02 22:10:24', '2017-02-02 22:10:24'),
(42, 40, 42, '', '2017-02-02 22:27:23', '2017-02-02 22:27:23'),
(43, 41, 43, '', '2017-02-02 22:36:35', '2017-02-02 22:36:35'),
(44, 42, 44, '', '2017-02-02 22:40:17', '2017-02-02 22:40:17'),
(45, 43, 45, '', '2017-02-02 22:47:58', '2017-02-02 22:47:58'),
(46, 44, 46, '', '2017-02-02 22:56:28', '2017-02-02 22:56:28'),
(47, 45, 47, '', '2017-02-02 23:01:30', '2017-02-02 23:01:30'),
(48, 46, 48, '', '2017-02-02 23:07:39', '2017-02-02 23:07:39'),
(49, 47, 49, '', '2017-02-02 23:10:53', '2017-02-02 23:10:53'),
(50, 48, 50, '', '2017-02-02 23:15:58', '2017-02-02 23:15:58'),
(51, 49, 51, '', '2017-02-02 23:28:34', '2017-02-02 23:28:34'),
(52, 50, 52, '', '2017-02-02 23:32:07', '2017-02-02 23:32:07'),
(53, 51, 53, '', '2017-02-03 02:03:34', '2017-02-03 02:03:34'),
(54, 52, 54, '', '2017-02-03 02:06:29', '2017-02-03 02:06:29'),
(55, 53, 55, '', '2017-02-03 02:08:24', '2017-02-03 02:08:24'),
(56, 54, 56, '', '2017-02-03 02:15:46', '2017-02-03 02:15:46'),
(57, 55, 57, '', '2017-02-04 16:54:13', '2017-02-04 16:54:13'),
(58, 56, 58, '', '2017-02-04 22:47:56', '2017-02-04 22:47:56'),
(59, 57, 59, '', '2017-02-04 22:51:03', '2017-02-04 22:51:03'),
(60, 58, 60, '', '2017-02-04 22:54:20', '2017-02-04 22:54:20'),
(61, 59, 61, '', '2017-02-06 18:42:51', '2017-02-06 18:42:51'),
(62, 60, 62, '', '2017-02-07 17:16:16', '2017-02-07 17:16:16'),
(63, 61, 63, '', '2017-02-07 17:21:11', '2017-02-07 17:21:11'),
(64, 62, 64, '', '2017-02-10 01:02:23', '2017-02-10 01:02:23'),
(65, 63, 65, '', '2017-02-10 01:06:27', '2017-02-10 01:06:27'),
(66, 64, 66, '', '2017-02-10 16:55:52', '2017-02-10 16:55:52'),
(67, 65, 67, '', '2017-02-11 16:32:08', '2017-02-11 16:32:08'),
(68, 66, 68, '', '2017-02-11 16:35:21', '2017-02-11 16:35:21'),
(69, 67, 69, '', '2017-02-14 22:31:12', '2017-02-14 22:31:12'),
(70, 68, 70, '', '2017-02-14 22:40:20', '2017-02-14 22:40:20'),
(71, 69, 71, '', '2017-02-14 22:44:53', '2017-02-14 22:44:53'),
(72, 70, 72, '', '2017-02-14 22:49:57', '2017-02-14 22:49:57'),
(73, 71, 73, '', '2017-02-21 17:53:36', '2017-02-21 17:53:36'),
(74, 72, 74, '', '2017-02-21 17:55:44', '2017-02-21 17:55:44'),
(75, 73, 75, '', '2017-02-21 23:04:30', '2017-02-21 23:04:30'),
(76, 74, 76, '', '2017-02-21 23:05:39', '2017-02-21 23:05:39'),
(77, 75, 77, '', '2017-02-22 23:15:54', '2017-02-22 23:15:54'),
(78, 76, 78, '', '2017-02-22 23:19:03', '2017-02-22 23:19:03'),
(79, 77, 79, '', '2017-02-22 23:22:48', '2017-02-22 23:22:48'),
(80, 78, 80, '', '2017-02-24 17:10:15', '2017-02-24 17:10:15'),
(81, 79, 81, '', '2017-02-24 17:18:53', '2017-02-24 17:18:53'),
(82, 80, 82, '', '2017-02-24 17:28:50', '2017-02-24 17:28:50'),
(83, 81, 83, '', '2017-02-24 18:20:01', '2017-02-24 18:20:01'),
(84, 82, 84, '', '2017-02-24 18:45:31', '2017-02-24 18:45:31'),
(85, 83, 85, '', '2017-02-24 18:48:48', '2017-02-24 18:48:48'),
(86, 84, 86, '', '2017-02-24 19:05:58', '2017-02-24 19:05:58'),
(87, 85, 87, '', '2017-02-24 19:10:02', '2017-02-24 19:10:02'),
(88, 86, 88, '', '2017-02-24 22:30:05', '2017-02-24 22:30:05'),
(89, 87, 89, '', '2017-02-24 22:34:45', '2017-02-24 22:34:45'),
(90, 88, 90, '', '2017-02-24 22:37:45', '2017-02-24 22:37:45'),
(91, 89, 91, '', '2017-02-24 22:40:55', '2017-02-24 22:40:55'),
(92, 90, 92, '', '2017-02-24 22:46:29', '2017-02-24 22:46:29'),
(93, 91, 93, '', '2017-02-24 22:49:33', '2017-02-24 22:49:33'),
(94, 92, 94, '', '2017-02-28 22:54:32', '2017-02-28 22:54:32'),
(95, 93, 95, '', '2017-03-01 23:23:18', '2017-03-01 23:23:18'),
(96, 94, 96, '', '2017-03-01 23:24:40', '2017-03-01 23:24:40'),
(97, 95, 97, '', '2017-03-03 16:28:44', '2017-03-03 16:28:44'),
(98, 96, 98, '', '2017-03-03 16:34:23', '2017-03-03 16:34:23'),
(99, 97, 99, '', '2017-03-03 22:43:10', '2017-03-03 22:43:10'),
(100, 98, 100, '', '2017-03-04 16:22:12', '2017-03-04 16:22:12'),
(101, 99, 101, '', '2017-03-04 16:39:06', '2017-03-04 16:39:06'),
(102, 100, 102, '', '2017-03-04 16:44:52', '2017-03-04 16:44:52'),
(103, 101, 103, '', '2017-03-11 06:45:22', '2017-03-11 06:45:22'),
(104, 102, 104, '', '2017-03-16 23:49:02', '2017-03-16 23:49:02'),
(105, 103, 105, '', '2017-03-16 23:52:47', '2017-03-16 23:52:47'),
(106, 104, 106, '', '2017-03-16 23:58:09', '2017-03-16 23:58:09'),
(107, 105, 107, '', '2017-03-17 00:04:50', '2017-03-17 00:04:50'),
(108, 106, 108, '', '2017-03-17 00:06:54', '2017-03-17 00:06:54'),
(109, 107, 109, '', '2017-03-17 00:08:28', '2017-03-17 00:08:28'),
(110, 108, 110, '', '2017-03-17 00:10:09', '2017-03-17 00:10:09'),
(111, 109, 111, '', '2017-03-17 00:13:14', '2017-03-17 00:13:14'),
(112, 110, 112, '', '2017-03-17 00:14:55', '2017-03-17 00:14:55'),
(113, 111, 113, '', '2017-03-17 00:18:20', '2017-03-17 00:18:20'),
(114, 112, 114, '', '2017-03-17 00:22:22', '2017-03-17 00:22:22'),
(115, 113, 115, '', '2017-03-17 00:25:31', '2017-03-17 00:25:31'),
(116, 114, 116, '', '2017-03-17 00:28:52', '2017-03-17 00:28:52'),
(117, 115, 117, '', '2017-03-17 00:33:47', '2017-03-17 00:33:47'),
(118, 116, 118, '', '2017-03-17 00:37:04', '2017-03-17 00:37:04'),
(119, 117, 119, '', '2017-03-17 00:38:34', '2017-03-17 00:38:34'),
(120, 118, 120, '', '2017-03-18 00:03:27', '2017-03-18 00:03:27'),
(121, 119, 121, '', '2017-03-18 01:27:19', '2017-03-18 01:27:19'),
(122, 120, 122, '', '2017-03-19 16:30:57', '2017-03-19 16:30:57'),
(123, 121, 123, '', '2017-03-19 16:37:57', '2017-03-19 16:37:57'),
(124, 122, 124, '', '2017-03-19 16:43:01', '2017-03-19 16:43:01'),
(125, 123, 125, '', '2017-03-19 16:49:24', '2017-03-19 16:49:24'),
(126, 124, 126, '', '2017-03-19 16:53:16', '2017-03-19 16:53:16'),
(127, 125, 127, '', '2017-03-23 16:52:52', '2017-03-23 16:52:52'),
(128, 126, 128, '', '2017-03-23 16:55:33', '2017-03-23 16:55:33'),
(129, 127, 129, '', '2017-03-23 16:58:48', '2017-03-23 16:58:48'),
(130, 128, 130, '', '2017-03-23 17:01:10', '2017-03-23 17:01:10'),
(131, 129, 131, '', '2017-03-23 17:03:27', '2017-03-23 17:03:27'),
(132, 130, 132, '', '2017-03-23 17:05:33', '2017-03-23 17:05:33'),
(133, 131, 133, '', '2017-03-23 17:07:13', '2017-03-23 17:07:13'),
(134, 132, 134, '', '2017-03-23 17:09:12', '2017-03-23 17:09:12'),
(135, 133, 135, '', '2017-03-24 18:55:46', '2017-03-24 18:55:46'),
(136, 134, 136, '', '2017-04-05 02:17:38', '2017-04-05 02:17:38'),
(137, 135, 137, '', '2017-04-05 02:23:10', '2017-04-05 02:23:10'),
(138, 136, 138, '', '2017-04-06 18:52:52', '2017-04-06 18:52:52'),
(139, 137, 139, '', '2017-04-06 19:00:34', '2017-04-06 19:00:34'),
(140, 138, 140, '', '2017-04-06 19:02:46', '2017-04-06 19:02:46'),
(141, 139, 141, '', '2017-04-06 19:04:27', '2017-04-06 19:04:27'),
(142, 140, 142, '', '2017-04-06 19:06:47', '2017-04-06 19:06:47'),
(143, 141, 143, '', '2017-04-06 19:07:02', '2017-04-06 19:07:02'),
(144, 142, 144, '', '2017-04-06 19:15:50', '2017-04-06 19:15:50'),
(145, 143, 145, '', '2017-04-06 19:19:34', '2017-04-06 19:19:34'),
(146, 144, 146, '', '2017-04-06 19:22:40', '2017-04-06 19:22:40'),
(147, 145, 147, '', '2017-04-06 19:24:21', '2017-04-06 19:24:21'),
(148, 146, 148, '', '2017-04-06 19:27:30', '2017-04-06 19:27:30'),
(149, 147, 149, '', '2017-04-06 22:25:51', '2017-04-06 22:25:51'),
(150, 148, 150, '', '2017-04-06 22:57:40', '2017-04-06 22:57:40'),
(151, 149, 151, '', '2017-04-06 23:06:40', '2017-04-06 23:06:40'),
(152, 150, 152, '', '2017-04-06 23:13:50', '2017-04-06 23:13:50'),
(153, 151, 153, '', '2017-04-06 23:21:00', '2017-04-06 23:21:00'),
(154, 152, 154, '', '2017-04-06 23:25:50', '2017-04-06 23:25:50'),
(155, 153, 155, '', '2017-04-06 23:27:10', '2017-04-06 23:27:10'),
(156, 154, 156, '', '2017-04-06 23:28:05', '2017-04-06 23:28:05'),
(157, 155, 157, '', '2017-04-06 23:33:46', '2017-04-06 23:33:46'),
(158, 156, 158, '', '2017-04-06 23:36:10', '2017-04-06 23:36:10'),
(159, 157, 159, '', '2017-04-06 23:43:22', '2017-04-06 23:43:22'),
(160, 158, 160, '', '2017-04-06 23:45:59', '2017-04-06 23:45:59'),
(161, 159, 161, '', '2017-04-11 16:28:02', '2017-04-11 16:28:02'),
(162, 160, 162, '', '2017-04-11 16:33:50', '2017-04-11 16:33:50'),
(163, 161, 163, '', '2017-04-11 16:37:24', '2017-04-11 16:37:24'),
(164, 162, 164, '', '2017-04-11 16:40:05', '2017-04-11 16:40:05'),
(165, 163, 165, '', '2017-04-11 16:43:02', '2017-04-11 16:43:02'),
(166, 164, 166, '', '2017-04-11 16:48:59', '2017-04-11 16:48:59'),
(168, 166, 168, '', '2017-04-16 17:47:37', '2017-04-16 17:47:37'),
(169, 167, 169, '', '2017-04-16 17:50:52', '2017-04-16 17:50:52'),
(170, 168, 170, '', '2017-04-17 17:04:44', '2017-04-17 17:04:44'),
(171, 169, 171, '', '2017-04-17 17:08:51', '2017-04-17 17:08:51'),
(172, 170, 172, '', '2017-04-17 17:12:08', '2017-04-17 17:12:08'),
(173, 171, 173, '', '2017-04-17 17:14:16', '2017-04-17 17:14:16'),
(174, 172, 174, '', '2017-04-17 17:19:26', '2017-04-17 17:19:26'),
(175, 173, 175, '', '2017-04-21 22:06:41', '2017-04-21 22:06:41'),
(176, 174, 176, '', '2017-04-21 22:08:03', '2017-04-21 22:08:03'),
(177, 175, 177, '', '2017-04-24 17:05:14', '2017-04-24 17:05:14'),
(178, 176, 178, '', '2017-04-24 17:26:42', '2017-04-24 17:26:42'),
(179, 177, 179, '', '2017-04-24 17:45:26', '2017-04-24 17:45:26'),
(180, 178, 180, '', '2017-04-24 18:04:30', '2017-04-24 18:04:30'),
(181, 179, 181, '', '2017-04-24 18:15:18', '2017-04-24 18:15:18'),
(182, 180, 182, '', '2017-04-24 18:45:17', '2017-04-24 18:45:17'),
(183, 181, 183, '', '2017-04-24 19:07:06', '2017-04-24 19:07:06'),
(184, 182, 184, '', '2017-04-25 23:54:23', '2017-04-25 23:54:23'),
(185, 183, 185, '', '2017-04-25 23:54:50', '2017-04-25 23:54:50'),
(186, 184, 186, '', '2017-04-25 23:57:04', '2017-04-25 23:57:04'),
(187, 185, 187, '', '2017-04-27 18:56:11', '2017-04-27 18:56:11'),
(188, 186, 188, '', '2017-04-27 18:57:52', '2017-04-27 18:57:52'),
(189, 187, 189, '', '2017-04-27 19:01:55', '2017-04-27 19:01:55'),
(190, 188, 190, '', '2017-04-27 19:04:31', '2017-04-27 19:04:31'),
(191, 189, 191, '', '2017-04-27 19:09:48', '2017-04-27 19:09:48'),
(192, 190, 192, '', '2017-04-27 19:11:26', '2017-04-27 19:11:26'),
(193, 191, 193, '', '2017-04-27 19:12:57', '2017-04-27 19:12:57'),
(194, 192, 194, '', '2017-04-27 19:14:37', '2017-04-27 19:14:37'),
(206, 236, 238, '', '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(207, 236, 239, '', '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(208, 236, 240, '', '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(209, 236, 241, '', '2017-07-06 04:14:43', '2017-07-06 04:14:43'),
(210, 237, 242, '', '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(211, 237, 243, '', '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(212, 237, 244, '', '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(213, 237, 245, '', '2017-07-06 04:16:21', '2017-07-06 04:16:21'),
(214, 238, 246, '', '2017-07-06 04:19:22', '2017-07-06 04:19:22'),
(215, 238, 247, '', '2017-07-06 04:19:22', '2017-07-06 04:19:22'),
(216, 238, 248, '', '2017-07-06 04:19:23', '2017-07-06 04:19:23'),
(217, 238, 249, '', '2017-07-06 04:19:23', '2017-07-06 04:19:23'),
(218, 239, 250, '', '2017-07-06 04:22:13', '2017-07-06 04:22:13'),
(219, 239, 251, '', '2017-07-06 04:22:14', '2017-07-06 04:22:14'),
(220, 239, 252, '', '2017-07-06 04:22:14', '2017-07-06 04:22:14'),
(221, 239, 253, '', '2017-07-06 04:22:14', '2017-07-06 04:22:14'),
(226, 241, 258, '', '2017-07-06 05:38:22', '2017-07-06 05:38:22'),
(227, 241, 259, '', '2017-07-06 05:38:22', '2017-07-06 05:38:22'),
(228, 241, 260, '', '2017-07-06 05:38:22', '2017-07-06 05:38:22'),
(229, 241, 261, '', '2017-07-06 05:38:22', '2017-07-06 05:38:22'),
(234, 243, 266, '', '2017-07-07 04:11:43', '2017-07-07 04:11:43'),
(235, 243, 267, '', '2017-07-07 04:11:43', '2017-07-07 04:11:43'),
(236, 243, 268, '', '2017-07-07 04:11:43', '2017-07-07 04:11:43'),
(237, 243, 269, '', '2017-07-07 04:11:43', '2017-07-07 04:11:43'),
(238, 244, 270, '', '2017-07-07 04:19:30', '2017-07-07 04:19:30'),
(239, 244, 271, '', '2017-07-07 04:19:30', '2017-07-07 04:19:30'),
(240, 244, 272, '', '2017-07-07 04:19:30', '2017-07-07 04:19:30'),
(241, 244, 273, '', '2017-07-07 04:19:30', '2017-07-07 04:19:30'),
(242, 245, 274, '', '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(243, 245, 275, '', '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(244, 245, 276, '', '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(245, 245, 277, '', '2017-07-07 04:33:46', '2017-07-07 04:33:46'),
(246, 246, 278, '', '2017-07-07 05:20:32', '2017-07-07 05:20:32'),
(247, 246, 279, '', '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(248, 246, 280, '', '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(249, 246, 281, '', '2017-07-07 05:20:33', '2017-07-07 05:20:33'),
(250, 247, 282, '', '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(251, 247, 283, '', '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(252, 247, 284, '', '2017-07-07 05:32:20', '2017-07-07 05:32:20'),
(253, 247, 285, '', '2017-07-07 05:32:21', '2017-07-07 05:32:21'),
(254, 248, 286, '', '2017-07-07 06:31:18', '2017-07-07 06:31:18'),
(255, 249, 287, '', '2017-07-07 06:32:58', '2017-07-07 06:32:58'),
(256, 250, 288, '', '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(257, 250, 289, '', '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(258, 250, 290, '', '2017-07-07 07:21:17', '2017-07-07 07:21:17'),
(259, 250, 291, '', '2017-07-07 07:21:18', '2017-07-07 07:21:18'),
(260, 251, 292, '', '2017-07-07 09:00:39', '2017-07-07 09:00:39'),
(261, 252, 293, '', '2017-07-08 04:25:00', '2017-07-08 04:25:00'),
(262, 253, 294, '', '2017-07-08 04:25:29', '2017-07-08 04:25:29'),
(263, 254, 295, '', '2017-07-08 04:27:18', '2017-07-08 04:27:18'),
(264, 255, 296, '', '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(265, 255, 297, '', '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(266, 255, 298, '', '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(267, 255, 299, '', '2017-07-08 04:32:21', '2017-07-08 04:32:21'),
(272, 257, 304, '', '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(273, 257, 305, '', '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(274, 257, 306, '', '2017-07-08 05:43:29', '2017-07-08 05:43:29'),
(275, 258, 307, '', '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(276, 258, 308, '', '2017-07-08 05:51:23', '2017-07-08 05:51:23'),
(277, 258, 309, '', '2017-07-08 05:51:24', '2017-07-08 05:51:24'),
(278, 258, 310, '', '2017-07-08 05:51:24', '2017-07-08 05:51:24'),
(279, 259, 311, '', '2017-07-08 05:59:17', '2017-07-08 05:59:17'),
(280, 259, 312, '', '2017-07-08 05:59:17', '2017-07-08 05:59:17'),
(281, 260, 313, '', '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(282, 260, 314, '', '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(283, 260, 315, '', '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(284, 260, 316, '', '2017-07-08 06:42:17', '2017-07-08 06:42:17'),
(285, 261, 317, '', '2017-07-08 06:44:47', '2017-07-08 06:44:47'),
(286, 261, 318, '', '2017-07-08 06:44:48', '2017-07-08 06:44:48'),
(287, 261, 319, '', '2017-07-08 06:44:48', '2017-07-08 06:44:48'),
(288, 261, 320, '', '2017-07-08 06:44:48', '2017-07-08 06:44:48'),
(293, 263, 325, '', '2017-07-08 06:49:30', '2017-07-08 06:49:30'),
(294, 263, 326, '', '2017-07-08 06:49:30', '2017-07-08 06:49:30'),
(295, 264, 327, '', '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(296, 264, 328, '', '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(297, 264, 329, '', '2017-07-08 07:29:39', '2017-07-08 07:29:39'),
(298, 264, 330, '', '2017-07-08 07:29:40', '2017-07-08 07:29:40'),
(299, 12, 331, '', '2017-07-08 07:41:38', '2017-07-08 07:41:38'),
(300, 265, 332, '', '2017-07-08 07:45:49', '2017-07-08 07:45:49'),
(301, 265, 333, '', '2017-07-08 07:45:50', '2017-07-08 07:45:50'),
(302, 265, 334, '', '2017-07-08 07:45:50', '2017-07-08 07:45:50'),
(303, 265, 335, '', '2017-07-08 07:45:50', '2017-07-08 07:45:50'),
(304, 266, 336, '', '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(305, 266, 337, '', '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(306, 266, 338, '', '2017-07-08 08:02:03', '2017-07-08 08:02:03'),
(307, 267, 339, '', '2017-07-08 08:04:22', '2017-07-08 08:04:22'),
(308, 267, 340, '', '2017-07-08 08:04:22', '2017-07-08 08:04:22'),
(309, 268, 341, '', '2017-07-11 10:02:45', '2017-07-11 10:02:45'),
(310, 268, 342, '', '2017-07-11 10:02:45', '2017-07-11 10:02:45'),
(312, 270, 344, '', '2017-07-12 09:41:48', '2017-07-12 09:41:48'),
(313, 270, 345, '', '2017-07-12 09:41:49', '2017-07-12 09:41:49'),
(314, 270, 346, '', '2017-07-12 09:41:49', '2017-07-12 09:41:49'),
(315, 270, 347, '', '2017-07-12 09:41:49', '2017-07-12 09:41:49'),
(316, 271, 348, '', '2017-07-12 10:33:04', '2017-07-12 10:33:04'),
(317, 271, 349, '', '2017-07-12 10:33:05', '2017-07-12 10:33:05'),
(318, 272, 350, '', '2017-07-12 11:13:51', '2017-07-12 11:13:51'),
(319, 273, 351, '', '2017-07-17 04:05:54', '2017-07-17 04:05:54'),
(320, 274, 352, '', '2017-07-19 05:20:43', '2017-07-19 05:20:43'),
(321, 274, 353, '', '2017-07-19 05:20:43', '2017-07-19 05:20:43'),
(322, 274, 354, '', '2017-07-19 05:20:43', '2017-07-19 05:20:43'),
(323, 274, 355, '', '2017-07-19 05:20:43', '2017-07-19 05:20:43'),
(330, 279, 362, '', '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(331, 279, 363, '', '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(332, 279, 364, '', '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(333, 279, 365, '', '2017-08-03 05:44:02', '2017-08-03 05:44:02'),
(352, 279, 346, '', '2017-08-21 08:05:40', '2017-08-21 08:05:40'),
(353, 279, 347, '', '2017-08-21 08:05:40', '2017-08-21 08:05:40'),
(364, 284, 358, '', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(365, 284, 359, '', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(366, 284, 360, '', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(367, 284, 361, '', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(368, 286, 362, '', '2017-09-04 05:59:00', '2017-09-04 05:59:00'),
(369, 286, 363, '', '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(370, 286, 364, '', '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(371, 286, 365, '', '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(376, 288, 370, '', '2017-09-04 06:17:22', '2017-09-04 06:17:22'),
(377, 289, 371, '', '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(378, 289, 372, '', '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(379, 290, 373, '', '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(380, 290, 374, '', '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(381, 291, 375, '', '2017-09-04 06:24:35', '2017-09-04 06:24:35'),
(382, 291, 376, '', '2017-09-04 06:24:36', '2017-09-04 06:24:36'),
(383, 292, 377, '', '2017-09-04 06:27:58', '2017-09-04 06:27:58'),
(384, 292, 378, '', '2017-09-04 06:27:59', '2017-09-04 06:27:59'),
(385, 292, 379, '', '2017-09-04 06:27:59', '2017-09-04 06:27:59'),
(386, 293, 380, '', '2017-09-04 06:30:38', '2017-09-04 06:30:38'),
(387, 293, 381, '', '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(388, 293, 382, '', '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(389, 293, 383, '', '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(390, 294, 384, '', '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(391, 294, 385, '', '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(392, 294, 386, '', '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(393, 294, 387, '', '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(394, 295, 388, '', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(395, 295, 389, '', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(396, 295, 390, '', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(397, 295, 391, '', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(398, 296, 392, '', '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(399, 296, 393, '', '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(400, 296, 394, '', '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(401, 296, 395, '', '2017-09-04 06:47:58', '2017-09-04 06:47:58');

-- --------------------------------------------------------

--
-- Table structure for table `contracts_shipment`
--

CREATE TABLE `contracts_shipment` (
  `Id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `company_detail` varchar(255) NOT NULL,
  `beneficiary_bank_detail` varchar(255) NOT NULL,
  `importer_address` varchar(255) NOT NULL,
  `notifier_party` text NOT NULL,
  `shipment_code` varchar(25) NOT NULL,
  `eta` varchar(255) NOT NULL,
  `payment_terms` varchar(255) NOT NULL,
  `container_size` varchar(255) NOT NULL,
  `quotation_freight` varchar(255) NOT NULL,
  `discharge_port` varchar(255) NOT NULL,
  `discharge_port2` varchar(255) NOT NULL,
  `discharge_port3` varchar(255) NOT NULL,
  `notes` text,
  `shipment_status` varchar(255) NOT NULL DEFAULT 'contract' COMMENT 'contract/delivery/rawmaterial/stuffing/complete',
  `document_ready_on` varchar(255) NOT NULL,
  `document_status` varchar(255) NOT NULL,
  `port_loading` int(11) NOT NULL,
  `final_destination` varchar(255) NOT NULL,
  `final_destination2` varchar(255) NOT NULL,
  `final_destination3` varchar(255) NOT NULL,
  `loading_at` varchar(255) DEFAULT NULL,
  `loading_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `docs_sent` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `docs_outstanding` varchar(255) DEFAULT NULL,
  `containers_detail` text,
  `containers_detail2` text,
  `dbk_scheme_no` varchar(255) DEFAULT '',
  `dbk_scheme` varchar(255) DEFAULT NULL,
  `examination_date` varchar(255) DEFAULT NULL,
  `invoice_no` varchar(100) DEFAULT '',
  `delivery_terms` varchar(255) NOT NULL,
  `export_invoice_date` varchar(255) DEFAULT NULL,
  `are_no` varchar(255) DEFAULT NULL,
  `are_date` varchar(255) DEFAULT NULL,
  `dbk_tariff_item_no` varchar(255) DEFAULT NULL,
  `dbk_rate` varchar(255) DEFAULT NULL,
  `examining_officer` varchar(255) DEFAULT NULL,
  `supervision_officer` varchar(255) DEFAULT NULL,
  `file_no` text,
  `examined` text,
  `multiple_invoice` tinyint(1) NOT NULL DEFAULT '0',
  `multiple_invoice_data` text,
  `invoice_buyer_address` text,
  `invoice_notifier_address` text,
  `invoice_consignee_address` text,
  `lc_no` varchar(255) DEFAULT '',
  `lc_date` date DEFAULT NULL,
  `bl_no` varchar(255) DEFAULT '',
  `bl_date` date DEFAULT NULL,
  `voyage_no` varchar(255) NOT NULL,
  `drawn_under` text,
  `specification` text,
  `final_destination_country` varchar(255) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `comm_invoice_note` text,
  `comm_invoice_note2` text,
  `comm_bank_no` varchar(255) DEFAULT '',
  `comm_bank_detail` text,
  `comm_eta` varchar(255) NOT NULL,
  `stuffing_phase` tinyint(2) DEFAULT '0',
  `markings` text,
  `flexitank` tinyint(1) DEFAULT '0',
  `comm_invoice_no` varchar(255) DEFAULT NULL,
  `comm_invoice_date` date DEFAULT '0000-00-00',
  `comm_freight` varchar(255) DEFAULT '0',
  `extra_charge_name` varchar(255) DEFAULT NULL,
  `extra_charge` varchar(255) DEFAULT '0',
  `comm_invoice_discount` varchar(255) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contracts_shipment`
--

INSERT INTO `contracts_shipment` (`Id`, `contract_id`, `company_detail`, `beneficiary_bank_detail`, `importer_address`, `notifier_party`, `shipment_code`, `eta`, `payment_terms`, `container_size`, `quotation_freight`, `discharge_port`, `discharge_port2`, `discharge_port3`, `notes`, `shipment_status`, `document_ready_on`, `document_status`, `port_loading`, `final_destination`, `final_destination2`, `final_destination3`, `loading_at`, `loading_date`, `docs_sent`, `docs_outstanding`, `containers_detail`, `containers_detail2`, `dbk_scheme_no`, `dbk_scheme`, `examination_date`, `invoice_no`, `delivery_terms`, `export_invoice_date`, `are_no`, `are_date`, `dbk_tariff_item_no`, `dbk_rate`, `examining_officer`, `supervision_officer`, `file_no`, `examined`, `multiple_invoice`, `multiple_invoice_data`, `invoice_buyer_address`, `invoice_notifier_address`, `invoice_consignee_address`, `lc_no`, `lc_date`, `bl_no`, `bl_date`, `voyage_no`, `drawn_under`, `specification`, `final_destination_country`, `comm_invoice_note`, `comm_invoice_note2`, `comm_bank_no`, `comm_bank_detail`, `comm_eta`, `stuffing_phase`, `markings`, `flexitank`, `comm_invoice_no`, `comm_invoice_date`, `comm_freight`, `extra_charge_name`, `extra_charge`, `comm_invoice_discount`, `created_at`, `updated_at`) VALUES
(8, 8, '', '', '0', '', 'A', '14TH JAN =2017', '', '[\"1\",\"\"]', '', '', '', '', 'REF NO.:STKOTH17-013', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2016-12-28 00:19:06', '2016-12-29 19:15:58'),
(11, 11, '', '', '0', '', '15153090', 'WITH ONE WEEK GAP', '', '[\"20\'\",\"\"]', '', '', '', '', '21 days detention free at POD.', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-10 21:19:15', '2017-01-10 21:52:22'),
(12, 12, '', '', '0', '', 'A', 'EACH SHIPMENT =44 MT ( WITH ONE WEEK GAP)', '', '[\"2\",\"\"]', '', '', '', '', '21 DAYS DETENTION FREE AT POD', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-11 01:14:52', '2017-01-11 01:14:52'),
(13, 12, '', '', '0', '', 'B', 'EACH SHIPMENT =44 MT ( WITH ONE WEEK GAP)', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-11 01:19:28', '2017-01-11 01:19:28'),
(14, 12, '', '', '0', '', 'C', 'EACH SHIPMENT =44 MT ( WITH ONE WEEK GAP)', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-11 01:19:53', '2017-01-11 01:19:53'),
(15, 13, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', 'ETD BEFORE 6TH FEB 2017', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-22 19:36:27', '2017-01-22 19:36:27'),
(16, 14, '', '', '0', '', 'A', 'LAST WEEK OF FEB-MARCH', '', '[\"6\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-22 19:51:38', '2017-01-22 19:51:38'),
(17, 15, '', '', '0', '', 'A', 'ETA BEFORE 10TH FEB-2017', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-23 00:14:17', '2017-01-23 00:14:17'),
(18, 16, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-25 17:53:46', '2017-01-25 17:53:46'),
(19, 17, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-25 18:16:49', '2017-01-25 18:16:49'),
(20, 18, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-25 18:30:40', '2017-01-25 18:30:40'),
(21, 19, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-25 22:11:23', '2017-01-25 22:11:23'),
(22, 20, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"1\"]', '[\"1\",\"2\"]', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"11\"],\"twentySelfSeal\":[\"\"],\"twentyLineSeal\":[\"\"],\"twentyPkg\":[\"0 Packages\"],\"twentyGw\":[\"0\"],\"twentyNw\":[\"\"],\"fortyContainer\":[\"22\"],\"fortySelfSeal\":[\"\"],\"fortyLineSeal\":[\"\"],\"fortyPkg\":[\"\"],\"fortyGw\":[\"\"],\"fortyNw\":[\"\"]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]}}', '', 'yes', '2017-07-22', '', '', '2017-07-22', '', '0000-00-00', '', '', '', '', '', '', 0, NULL, '121 ALBERT STREET, FLEET, HAMPSHIRE GU51 3SR\r\nTELEPHONE : 01252 613058 FAX 01252 616374\r\nEMAIL : jon@hampshire-commodities.co.uk\r\n', 'HAMPSHIRE COMMODITIES LIMITED\r\n121 ALBERT STREET, FLEET, HAMPSHIRE GU51 3SR\r\nTELEPHONE : 01252 613058 FAX 01252 616374\r\nEMAIL : jon@hampshire-commodities.co.uk\r\n', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-01-25 22:43:56', '2017-07-22 10:59:15'),
(24, 22, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-26 01:41:42', '2017-01-26 01:41:42'),
(25, 23, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', 'FFA : 0.50 % MAX', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-26 16:24:56', '2017-01-26 16:24:56'),
(26, 24, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-01-26 16:27:23', '2017-01-26 16:27:23'),
(27, 25, '', '', '0', '', 'A', 'PROMPT', '', '[\"8\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"11\",\"22\",\"33\",\"44\",\"55\",\"66\",\"77\",\"88\"],\"twentySelfSeal\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyLineSeal\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyPkg\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyGw\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyNw\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"3\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"4\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"5\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"6\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"7\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"8\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]}}', '', 'yes', '2017-07-22', '', '', '2017-07-22', '', '0000-00-00', '', '', '', '', '', '', 0, NULL, '7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', 'SINOCHEM PLASTICS CO., LTD.\r\n7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-02-01 17:09:35', '2017-07-22 08:00:48'),
(28, 26, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-01 17:18:26', '2017-02-01 17:18:26'),
(29, 27, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-01 17:21:41', '2017-02-01 17:21:41'),
(30, 28, '', '', '0', '', 'A', 'TWO WEEK LATER THAN CO-419', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-01 17:39:46', '2017-02-01 17:39:46'),
(31, 29, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"MRKU6982001\"],\"twentySelfSeal\":[\"017873\"],\"twentyLineSeal\":[\"ML-IN2759496\"],\"twentyPkg\":[\"720\"],\"twentyGw\":[\"18220\"],\"twentyNw\":[\"18000\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"12HYDROXY STEARIC ACID\",\"HYDROGENATED CASTOR OIL - NORMAL\"],\"bn\":[\"GI\\/HCO\\/02\\/17\\/040\",\"\"],\"np\":[\"480\",\"240\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]}}', '2915B & 1516A', 'yes', '2017-02-07', '486', '', '2017-02-07', '73/16-17', '2017-02-07', '', '', 'PANKAJ KUMAR', 'R.A.JADEJA', 'S/469/EXP/FSPMCS/2015-16 DATED 20.10.2015', '20', 0, NULL, NULL, NULL, '', '', '2017-11-27', '', '2017-11-27', '', '', '', '', NULL, NULL, '', '', '', 1, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-01 17:45:35', '2017-03-27 19:37:19'),
(32, 30, '', '', '0', '', 'A', 'IMMIDIATE', '', '[\"2\",\"2\"]', '[\"\",\"\"]', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-01 18:41:26', '2017-07-21 10:52:10'),
(33, 31, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', 'PRE-ANALYSIS BY SGS', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-01 18:48:42', '2017-02-01 18:48:42'),
(34, 32, '', '', '0', '', 'A', '1 FCL : ETD END FEB-2017 + 1 FCL : ETD END MAR-2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-01 18:57:22', '2017-02-01 18:57:22'),
(35, 33, '', '', '0', '', 'A', 'ETA MID - END MAR - 2017', '', '[\"1\",\"2\"]', '[\"\",\"\"]', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"3423423\"],\"twentySelfSeal\":[\"\"],\"twentyLineSeal\":[\"\"],\"twentyPkg\":[\"\"],\"twentyGw\":[\"\"],\"twentyNw\":[\"\"],\"fortyContainer\":[\"324234\",\"23423423\"],\"fortySelfSeal\":[\"\",\"\"],\"fortyLineSeal\":[\"\",\"\"],\"fortyPkg\":[\"\",\"\"],\"fortyGw\":[\"\",\"\"],\"fortyNw\":[\"\",\"\"]}', '{\"1\":{\"pn\":[\"HYDROGENATED CASTOR OIL - STANDARD\",\"12HYDROXY STEARIC ACID - STANDARD\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"2\":{\"pn\":[\"HYDROGENATED CASTOR OIL - STANDARD\",\"12HYDROXY STEARIC ACID - STANDARD\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"3\":{\"pn\":[\"HYDROGENATED CASTOR OIL - STANDARD\",\"12HYDROXY STEARIC ACID - STANDARD\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]}}', '', 'yes', '2017-07-24', '', '', '2017-07-24', '', '0000-00-00', '', '1', '', '', '', '', 0, NULL, 'Jesica Road, Campbellfield,\r\nVictoria 3061', 'AUSTRALIAN VINYLS CORPORATION PTY LTD\r\nJesica Road, Campbellfield,\r\nVictoria 3061', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-02-01 19:07:09', '2017-07-24 04:21:42'),
(36, 34, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 17:06:15', '2017-02-02 17:06:15'),
(37, 35, '', '', '0', '', 'A', '4 FCL PROMPT + 4FCL WITH 10DAYS GAP', '', '[\"8\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 17:42:48', '2017-02-02 17:42:48'),
(38, 36, '', '', '0', '', 'A', 'ETA 20-MAR,2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 17:51:09', '2017-02-02 17:51:09'),
(39, 37, '', '', '0', '', 'A', 'ETA MID APRIL - 2017', '', '[\"\",\"1\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 17:59:53', '2017-02-02 17:59:53'),
(40, 38, '', '', '0', '', 'A', 'ETD MID FEB-2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 18:10:34', '2017-02-02 18:10:34'),
(41, 39, '', '', '0', '', 'A', '1FCL PROMPT + 1FCL WITH 10DAYS GAP WITH 1ST FCL', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 22:10:24', '2017-02-02 22:10:24'),
(42, 40, '', '', '0', '', 'A', 'FEB - 2017', '', '[\"14\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 22:27:23', '2017-02-02 22:27:23'),
(43, 41, '', '', '0', '', 'A', '1ST TO 10TH FEB - 2017', '', '[\"24\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 22:36:35', '2017-02-02 22:36:35'),
(44, 42, '', '', '0', '', 'A', '500 MTS - 2ND HALF FEB-2017 + 500MTS  2ND HALF MAR-2017', '', '[\"48\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 22:40:17', '2017-02-02 22:40:17'),
(45, 43, '', '', '0', '', 'A', 'ETD 2ND HALF FEB - 2017', '', '[\"14\",\"\"]', '', '', '', '', '100MTS KAOHSIUNG\r\n200MTS TAICHUNG', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 22:47:58', '2017-02-02 22:47:58'),
(46, 44, '', '', '0', '', 'A', 'FEB - 2017', '', '[\"2\",\"\"]', '', '', '', '', 'ONE WEEK LATER THAN CO-378', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 22:56:28', '2017-02-02 22:56:28'),
(47, 45, '', '', '0', '', 'A', 'ETA EARLY MAY - 2017', '', '[\"\",\"1\"]', '', '', '', '', 'Hydroxyl Value (D1957),mg KOH/g : Min. 156\r\nMelting Point (D5440), â„ƒ : Min. 85\r\nIodine Value (D1959) : Max. 2.5\r\nAcid Value (D974), mg KOH/g : Max. 2.0\r\nSaponification Value (D1962), mg KOH/g : 175-185', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 23:01:30', '2017-02-02 23:02:53'),
(48, 46, '', '', '0', '', 'A', 'ETA MID MAR-2017', '', '[\"\",\"1\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[],\"twentySelfSeal\":[],\"twentyLineSeal\":[],\"twentyPkg\":[],\"twentyGw\":[],\"twentyNw\":[],\"fortyContainer\":[\"YMLU8503520\"],\"fortySelfSeal\":[\"GI003113\"],\"fortyLineSeal\":[\"082922\"],\"fortyPkg\":[\"44 BAG\"],\"fortyGw\":[\"22705\"],\"fortyNw\":[\"22000\"]}', '{\"1\":{\"pn\":[\"12HYDROXY STEARIC ACID - BLEACHED\"],\"bn\":[\"GI\\/12HSA\\/02\\/17\\/045\"],\"np\":[\"44 BAG\"],\"gw\":[\"\"],\"nw\":[\"500 KGS\"]}}', '3823B', 'yes', '', '501', '', '2017-02-14', '75/16-17 DT 14-02-2017', '2017-02-25', '3823199000', '1%', '', '', '', '', 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 1, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 23:07:39', '2017-03-05 17:48:11'),
(49, 47, '', '', '0', '', 'A', 'ETD 2ND HALF MAR - 2017', '', '[\"24\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 23:10:53', '2017-02-02 23:10:53'),
(50, 48, '', '', '0', '', 'A', '300MTS : ETD 16-28 FEB,2017 + 400MTS : ETD TWO WEEKS LATER THAN 1ST LOT', '', '[\"32\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 23:15:58', '2017-02-02 23:15:58'),
(51, 49, '', '', '0', '', 'A', 'ETA MID MAR-2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"fdgfsgdfg\"],\"twentySelfSeal\":[\"dfgdfg\"],\"twentyLineSeal\":[\"\"],\"twentyPkg\":[\"\"],\"twentyGw\":[\"\"],\"twentyNw\":[\"\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"12HYDROXY STEARIC ACID - STANDARD\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]}}', '', 'yes', '2017-07-24', '', '', '2017-07-24', '', '0000-00-00', '', '1.5', '', '', '', '', 0, NULL, '5-1, 1-CHOME, AZUCHIMACHI, CHUOKU, OSAKA,\r\nJAPAN\r\nTEL: +81 06 6262 2701', 'SHOEI YAKUHIN CO.,LTD.\r\n5-1, 1-CHOME, AZUCHIMACHI, CHUOKU, OSAKA,\r\nJAPAN\r\nTEL: +81 06 6262 2701', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-02-02 23:28:34', '2017-07-24 04:20:14'),
(52, 50, '', '', '0', '', 'A', 'ETD 15-28 FEB-2017', '', '[\"24\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-02 23:32:07', '2017-02-02 23:32:07'),
(53, 51, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '[\"\",\"\"]', '', '', '', 'PALLET SIZE :55 X 44 INCH ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-03 02:03:34', '2017-07-31 06:28:47'),
(54, 52, '', '', '0', '', 'A', 'PROMPT', '', '[\"ISO\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-03 02:06:29', '2017-02-03 02:06:29'),
(55, 53, '', '', '0', '', 'A', 'AS PER CALL', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-03 02:08:24', '2017-02-03 02:08:24'),
(56, 54, '', '', '0', '', 'A', 'AFTER ADVANCE', '', '[\"2\",\"0\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"CLHU2296932\",\"FSCU3985430\"],\"twentySelfSeal\":[\"GI003025\",\"GI003027\"],\"twentyLineSeal\":[\"1317713\",\"1317746\"],\"twentyPkg\":[\"680\",\"680\"],\"twentyGw\":[\"17285\",\"17280\"],\"twentyNw\":[\"17000\",\"17000\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"12HYDROXY STEARIC ACID\",\"HYDROGENATED CASTOR OIL - NORMAL\"],\"np\":[\"\",\"\"]},\"2\":{\"pn\":[\"12HYDROXY STEARIC ACID\",\"HYDROGENATED CASTOR OIL - NORMAL\"],\"np\":[\"\",\"\"]}}', '', 'yes', '2017-03-28', '564', '', '2017-03-28', '85', '2017-03-18', '', '1.5', '', '', '', '', 0, NULL, 'ADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\n', 'TADBIR GOSTAR ZAMAN\r\nADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran', 'TADBIR GOSTAR ZAMAN\r\nNO. 15, OSTAD SHAHRIYAR ALLEY, \r\nBAGHSHOMAL TABRIZ, IRAN\r\nNATIONAL ID : 10200452390', '', '0000-00-00', 'EMKMUNBND4185', '2017-03-23', '', '', '', '', NULL, NULL, '', '', '', 1, 'GIRNAR INDUSTRIES\r\nWEIGHT : 25KGS NET\r\nBATCH NO : GI/12HSA/031/0217\r\nMFG DATE : FEB-2017\r\nMADE IN INDIA', 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-03 02:15:46', '2017-03-29 00:39:30'),
(57, 55, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', 'FEB-2017', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-04 16:54:13', '2017-02-04 16:54:13'),
(58, 56, '', '', '0', '', 'A', 'ETD 16TH FEB, 2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-04 22:47:56', '2017-02-04 22:47:56'),
(59, 57, '', '', '0', '', 'A', 'ETD 16TH FEB, 2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-04 22:51:03', '2017-02-04 22:51:03'),
(60, 58, '', '', '0', '', 'A', 'AFTER 15TH FEB, 2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"WHLU0346864\",\"TGHU0916937\"],\"twentySelfSeal\":[\"017882\",\"017883\"],\"twentyLineSeal\":[\"WHL9981558\",\"WHL9981557\"],\"twentyPkg\":[\"80\",\"80\"],\"twentyGw\":[\"17440\",\"17450\"],\"twentyNw\":[\"16000\",\"16000\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"bn\":[\"GI\\/2017\\/02\\/130\"],\"np\":[\"80\"],\"gw\":[\"\"],\"nw\":[\"200KGS\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"bn\":[\"GI\\/2017\\/02\\/131\"],\"np\":[\"80\"],\"gw\":[\"\"],\"nw\":[\"200KG\"]}}', '1515A', 'yes', '2017-02-13', '500', '', '2017-02-13', 'NA', '', '', '', 'PANKAJ KUMAR', 'R.A.JADEJA', 'S/573/EXP/FSPMCS/2016-17 DATE 16.02.2016', '08 DRUMS', 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 1, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-04 22:54:20', '2017-03-05 17:31:44'),
(61, 59, '', '', '0', '', 'A', 'ETA MID MAR-2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-06 18:42:51', '2017-02-06 18:42:51'),
(62, 60, '', '', '0', '', 'A', 'PROMPT', '', '[\"10\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-07 17:16:16', '2017-02-07 17:16:16'),
(63, 61, '', '', '0', '', 'A', 'ETD END MAR-2017', '', '[\"1\",\"\"]', '', '', '', '', 'CARGO NOT BIGGER THAN 10MM', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-07 17:21:11', '2017-02-07 17:21:11'),
(64, 62, '', '', '0', '', 'A', 'PROMPT', '', '[\"6\",\"\"]', '', '', '', '', '4FCL PELLETS + 2FCL POWDER', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-10 01:02:22', '2017-02-10 01:02:22'),
(65, 63, '', '', '0', '', 'A', '2FCL PROMPT + 2FCL WITH TWO WEEKS GAP', '', '[\"4\",\"\"]', '', '', '', '', 'FFA : 0.75% MAX\r\nAV  : 1.5% MAX\r\nMIV : 0.20 MAX', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-10 01:06:27', '2017-02-10 01:06:27'),
(66, 64, '', '', '0', '', 'A', 'FEB - 2017', '', '[\"7\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-10 16:55:52', '2017-02-10 16:55:52'),
(67, 65, '', '', '0', '', 'A', 'FEB -2017', '', '[\"\",\"40\\\"\"]', '', '', '', '', 'ACID VALUE : 180 MIN\r\nCOLOUR : 4G\r\nIV : 3 MAX\r\n\r\n$1680+$50 COMMISSION ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-11 16:32:08', '2017-02-11 16:32:08'),
(68, 66, '', '', '0', '', 'A', 'MARCH-17', '', '[\"\",\"40\"]', '', '', '', '', 'ACID VALUE : 180 MIN\r\nCOLOUR : 4G\r\nIV : 3 MAX\r\n\r\n$1695+$50 = COMMISSION ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-11 16:35:21', '2017-02-11 16:35:21'),
(69, 67, '', '', '0', '', 'A', 'ETA END FEB-2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-14 22:31:12', '2017-02-14 22:31:12'),
(70, 68, '', '', '0', '', 'A', 'ETD WEEK - 8', '', '[\"3\",\"\"]', '', '', '', '', 'WITH DTHC\r\n\r\nMOISTURE : 0.15MAX', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-14 22:40:20', '2017-02-14 22:40:38'),
(71, 69, '', '', '0', '', 'A', 'ETA END MAR-2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-14 22:44:53', '2017-02-14 22:44:53'),
(72, 70, '', '', '0', '', 'A', 'ETD 1ST - 10TH MAR, 2017', '', '[\"14\",\"\"]', '', '', '', '', '100MTS : KAOHSIUNG\r\n200MTS : TAICHUNG', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-14 22:49:57', '2017-02-14 22:49:57'),
(73, 71, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-21 17:53:36', '2017-02-21 17:53:36'),
(74, 72, '', '', '0', '', 'A', 'MARCH - 2017', '', '[\"14\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-21 17:55:44', '2017-02-21 17:55:44'),
(75, 73, '', '', '0', '', 'A', 'ETA MID APR-2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-21 23:04:30', '2017-02-21 23:04:30'),
(76, 74, '', '', '0', '', 'A', 'ETA EARLY MAY - 2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-21 23:05:39', '2017-02-21 23:05:39'),
(77, 75, '', '', '0', '', 'A', 'PROMPT', '', '[\"5\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-22 23:15:54', '2017-02-22 23:15:54'),
(78, 76, '', '', '0', '', 'A', 'ETA EARLY APR-2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"TEMU0271570\"],\"twentySelfSeal\":[\"017884\"],\"twentyLineSeal\":[\"SSA0113933\"],\"twentyPkg\":[\"80 DRUMS\"],\"twentyGw\":[\"17440\"],\"twentyNw\":[\"16000\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"bn\":[\"GI003011\"],\"np\":[\"80 DRUMS\"],\"gw\":[\"\"],\"nw\":[\"\"]}}', '1515A', 'yes', '2017-02-14', 'GI/EXP/P0504/16-17', '', '2017-02-14', '', '', '15153090', '1%', 'PANKAJ KUMAR', 'R.A.JADEJA', 'S/573/EXP/FSPMCS/2016-17 DATE 16.02.2016', '08', 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 1, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-22 23:19:03', '2017-03-03 17:51:00'),
(79, 77, '', '', '0', '', 'A', 'ETD END MAR-2017', '', '[\"2\",\"\"]', '', '', '', '', 'USE LONG TRANSIT VOYAGE', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-22 23:22:48', '2017-02-22 23:22:48'),
(80, 78, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 17:10:15', '2017-02-24 17:10:15'),
(81, 79, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 17:18:53', '2017-02-24 17:18:53'),
(82, 80, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 17:28:50', '2017-02-24 17:28:50'),
(83, 81, '', '', '0', '', 'A', 'PROMPT', '', '[\"5\",\"\"]', '', '', '', '', '* Try to use Moonway Oy and Transitainer shipping Oy Instead of New Port\r\n* Heating charge of an hour also will be paid here with sea freight', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 18:20:01', '2017-02-24 18:20:01'),
(84, 82, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 18:45:31', '2017-02-24 18:45:31'),
(85, 83, '', '', '0', '', 'A', 'MARCH-17', '', '[\"\",\"\"]', '', '', '', '', '1 \"ISO TANK \"', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 18:48:48', '2017-02-24 18:48:48'),
(86, 84, '', '', '0', '', 'A', 'MARCH -17', '', '[\"\",\"\"]', '', '', '', '', '2 \"ISO \" TANK', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 19:05:58', '2017-02-24 19:05:58'),
(87, 85, '', '', '0', '', 'A', 'MARCH-17', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 19:10:02', '2017-02-24 19:10:02'),
(88, 86, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', 'WANHAI NOMINATED', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 22:30:05', '2017-02-24 22:30:05'),
(89, 87, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 22:34:45', '2017-02-24 22:34:45'),
(90, 88, '', '', '0', '', 'A', 'PROMPT', '', '[\"9\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 22:37:45', '2017-02-24 22:37:45'),
(91, 89, '', '', '0', '', 'A', 'PROMPT', '', '[\"10\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 22:40:55', '2017-02-24 22:40:55'),
(92, 90, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 22:46:29', '2017-02-24 22:46:29'),
(93, 91, '', '', '0', '', 'A', 'PROMPT', '', '[\"10\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-24 22:49:33', '2017-02-24 22:49:33'),
(94, 92, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', 'FLEXI WITH HEATING PAD', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-02-28 22:54:32', '2017-02-28 22:54:32'),
(95, 93, '', '', '0', '', 'A', 'MARCH - 2017', '', '[\"14\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-01 23:23:18', '2017-03-01 23:23:18');
INSERT INTO `contracts_shipment` (`Id`, `contract_id`, `company_detail`, `beneficiary_bank_detail`, `importer_address`, `notifier_party`, `shipment_code`, `eta`, `payment_terms`, `container_size`, `quotation_freight`, `discharge_port`, `discharge_port2`, `discharge_port3`, `notes`, `shipment_status`, `document_ready_on`, `document_status`, `port_loading`, `final_destination`, `final_destination2`, `final_destination3`, `loading_at`, `loading_date`, `docs_sent`, `docs_outstanding`, `containers_detail`, `containers_detail2`, `dbk_scheme_no`, `dbk_scheme`, `examination_date`, `invoice_no`, `delivery_terms`, `export_invoice_date`, `are_no`, `are_date`, `dbk_tariff_item_no`, `dbk_rate`, `examining_officer`, `supervision_officer`, `file_no`, `examined`, `multiple_invoice`, `multiple_invoice_data`, `invoice_buyer_address`, `invoice_notifier_address`, `invoice_consignee_address`, `lc_no`, `lc_date`, `bl_no`, `bl_date`, `voyage_no`, `drawn_under`, `specification`, `final_destination_country`, `comm_invoice_note`, `comm_invoice_note2`, `comm_bank_no`, `comm_bank_detail`, `comm_eta`, `stuffing_phase`, `markings`, `flexitank`, `comm_invoice_no`, `comm_invoice_date`, `comm_freight`, `extra_charge_name`, `extra_charge`, `comm_invoice_discount`, `created_at`, `updated_at`) VALUES
(96, 94, '', '', '0', '', 'A', 'MARCH - 2017', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"WHLU0346864\",\"DSDSNKNDSKN\",\"SDDSFMGNKJDF\",\"SDGNKFGTJHJG\",\"CSNCGBJFJFGHJ\",\"SDFHGDJHFDV\",\"DFHJJJHMJ\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentySelfSeal\":[\"1243465\",\"017882\",\"234365\",\"32548\",\"2354576\",\"346589\",\"3465767\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyLineSeal\":[\"23547\",\"34534665\",\"45658769\",\"3547\",\"456768\",\"346789\",\"2354766\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyPkg\":[\"------\",\"------\",\"--------\",\"\",\"-------\",\"--------\",\"-------\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyGw\":[\"20456\",\"21607\",\"21456\",\"22000\",\"21456\",\"21567\",\"21000\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyNw\":[\"20456\",\"21506\",\"21456\",\"22000\",\"21456\",\"21567\",\"21000\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"2\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"3\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"4\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"5\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"6\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"7\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"8\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"9\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"bn\":[\"\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]}}', '2306A', 'yes', '2017-03-05', '510', '', '2017-02-16', '', '', '23069027', '1', '', '', '', '', 0, NULL, NULL, NULL, '', '', '2017-03-27', '', '2017-03-27', '', '', '', '', NULL, NULL, '', '', '', 1, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-01 23:24:40', '2017-03-27 18:56:56'),
(97, 95, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-03 16:28:44', '2017-03-03 16:28:44'),
(98, 96, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', 'SPECIFICATION :\r\nAcid Value : 175 Min\r\nSaponification Value : 180 190\r\nHydroxyl Value : 150 - 160\r\nIodine Value : 85 - 90\r\nColour Gardner : 5 Max\r\n', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-03 16:34:23', '2017-03-03 16:34:23'),
(99, 97, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-03 22:43:10', '2017-03-03 22:44:27'),
(100, 98, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', 'ISO TANK, FOB MUNDRA SHIPMENT', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-04 16:22:12', '2017-03-04 16:22:12'),
(101, 99, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', 'PROMPT', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-04 16:39:06', '2017-03-04 16:39:06'),
(102, 100, '', '', '0', '', 'A', 'PROMPT', '', '[\"6\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-04 16:44:52', '2017-03-04 16:44:52'),
(103, 101, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-11 06:45:22', '2017-03-11 06:45:22'),
(104, 102, '', '', '0', '', 'A', 'ETA MID APR-2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-16 23:49:02', '2017-03-16 23:49:02'),
(105, 103, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-16 23:52:47', '2017-03-16 23:52:47'),
(106, 104, '', '', '0', '', 'A', 'ETA BEFORE MID APR-2017', '', '[\"2\",\"\"]', '', '', '', '', 'Water ( Wt% )		0.5 Max\r\nACID VALUE		1.5 Max.\r\nF.F.A(AS OLECI)		1.0 % MAX.\r\nAppearance ( 25åº¦C ) Liquid\r\nColor (APHA )	G=2 Max\r\nColor on 5-1/4\" 	15Y / 1.5R  Max.\r\nSaponification Value(mgKOH/g)	176-187\r\nIodine Value(gI2/100g )		(81-91)\r\nHYDROXYL VALUE(mgKOH/g)		(161-169)\r\nGravity(Sp.gr/25åº¦C)		0.960 \r\n', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-16 23:58:09', '2017-03-16 23:58:09'),
(107, 105, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:04:50', '2017-03-17 00:04:50'),
(108, 106, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:06:54', '2017-03-17 00:06:54'),
(109, 107, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:08:28', '2017-03-17 00:08:28'),
(110, 108, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '2017-7-13', 'sadsadasd', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"BSIU2850079\",\"BSIU2849904\"],\"twentySelfSeal\":[\"017935\",\"017936\"],\"twentyLineSeal\":[\"930462\",\"930463\"],\"twentyPkg\":[\"80 DRUM\",\"80 DRUM\"],\"twentyGw\":[\"19545\",\"19546\"],\"twentyNw\":[\"18000\",\"18000\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"]}}', '', 'yes', '2017-03-20', '569', '', '2017-03-20', '', '', '', '', 'PANKAJ KUMAR', 'R.A.JADEJA', 'S/573/EXP/FSPMCS/2016-17 DATED 16,02,2016', '8', 0, NULL, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\n', 'OOO Nortex\r\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\n', 'OOO NORTEX\r\n101000, RUSSIA, MOSCOW\r\nARMYANSKIY PER. 4 BLD. 1 AB', '', '0000-00-00', 'INIXY051146', '2017-03-29', '', '', '', '', NULL, NULL, '', '', '', 1, 'CASTOR OIL FIRST SPICEAL GRADE\r\nMADE IN INDIA\r\nLOT NO : GI/2017/01/221\r\nNORTEX LTD.\r\n\r\n\r\nCASTOR OIL FIRST SPICEAL GRADE\r\nMADE IN INDIA\r\nLOT NO : GI/2017/01/222\r\nNORTEX LTD.', 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:10:09', '2017-03-29 00:01:22'),
(111, 109, '', '', '0', '', 'A', 'ETA MID MAY-2017', '', '[\"\",\"1\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:13:14', '2017-03-17 00:13:14'),
(112, 110, '', '', '0', '', 'A', 'PROMPT', '', '[\"4\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:14:55', '2017-03-17 00:14:55'),
(113, 111, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:18:20', '2017-03-17 00:18:20'),
(114, 112, '', '', '0', '', 'A', 'MARCH - 2017', '', '[\"20\",\"\"]', '', '', '', '', 'SHIPMENT IN MARCH-2017 ONE WEEK GAP OF TWO LOTS IN 10FCL', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"WHKN 2353408\",\"WHG29389890\",\"WEHG28288109\",\"WGHJ12872187\",\"WGJHE2198082\",\"WGDG29809129\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentySelfSeal\":[\"3250396549\",\"2340349346\",\"238098129082\",\"213812737\",\"12939899\",\"128388183\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyLineSeal\":[\"325469\",\"34509435609\",\"213721\",\"217382189\",\"2198128098\",\"217812097\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyPkg\":[\"345345345345\",\"34534534534\",\"34534534\",\"345345345345\",\"345345345\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyGw\":[\"21435\",\"21520\",\"21775\",\"21690\",\"21275\",\"21325\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyNw\":[\"21435\",\"21520\",\"21775\",\"21690\",\"21275\",\"21325\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"21435\"],\"nw\":[\"21435\"]},\"2\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"3\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"4\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"5345\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"5\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"6\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"7\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"8\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"9\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"10\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"11\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"12\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"13\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"14\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"15\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"16\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"17\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"18\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"19\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]},\"20\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\"],\"gw\":[\"\"],\"nw\":[\"\"]}}', '', 'yes', '2017-04-02', '002', '', '2017-04-02', '', '0000-00-00', '', '1', '', '', '', '', 0, '[{\"invoice_no\":\"002\",\"invoice_date\":\"2017-04-02\",\"uptotwenty\":\"6\",\"uptoforty\":\"0\",\"product_0\":\"129.020\"},{\"invoice_no\":\"\",\"invoice_date\":\"2017-04-06\",\"uptotwenty\":\"\",\"uptoforty\":\"\",\"product_0\":\"\"}]', '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\r\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', '', '', '2017-06-04', '', '2017-06-04', '', '', 'dfsgdg', '', 'bsdgsfg', 'Country Of Origin : India', '', '', '', 1, '', 1, '', '0000-00-00', '10', 'Extra CHarge', '400', '500', '2017-03-17 00:22:22', '2017-06-04 06:09:43'),
(115, 113, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:25:31', '2017-03-17 00:25:31'),
(116, 114, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"1\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:28:52', '2017-03-17 00:28:52'),
(117, 115, '', '', '0', '', 'A', 'ETA END APRIL-2017', '', '[\"1\",\"\"]', '', '', '', '', 'USE 44X44 PELLET WITH 700KG WEIGHT OF EACH PELLET', 'contract', '2017-7-13', 'sadsadsad', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"SEGU1879779\"],\"twentySelfSeal\":[\"GI003172\"],\"twentyLineSeal\":[\"SSA0113940\"],\"twentyPkg\":[\"560 BAGS\"],\"twentyGw\":[\"14580\"],\"twentyNw\":[\"14000\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"12HYDROXY STEARIC ACID - BLEACHED\"],\"np\":[\"\"]}}', '', 'yes', '2017-03-27', '580', '', '2017-03-27', '89', '2017-03-27', '', '2915B', '', '', '', '', 0, NULL, 'Kowon Building 20-19\r\nYangjae-Dong,Seocho-ku Seoul,Korea\r\n', 'KOWON EVERCHEM CORPORATION\r\nKowon Building 20-19\r\nYangjae-Dong,Seocho-ku Seoul,Korea\r\n', 'TO THE ORDER OF WOORI BANK', 'MD1QV703NS90097', '2017-03-22', 'KMTCPAV0032725', '2017-04-04', '', 'DRAWN UNDER DOCUMENTARY CREDIT NO. MD1QV703NS90097 DATED 22.03.2017 OF WOORI BANK', '', '', NULL, NULL, 'HVBKKRSEXXX', 'WOORI BANK\r\n17 WORLD CUP BUK-RO 60-GIL MAPO-GU SEOUL 03921\r\nREPUBLIC OF KOREA (1585 SANGAM-DONG).', '', 1, '12 HYDROXY STEARIC ACID BLEACHED\r\nNET WEIGHT : 25KGS\r\nBATCH NO.GI/12HSA/BL071\r\nMFG DATE : MAR-2017\r\nEXP DATE : FEB-2019\r\nMADE IN INDIA', 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:33:47', '2017-03-29 00:12:40'),
(118, 116, '', '', '0', '', 'A', 'PROMPT', '', '[\"10\",\"\"]', '', '', '', '', 'STICK LABEL ON CONTAINER DOOR', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:37:04', '2017-03-17 00:37:04'),
(119, 117, '', '', '0', '', 'A', 'PROMPT', '', '[\"7\",\"\"]', '', '', '', '', '', 'contract', '2017-7-13', 'sadasdasd', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"FCIU2978313\",\"TCKU2387081\",\"\",\"\",\"\",\"\",\"\"],\"twentySelfSeal\":[\"GI003029\",\"GI003030\",\"\",\"\",\"\",\"\",\"\"],\"twentyLineSeal\":[\"1374366\",\"1374361\",\"\",\"\",\"\",\"\",\"\"],\"twentyPkg\":[\"468\",\"467\",\"\",\"\",\"\",\"\",\"\"],\"twentyGw\":[\"18710\",\"18660\",\"\",\"\",\"\",\"\",\"\"],\"twentyNw\":[\"18675\",\"18660\",\"\",\"\",\"\",\"\",\"\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\"],\"np\":[\"\"]},\"2\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\"],\"np\":[\"\"]},\"3\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\"],\"np\":[\"\"]},\"4\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\"],\"np\":[\"\"]},\"5\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\"],\"np\":[\"\"]},\"6\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\"],\"np\":[\"\"]},\"7\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\"],\"np\":[\"\"]}}', '', 'yes', '2017-03-28', '565', '', '2017-03-28', '', '', '', '1', '', '', '', '', 0, '[]', 'TD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\n', 'CAHAYA LAGENDA RESOURCES SDN. BHD.\r\nTD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\n', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 1, '', 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-17 00:38:34', '2017-03-29 00:37:50'),
(120, 118, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', 'FOB MUNDRA ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-18 00:03:27', '2017-03-18 00:03:27'),
(121, 119, '', '', '0', '', 'A', 'ETA AFTER 10-MAY, 2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-18 01:27:19', '2017-03-18 01:27:19'),
(122, 120, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-19 16:30:57', '2017-03-19 16:30:57'),
(123, 121, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-19 16:37:57', '2017-03-19 16:37:57'),
(124, 122, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-19 16:43:01', '2017-03-19 16:43:01'),
(125, 123, '', '', '0', '', 'A', 'PROMPT', '', '[\"5\",\"\"]', '', '', '', '', 'USE BLT FLEXI TANK', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-19 16:49:24', '2017-03-19 16:49:24'),
(126, 124, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"1\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-19 16:53:16', '2017-03-19 16:53:16'),
(127, 125, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-23 16:52:52', '2017-03-23 16:52:52'),
(128, 126, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-23 16:55:33', '2017-03-23 16:55:33'),
(129, 127, '', '', '0', '', 'A', 'PROMPT', '', '[\"9\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-23 16:58:48', '2017-03-23 16:58:48'),
(130, 128, '', '', '0', '', 'A', '4 FCL PROMPT + 4FCL WITH A WEEK GAP', '', '[\"8\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '12511', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-23 17:01:10', '2017-03-23 17:01:10'),
(131, 129, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-23 17:03:27', '2017-03-23 17:03:27'),
(132, 130, '', '', '0', '', 'A', '15DAYS AFTER CO-499 SHIPMENT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-23 17:05:33', '2017-03-23 17:05:33'),
(133, 131, '', '', '0', '', 'A', '15DAYS AFTER CO-500 SHIPMENT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"\",\"\"],\"twentySelfSeal\":[\"\",\"\"],\"twentyLineSeal\":[\"\",\"\"],\"twentyPkg\":[\"60 Packages\",\"140 Drums\"],\"twentyGw\":[\"937\",\"550\"],\"twentyNw\":[\"935\",\"545\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\",\"12HYDROXY STEARIC ACID\"],\"np\":[\"50 Drums\",\"10 IBC\"],\"gw\":[\"512\",\"425\"],\"nw\":[\"510\",\"425\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\",\"12HYDROXY STEARIC ACID\"],\"np\":[\"100 Drums\",\"40 Drums\"],\"gw\":[\"100\",\"450\"],\"nw\":[\"95\",\"450\"]}}', '', 'yes', '2017-06-06', '', '', '2017-06-06', '', '0000-00-00', '', '', '', '', '', '', 0, NULL, 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\r\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-03-23 17:07:13', '2017-06-06 04:26:13'),
(134, 132, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '2017-7-13', 'asdasdasd', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"BSIU2850079\",\"BSIU2849904\"],\"twentySelfSeal\":[\"017935\",\"017936\"],\"twentyLineSeal\":[\"930462\",\"930463\"],\"twentyPkg\":[\"80 DRUM\",\"80 DRUMS\"],\"twentyGw\":[\"19545\",\"19550\"],\"twentyNw\":[\"18000\",\"18000\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"\"]}}', '', 'yes', '2017-03-20', '569', '', '2017-03-20', '', '', '', '1515A', 'PANKAJ KUMAR', 'R.A.JADEJA', '573', '8', 0, NULL, '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', 'OOO Nortex\r\n101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, 'CASOTR OIL FIRST SPICAL GRADE\r\nMADE IN INDIA\r\nLOT NO : GI/2017/03/222\r\nNET WEIGHT : 225KGS\r\nNORTEX LTD.', 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-23 17:09:12', '2017-03-28 21:44:30'),
(135, 133, '', '', '0', '', 'A', 'PROMPT', '', '[\"4\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-03-24 18:55:46', '2017-03-24 18:55:46'),
(136, 134, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-05 02:17:38', '2017-04-05 02:17:38'),
(137, 135, '', '', '0', '', 'A', 'MID OF APRIL 2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-05 02:23:10', '2017-04-05 02:23:10'),
(138, 136, '', '', '0', '', 'A', 'MARCH -17', '', '[\"\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 18:52:52', '2017-04-06 18:52:52'),
(139, 137, '', '', '0', '', 'A', 'ETD 2ND HALF MAY - 2017', '', '[\"9\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:00:34', '2017-04-06 19:00:34'),
(140, 138, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:02:46', '2017-04-06 19:02:46'),
(141, 139, '', '', '0', '', 'A', 'MARCH-17', '', '[\"\",\"\"]', '', '', '', '', 'ISO TANK \r\n', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:04:27', '2017-04-06 19:04:27'),
(142, 140, '', '', '0', '', 'A', 'ETD LATE APRIL-2017', '', '[\"3\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:06:47', '2017-04-06 19:06:47'),
(143, 141, '', '', '0', '', 'A', 'APRIL-17', '', '[\"\",\"\"]', '', '', '', '', 'ISO TANK', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:07:02', '2017-04-06 19:07:02'),
(144, 142, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', 'APRIL SHIPMENT ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:15:50', '2017-04-06 19:15:50'),
(145, 143, '', '', '0', '', 'A', 'APRIL-17', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:19:34', '2017-04-06 19:19:34'),
(146, 144, '', '', '0', '', 'A', 'APRIL-17', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:22:40', '2017-04-06 19:22:40'),
(147, 145, '', '', '0', '', 'A', 'PROMPT', '', '[\"13\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:24:21', '2017-04-06 19:24:21'),
(148, 146, '', '', '0', '', 'A', '', '', '[\"\",\"\"]', '', '', '', '', '2 X 40\" FCL \r\n\r\nEACH CONTAINER 26.6 MT ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 19:27:30', '2017-04-06 19:27:30'),
(149, 147, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"1\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"\",\"\"],\"twentySelfSeal\":[\"\",\"\"],\"twentyLineSeal\":[\"\",\"\"],\"twentyPkg\":[\"30 Packages\",\"60 Packages\"],\"twentyGw\":[\"\",\"\"],\"twentyNw\":[\"\",\"\"],\"fortyContainer\":[\"\"],\"fortySelfSeal\":[\"\"],\"fortyLineSeal\":[\"\"],\"fortyPkg\":[\"\"],\"fortyGw\":[\"\"],\"fortyNw\":[\"\"]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\",\"12HYDROXY STEARIC ACID - NORMAL\"],\"np\":[\"20\",\"10\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\",\"12HYDROXY STEARIC ACID - NORMAL\"],\"np\":[\"25\",\"35\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"3\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\",\"12HYDROXY STEARIC ACID - NORMAL\"],\"np\":[\"12\",\"54\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]}}', '', 'yes', '2017-06-14', '', '', '2017-06-14', '', '0000-00-00', '', '', '', '', '', '', 0, NULL, '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-04-06 22:25:50', '2017-06-14 08:18:40'),
(150, 148, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 22:57:40', '2017-04-06 22:57:40'),
(151, 149, '', '', '0', '', 'A', 'ETD 2ND HALF APR - 2017', '', '[\"14\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:06:40', '2017-04-06 23:06:40'),
(152, 150, 'Girnar Industries \r\nOpp. Rajdhani weighbridge,\r\nRajkot Road, Dolatpara, Junagadh(362003),\r\nGUJARAT, INDIA\r\nTel. +91 285 2660893', '', 'NO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', 'YU YUAN TECHNOLOGY CO., LTD \r\nNO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', 'A', 'ETD 2ND HALF APR - 2017', 'LC AT SIGHT', '[\"14\",\"\"]', '', 'KAOHSIUNG', '', '', '', 'contract', '2017-08-30', '', 2, 'KAOHSIUNG', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', 'CFR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 'YU YUAN TECHNOLOGY CO., LTD \r\nNO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', '', '2017-08-30', '', '2017-08-30', '', '', '', '2', '', '', 'AXIS BANK', 'Shop No. 1,2,3 Raiji Nagar Shoping Centre,\r\nN K mehta Road,Motibag,\r\nJunagadh(362001), Gujarat, INDIA.\r\nA/C No. 912030004364581, SWIFT Code:- AXISINBB087', '30-11--0001', 0, NULL, 0, '', '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:13:50', '2017-08-30 08:27:57'),
(153, 151, '', '', '0', '', 'A', 'APRIL - 2017', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:21:00', '2017-04-06 23:21:00'),
(154, 152, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:25:50', '2017-04-06 23:25:50'),
(155, 153, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:27:10', '2017-04-06 23:27:10'),
(156, 154, '', '', '0', '', 'A', 'ETD 15DAYS AFTER 2109', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:28:05', '2017-04-06 23:28:05'),
(157, 155, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"5\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:33:46', '2017-04-06 23:33:46'),
(158, 156, '', '', '0', '', 'A', 'ETD END APR - 2017', '', '[\"2\",\"\"]', '', '', '', '', 'CERTIFICATE OF CONFORMITY', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:36:09', '2017-04-06 23:36:09'),
(159, 157, '', '', '0', '', 'A', 'APRIL - 2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:43:22', '2017-04-06 23:43:22'),
(160, 158, '', '', '0', '', 'A', 'PROMPT', '', '[\"3\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-06 23:45:59', '2017-04-06 23:45:59'),
(161, 159, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-11 16:28:02', '2017-04-11 16:28:02'),
(162, 160, '', '', '0', '', 'A', 'ETD END APR - 2017', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-11 16:33:50', '2017-04-11 16:33:50'),
(163, 161, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-11 16:37:24', '2017-04-11 16:37:24'),
(164, 162, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-11 16:40:05', '2017-04-11 16:40:05'),
(165, 163, '', '', '0', '', 'A', 'PROMPT', '', '[\"\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-11 16:43:02', '2017-04-11 16:43:02'),
(166, 164, '', '', '0', '', 'A', '23/24TH APRIL 2017', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-11 16:48:59', '2017-04-11 16:48:59'),
(168, 166, '', '', '0', '', 'A', 'PROMPT', '', '[\"5\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-16 17:47:37', '2017-04-16 17:47:37'),
(169, 167, '', '', '0', '', 'A', 'ETD BY FIRST WEEK OF MAY', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-16 17:50:52', '2017-04-16 17:50:52'),
(170, 168, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-17 17:04:44', '2017-04-17 17:04:44'),
(171, 169, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-17 17:08:51', '2017-04-17 17:08:51'),
(172, 170, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"1\"]', '[\"\",\"\"]', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-17 17:12:08', '2017-07-22 04:15:44'),
(173, 171, '', '', '0', '', 'A', 'PROMPT', '', '[\"10\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-17 17:14:16', '2017-04-17 17:14:16');
INSERT INTO `contracts_shipment` (`Id`, `contract_id`, `company_detail`, `beneficiary_bank_detail`, `importer_address`, `notifier_party`, `shipment_code`, `eta`, `payment_terms`, `container_size`, `quotation_freight`, `discharge_port`, `discharge_port2`, `discharge_port3`, `notes`, `shipment_status`, `document_ready_on`, `document_status`, `port_loading`, `final_destination`, `final_destination2`, `final_destination3`, `loading_at`, `loading_date`, `docs_sent`, `docs_outstanding`, `containers_detail`, `containers_detail2`, `dbk_scheme_no`, `dbk_scheme`, `examination_date`, `invoice_no`, `delivery_terms`, `export_invoice_date`, `are_no`, `are_date`, `dbk_tariff_item_no`, `dbk_rate`, `examining_officer`, `supervision_officer`, `file_no`, `examined`, `multiple_invoice`, `multiple_invoice_data`, `invoice_buyer_address`, `invoice_notifier_address`, `invoice_consignee_address`, `lc_no`, `lc_date`, `bl_no`, `bl_date`, `voyage_no`, `drawn_under`, `specification`, `final_destination_country`, `comm_invoice_note`, `comm_invoice_note2`, `comm_bank_no`, `comm_bank_detail`, `comm_eta`, `stuffing_phase`, `markings`, `flexitank`, `comm_invoice_no`, `comm_invoice_date`, `comm_freight`, `extra_charge_name`, `extra_charge`, `comm_invoice_discount`, `created_at`, `updated_at`) VALUES
(174, 172, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"1\"]', '[\"\",\"\"]', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentySelfSeal\":[\"1\",\"2\",\"3\",\"4\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyLineSeal\":[\"A\",\"B\",\"C\",\"D\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyPkg\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyGw\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyNw\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"2\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"3\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"4\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"5\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"6\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"7\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"8\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"9\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"10\":{\"pn\":[\"INDIAN CASTOR SEED EXTRACTION MEAL\",\"INDIAN CASTOR SEED EXTRACTION MEAL\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]}}', '', 'yes', '2017-06-23', '', '', '2017-06-23', '', '0000-00-00', '', '1', '', '', '', '8 drums from each of the containers.', 1, '[{\"invoice_no\":\"A\",\"invoice_date\":\"2017-06-22\",\"uptotwenty\":\"2\",\"uptoforty\":\"\",\"product_0\":\"1\",\"product_1\":\"\"},{\"invoice_no\":\"B\",\"invoice_date\":\"2017-06-23\",\"uptotwenty\":\"2\",\"uptoforty\":\"\",\"product_0\":\"1\",\"product_1\":\"\"}]', '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', 'B.J. HONG ENTERPRISE CO., LTD.\r\n11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-04-17 17:19:26', '2017-07-22 04:13:46'),
(175, 173, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-21 22:06:41', '2017-04-21 22:06:41'),
(176, 174, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"1\"]', '[\"1\",\"1\"]', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-21 22:08:03', '2017-07-22 04:26:39'),
(177, 175, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-24 17:05:14', '2017-04-24 17:05:14'),
(178, 176, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-24 17:26:42', '2017-04-24 17:26:42'),
(179, 177, '', '', '0', '', 'A', 'END OF APRIL', '', '[\"20\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-24 17:45:26', '2017-04-24 17:45:26'),
(180, 178, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-24 18:04:30', '2017-04-24 18:04:30'),
(181, 179, '', '', '0', '', 'A', 'prompt', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-24 18:15:18', '2017-04-24 18:15:18'),
(182, 180, '', '', '0', '', 'A', '1ST CONTAINER = PROMPT   2ND CONTAINER= JULY ', '', '[\"20\",\"1\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentySelfSeal\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyLineSeal\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyPkg\":[\"25 Packages\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyGw\":[\"600\",\"\",\"350\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"twentyNw\":[\"600\",\"\",\"350\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"],\"fortyContainer\":[\"\"],\"fortySelfSeal\":[\"\"],\"fortyLineSeal\":[\"\"],\"fortyPkg\":[\"510 Packages\"],\"fortyGw\":[\"0\"],\"fortyNw\":[\"0\"]}', '{\"1\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"10\",\"15 Drums\"],\"gw\":[\"500\",\"100\"],\"nw\":[\"500\",\"100\"]},\"2\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"3\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"300\",\"50\"],\"nw\":[\"300\",\"50\"]},\"4\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"5\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"6\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"7\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"8\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"9\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"10\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"11\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"12\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"13\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"14\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"15\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"16\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"17\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"18\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"19\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"20\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"\",\"\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]},\"21\":{\"pn\":[\"RICINOLEIC ACID\",\"RICINOLEIC ACID\"],\"np\":[\"500\",\"10\"],\"gw\":[\"\",\"\"],\"nw\":[\"\",\"\"]}}', '', 'yes', '2017-06-14', '', '', '2017-06-14', '', '0000-00-00', '', '1.4', '', '', '', '', 1, '[{\"invoice_no\":\"A\",\"invoice_date\":\"0000-00-00\",\"uptotwenty\":\"2\",\"uptoforty\":\"\",\"product_0\":\"1\",\"product_1\":\"1\"}]', 'ASSENEDESTRAAT 2\r\n9940 ERTVELDE\r\nBELGIUM', 'OLEON N.V.\r\nASSENEDESTRAAT 2\r\n9940 ERTVELDE\r\nBELGIUM', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-04-24 18:45:17', '2017-06-19 17:08:42'),
(183, 181, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-24 19:07:06', '2017-04-24 19:07:06'),
(184, 182, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-25 23:53:43', '2017-04-25 23:53:43'),
(185, 183, '', '', '0', '', 'A', 'PROMPT', '', '[\"20\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-25 23:54:50', '2017-04-25 23:54:50'),
(186, 184, '', '', '0', '', 'A', 'MAY 2017', '', '[\"20\",\"\"]', '', '', '', '', 'ISO TANK ', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-25 23:57:04', '2017-04-25 23:57:04'),
(187, 185, '', '', '0', '', 'A', 'AFTER ADVANCE', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-27 18:56:11', '2017-04-27 18:56:11'),
(188, 186, '', '', '0', '', 'A', 'MAY - 2017', '', '[\"2\",\"\"]', '', '', '', '', 'SHIP 1+1 IN SAME VESSEL', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-27 18:57:52', '2017-04-27 18:57:52'),
(189, 187, '', '', '0', '', 'A', '4FCL PROMPT WITH 10DAYS GAP FOR NEXT 4+4 FCL', '', '[\"12\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-27 19:01:55', '2017-04-27 19:01:55'),
(190, 188, '', '', '0', '', 'A', 'PROMPT', '', '[\"1\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-27 19:04:31', '2017-04-27 19:04:31'),
(191, 189, '', '', '0', '', 'A', 'ETD 16TH-31ST MAY, 2017', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-27 19:09:48', '2017-04-27 19:09:48'),
(192, 190, '', '', '0', '', 'A', 'PROMPT', '', '[\"14\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-04-27 19:11:26', '2017-04-27 19:11:26'),
(193, 191, '', '', '0', '', 'A', 'PROMPT', '', '[\"2\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"\",\"\"],\"twentySelfSeal\":[\"\",\"\"],\"twentyLineSeal\":[\"\",\"\"],\"twentyPkg\":[\"50 Drums\",\"200 Crates\"],\"twentyGw\":[\"500\",\"253\"],\"twentyNw\":[\"500\",\"250\"],\"fortyContainer\":[],\"fortySelfSeal\":[],\"fortyLineSeal\":[],\"fortyPkg\":[],\"fortyGw\":[],\"fortyNw\":[]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"50 Drums\"],\"gw\":[\"500\"],\"nw\":[\"500\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"200 Crates\"],\"gw\":[\"253\"],\"nw\":[\"250\"]}}', '', 'yes', '2017-06-06', '', '', '2017-06-06', '', '0000-00-00', '', '', '', '', '', '', 0, NULL, '60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', 'CJP CHEMICALS (PTY) LTD\r\n60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 0, NULL, '0000-00-00', '0', '', '0', '0', '2017-04-27 19:12:57', '2017-06-06 04:03:02'),
(194, 192, '', '', '0', '', 'A', '15DAYS AFTER CO-525 SHIPMENT', '', '[\"1\",\"2\"]', '[\"\",\"\"]', '', '', '', '', 'contract', '', '', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '{\"twentyContainer\":[\"123\"],\"twentySelfSeal\":[\"321\"],\"twentyLineSeal\":[\"lineseal\"],\"twentyPkg\":[\"50\"],\"twentyGw\":[\"400\"],\"twentyNw\":[\"5000\"],\"fortyContainer\":[\"234\",\"345\"],\"fortySelfSeal\":[\"432\",\"543\"],\"fortyLineSeal\":[\"\",\"\"],\"fortyPkg\":[\"50 Packages\",\"50 Packages\"],\"fortyGw\":[\"500\",\"500\"],\"fortyNw\":[\"500\",\"500\"]}', '{\"1\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"50\"],\"gw\":[\"400\"],\"nw\":[\"5000\"]},\"2\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"50\"],\"gw\":[\"500\"],\"nw\":[\"500\"]},\"3\":{\"pn\":[\"CASTOR OIL FIRST SPECIAL GRADE\"],\"np\":[\"50\"],\"gw\":[\"500\"],\"nw\":[\"500\"]}}', '', 'yes', '2017-05-01', '50', '', '2017-05-01', '', '0000-00-00', '', '', '', '', '', '', 0, '[{\"invoice_no\":\"123\",\"invoice_date\":\"2017-08-12\",\"uptotwenty\":\"1\",\"uptoforty\":\"1\",\"product_0\":\"2\"}]', 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', 'QUIMICA OCCIDENTAL, S.A. DE C.V.\r\nLUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', '', '', '2017-05-01', '', '2017-05-01', '', '', '', '', NULL, NULL, '', '', '', 1, '', 0, '', '0000-00-00', '5500', '', '0', '0', '2017-04-27 19:14:37', '2017-08-12 09:33:45'),
(331, 12, '', '', '0', '', 'B', '', '', '[\"\",\"\"]', '', '', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-07-08 07:41:38', '2017-07-08 07:41:38'),
(346, 279, 'Girnar Industries Opp. Rajdhani weighbridge,\r\nRajkot Road, Dolatpara, Junagadh(362003),\r\nGUJARAT, INDIA\r\nTel. +91 285 2660893', 'AXIS BANK Shop No. 1,2,3 Raiji Nagar Shoping Centre,\r\nN K mehta Road,Motibag,\r\nJunagadh(362001), Gujarat, INDIA.\r\nA/C No. 912030004364581, SWIFT Code:- AXISINBB087', '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', 'A', 'A', 'CAD', '[\"1\",\"1\"]', '[\"1\",\"1\"]', 'felixstowe', 'ALTAMIRA', 'GENOVA', 'test', 'contract', '2017-08-30', '', 6, 'HO CHI MINH', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '{\"twentyContainer\":[\"\"],\"twentySelfSeal\":[\"\"],\"twentyLineSeal\":[\"\"],\"twentyPkg\":[\"10 Packages\"],\"twentyGw\":[\"600\"],\"twentyNw\":[\"900\"],\"fortyContainer\":[\"\"],\"fortySelfSeal\":[\"\"],\"fortyLineSeal\":[\"\"],\"fortyPkg\":[\"15 Packages\"],\"fortyGw\":[\"1200\"],\"fortyNw\":[\"1500\"]}', '{\"1\":{\"pn\":[\"RICINOLEIC ACID\",\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\",\"12HYDROXY STEARIC ACID - STANDARD\"],\"np\":[\"2\",\"3\",\"5\"],\"gw\":[\"100\",\"200\",\"300\"],\"nw\":[\"200\",\"300\",\"400\"]},\"2\":{\"pn\":[\"RICINOLEIC ACID\",\"INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM\",\"12HYDROXY STEARIC ACID - STANDARD\"],\"np\":[\"4\",\"5\",\"6\"],\"gw\":[\"300\",\"400\",\"500\"],\"nw\":[\"400\",\"500\",\"600\"]}}', '', 'yes', '2017-08-21', '', 'C&F', '2017-08-21', '', '0000-00-00', '', '1.4', '', '', '', '', 1, '[{\"invoice_no\":\"123\",\"invoice_date\":\"2017-09-07\",\"uptotwenty\":\"1\",\"uptoforty\":\"1\",\"product_0\":\"2\",\"product_1\":\"2\",\"product_2\":\"2\"}]', '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '\"AKTAM\"\r\n198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', '', '2017-08-30', '', '2017-08-30', '', '', '', '4', '', '', 'twin bank', 'Shop No. 1,2,3 Raiji Nagar Shoping Centre,\r\nN K mehta Road,Motibag,\r\nJunagadh(362001), Gujarat, INDIA.\r\nA/C No. 912030004364581, SWIFT Code:- ', '30-11--0001', 0, '', 0, '', '0000-00-00', '0', '', '0', '0', '2017-08-21 08:05:40', '2017-09-07 04:40:03'),
(347, 279, '', '', '0', '', 'B', 'B', '', '[\"1\",\"1\"]', '[\"1\",\"1\"]', 'ALEXANDRIA', '', '', 'q', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-08-21 08:05:40', '2017-08-21 08:05:40'),
(358, 284, '', '', '', '', 'A', '123213', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BARCELONA, LIANYUNGANG', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(359, 284, '', '', '', '', 'B', '213', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BARCELONA, LIANYUNGANG', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(360, 284, '', '', '', '', 'C', '123', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BARCELONA, LIANYUNGANG', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(361, 284, '', '', '', '', 'D', '12321', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BARCELONA, LIANYUNGANG', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 05:51:58', '2017-09-04 05:51:58'),
(362, 286, '', '', '', '', 'A', '123213', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BARCELONA, LIANYUNGANG', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 05:59:00', '2017-09-04 05:59:00'),
(363, 286, '', '', '', '', 'B', '213', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BARCELONA, LIANYUNGANG', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(364, 286, '', '', '', '', 'C', '123', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BARCELONA, LIANYUNGANG', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(365, 286, '', '', '', '', 'D', '12321', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BARCELONA, LIANYUNGANG', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 05:59:01', '2017-09-04 05:59:01'),
(370, 288, '', '', '', '', 'A', 'a', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA, ALEXANDRIA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:17:22', '2017-09-04 06:17:22'),
(371, 289, '', '', '', '', 'A', 'a', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, ALTAMIRA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(372, 289, '', '', '', '', 'B', 'b', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, ALTAMIRA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:19:48', '2017-09-04 06:19:48'),
(373, 290, '', '', '', '', 'A', 'a', '', '[\"1\",\"2\"]', '[\"\",\"\"]', 'ALEXANDRIA OLD PORT, ALTAMIRA', '', '', '321', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(374, 290, '', '', '', '', 'B', 'b', '', '[\"1\",\"3\"]', '[\"\",\"\"]', 'ALEXANDRIA OLD PORT, ALTAMIRA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:22:30', '2017-09-04 06:22:30'),
(375, 291, '', '', '', '', 'A', 'a', '', '[\"1\",\"2\"]', '[\"\",\"\"]', 'ALEXANDRIA, BANDAR ABBAS', '', '', '321', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:24:35', '2017-09-04 06:24:35'),
(376, 291, '', '', '', '', 'B', 'b', '', '[\"1\",\"3\"]', '[\"\",\"\"]', 'ALEXANDRIA, BANDAR ABBAS', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:24:35', '2017-09-04 06:24:35'),
(377, 292, '', '', '', '', 'A', 'a', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, ALTAMIRA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:27:58', '2017-09-04 06:27:58'),
(378, 292, '', '', '', '', 'B', 'b', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, ALTAMIRA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:27:58', '2017-09-04 06:27:58'),
(379, 292, '', '', '', '', 'C', 'c', '', '[\"\",\"\"]', '[\"\",\"\"]', 'ALEXANDRIA OLD PORT, ALTAMIRA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:27:59', '2017-09-04 06:27:59'),
(380, 293, '', '', '', '', 'A', 'a', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, BARCELONA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:30:38', '2017-09-04 06:30:38'),
(381, 293, '', '', '', '', 'B', 'b', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, BARCELONA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(382, 293, '', '', '', '', 'C', 'c', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, BARCELONA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(383, 293, '', '', '', '', 'D', 'd', '', '[\"2\",\"2\"]', '[\"2\",\"\"]', 'ALEXANDRIA OLD PORT, BARCELONA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:30:39', '2017-09-04 06:30:39'),
(384, 294, '', '', '', '', 'A', 'a', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:34:56', '2017-09-04 06:34:56'),
(385, 294, '', '', '', '', 'B', 'b', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(386, 294, '', '', '', '', 'C', 'c', '', '[\"2\",\"2\"]', '[\"12\",\"2\"]', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(387, 294, '', '', '', '', 'D', 'd', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'ALEXANDRIA OLD PORT, ALEXANDRIA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:34:57', '2017-09-04 06:34:57'),
(388, 295, '', '', '', '', 'A', 'a', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BANDARABBAS, DOHA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(389, 295, '', '', '', '', 'B', 'b', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BANDARABBAS, DOHA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(390, 295, '', '', '', '', 'C', 'c', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BANDARABBAS, DOHA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(391, 295, '', '', '', '', 'D', 'd', '', '[\"1\",\"2\"]', '[\"2\",\"2\"]', 'BANDARABBAS, DOHA', '', '', '', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:38:37', '2017-09-04 06:38:37'),
(392, 296, '', '', '', '', 'A', 'a', '', '[\"5\",\"2\"]', '[\"2\",\"2\"]', 'BANGKOK, DOHA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '{\"twentyContainer\":[\"123\",\"456\",\"\",\"\",\"\"],\"twentySelfSeal\":[\"123\",\"456\",\"\",\"\",\"\"],\"twentyLineSeal\":[\"123\",\"456\",\"\",\"\",\"\"],\"twentyPkg\":[\"a\",\"c\",\"e\",\"h\",\"j\"],\"twentyGw\":[\"220\",\"90\",\"22\",\"24\",\"24\"],\"twentyNw\":[\"220\",\"90\",\"32\",\"24\",\"24\"],\"fortyContainer\":[\"987\",\"\"],\"fortySelfSeal\":[\"987\",\"\"],\"fortyLineSeal\":[\"987\",\"\"],\"fortyPkg\":[\"l\",\"n\"],\"fortyGw\":[\"24\",\"24\"],\"fortyNw\":[\"24\",\"24\"]}', '{\"1\":{\"pn\":[\"GROUNDNUT EXTRACTION MEAL - 40%\",\"DEHYDRATED CASTOR OIL\"],\"np\":[\"a\",\"b\"],\"gw\":[\"100\",\"120\"],\"nw\":[\"100\",\"120\"]},\"2\":{\"pn\":[\"GROUNDNUT EXTRACTION MEAL - 40%\",\"DEHYDRATED CASTOR OIL\"],\"np\":[\"c\",\"d\"],\"gw\":[\"50\",\"40\"],\"nw\":[\"50\",\"40\"]},\"3\":{\"pn\":[\"GROUNDNUT EXTRACTION MEAL - 40%\",\"DEHYDRATED CASTOR OIL\"],\"np\":[\"e\",\"f\"],\"gw\":[\"12\",\"10\"],\"nw\":[\"12\",\"20\"]},\"4\":{\"pn\":[\"GROUNDNUT EXTRACTION MEAL - 40%\",\"DEHYDRATED CASTOR OIL\"],\"np\":[\"g\",\"h\"],\"gw\":[\"12\",\"12\"],\"nw\":[\"12\",\"12\"]},\"5\":{\"pn\":[\"GROUNDNUT EXTRACTION MEAL - 40%\",\"DEHYDRATED CASTOR OIL\"],\"np\":[\"i\",\"j\"],\"gw\":[\"12\",\"12\"],\"nw\":[\"12\",\"12\"]},\"6\":{\"pn\":[\"GROUNDNUT EXTRACTION MEAL - 40%\",\"DEHYDRATED CASTOR OIL\"],\"np\":[\"k\",\"l\"],\"gw\":[\"12\",\"12\"],\"nw\":[\"12\",\"12\"]},\"7\":{\"pn\":[\"GROUNDNUT EXTRACTION MEAL - 40%\",\"DEHYDRATED CASTOR OIL\"],\"np\":[\"m\",\"n\"],\"gw\":[\"12\",\"12\"],\"nw\":[\"12\",\"12\"]}}', '', 'yes', '2017-09-06', '', '', '2017-09-06', '', '0000-00-00', '', '1', '', '', '', '', 0, NULL, '1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', 'CHANG HSING BIOTECHNOLOGY CO., LTD.\r\n1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, '', 1, NULL, '0000-00-00', '0', '', '0', '0', '2017-09-04 06:47:58', '2017-09-06 11:13:47'),
(393, 296, '', '', '', '', 'B', 'b', '', '[\"2\",\"2\"]', '[\"2\",\"222\"]', 'BANGKOK, DOHA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(394, 296, '', '', '', '', 'C', 'c', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BANGKOK, DOHA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:47:58', '2017-09-04 06:47:58'),
(395, 296, '', '', '', '', 'D', 'd', '', '[\"2\",\"2\"]', '[\"2\",\"2\"]', 'BANGKOK, DOHA', '', '', 'test', 'contract', '', '', 0, '', '', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, '', 0, NULL, 0, NULL, '0000-00-00', '0', NULL, '0', '0', '2017-09-04 06:47:58', '2017-09-04 06:47:58');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `Id` int(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_active` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`Id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Indonesia', 1, '2016-09-30 13:37:22', '2016-09-30 13:37:22'),
(2, 'TAIWAN', 1, '2016-10-13 17:54:34', '2016-10-13 17:54:34'),
(3, 'SOUTH KOREA', 1, '2016-10-29 17:53:42', '2016-10-29 17:53:42'),
(4, 'Afghanistan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(5, 'Albania', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(6, 'Algeria', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(7, 'American Samoa', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(8, 'Andorra', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(9, 'Angola', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(10, 'Anguilla', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(11, 'Antarctica', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(12, 'Antigua and Barbuda', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(13, 'Argentina', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(14, 'Armenia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(15, 'Aruba', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(16, 'Australia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(17, 'Austria', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(18, 'Azerbaijan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(19, 'Bahamas', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(20, 'Bahrain', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(21, 'Bangladesh', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(22, 'Barbados', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(23, 'Belarus', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(24, 'Belgium', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(25, 'Belize', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(26, 'Benin', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(27, 'Bermuda', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(28, 'Bhutan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(29, 'Bolivia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(30, 'Bosnia and Herzegovina', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(31, 'Botswana', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(32, 'Bouvet Island', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(33, 'Brazil', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(34, 'British Indian Ocean Territory', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(35, 'Brunei Darussalam', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(36, 'Bulgaria', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(37, 'Burkina Faso', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(38, 'Burundi', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(39, 'Cambodia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(40, 'Cameroon', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(41, 'Canada', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(42, 'Cape Verde', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(43, 'Cayman Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(44, 'Central African Republic', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(45, 'Chad', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(46, 'Chile', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(47, 'China', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(48, 'Christmas Island', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(49, 'Cocos (Keeling) Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(50, 'Colombia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(51, 'Comoros', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(52, 'Congo', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(53, 'Cook Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(54, 'Costa Rica', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(55, 'Croatia (Hrvatska)', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(56, 'Cuba', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(57, 'Cyprus', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(58, 'Czech Republic', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(59, 'Denmark', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(60, 'Djibouti', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(61, 'Dominica', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(62, 'Dominican Republic', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(63, 'East Timor', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(64, 'Ecuador', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(65, 'Egypt', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(66, 'El Salvador', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(67, 'Equatorial Guinea', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(68, 'Eritrea', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(69, 'Estonia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(70, 'Ethiopia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(71, 'Falkland Islands (Malvinas)', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(72, 'Faroe Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(73, 'Fiji', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(74, 'Finland', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(75, 'France', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(76, 'France, Metropolitan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(77, 'French Guiana', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(78, 'French Polynesia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(79, 'French Southern Territories', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(80, 'Gabon', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(81, 'Gambia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(82, 'Georgia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(83, 'Germany', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(84, 'Ghana', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(85, 'Gibraltar', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(86, 'Guernsey', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(87, 'Greece', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(88, 'Greenland', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(89, 'Grenada', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(90, 'Guadeloupe', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(91, 'Guam', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(92, 'Guatemala', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(93, 'Guinea', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(94, 'Guinea-Bissau', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(95, 'Guyana', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(96, 'Haiti', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(97, 'Heard and Mc Donald Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(98, 'Honduras', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(99, 'Hong Kong', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(100, 'Hungary', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(101, 'Iceland', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(102, 'India', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(103, 'Isle of Man', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(104, 'Iran', 1, '2016-11-05 13:18:14', '2017-04-08 19:36:37'),
(105, 'Iraq', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(106, 'Ireland', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(107, 'Israel', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(108, 'Italy', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(109, 'Ivory Coast', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(110, 'Jersey', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(111, 'Jamaica', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(112, 'Japan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(113, 'Jordan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(114, 'Kazakhstan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(115, 'Kenya', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(116, 'Kiribati', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(117, 'Korea, Democratic People\'s Republic of', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(118, 'Korea, Republic of', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(119, 'Kosovo', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(120, 'Kuwait', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(121, 'Kyrgyzstan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(122, 'Lao People\'s Democratic Republic', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(123, 'Latvia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(124, 'Lebanon', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(125, 'Lesotho', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(126, 'Liberia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(127, 'Libyan Arab Jamahiriya', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(128, 'Liechtenstein', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(129, 'Lithuania', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(130, 'Luxembourg', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(131, 'Macau', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(132, 'Macedonia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(133, 'Madagascar', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(134, 'Malawi', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(135, 'Malaysia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(136, 'Maldives', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(137, 'Mali', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(138, 'Malta', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(139, 'Marshall Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(140, 'Martinique', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(141, 'Mauritania', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(142, 'Mauritius', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(143, 'Mayotte', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(144, 'Mexico', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(145, 'Micronesia, Federated States of', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(146, 'Moldova, Republic of', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(147, 'Monaco', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(148, 'Mongolia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(149, 'Montenegro', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(150, 'Montserrat', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(151, 'Morocco', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(152, 'Mozambique', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(153, 'Myanmar', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(154, 'Namibia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(155, 'Nauru', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(156, 'Nepal', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(157, 'Netherlands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(158, 'Netherlands Antilles', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(159, 'New Caledonia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(160, 'New Zealand', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(161, 'Nicaragua', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(162, 'Niger', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(163, 'Nigeria', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(164, 'Niue', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(165, 'Norfolk Island', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(166, 'Northern Mariana Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(167, 'Norway', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(168, 'Oman', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(169, 'Pakistan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(170, 'Palau', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(171, 'Palestine', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(172, 'Panama', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(173, 'Papua New Guinea', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(174, 'Paraguay', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(175, 'Peru', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(176, 'Philippines', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(177, 'Pitcairn', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(178, 'Poland', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(179, 'Portugal', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(180, 'Puerto Rico', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(181, 'Qatar', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(182, 'Reunion', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(183, 'Romania', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(184, 'Russian Federation', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(185, 'Rwanda', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(186, 'Saint Kitts and Nevis', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(187, 'Saint Lucia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(188, 'Saint Vincent and the Grenadines', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(189, 'Samoa', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(190, 'San Marino', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(191, 'Sao Tome and Principe', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(192, 'Saudi Arabia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(193, 'Senegal', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(194, 'Serbia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(195, 'Seychelles', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(196, 'Sierra Leone', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(197, 'Singapore', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(198, 'Slovakia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(199, 'Slovenia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(200, 'Solomon Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(201, 'Somalia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(202, 'South Africa', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(203, 'South Georgia South Sandwich Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(204, 'Spain', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(205, 'Sri Lanka', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(206, 'St. Helena', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(207, 'St. Pierre and Miquelon', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(208, 'Sudan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(209, 'Suriname', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(210, 'Svalbard and Jan Mayen Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(211, 'Swaziland', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(212, 'Sweden', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(213, 'Switzerland', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(214, 'Syrian Arab Republic', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(215, 'Tajikistan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(216, 'Tanzania, United Republic of', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(217, 'Thailand', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(218, 'Togo', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(219, 'Tokelau', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(220, 'Tonga', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(221, 'Trinidad and Tobago', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(222, 'Tunisia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(223, 'Turkey', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(224, 'Turkmenistan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(225, 'Turks and Caicos Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(226, 'Tuvalu', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(227, 'Uganda', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(228, 'Ukraine', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(229, 'United Arab Emirates', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(230, 'United Kingdom', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(231, 'United States', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(232, 'United States minor outlying islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(233, 'Uruguay', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(234, 'Uzbekistan', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(235, 'Vanuatu', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(236, 'Vatican City State', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(237, 'Venezuela', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(238, 'Vietnam', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(239, 'Virgin Islands (British)', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(240, 'Virgin Islands (U.S.)', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(241, 'Wallis and Futuna Islands', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(242, 'Western Sahara', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(243, 'Yemen', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(244, 'Zaire', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(245, 'Zambia', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(246, 'Zimbabwe', 1, '2016-11-05 13:18:14', '0000-00-00 00:00:00'),
(247, 'SLOVENIA', 1, '2017-01-22 19:29:43', '2017-01-22 19:29:43'),
(248, 'EGYPT', 1, '2017-01-22 19:49:13', '2017-01-22 19:49:13'),
(249, 'RUSSIA', 1, '2017-01-25 22:13:46', '2017-01-25 22:13:46');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_terms`
--

CREATE TABLE `delivery_terms` (
  `Id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `delivery_terms`
--

INSERT INTO `delivery_terms` (`Id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'CFR', '2016-10-13 17:54:40', '2016-10-13 17:54:40'),
(3, 'CNF', '2016-12-28 00:14:27', '2016-12-28 00:14:27'),
(5, 'FOB PIPAVAV', '2017-01-25 18:14:41', '2017-03-23 17:13:56'),
(6, 'CIF', '2017-01-25 22:13:04', '2017-01-25 22:13:04'),
(7, 'DAP', '2017-02-24 18:09:53', '2017-02-24 18:09:53'),
(9, 'EX -WORKS', '2017-02-24 22:32:31', '2017-02-24 22:32:31'),
(11, 'C&F', '2017-03-19 16:40:40', '2017-03-19 16:40:40'),
(12, 'FOB MUNDRA', '2017-03-23 17:14:09', '2017-03-23 17:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `discharge_ports`
--

CREATE TABLE `discharge_ports` (
  `Id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discharge_ports`
--

INSERT INTO `discharge_ports` (`Id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'KAOHSIUNG', '2016-10-13 17:54:17', '2016-10-13 17:54:17'),
(3, 'BUSAN', '2016-10-29 17:50:42', '2016-10-29 17:50:42'),
(4, 'TAICHUNG', '2016-11-19 00:10:35', '2016-11-19 00:10:35'),
(5, 'HOUSTON', '2016-12-28 00:12:05', '2016-12-28 00:12:05'),
(6, 'SHARJAH', '2017-01-10 21:17:47', '2017-03-23 17:10:19'),
(7, 'KOPER', '2017-01-22 19:29:05', '2017-01-22 19:29:05'),
(8, 'ALEXANDRIA OLD PORT', '2017-01-22 19:48:37', '2017-01-22 19:48:37'),
(9, 'SANTOS', '2017-01-25 17:49:19', '2017-01-25 17:49:19'),
(10, 'BARCELONA', '2017-01-25 18:13:43', '2017-01-25 18:13:43'),
(12, 'GENOVA', '2017-01-25 22:14:47', '2017-01-25 22:14:47'),
(13, 'felixstowe', '2017-01-25 23:16:42', '2017-01-25 23:16:42'),
(14, 'SHANGHAI', '2017-02-01 16:51:24', '2017-02-01 16:51:24'),
(15, 'HUANGPU', '2017-02-01 17:19:12', '2017-02-01 17:19:12'),
(16, 'VERACRUZ', '2017-02-01 17:33:38', '2017-02-01 17:33:38'),
(17, 'DURBAN', '2017-02-01 18:04:27', '2017-02-01 18:04:27'),
(18, 'CAPETOWN', '2017-02-01 18:04:42', '2017-02-01 18:04:42'),
(19, 'JEBEL ALI', '2017-02-01 18:46:28', '2017-02-01 18:46:28'),
(20, 'DAMMAM', '2017-02-01 18:55:03', '2017-02-01 18:55:03'),
(21, 'SYDNEY', '2017-02-01 19:03:28', '2017-02-01 19:03:28'),
(22, 'BANGKOK', '2017-02-02 17:05:16', '2017-02-02 17:05:16'),
(23, 'ST.PETERSBURG', '2017-02-02 17:40:27', '2017-02-02 17:40:27'),
(24, 'ALTAMIRA', '2017-02-02 17:47:29', '2017-02-02 17:47:29'),
(25, 'ALEXANDRIA', '2017-02-02 18:07:25', '2017-02-02 18:07:25'),
(26, 'OSAKA', '2017-02-02 23:24:24', '2017-02-02 23:24:24'),
(27, 'BANDARABBAS', '2017-02-03 02:12:51', '2017-02-03 02:12:51'),
(28, 'ROTTERDAM', '2017-02-04 16:51:43', '2017-02-04 16:51:43'),
(29, 'INCHEON', '2017-02-06 18:41:27', '2017-02-06 18:41:27'),
(30, 'SHIBUSHI', '2017-02-07 17:18:45', '2017-02-07 17:18:45'),
(31, 'PORT KELANG', '2017-02-10 00:59:47', '2017-02-10 00:59:47'),
(33, 'ITAJAI / NAVEGANTS', '2017-02-11 16:29:25', '2017-02-11 16:29:25'),
(34, 'HCMC(CAT LAI)', '2017-02-14 22:43:32', '2017-02-14 22:43:32'),
(35, 'CHITTAGONG', '2017-02-21 17:51:53', '2017-02-21 17:51:53'),
(36, 'KEELUNG', '2017-02-22 23:20:54', '2017-02-22 23:20:54'),
(37, 'SHUWAIKH', '2017-02-24 17:05:55', '2017-02-24 17:43:19'),
(38, 'KARACHI', '2017-02-24 17:12:47', '2017-02-24 17:12:47'),
(39, 'LEMPAALA', '2017-02-24 18:09:29', '2017-02-24 18:09:29'),
(40, 'EX -WORKS', '2017-02-24 22:32:02', '2017-02-24 22:32:02'),
(41, 'MANZANILLO', '2017-02-24 22:44:46', '2017-02-24 22:44:46'),
(42, 'SAN-ANTONIO', '2017-03-03 16:27:19', '2017-03-03 16:27:19'),
(43, 'JAKARTA', '2017-03-11 06:37:55', '2017-03-11 06:37:55'),
(44, 'JOHANNESBURG', '2017-03-17 00:03:09', '2017-03-17 00:03:09'),
(45, 'MANILA', '2017-03-17 00:27:22', '2017-03-17 00:27:22'),
(46, 'ODESSA', '2017-03-18 01:25:38', '2017-03-18 01:25:38'),
(47, 'BUENOS AIRES', '2017-03-19 16:51:36', '2017-03-19 16:51:36'),
(48, 'FREEMANTLE', '2017-04-05 02:13:52', '2017-04-05 02:13:52'),
(49, 'HO CHI MINH', '2017-04-05 02:19:23', '2017-04-05 02:19:23'),
(50, 'DOHA', '2017-04-06 23:42:13', '2017-04-06 23:42:13'),
(51, 'LIANYUNGANG', '2017-04-16 17:44:39', '2017-04-16 17:44:39'),
(52, 'LIANYUNGANG', '2017-04-16 17:44:39', '2017-04-16 17:44:39'),
(53, 'ANTWERP', '2017-04-24 18:34:47', '2017-04-24 18:34:47'),
(54, 'ANTWERP', '2017-04-24 18:34:48', '2017-04-24 18:34:48');

-- --------------------------------------------------------

--
-- Table structure for table `final_destination`
--

CREATE TABLE `final_destination` (
  `Id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `final_destination`
--

INSERT INTO `final_destination` (`Id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Test Destination', '2016-09-30 13:37:11', '2016-09-30 13:37:11'),
(2, 'KAOHSIUNG', '2016-10-13 17:54:26', '2016-10-13 17:54:26'),
(3, 'BUSAN', '2016-10-29 17:50:52', '2016-10-29 17:50:52'),
(4, 'TAICHUNG', '2016-11-19 00:10:54', '2016-11-19 00:10:54'),
(5, 'HOUSTON', '2016-12-28 00:13:50', '2016-12-28 00:13:50'),
(6, 'sharjah', '2017-01-10 21:19:43', '2017-01-10 21:19:43'),
(7, 'KOPER', '2017-01-22 19:29:23', '2017-01-22 19:29:23'),
(8, 'KOPER', '2017-01-22 19:29:24', '2017-01-22 19:29:24'),
(9, 'ALEXANDRIA OLD PORT', '2017-01-22 19:48:50', '2017-01-22 19:48:50'),
(10, 'EGYPT', '2017-01-22 19:49:00', '2017-01-22 19:49:00'),
(11, 'SANTOS', '2017-01-25 17:50:03', '2017-01-25 17:50:03'),
(12, 'ST.PETERSBURG', '2017-01-25 22:12:50', '2017-01-25 22:12:50'),
(13, 'GENOVA', '2017-01-25 22:15:02', '2017-01-25 22:15:02'),
(14, 'felixstowe', '2017-01-25 23:17:06', '2017-01-25 23:17:06'),
(15, 'BARCELONA', '2017-01-26 16:18:32', '2017-01-26 16:18:32'),
(16, 'SHANGHAI', '2017-02-01 16:52:04', '2017-02-01 16:52:04'),
(17, 'HUANGPU', '2017-02-01 17:19:25', '2017-02-01 17:19:25'),
(18, 'VERACRUZ', '2017-02-01 17:33:52', '2017-02-01 17:33:52'),
(19, 'MEXICO', '2017-02-01 17:34:06', '2017-02-01 17:34:06'),
(20, 'CAPETOWN', '2017-02-01 18:04:53', '2017-02-01 18:04:53'),
(21, 'JEBEL ALI', '2017-02-01 18:46:39', '2017-02-01 18:46:39'),
(22, 'DAMMAM', '2017-02-01 18:55:13', '2017-02-01 18:55:13'),
(23, 'SYDNEY', '2017-02-01 19:03:55', '2017-02-01 19:03:55'),
(24, 'BANGKOK', '2017-02-02 17:05:28', '2017-02-02 17:05:28'),
(25, 'ALTAMIRA', '2017-02-02 17:47:44', '2017-02-02 17:47:44'),
(26, 'ALEXANDRIA', '2017-02-02 18:07:34', '2017-02-02 18:07:34'),
(27, 'OSAKA', '2017-02-02 23:24:32', '2017-02-02 23:24:32'),
(28, 'BANDAR ABBAS', '2017-02-03 02:13:05', '2017-02-03 02:13:05'),
(29, 'ROTTERDAM', '2017-02-04 16:51:54', '2017-02-04 16:51:54'),
(30, 'INCHEON', '2017-02-06 18:41:35', '2017-02-06 18:41:35'),
(31, 'SHIBUSHI', '2017-02-07 17:19:02', '2017-02-07 17:19:02'),
(32, 'PORT KELANG', '2017-02-10 00:59:55', '2017-02-10 00:59:55'),
(33, 'ITAJAI / NAVEGANTS', '2017-02-11 16:29:33', '2017-02-11 16:29:33'),
(34, 'HCMC(CAT LAI)', '2017-02-14 22:43:42', '2017-02-14 22:43:42'),
(35, 'CHITTAGONG', '2017-02-21 17:52:05', '2017-02-21 17:52:05'),
(36, 'KEELUNG', '2017-02-22 23:21:09', '2017-02-22 23:21:09'),
(37, 'SHUWAIKH', '2017-02-24 17:06:15', '2017-02-24 17:06:15'),
(38, 'KARACHI', '2017-02-24 17:13:10', '2017-02-24 17:13:10'),
(39, 'LEMPAALA', '2017-02-24 18:09:39', '2017-02-24 18:09:39'),
(40, 'KATHMANDU', '2017-02-24 22:32:16', '2017-02-24 22:32:16'),
(41, 'MANZANILLO', '2017-02-24 22:45:00', '2017-02-24 22:45:00'),
(42, 'SAN-ANTONIO', '2017-03-03 16:27:28', '2017-03-03 16:27:28'),
(43, 'DURBAN', '2017-03-03 16:31:13', '2017-03-03 16:31:13'),
(44, 'SPAIN', '2017-03-04 16:20:38', '2017-03-04 16:20:38'),
(45, 'JAKARTA', '2017-03-11 06:38:27', '2017-03-11 06:38:27'),
(46, 'JOHANNESBURG', '2017-03-17 00:03:30', '2017-03-17 00:03:30'),
(47, 'MANILA', '2017-03-17 00:27:33', '2017-03-17 00:27:33'),
(48, 'ODESSA', '2017-03-18 01:25:48', '2017-03-18 01:25:48'),
(49, 'BUENOS AIRES', '2017-03-19 16:51:48', '2017-03-19 16:51:48'),
(50, 'FREEMANTLE', '2017-04-05 02:14:48', '2017-04-05 02:14:48'),
(51, 'HO CHI MINH', '2017-04-05 02:19:31', '2017-04-05 02:19:31'),
(52, 'DOHA', '2017-04-06 23:42:22', '2017-04-06 23:42:22'),
(53, 'LIANYUNGANG', '2017-04-16 17:44:51', '2017-04-16 17:44:51'),
(54, 'ANTWERP', '2017-04-24 18:34:59', '2017-04-24 18:34:59');

-- --------------------------------------------------------

--
-- Table structure for table `forwarders`
--

CREATE TABLE `forwarders` (
  `Id` int(5) NOT NULL,
  `company_id` int(5) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_active` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forwarders`
--

INSERT INTO `forwarders` (`Id`, `company_id`, `name`, `contact_person`, `phone`, `email`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 0, 'Test Forwarder', '', '', '', 1, '2016-11-06 04:34:54', '2016-11-06 04:34:54'),
(2, 0, 'Test Forwarder 2', '', '', '', 1, '2016-11-06 04:35:07', '2016-11-06 04:35:07'),
(3, 0, 'SURYA SHIPPING SERVICES', 'MR. SURESH NAIR', '9099023246', 'SURESH@SURYASHIP.COM', 1, '2016-12-28 00:41:27', '2016-12-28 00:41:27'),
(4, 0, 'ORANGE MARITIME', 'BHAVIN RAJAYAGURU', '', 'BHAVINR@ORANGEMARITIME.COM', 1, '2017-04-27 19:04:52', '2017-04-27 19:04:52');

-- --------------------------------------------------------

--
-- Table structure for table `importers`
--

CREATE TABLE `importers` (
  `Id` int(5) NOT NULL,
  `company_id` int(5) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `address` tinytext,
  `address2` tinytext,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `phone2` varchar(255) NOT NULL,
  `is_active` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `importers`
--

INSERT INTO `importers` (`Id`, `company_id`, `name`, `address`, `address2`, `country`, `phone`, `phone2`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 0, 'Test Buyer', '', NULL, '', '', '', 0, '2016-09-30 13:36:28', '2017-02-10 01:08:18'),
(2, 0, 'ABC', 'ABC', NULL, '', '', '', 0, '2016-10-13 17:53:54', '2017-02-10 01:07:17'),
(3, 0, 'BJ HONG', 'KAOHSIUNG\r\nTAIWAN', NULL, 'TAIWAN', '+1234656787', '', 0, '2016-10-24 19:28:24', '2017-02-10 01:07:24'),
(4, 0, 'SAMYANG FINE', 'SOUTH KOREA', NULL, 'SOUTH KOREA', '', '', 0, '2016-10-29 17:50:17', '2017-02-10 01:07:49'),
(5, 0, 'STAR ASIA (FAR EAST) CO. LTD', '972/1 3RD FLOOR, VORASUBIN BUILDING\r\nSOI RANA 9 HOSPITAL,RAMA 9 ROAD, BANG KAPI,\r\nBANG KAPI, HUAI KHWANG,BANGKOK 10320 THAILAND', NULL, 'THAILAND', '', '', 1, '2016-12-28 00:05:52', '2016-12-28 00:05:52'),
(6, 0, 'test', 'tsdaga', NULL, '', '', '', 0, '2016-12-28 05:58:21', '2017-02-10 01:08:14'),
(7, 0, 'fasdf', '', NULL, '', '', '', 0, '2016-12-28 05:59:24', '2017-02-10 01:07:39'),
(8, 0, 'Test buyer', '', NULL, '', '', '', 0, '2017-01-09 00:44:11', '2017-02-10 01:08:24'),
(9, 0, 'Test buyer', '', NULL, '', '', '', 0, '2017-01-09 00:44:18', '2017-02-10 01:08:29'),
(10, 0, 'BAALBAKI GROUP SA OFFSHORE', 'P.O. Box: 42448 HAMRIYAH FREE ZONE, SHARJAH - UAE Tel: +971 6 52 60788 Fax: +971 6 52 61799', NULL, 'UAE', '+971 6 52 60788', '', 1, '2017-01-10 21:16:04', '2017-01-10 21:16:04'),
(11, 0, 'DONGYANG OIL', '', NULL, '', '', '', 0, '2017-01-11 18:26:35', '2017-02-10 01:07:33'),
(12, 0, 'SOLCHEM D.O.O.', 'KOPER, SLOVENIA', NULL, 'SLOVENIA', '', '', 1, '2017-01-22 19:28:07', '2017-01-22 19:28:07'),
(13, 0, 'EAGLE POLYMERS', 'INDUSTRIAL ZONE # 2,\r\nPART 233, 6th OF OCTOBER CITY\r\n', NULL, 'EGYPT', '', '', 1, '2017-01-22 19:47:45', '2017-01-22 19:47:45'),
(14, 0, 'A. AZEVEDO INDÃšSTRIA E COMÃ‰RCIO DE Ã“LEOS LTDA', 'ESTRADA MUNICIPAL BENTO PEREIRA DE TOLEDO, 2043\r\nCEP 13295-000 â€“ ITUPEVA â€“ SÃƒO PAULO â€“ BRASIL\r\nCNPJ 61.278.875/0003-06     I.E. 388.010.905.115\r\nTEL: 55-11-3806-4800 FAX: 55-11-2061-2111', NULL, 'BRASIL', '+55-11-3806-4800 ', '', 1, '2017-01-25 17:46:52', '2017-01-25 17:46:52'),
(15, 0, 'INTERFAT, S.A.', 'AVD. DIAGONAL, 403, 6-2 08808 BARCELONA\r\nES TELF : 934161999 FAX: 934161048    A08461089', NULL, 'SPAIN', ' 934161999', '', 1, '2017-01-25 18:12:59', '2017-01-25 18:12:59'),
(16, 0, '\"AKTAM\"', '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', NULL, 'RUSSIA', '', '', 0, '2017-01-25 18:24:07', '2017-01-28 03:43:04'),
(17, 0, '\"AKTAM\"', '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', NULL, 'RUSSIA', '', '', 0, '2017-01-25 18:24:08', '2017-01-28 03:43:08'),
(18, 0, '\"AKTAM\"', '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', NULL, 'RUSSIA', '', '', 0, '2017-01-25 18:24:09', '2017-01-28 03:43:10'),
(19, 0, '\"AKTAM\"', '198096,SAINT-PETERSBURG,PORTOVAYA ST, \r\n15, LIT. B, OF-4H. OGRN 1117847461181\r\nINN 7807364464 KRP 780701001', NULL, 'RUSSIA', '', '', 1, '2017-01-25 18:24:10', '2017-01-28 03:43:34'),
(20, 0, 'LIBA CHEM', '20061 CARUGATE(MI)-VIA ITALIA,30\r\nTEL-.02 92505648 , FAX: 02 92153614', NULL, 'SPAIN', '02 92505648', '', 1, '2017-01-25 22:08:46', '2017-01-25 22:08:46'),
(21, 0, 'HAMPSHIRE COMMODITIES LIMITED', '121 ALBERT STREET, FLEET, HAMPSHIRE GU51 3SR\r\nTELEPHONE : 01252 613058 FAX 01252 616374\r\nEMAIL : jon@hampshire-commodities.co.uk\r\n', NULL, 'UK', '01252 613058', '', 1, '2017-01-25 22:42:27', '2017-01-25 22:42:27'),
(22, 0, 'CHIN MING TRADING CO,.LTD', '3FL, NO 296, SEC 1, CHUNG SHAN ROAD SHULIN\r\nTAIWAN 23842\r\n', NULL, 'TAIWAN', '', '', 1, '2017-01-25 23:24:55', '2017-01-25 23:24:55'),
(23, 0, 'TEXTRON TECNICA S.L.', 'GIRONA,34\r\n08400 GRANOLLERS (BARCELONA)\r\nSPAIN', NULL, 'SPAIN', '', '', 1, '2017-01-26 16:16:54', '2017-01-26 16:16:54'),
(24, 0, 'SINOCHEM PLASTICS CO., LTD.', '7F, SINOCHEM TOWER, A2 FUXINGMENWAI STREET\r\nBEIJING', NULL, 'CHINA', '', '', 1, '2017-02-01 16:50:52', '2017-02-01 16:50:52'),
(25, 0, 'QUIMICA OCCIDENTAL, S.A. DE C.V.', 'LUIS BRAILLE NO.135, COL.INDEPENDANCIA\r\n03630 MEXICO, D.F.', NULL, 'MEXICO', '+52 55 5539 3612', '+52 55 5672 4581', 1, '2017-02-01 17:32:16', '2017-02-01 17:32:16'),
(26, 0, 'CJP CHEMICALS (PTY) LTD', '60 ELECTRON AVENUE\r\nISANDO\r\nJOHANNESBURG 1600', NULL, 'SOUTH AFRICA', '+27 11 494 6700', '', 1, '2017-02-01 18:03:17', '2017-02-01 18:03:17'),
(27, 0, 'IFFCO CHEMICALS FZE', 'PO BOX 41671. HAMIRIYAH FREE ZONE, PHASE I,\r\nSHARJAH', NULL, 'UAE', '+971 6526 3922', '', 1, '2017-02-01 18:45:47', '2017-02-01 18:45:47'),
(28, 0, 'SAUDI INDUSTRIAL DETERGENTS CO.', 'PO BOX 2571 DAMMAM', NULL, 'SAUDI ARABIA', '+966 13 816 6777', '', 1, '2017-02-01 18:53:39', '2017-02-01 18:53:39'),
(29, 0, 'AUSTRALIAN VINYLS CORPORATION PTY LTD', 'Jesica Road, Campbellfield,\r\nVictoria 3061', NULL, 'AUSTRALIA', '+61 03 8359 7364', '', 1, '2017-02-01 19:02:50', '2017-02-01 19:02:50'),
(30, 0, 'SIROCKO (THAILAND) CO. LTD.', '129/112 Moo1 Bangkayang, Moung,\r\nANY PORT, INDIA\r\nPathumthani 12000', NULL, 'THAILAND', '+66(0)-81-810-2780', '', 1, '2017-02-02 17:04:55', '2017-02-02 17:04:55'),
(31, 0, 'NEFTECHEMEX LLC', '115516, Russia, Moscow,\r\nPromyshlennaya, 11, building 3, of.419', NULL, 'RUSSIA', '+7 (495) 646 81 35', '', 1, '2017-02-02 17:39:48', '2017-02-02 17:39:48'),
(32, 0, 'LUBRICANTES DE AMERICA, S.A. DE C.V.', 'Carretera a GarcÃ­a Km 1.2 S/N\r\nSanta Catarina, N.L.\r\nCP 66350 MÃ©xico\r\nTax ID: LAM951127KF6', NULL, 'MEXICO', '+52 81 81227400 ext. 7459', '', 1, '2017-02-02 17:46:28', '2017-02-02 17:46:28'),
(33, 0, 'PATECH FINE CHEMICALS CO., LTD.', 'NO.41, ALLEY 1, LANE420, KUANG-FU S.RD.,\r\nTAIPEI, TAIWAN R.O.C. 10695', NULL, 'TAIWAN', '+886 2 2703 3268', '', 1, '2017-02-02 17:57:05', '2017-02-02 17:57:05'),
(34, 0, 'AFRIGLOBAL COMMODITIES DMCC', '905, JBC 2, Jumeirah Lake Towers\r\nDubai, UAE\r\nPhone - +971 4 4484258   Fax - +971 4 4484259', NULL, 'UAE', ' +971 4 4484258', '', 1, '2017-02-02 18:06:04', '2017-02-02 18:06:04'),
(35, 0, 'YU YUAN TECHNOLOGY CO., LTD ', 'NO.20-56, NIAOSONG VILLAGE,DONGSHIH TOWNSHIP,\r\nCHIAYI COUNTY 614, TAIWAN R.O.C.', NULL, 'TAIWAN', '', '', 1, '2017-02-02 22:23:49', '2017-02-02 22:23:49'),
(36, 0, 'LVLIN BIOTECH CO LTD.', '2F, NO 58, CHANGPING E, 3RD BEITUN DISTRICT,\r\nTAICHUNG CITY 406, TAIWAN R.O.C.', NULL, 'TAIWAN', '', '', 1, '2017-02-02 22:34:19', '2017-02-02 22:34:19'),
(37, 0, 'B.J. HONG ENTERPRISE CO., LTD.', '11F-3, NO. 372 CHUNG CHENG 1ST ROAD,\r\nKAOHSIUNG, TAIWAN R.O.C.\r\nTel:886-7-7232212  Fax:886-7-7238557', NULL, 'TAIWAN', '+886-7-7232212', '', 1, '2017-02-02 22:43:01', '2017-02-02 22:43:01'),
(38, 0, 'OOO Nortex', '101000, Russia, Moscow\r\nArmyanskiy per. 4 bld. 1 AB\r\nTel./Fax +7 (495) 225 44 40', NULL, 'RUSSIA', '+7 (495) 225 44 40', '', 1, '2017-02-02 22:49:54', '2017-02-02 22:49:54'),
(39, 0, 'TELENOLOGY INTERNATIONAL CO., LTD', '12F No.695 MingCheng 3rd Road\r\nKaohsiung 804, Taiwan\r\nTel: +886 7 553-8738 Fax: +886 7 553-6867', NULL, 'TAIWAN', '+886 7 553-8738', '', 1, '2017-02-02 22:59:28', '2017-02-02 22:59:28'),
(40, 0, 'DONGEUN CO., LTD', '28-19, Bunseong-ro 5412eon-gi3, Gi0hae-si,\r\nGyeongsangna0-do, 50820, Korea\r\nTe3 : +82 55 322 9986 Fax : +82 55 322 9989', NULL, 'SOUTH KOREA', '+82 55 322 9986', '', 1, '2017-02-02 23:05:45', '2017-02-02 23:05:45'),
(41, 0, 'KING NUNG YEOU ENTERPRISE CO., LTD', 'NO.600. TING-HO SEC., TOU YUAN RD., FANG YUAN TOWNSHIP,\r\nCHANGHUA COUNTY 528, TAIWAN, R.O.C.', NULL, 'TAIWAN', '', '', 1, '2017-02-02 23:09:21', '2017-02-02 23:09:21'),
(42, 0, 'TAI YEH CHEMICAL INDUSTRY CO., LTD.', '68-1, 9 LING, BAO TANQ TSUN, GUAN IN SHIANG,\r\nTAO YUAN, TAIWAN, R.O.C.', NULL, 'TAIWAN', '', '', 1, '2017-02-02 23:12:48', '2017-02-02 23:12:48'),
(43, 0, 'SHOEI YAKUHIN CO.,LTD.', '5-1, 1-CHOME, AZUCHIMACHI, CHUOKU, OSAKA,\r\nJAPAN\r\nTEL: +81 06 6262 2701', NULL, 'JAPAN', '+81 06 6262 2701', '', 1, '2017-02-02 23:24:06', '2017-02-02 23:24:06'),
(44, 0, 'FWUSOW INDUSTRY CO., LTD', '4th FLOOR, NO.72, NINGPO WEST STREET,\r\nTAIPEI 10075, TAIWAN', NULL, 'TAIWAN', '', '', 1, '2017-02-02 23:29:57', '2017-02-02 23:29:57'),
(45, 0, 'EVERKEM SRL ', 'VIA DELLA LIRICA 11\r\n48124 RAVENNA RA', NULL, 'ITALY', '', '', 1, '2017-02-03 01:54:02', '2017-02-03 01:54:02'),
(46, 0, 'TADBIR GOSTAR ZAMAN', 'ADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\nTel. : +98 41 35544872  \r\n Fax : +98 41 35538475', NULL, 'IRAN', '+98 41 35544872 ', '', 0, '2017-02-03 02:10:35', '2017-02-13 02:23:49'),
(47, 0, 'TADBIR GOSTAR ZAMAN', 'ADD : No.15, Ostad Shahriyar Alley, Baghshomal Tabriz, Iran.\r\nTel. : +98 41 35544872  \r\n Fax : +98 41 35538475', NULL, 'IRAN', '+98 41 35544872 ', '', 1, '2017-02-03 02:10:38', '2017-02-03 02:10:38'),
(48, 0, 'CHARLOTTE CHEMICAL INC.', '9901 I-1O West Suite 800,\r\nSan Antonio TX 78230 USA\r\nPhone 210 558 2886', NULL, 'USA', '210 558 2886', '', 1, '2017-02-04 16:48:01', '2017-02-04 16:48:01'),
(49, 0, 'CASTOR INTERNATIONAL BV', 'P.O. BOX 96.\r\n5750 AB DEURNE\r\nTHE NETHERLANDS', NULL, 'NETHERLAND', '', '', 1, '2017-02-04 16:50:54', '2017-02-04 16:50:54'),
(50, 0, 'CHEMICAL-LAND TRADING CO. LTD', '69, BANSONG-RO, YEONJE KU,\r\nBUSAN, SOUTH KOREA\r\nT:051-865-3232', NULL, 'SOUTH KOREA', '+82-51-865-3232', '', 1, '2017-02-04 22:52:33', '2017-02-04 22:52:33'),
(51, 0, 'SAMYANG FINE CHEMICAL CO.,LTD.', '301-804, BUCHEON TECHNO-PARK,SSANGYONG 3RD.,\r\n397,SEOKCHEON-RO,OJEONG-GU,BUCHEON-SI,\r\nGYEONGGI-DO,KOREA', NULL, 'SOUTH KOREA', '+82 32 624 2021', '', 1, '2017-02-06 18:41:06', '2017-02-06 18:41:06'),
(52, 0, 'WELLNESS-TOP BIOTECHNOLOGY CO.,LTD.', 'No.11, Shennong E. Rd., Changzhi Township,\r\nPingtung County 908, Taiwan\r\nTEL:886-7-2266699 FAX:886-7-2273957', NULL, 'TAIWAN', '+886-7-2266699', '', 1, '2017-02-07 17:10:23', '2017-02-07 17:10:23'),
(53, 0, 'FYC CO LTD', 'FYC BLDG,2-1-5,MINATO CHUO-KU,FUKUOKA\r\nJAPAN, POSTCODE :810-0075', NULL, 'JAPAN', '+81-92-712-4121', '', 1, '2017-02-07 17:18:20', '2017-02-07 17:18:20'),
(54, 0, 'CAHAYA LAGENDA RESOURCES SDN. BHD.', 'TD-03-04, CRYSTAL TOWER, CHANGKAT BUKIT INDAH DUA,\r\nBUKIT INDAH, 68000 AMPANG, SELANGOR D.E.\r\nTEL: +603-2289 0632 FAX: +603-2289 0636', NULL, 'MALAYSIA', '+603-2289 0632', '', 1, '2017-02-10 00:59:21', '2017-02-10 00:59:21'),
(55, 0, 'DONG YANG OIL CHEMICAL CO. LTD.', '1785-6, CHEONGWON RO, WONGOK MYEON,\r\nANSEONG SI, GYEONGGI DO, SEOUL,\r\nSOUTH KOREA', NULL, 'SOUTH KOREA', '', '', 1, '2017-02-10 01:03:45', '2017-02-10 01:03:45'),
(56, 0, 'INDUSTRIA QUIMICA ANASTACIO S/A', 'RUA GIL STEIN FERREIRA,357 , SALA 703,\r\n 88 301-210 CENTRO, ITAJAI-SC,\r\nCNPJ 60.874.724/0005-10', NULL, 'BRASIL', '', '', 1, '2017-02-11 16:26:56', '2017-02-11 16:26:56'),
(57, 0, 'MFRP ENGINEERING SDN BHD', 'NO. 26, JALAN PUTERI 5/16, BANDAR PUTERI,\r\n47100 PUCHONG, SELANGOR DARUL EHSAN, MALAYSIA\r\nTEL: +603-80624478 FAX: +603-80616876', NULL, 'MALAYSIA', '+603-80624478', '', 1, '2017-02-14 22:26:26', '2017-02-14 22:26:26'),
(58, 0, 'OOO \"TD HALMEK\"', '3.BLDG ., 7A DOROZHNAYA ST. ,\r\nMOSCOW, RUSSIA 117545\r\nTEL : +7 (963) 682-99-44', NULL, 'RUSSIA', '+7 (963) 682-99-44', '', 1, '2017-02-14 22:36:11', '2017-02-14 22:36:11'),
(59, 0, 'GREATWILL INTERNATIONAL CO., LTD.', 'RM51, 5TH FLOOR, BRITANNIA HOUSE, JALAN CATOR,\r\nBANDAR SERI BEGAWAN BS 8811,NEGARA BRUNEI DARUSSALAM\r\nTEL:+886-6-2531668 FAX: +886-6-2421613', NULL, 'TAIWAN', '', '', 1, '2017-02-14 22:42:16', '2017-02-14 22:42:16'),
(60, 0, 'LUBRICANTS ASIA LTD.', 'BSCIS I/E, Sagarika Road\r\nBlock-A, P.O. Customs Academy, Chittagong-4219\r\nTel: +880-31-750060/750446 \r\nFAX: +880-31-750061', NULL, 'BANGLADESH', '+880-31-750060/750446', '', 1, '2017-02-21 17:51:32', '2017-02-21 17:51:32'),
(61, 0, 'GO-FENG INDUSTRY CO., LTD', 'NO.7-1, LN. 520, DAREN RD., LUZHU DIST.,\r\nKAOHSIUNG CITY 821, TAIWAN (R.O.C.)', NULL, 'TAIWAN', '', '', 1, '2017-02-21 17:54:37', '2017-02-21 17:54:37'),
(62, 0, 'THAI CASTOR OIL INDUSTRIES CO., LTD', '12th Fl., Orakarn Bldg., 26/42 Soi Chidlom,Ploenchit Rd.,\r\nLumpini, Patumvan, Bangkok 10330, Thailand\r\nTel : +66 2254 1490-7', NULL, 'THAILAND', '+66 2254 1490-7', '', 1, '2017-02-22 23:13:21', '2017-02-22 23:13:21'),
(63, 0, 'HORN HAUR ENTERPRISE CO., LTD.', '5F, No.3, Alley 14, Lane 151, Yuan Shan Rd.,\r\nChung Ho Dist., New Taipei City 235, Taiwan\r\nTel : +886-2-2226-5160\r\nFax: +886-2-2226-9687', NULL, 'TAIWAN', '+886-2-2226-5160', '', 1, '2017-02-22 23:20:31', '2017-02-22 23:20:31'),
(64, 0, 'KUWAIT LUBE OIL COMPANY', 'INDUSTRIAL AREA, BUILDING NO-52, BLOCK-4\r\nSTREET-7, SHUAIBAH, P.O.BOX 9748\r\nAHMADI 61008, KUWAIT', NULL, 'KUWAIT', '', '', 1, '2017-02-24 17:05:24', '2017-02-24 17:05:24'),
(65, 0, 'PAK GREASE MFG CO (PVT) LTD', 'NO. 6 OIL INSTALLATION AREA, KHEMARI\r\nKARACHI - 75620 PAKISTAN', NULL, 'PAKISTAN', '', '', 1, '2017-02-24 17:12:24', '2017-02-24 17:12:24'),
(66, 0, 'KIILTO OY', 'KEHATIE 1\r\n33880 LEMPAALA\r\nFINLAND', NULL, 'FINLAND', '', '', 1, '2017-02-24 18:08:52', '2017-02-24 18:08:52'),
(67, 0, 'MB PETROLUBE INDUSTRIES PVT. LTD.', '303 Kamaladi, Kathmandu Nepal\r\nCustomers VAT/PAN Number: 300020748\r\nTel: + 977 1 4240363', NULL, 'NEPAL', '+977 1 4240363', '', 1, '2017-02-24 22:31:25', '2017-02-24 22:31:25'),
(68, 0, 'OMNI - CHEM', '1427 W. 86th Street Suite 365\r\nIndianapolis, IN 46260\r\nTel : +1 -317-852-1986', NULL, 'USA', '+1 -317-852-1986', '', 1, '2017-02-24 22:43:09', '2017-02-24 22:43:09'),
(69, 0, 'CHURWAN YU CO., LTD.', 'NO.29, LANE 180, CHIEN CHENG RD.,\r\nMIN SHENG TSUN, FANG YUAN HSIANG, \r\nCHANGHUA HSIEN, TAIWAN', NULL, 'TAIWAN', '', '', 1, '2017-03-01 23:22:14', '2017-03-01 23:22:14'),
(70, 0, 'MADESA S.A.', 'Av. Presidente Eduardo Frei Montalva 9431\r\nQuilicura, Santiago, Chile - 8710004\r\nVAT NÂ°: 84.083.400-K\r\nContact person: Jose Miralles', NULL, 'CHILE', '+56 224357781', '+56 224357782', 1, '2017-03-03 16:25:27', '2017-03-03 16:25:27'),
(71, 0, 'EPIC FOODS (PTY) LTD', '1 Guy Gibson Avenue\r\nAeroton 2013, Gauteng, South Africa\r\nTel : (+27) 11 248 0000 Fax: (+27) 494 1115', NULL, 'SOUTH AFRICA', '+27 11 248 0000', '', 1, '2017-03-03 16:30:51', '2017-03-03 16:30:51'),
(72, 0, 'ZAO \"Ruskhimset\"', 'Noviy Arbat str., h. 21 office 1806\r\nMoscow 119019, Russia\r\nTel :+7-495-789-8399, 739-5457', NULL, 'RUSSIA', '+7-495-789-8399', '+7 -495-739-5457', 1, '2017-03-03 16:35:54', '2017-03-03 16:35:54'),
(73, 0, 'PT. BALMER LAWRIE INDONESIA', 'GRAHA STR 2nd FLOOR, JI. AMPERA RAYA NO.11\r\nRAGUNAN - PASAR MINGGU, JAKARTA SELATION 12550', NULL, 'INDONESIA', '', '', 1, '2017-03-11 06:37:35', '2017-03-11 06:37:35'),
(74, 0, 'PAN ASIA CHEMICAL CORPORATION', '10F., NO.50, SEC. 1, XINSHENG S. RD.,\r\nTAIPEI, TAIWAN(R.O.C.)\r\nTEL : 886-2-23937111', NULL, 'TAIWAN', '+886-2-23937111', '', 1, '2017-03-16 23:55:19', '2017-03-16 23:55:19'),
(75, 0, 'THE PEKAY GROUP (PTY) LTD.', '24 Fulton Street, Industria West,\r\nJohannesburg, Gauteng\r\nSOUTH AFRICA', NULL, 'SOUTH AFRICA', '', '', 1, '2017-03-17 00:02:20', '2017-03-17 00:02:20'),
(76, 0, 'HUBE GLOBAL CO., LTD', 'Office 610, Wooree Venture Town II, 82-29, 3-Ga Mullae-dong,\r\nYeongdeungpo-gu, Seoul, Republic of Korea (#150-836)\r\nTEL : +82-2-2068-9312, \r\nFAX : +82-2-2068-8761,', NULL, 'SOUTH KOREA', '+82-2-2068-9312', '', 1, '2017-03-17 00:16:27', '2017-03-17 00:16:27'),
(77, 0, 'TRADECOM SERVICES PVT LTD', '3 SHENTON WAY, #09-08 SHENTON HOUSE\r\nSINGAPORE 068805\r\nTEL : (65) 62240036 FAX : (65) 62230611', NULL, 'SINGAPORE', '+65 62240036', '', 1, '2017-03-17 00:26:59', '2017-03-17 00:26:59'),
(78, 0, 'KOWON EVERCHEM CORPORATION', 'Kowon Building 20-19\r\nYangjae-Dong,Seocho-ku Seoul,Korea\r\nTel : +82-2-578-6181 Fax : +82-2-578-6692', NULL, 'SOUTH KOREA', '+82-2-578-6181', '', 1, '2017-03-17 00:31:52', '2017-03-17 00:31:52'),
(79, 0, 'LLCâ€œRU SIE AGRINOLâ€', '3-a, Stroitelnaya str., Berdyansk\r\nUkraine, 71100\r\nTel.: +38 06153 60701', NULL, 'ODESSA', '+38 06153 60701', '', 1, '2017-03-18 01:25:12', '2017-03-18 01:25:12'),
(80, 0, 'EL KAYAR IMPORT, EXPORT & COMMERCIAL AGENCIES CORP', '14 SELIM HASSAN STREET - EL SHATBY\r\nALEXANDRIA - EGYPT', NULL, 'EGYPT', '', '', 1, '2017-03-19 16:27:41', '2017-03-19 16:27:41'),
(81, 0, 'AL SHARHAN INDUSTRIES', 'P.O. BOX 424\r\n13005 SAFAT\r\nKUWAIT', NULL, 'KUWAIT', '', '', 1, '2017-03-19 16:32:10', '2017-03-19 16:32:10'),
(82, 0, 'SUFI SOAP & CHEMICAL INDUSTRIES (PVT) LTD', '1-B GARDEN BLOCK, GARDEN TOWN\r\nLAHORE, PAKISTAN', NULL, 'PAKISTAN', '', '', 1, '2017-03-19 16:39:58', '2017-03-19 16:39:58'),
(83, 0, 'TIANXING BIOTECHNOLOGY CO., LTD', 'HANDIAN INDUSTRIAL ZONE, ZOUPING\r\nSHANDONG 256200 CHINA', NULL, 'CHINA', '', '', 1, '2017-03-19 16:46:17', '2017-03-19 16:46:17'),
(84, 0, 'FLEXA ADHESIVOS Y RECUBRIMIENTOS S. R. L.', 'ALBERTI 101-(1722) MERLO - PCIA. DE BS. AS.\r\nREPUBLICA ARGENTINA', NULL, 'ARGENTINA', '', '', 1, '2017-03-19 16:51:19', '2017-03-19 16:51:19'),
(85, 0, 'AUSTRALIAN MUD COMPANY', '216 BALCATTA ROAD, BALCATTA\r\nWA 6021 AUSTRALIA', NULL, 'AUSTRALIA', '', '', 1, '2017-04-05 02:13:23', '2017-04-05 02:13:23'),
(86, 0, 'CHOLIMEX FOODS JOINT STOCK COMPANY', '7th ST. VINH LOC INDUSTRIAL PARK\r\nBINH CHANH DIS., HO CHI MINH CITY,\r\nVIETNAM', NULL, 'VIETNAM', '', '', 1, '2017-04-05 02:19:06', '2017-04-05 02:19:06'),
(87, 0, 'THREE JACK FEED INDUSTRY CO., LTD.', 'NO.282, FU HSING RD., IA LIAO VILL.,\r\nLU JWU HSIANG, KAOHSIUNG, TAIWAN R.O.C.', NULL, 'TAIWAN', '', '', 1, '2017-04-06 18:58:01', '2017-04-06 18:58:01'),
(88, 0, 'SABO SPA', 'VIA CARAVAGGI - 24040 LEVATE (BG)\r\nITALY\r\nTEL. +39 035596000 FAX. : +39.035594400', NULL, 'ITALY', '+39 035596000', '+39 035594400', 1, '2017-04-06 22:29:13', '2017-04-06 22:29:13'),
(89, 0, 'BILTREC S.A.,', 'ATLANTIDA, 21 , 102a\r\n08930 SANT ADRIA DE BESOS\r\nBARCELONA SPAIN', NULL, 'SPAIN', '', '', 1, '2017-04-06 23:25:01', '2017-04-06 23:25:01'),
(90, 0, 'BILTREC S.A.,', 'ATLANTIDA, 21 , 102a\r\n08930 SANT ADRIA DE BESOS\r\nBARCELONA SPAIN', NULL, 'SPAIN', '', '', 1, '2017-04-06 23:25:01', '2017-04-06 23:25:01'),
(91, 0, 'N H TRADING', '(553, Sungnae-dong) 7, Olympic-ro 48-gil,\r\nGangdong-gu, Seoul, Korea', NULL, 'SOUTH KOREA', '', '', 1, '2017-04-06 23:31:42', '2017-04-06 23:31:42'),
(92, 0, 'Universal Marketing & Consultancy FZCo', 'Warehouse RA08BB04, JAFZA, UAE\r\nTel: +9714 880 8272\r\nFax: +9714 8808 273', NULL, 'UAE', '', '', 1, '2017-04-06 23:40:13', '2017-04-06 23:40:13'),
(93, 0, 'VVF SINGAPORE PTE LTD', '133 Cecil Street, #9-01A, Keck Seng Tower,\r\nSingapore 069535\r\nTel : +65 6224 8871 Fax : +65 6224 8082', NULL, 'SINGAPORE', '+65 6224 8871', '', 1, '2017-04-11 16:23:19', '2017-04-11 16:23:19'),
(94, 0, 'FERMIC , S.A. DE C.V.', 'REFORMA NO. 873, COL. SAN NICOLAS TOLENTINO\r\nC.P. 09850, DEL .IZTAPALAPA\r\nMEXICO, D.F', NULL, 'MEXICO', '', '', 1, '2017-04-11 17:01:10', '2017-04-11 17:01:10'),
(95, 0, 'SYNCHEM INTERNATIONAL CO., LTD', 'NO 25, TONG XING STREET, ROOM 3208, 3209\r\nWORLD TRADE CENTRE, DALIAN, ZIP CODE : 116001\r\n', NULL, 'CHINA', '', '', 1, '2017-04-16 17:44:11', '2017-04-16 17:44:11'),
(96, 0, 'United Trading System Scandinavia AB', 'Box 187\r\n265 22 Ã…STORP\r\nSWEDEN', NULL, 'SWEDEN', '', '', 1, '2017-04-17 17:06:29', '2017-04-17 17:06:29'),
(97, 0, 'CHANG HSING BIOTECHNOLOGY CO., LTD.', '1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', NULL, 'TAIWAN', '', '', 1, '2017-04-17 17:10:15', '2017-04-17 17:10:15'),
(98, 0, 'CHANG HSING BIOTECHNOLOGY CO., LTD.', '1F, NO.21-2, HUZI TSUO, LONGHSING VILLAGE\r\nCHONGPU HSIANG, CHIAYI COUNTY,TAIWAN, R.O.C', NULL, 'TAIWAN', '', '', 1, '2017-04-17 17:10:15', '2017-04-17 17:10:15'),
(99, 0, 'CHEONG-MU ENTERPRISE CO., LTD.', 'NO.160, LI-KANG RD., CHUN-LIN VILLAGE,\r\nLI-KANG HSIANG, PING-TUNG HSIEN,\r\nTAIWAN(R.O.C.)', NULL, 'TAIWAN', '', '', 1, '2017-04-17 17:13:12', '2017-04-17 17:13:12'),
(100, 0, 'OLEON N.V.', 'ASSENEDESTRAAT 2\r\n9940 ERTVELDE\r\nBELGIUM', NULL, 'BELGIUM', '', '', 1, '2017-04-24 18:33:20', '2017-04-24 18:33:20'),
(101, 0, 'EL NABILA GROUP COMPANY', '1st Elshiek Rehan, Inside Panasonic Building, \r\n9th Floor, Abdeen, Cairo, Egypt', NULL, 'EGYPT', '+202 2512 1473', '', 1, '2017-04-27 18:53:57', '2017-04-27 18:53:57'),
(102, 0, 'VANDEPUTTE OLEOCHEMICALS SA', 'BOULEVARD INDUSTRIAL, 120\r\nB-7700 BELGIUM\r\nVAT NUMBER : BE - 471.546.593', NULL, 'BELGIUM', '', '', 1, '2017-04-27 18:59:35', '2017-04-27 18:59:35');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `Id` bigint(20) NOT NULL,
  `company_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `table_id` varchar(20) NOT NULL,
  `operation` varchar(20) NOT NULL,
  `log` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `Id` int(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`Id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Test Package', '2016-09-30 13:38:05', '2016-09-30 13:38:05'),
(2, '200KG DRUMS NON-PALLETIZED', '2016-10-13 17:56:09', '2016-10-13 17:56:09'),
(3, 'BULK IN CONTAINER', '2016-10-24 19:34:36', '2016-10-24 19:34:36'),
(4, 'BULK IN FLEXI TANK  (TLBD)', '2016-12-28 00:17:53', '2016-12-28 00:17:53'),
(5, 'FLEXI TANK(BLBD)', '2017-01-10 21:52:08', '2017-01-10 21:52:08'),
(6, '40KG PP BAGS', '2017-01-23 00:22:33', '2017-01-23 00:22:33'),
(10, 'BULK IN \"ISO\" TANK', '2017-01-25 18:16:17', '2017-01-25 18:16:17'),
(12, ' IN NEW MS DRUMS 225KGS NET', '2017-01-25 18:27:48', '2017-01-25 18:27:48'),
(13, 'BULK IN FLEXI TANK (TLTD)', '2017-02-01 17:08:43', '2017-02-01 17:08:43'),
(15, '25KG PAPER BAGS NON-PALLETIZED', '2017-02-01 17:44:20', '2017-02-01 17:44:20'),
(16, '225KG DRUMS NON-PALLETIZED', '2017-02-01 18:41:10', '2017-02-01 18:41:10'),
(17, '200KG DRUMS PALLETIZED', '2017-02-01 18:48:16', '2017-02-01 18:48:16'),
(18, '225KG DRUMS PALLETIZZED', '2017-02-01 18:56:54', '2017-02-01 18:56:54'),
(19, 'IN 225KG DRUMS PALLETIZED', '2017-02-01 18:58:37', '2017-02-01 18:58:37'),
(20, 'IN 25KG PAPER BAGS PALLETIZED', '2017-02-01 19:05:55', '2017-02-01 19:05:55'),
(21, 'IN 225KG DRUMS NON-PALLETIZED', '2017-02-02 17:42:31', '2017-02-02 17:42:31'),
(22, 'IN 25KG PAPER BAGS NON-PALLETIZED', '2017-02-02 17:59:16', '2017-02-02 17:59:16'),
(23, 'IN 200KG DRUMS PALLETIZED', '2017-02-02 18:10:13', '2017-02-02 18:10:13'),
(24, 'IN FLEXI TANK(TOP LOADING BOTTON DISCHARGE) WITH H', '2017-02-02 22:09:51', '2017-02-02 22:09:51'),
(25, 'IN BULK IN CONTAINER', '2017-02-02 22:26:56', '2017-02-02 22:26:56'),
(26, 'IN 500KG JUMBO BAGS PALLETIZED', '2017-02-02 23:07:17', '2017-02-02 23:07:17'),
(27, 'IN 200KG DRUMS NON-PALLETIZED', '2017-02-04 22:54:09', '2017-02-04 22:54:09'),
(28, 'IN 850KG JUMBO BAGS NON-PALLETIZED', '2017-02-07 17:20:56', '2017-02-07 17:20:56'),
(29, 'IN 50KG PP BAGS NON-PALLETIZED', '2017-02-24 22:33:22', '2017-02-24 22:33:22'),
(30, 'IN 950KG NEW IBC TANK', '2017-03-03 16:28:31', '2017-03-03 16:28:31'),
(31, 'IN 225KG NEW HDPE DRUMS NON-PALLETIZED', '2017-03-03 16:33:51', '2017-03-03 16:33:51'),
(32, 'BULK IN FLEXITANK(BLBD)', '2017-04-06 19:06:11', '2017-04-06 19:06:11'),
(33, 'IN 30KG PP BAGS', '2017-04-06 19:23:59', '2017-04-06 19:23:59'),
(34, 'abc_packing', '2017-09-04 07:12:38', '2017-09-04 07:12:38');

-- --------------------------------------------------------

--
-- Table structure for table `payment_terms`
--

CREATE TABLE `payment_terms` (
  `Id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_terms`
--

INSERT INTO `payment_terms` (`Id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'LC AT SIGHT', '2016-10-13 17:54:49', '2016-10-13 17:54:49'),
(3, 'CAD THROUGH BANK', '2016-12-28 00:14:51', '2016-12-28 00:14:51'),
(4, '30% ADVANCE & 70% AGAINST DOCS', '2017-01-22 19:30:14', '2017-01-22 19:30:14'),
(6, 'CAD', '2017-01-25 18:24:37', '2017-01-25 18:24:37'),
(7, '100% TT PAYMENT', '2017-01-25 18:25:05', '2017-01-25 18:25:05'),
(8, 'TT 45 DAYS FROM B/L DATE', '2017-01-26 16:19:15', '2017-01-26 16:19:15'),
(9, 'CASH AGAINST DOCUMENT', '2017-01-26 16:26:49', '2017-01-26 16:26:49'),
(10, 'TT 15DAYS AFTER VESSEL ARRIVES AT DEST. PORT', '2017-02-01 17:34:51', '2017-02-01 17:34:51'),
(11, '60 DAYS LC FROM BL DATE', '2017-02-01 18:05:50', '2017-02-01 18:05:50'),
(12, '100% ADVANCE PAYMENT', '2017-02-01 18:47:23', '2017-02-01 18:47:23'),
(13, 'DP AT SIGHT(AGAINST SCAN DOCS)', '2017-02-02 18:08:37', '2017-02-02 18:08:37'),
(14, '50% ADVANCE + 50% AGAINST SCAN BL', '2017-02-14 22:27:55', '2017-02-14 22:27:55'),
(15, 'CAD THROUGH BANK 60 DAYS', '2017-02-24 18:10:15', '2017-02-24 18:10:15'),
(16, 'TT 30DAYS FROM BL DATE', '2017-03-17 00:28:07', '2017-03-17 00:28:07'),
(17, 'LC SIGHT 30 DAYS', '2017-04-05 02:19:57', '2017-04-05 02:19:57'),
(18, 'CAD THROUGH BANK 30DAYS FROM B/L DATE', '2017-04-24 18:35:51', '2017-04-24 18:35:51');

-- --------------------------------------------------------

--
-- Table structure for table `ports`
--

CREATE TABLE `ports` (
  `Id` int(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `permission_no` varchar(200) NOT NULL,
  `is_active` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ports`
--

INSERT INTO `ports` (`Id`, `name`, `permission_no`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'MUNDRA', '', 1, '2016-09-30 13:36:48', '2017-09-06 10:46:41'),
(2, 'PIPAVAV', '', 1, '2016-09-30 13:36:53', '2017-09-06 10:47:01'),
(3, 'EX -WORKS', '', 1, '2017-02-24 22:31:42', '2017-02-24 22:31:42'),
(4, 'ANY', '', 0, '2017-03-23 17:02:04', '2017-03-23 17:09:39'),
(5, 'ANY PORT IN INDIA', '', 1, '2017-03-23 17:02:22', '2017-03-23 17:02:22'),
(6, 'MUMBAI', '', 1, '2017-07-06 06:57:05', '2017-08-17 04:10:20'),
(7, 'test', '', 1, '2017-09-06 10:44:42', '2017-09-06 10:44:42');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `Id` int(11) NOT NULL,
  `company_id` int(5) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `hs_code` varchar(255) NOT NULL DEFAULT '',
  `shortcode` varchar(255) NOT NULL,
  `dbk_scheme_no` varchar(255) NOT NULL DEFAULT '',
  `fob` varchar(255) NOT NULL DEFAULT '',
  `file_no` varchar(255) NOT NULL DEFAULT '',
  `extra1` varchar(255) NOT NULL DEFAULT '',
  `extra2` varchar(255) NOT NULL DEFAULT '',
  `extra3` varchar(255) NOT NULL DEFAULT '',
  `extra4` varchar(255) NOT NULL DEFAULT '',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`Id`, `company_id`, `name`, `hs_code`, `shortcode`, `dbk_scheme_no`, `fob`, `file_no`, `extra1`, `extra2`, `extra3`, `extra4`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 0, 'Test Product 1', '123456', '', '2307A', '1', '', '', '', '', '', 0, '2016-09-30 13:37:58', '2017-02-04 22:55:38'),
(2, 0, 'CASTOR OIL FIRST SPECIAL GRADE', '15153090', 'COFSG', '2208A', '1', '', 'Non Edible', '', '', '', 1, '2016-10-13 17:55:50', '2017-08-10 06:16:47'),
(3, 0, 'CASTOR SEED MEAL', '23069027', '', '2306A', '1', '', '', '', '', '', 0, '2016-10-24 19:34:19', '2017-03-23 17:11:14'),
(4, 0, 'CASTOR OIL -REFINED- NOT FOR EDIBLE USE', '15153090', 'CO-R-NFEU', '22098A', '1', '', 'Non Edible', '', '', '', 1, '2016-12-28 00:17:18', '2017-08-10 06:16:41'),
(5, 0, 'DEHYDRATED CASTOR OIL', '15180029', 'DCO', '23065A', '1', '', 'Non Edible', '', '', '', 1, '2017-01-22 19:35:30', '2017-08-10 06:16:57'),
(6, 0, 'CASTOR OIL PALE PRESSED GRADE', '15153090', 'COPPG', '', '1', '', 'Non Edible', '', '', '', 1, '2017-01-25 23:23:15', '2017-08-10 06:16:52'),
(7, 0, '12HYDROXY STEARIC ACID', '29157040', '12HSA', '2915B', '1.5', '', '', '', '', '', 1, '2017-02-01 17:44:01', '2017-07-17 08:36:02'),
(8, 0, 'HYDROGENATED CASTOR OIL - NORMAL', '15162039', 'HCO-N', '1516A', '1', '', 'Non Edible', '', '', '', 1, '2017-02-01 17:45:09', '2017-07-17 08:40:27'),
(9, 0, 'HYDROGENATED CASTOR OIL - STANDARD', '15162039', 'H CO - STANDARD', '1516A', '1', '', 'Non Edible', '', '', '', 1, '2017-02-01 19:05:29', '2017-07-17 09:04:43'),
(10, 0, '12HYDROXY STEARIC ACID - STANDARD', '29157040', '12HSA-S', '2915B', '1.5', '', '', '', '', '', 1, '2017-02-01 19:06:45', '2017-07-17 08:37:06'),
(11, 0, '12HYDROXY STEARIC ACID - NORMAL', '29157040', '12HSA-N', '2915B', '1.5', '', '', '', '', '', 1, '2017-02-02 17:50:47', '2017-07-17 08:36:51'),
(12, 0, 'INDIAN CASTOR SEED EXTRACTION MEAL', '23069027', 'ICSEM', '2306A', '1', '', '', '', '', '', 1, '2017-02-02 22:26:41', '2017-07-17 08:41:14'),
(13, 0, '12HYDROXY STEARIC ACID - BLEACHED', '38231990', '12HSA-B', '2105A', '1.5', '', '', '', '', '', 1, '2017-02-02 23:07:01', '2017-08-10 06:16:26'),
(14, 0, 'H.C.O FATTY ACID ', '38231900', 'H.C.O FA', '', '1', '', '', '', '', '', 1, '2017-02-11 16:30:23', '2017-08-10 06:17:08'),
(15, 0, 'RICINOLEIC ACID', '29161990', 'RA', '1516A', '1.4', '', '', '', '', '', 1, '2017-02-24 18:48:24', '2017-07-17 08:42:00'),
(16, 0, 'INDIAN CASTOR SEED EXTRACTION MEAL - PELLET FORM', '23069027', 'ICSEM-PF', '2306A', '1', '', '', '', '', '', 1, '2017-02-24 22:40:35', '2017-07-17 08:43:42'),
(17, 0, 'GROUNDNUT EXTRACTION MEAL - 40%', '23050020', 'GEM-40%', '2305A', '1', '', '', '', '', '', 1, '2017-02-24 22:49:08', '2017-07-17 09:01:58'),
(18, 0, 'GROUNDNUT MEAL 45%', '23050090', 'GEM-45%', '1212A', '1', '', '', '', '', '', 1, '2017-04-05 02:22:14', '2017-08-10 06:17:03'),
(19, 0, 'INDIAN CASTOR SEED EXTRACTION MEAL - HIGH PROTEIN', '23069027', 'ICSEM-HP', '2212A', '1', '', '', '', '', '', 1, '2017-04-06 23:33:17', '2017-08-10 06:17:14'),
(20, 0, 'CASTOR  CAKE', '23069027', 'CC', '1210A', '1', '', '', '', '', '', 1, '2017-04-24 17:21:10', '2017-08-10 06:16:35'),
(22, 0, 'sahgdjhsgdhgjhgsad', '2432432', 'fgh', '1123A', '1', '', '', '', '', '', 0, '2017-07-17 07:47:13', '2017-07-17 08:42:06'),
(23, 0, 'abc_1234', 'abc1234', '0', '', '', '', '', '', '', '', 1, '2017-09-04 07:00:11', '2017-09-04 07:00:11'),
(24, 0, 'abc_1234', 'abc1234', '0', '', '', '', '', '', '', '', 1, '2017-09-04 07:00:12', '2017-09-04 07:00:12'),
(25, 0, 'abc_1234', 'abc1234', '0', '', '', '', '', '', '', '', 1, '2017-09-04 07:00:13', '2017-09-04 07:00:13'),
(26, 0, 'abc_1234', 'abc1234', '0', '', '', '', '', '', '', '', 1, '2017-09-04 07:00:13', '2017-09-04 07:00:13'),
(27, 0, 'abc_1234', 'abc1234', '0', '', '', '', '', '', '', '', 1, '2017-09-04 07:00:13', '2017-09-04 07:00:13'),
(28, 0, 'abc_1234', 'abc1234', '0', '', '', '', '', '', '', '', 1, '2017-09-04 07:00:13', '2017-09-04 07:00:13'),
(29, 0, 'abc_123', 'abc_123', '', '', '', '', '', '', '', '', 1, '2017-09-04 07:00:35', '2017-09-04 07:00:35'),
(30, 0, 'adsadasd', 'sad', '', '', '', '', '', '', '', '', 1, '2017-09-04 07:05:46', '2017-09-04 07:05:46'),
(31, 0, 'abc_112233', '112233', '', '', '', '', '', '', '', '', 1, '2017-09-04 07:09:42', '2017-09-04 07:09:42');

-- --------------------------------------------------------

--
-- Table structure for table `schemes`
--

CREATE TABLE `schemes` (
  `Id` int(5) NOT NULL,
  `company_id` int(5) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `detail` tinytext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schemes`
--

INSERT INTO `schemes` (`Id`, `company_id`, `name`, `detail`, `created_at`, `updated_at`) VALUES
(1, 0, 'VKGUY', 'Vishesh Krishi And Gram Udyog Yojana.\r\n\r\n(SPECIAL AGRICULTURE AND VILLAGE INDUSTRY SCHEME)', '2016-10-04 12:48:25', '2016-10-04 12:48:25');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `Id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(5) NOT NULL,
  `order_date` date NOT NULL DEFAULT '0000-00-00',
  `qty` varchar(255) NOT NULL,
  `vendor_id` int(5) NOT NULL,
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `amount` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`Id`, `company_id`, `user_id`, `product_id`, `order_date`, `qty`, `vendor_id`, `delivery_date`, `amount`, `created_at`, `updated_at`) VALUES
(4, 1, 1, 3, '2017-01-03', '1000', 3, '2017-01-03', '10000', '2017-01-03 17:43:42', '2017-01-03 17:43:42'),
(5, 1, 3, 4, '2017-01-09', '4', 4, '2017-01-20', '15000', '2017-01-11 01:06:59', '2017-01-11 01:06:59'),
(7, 1, 3, 5, '2017-01-10', '80', 5, '2017-01-10', '0', '2017-01-11 01:58:42', '2017-01-11 01:58:42'),
(11, 1, 3, 12, '2017-01-11', '3500', 7, '2017-01-12', '21.25', '2017-01-11 17:52:16', '2017-01-11 17:52:16'),
(12, 1, 3, 13, '2017-01-11', '3500', 6, '2017-01-19', '21.25', '2017-01-11 17:53:10', '2017-01-11 17:53:10'),
(13, 1, 3, 14, '2017-01-11', '10986', 6, '2017-01-20', '21.25', '2017-01-11 17:54:44', '2017-01-11 17:54:44'),
(14, 1, 3, 6, '2017-01-11', '10', 4, '2017-01-12', '310$', '2017-01-11 17:57:52', '2017-01-11 17:57:52'),
(15, 1, 3, 8, '2017-01-11', '14', 4, '2017-01-11', '250', '2017-01-11 17:59:24', '2017-01-11 17:59:24'),
(16, 1, 3, 15, '2017-01-11', '10', 4, '2017-01-12', '280$', '2017-01-11 18:01:17', '2017-01-11 18:01:17'),
(17, 1, 3, 16, '2017-01-11', '4', 8, '2017-01-12', '6700', '2017-01-11 18:04:10', '2017-01-11 18:04:10'),
(18, 1, 3, 17, '2017-01-11', '88', 9, '2017-01-12', '490++', '2017-01-11 18:06:14', '2017-01-11 18:06:14'),
(19, 1, 1, 5, '2017-08-12', '90', 10, '2017-08-12', '1150', '2017-01-11 18:08:52', '2017-08-12 17:20:13'),
(20, 1, 3, 10, '2017-01-11', '117', 10, '2017-01-12', '1150', '2017-01-11 18:10:33', '2017-01-11 18:10:33'),
(21, 1, 3, 18, '2017-01-11', '3', 10, '2017-01-12', '1150', '2017-01-11 18:13:38', '2017-01-11 18:13:38'),
(22, 1, 1, 5, '2017-11-30', '314', 5, '2017-11-30', '123', '2017-01-23 00:32:14', '2017-08-16 04:41:31'),
(23, 1, 1, 7, '2017-11-30', '725', 5, '2017-11-30', '234234', '2017-01-23 00:35:33', '2017-08-12 17:25:45'),
(24, 1, 1, 5, '2017-11-30', '43', 10, '2017-11-30', '123213', '2017-01-23 00:36:50', '2017-08-12 17:26:03'),
(25, 1, 1, 9, '2017-01-22', '60', 10, '2017-01-23', '123', '2017-01-23 00:37:32', '2017-08-12 11:22:51'),
(26, 1, 3, 10, '2017-01-22', '97', 10, '2017-01-23', '', '2017-01-23 00:38:24', '2017-01-23 00:38:24'),
(27, 1, 3, 6, '2017-01-22', '12', 4, '2017-01-23', '', '2017-01-23 00:40:22', '2017-01-23 00:40:22'),
(28, 1, 3, 18, '2017-01-22', '43', 10, '2017-01-23', '', '2017-01-23 00:42:34', '2017-01-23 00:42:34'),
(29, 1, 1, 5, '2017-11-30', '8', 6, '2017-11-30', '123', '2017-01-25 17:16:26', '2017-08-12 17:26:19'),
(30, 1, 1, 4, '2017-11-30', '123', 4, '2017-11-30', '123', '2017-08-12 09:53:22', '2017-08-16 05:06:47'),
(31, 1, 1, 4, '2017-08-12', '213', 4, '2017-08-12', '123', '2017-08-12 09:53:32', '2017-08-12 11:15:03'),
(32, 1, 1, 5, '0000-00-00', '123', 6, '2017-08-12', '123', '2017-08-12 09:53:37', '2017-08-12 11:19:38'),
(33, 1, 1, 5, '2017-08-16', '', 6, '2017-08-16', '', '2017-08-16 10:10:08', '2017-08-16 10:10:38');

-- --------------------------------------------------------

--
-- Table structure for table `stocks_product`
--

CREATE TABLE `stocks_product` (
  `Id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks_product`
--

INSERT INTO `stocks_product` (`Id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(3, 0, 'Test Product', '2017-01-03 17:43:28', '2017-01-03 17:43:28'),
(4, 0, 'FLEXI TANK', '2017-01-11 01:02:56', '2017-01-11 01:02:56'),
(5, 0, '200 KG BARREL', '2017-01-11 01:57:36', '2017-01-11 01:57:36'),
(6, 0, 'FLEXI TANK TLBD', '2017-01-11 17:43:08', '2017-01-11 17:43:08'),
(7, 0, '225 KG BARREL', '2017-01-11 17:43:25', '2017-01-11 17:43:25'),
(8, 0, 'FLEXI TANK TLTD', '2017-01-11 17:43:46', '2017-01-11 17:43:46'),
(9, 0, '44X44 PELLET', '2017-01-11 17:44:08', '2017-01-11 17:44:08'),
(10, 0, '44X55 PELLET', '2017-01-11 17:44:24', '2017-01-11 17:44:24'),
(11, 0, '225 KG DRUMS', '2017-01-11 17:49:25', '2017-01-11 17:49:25'),
(12, 0, 'PAPER BAG (HCO)', '2017-01-11 17:50:48', '2017-01-11 17:50:48'),
(13, 0, 'PAPER BAG (12HSA)', '2017-01-11 17:52:48', '2017-01-11 17:52:48'),
(14, 0, 'PAPER BAG', '2017-01-11 17:53:56', '2017-01-11 17:53:56'),
(15, 0, 'FLEXI TANK TLTD (21KL)', '2017-01-11 18:00:19', '2017-01-11 18:00:19'),
(16, 0, 'IBC TANK', '2017-01-11 18:02:33', '2017-01-11 18:02:33'),
(17, 0, 'JUMBO BAG', '2017-01-11 18:05:19', '2017-01-11 18:05:19'),
(18, 0, '39X44 PELLET', '2017-01-11 18:11:59', '2017-01-11 18:11:59'),
(19, 0, 'FLEXI TANK BLBD', '2017-01-25 17:15:55', '2017-01-25 17:15:55');

-- --------------------------------------------------------

--
-- Table structure for table `stocks_vendor`
--

CREATE TABLE `stocks_vendor` (
  `Id` int(5) NOT NULL,
  `company_id` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks_vendor`
--

INSERT INTO `stocks_vendor` (`Id`, `company_id`, `name`, `created_at`, `updated_at`) VALUES
(3, 0, 'Test Vendor', '2017-01-03 17:43:33', '2017-01-03 17:43:33'),
(4, 0, 'SUN FLEXI', '2017-01-11 01:05:33', '2017-01-11 01:05:33'),
(5, 0, 'GURDEV CONTAINERS', '2017-01-11 01:58:02', '2017-01-11 01:58:02'),
(6, 0, 'ALAY PAPER BAG', '2017-01-11 17:51:16', '2017-01-11 17:51:16'),
(7, 0, 'SUPACK PAPER BAG', '2017-01-11 17:51:32', '2017-01-11 17:51:32'),
(8, 0, 'TIME TECHONPLAST', '2017-01-11 18:03:47', '2017-01-11 18:03:47'),
(9, 0, 'TPI INDIA', '2017-01-11 18:05:51', '2017-01-11 18:05:51'),
(10, 0, 'KINJAL PELLET', '2017-01-11 18:08:13', '2017-01-11 18:08:13');

-- --------------------------------------------------------

--
-- Table structure for table `surveyors`
--

CREATE TABLE `surveyors` (
  `Id` int(5) NOT NULL,
  `company_id` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surveyors`
--

INSERT INTO `surveyors` (`Id`, `company_id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 0, 'Test Surveyor', 0, '2016-09-30 13:36:07', '2017-02-04 22:55:51'),
(2, 0, 'GEOCHEM', 1, '2016-10-13 17:53:36', '2016-10-13 17:53:36'),
(3, 0, 'SGS', 1, '2016-10-24 19:27:44', '2016-10-24 19:27:44'),
(4, 0, 'INTERTEK', 1, '2016-10-29 17:49:47', '2016-10-29 17:49:47'),
(5, 0, 'ANY', 1, '2017-02-02 22:41:53', '2017-02-02 22:41:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `skype` varchar(50) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `restricted` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `username`, `email`, `password`, `first_name`, `last_name`, `website`, `facebook`, `skype`, `remember_token`, `restricted`, `created_at`, `updated_at`) VALUES
(1, 'hiren', 'modhvadia.hiren@gmail.com', 'z', 'hiren', 'Modhvadia', 'http://www.elementarysoft.in', 'hirenm', 'hiren.modhvadia2', NULL, '[]', '2016-08-30 02:07:05', '2016-09-12 18:05:30'),
(2, 'admin', 'admin@gmail.com', '123', 'Hiren', 'Modhvadia', 'http://www.google.co.in', 'admin.fb', 'admin.skype', NULL, '[\"get:contract@index\",\"get:contract@create\",\"get:contract@_new\",\"get:contract@edit\"]', '2016-09-09 19:46:30', '2016-09-12 18:05:06'),
(3, 'chirag', '', '123', 'Chirag', '', NULL, NULL, NULL, NULL, '[]', '2016-09-30 12:13:25', '2017-07-08 07:15:42'),
(6, 'subhash', NULL, 'subhash123', 'Subhash', '', NULL, NULL, NULL, NULL, '', '2017-04-18 19:29:13', '2017-04-18 19:29:13');

-- --------------------------------------------------------

--
-- Table structure for table `vgm_details`
--

CREATE TABLE `vgm_details` (
  `Id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `shipment_id` int(11) NOT NULL,
  `name_authorized` varchar(255) CHARACTER SET utf8 NOT NULL,
  `designation_authorized` varchar(255) CHARACTER SET utf8 NOT NULL,
  `contact_detail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `max_weight_csc` varchar(255) CHARACTER SET utf8 NOT NULL,
  `gross_mass` text CHARACTER SET utf8,
  `gross_method` text CHARACTER SET utf8,
  `gross_net` text CHARACTER SET utf8,
  `gross_tare` text CHARACTER SET utf8,
  `gross_packing` text CHARACTER SET utf8,
  `date_time` varchar(255) CHARACTER SET utf8 NOT NULL,
  `weight_slip_no` text CHARACTER SET utf8 NOT NULL,
  `type` text CHARACTER SET utf8 NOT NULL,
  `hazardous` varchar(255) CHARACTER SET utf8 NOT NULL,
  `booking` text CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vgm_details`
--

INSERT INTO `vgm_details` (`Id`, `contract_id`, `shipment_id`, `name_authorized`, `designation_authorized`, `contact_detail`, `max_weight_csc`, `gross_mass`, `gross_method`, `gross_net`, `gross_tare`, `gross_packing`, `date_time`, `weight_slip_no`, `type`, `hazardous`, `booking`, `created_at`, `updated_at`) VALUES
(1, 174, 176, '227171008am', '227171008am', '227171008am', '[\"aasAS\",\"FGDFG\",\"234234\"]', '[\"324234\",\"23423\",\"4234234\"]', NULL, NULL, NULL, NULL, '2017-07-22 14:00:00', '[\"1\",\"2\",\"3\"]', '[\"Reefer\",\"Hazardous\",\"Other\"]', '227171008am', '[\"gfgsfsfg\",\"234234\",\"oiuuoiuoiuo\"]', '2017-07-22 06:25:14', '2017-07-22 06:25:14'),
(3, 172, 174, 'sadsadasd', 'asdsad', 'sadsadsad', '[\"30480\",\"30480\",\"8554\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '22-07-2017 12:00:00:00', '[\"12543\",\"asdasd\",\"234324\"]', '[\"Reefer\",\"Other\",\"Normarl\"]', '', '[\"sadasd\",\"asdasd\",\"423423423\"]', '2017-07-22 07:03:13', '2017-07-22 07:03:13'),
(4, 273, 351, '', '', '', '[\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-22 :00', '[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', '[\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\"]', '', '[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]', '2017-07-22 07:03:59', '2017-07-22 07:03:59'),
(6, 253, 294, '22717108', '22717108', '22717108', '[\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '22-07-2017 03:00:00:00', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\"]', '[\"Normarl\",\"Reefer\",\"Hazardous\",\"Other\",\"Normarl\",\"Reefer\",\"Hazardous\",\"Other\",\"Hazardous\",\"Reefer\"]', 'na', '[\"no\",\"no\",\"no\",\"no\",\"no\",\"no\",\"no\",\"no\",\"no\",\"no\"]', '2017-07-22 07:59:08', '2017-07-22 07:59:08'),
(7, 25, 27, 'Jatinbhai', 'Head', '1122334455', '[\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\",\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-22 :00', '[\"11\",\"22\",\"33\",\"44\",\"55\",\"66\",\"77\",\"88\"]', '[\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\",\"Normarl\"]', 'NA', '[\"123\",\"\",\"123\",\"\",\"\",\"123\",\"\",\"345\"]', '2017-07-22 08:02:04', '2017-07-22 08:02:04'),
(8, 20, 22, 'test', 'sdfsdfsdfsdfsdfsdf', '23432424234234234', '[\"30480\",\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-11 08:00:00', '[\"sadsadasd\",\"asdasdasdasd\"]', '[\"Other\",\"Hazardous\"]', 'NA', '[\"asdasd\",\"asdasdasd\"]', '2017-07-22 11:33:31', '2017-07-22 11:33:31'),
(9, 49, 51, 'test', 'dsfsddf', 'sdfsdf', '[\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-24 11:00:00:00:00', '[\"dfgdgdfg\"]', '[\"Normarl\"]', 'NA', '[\"\"]', '2017-07-24 04:20:35', '2017-07-24 04:20:35'),
(10, 33, 35, '3rwerwer', 'rwerwere', 'werwerwer', '[\"30480\",\"30480\",\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-20 10:00:00', '[\"gfhgfh\",\"fghfgh\",\"fghfghfgh\"]', '[\"Reefer\",\"Hazardous\",\"Other\"]', 'NA', '[\"fghfghf\",\"fghfgh\",\"fghfghfghfgh\"]', '2017-07-24 04:22:09', '2017-07-24 04:22:09'),
(11, 276, 358, 'sdfsdf', 'dsrwer', '234234442', '[\"30480\",\"30480\",\"30480\",\"30480\",\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-29 06:00:00', '[\"dfsdf\",\"sdfsdf\",\"sff\",\"sfdf\",\"sdfsdfsdf\"]', '[\"Reefer\",\"Other\",\"Hazardous\",\"Normarl\",\"Other\"]', 'asdasd', '[\"sadsad\",\"asdasd\",\"asdasd\",\"sadasd\",\"asdsad\"]', '2017-07-29 07:07:22', '2017-07-29 07:07:22'),
(12, 51, 53, 'sad', 'sadsa', 'sad', '[\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-31 11:59:00', '[\"sadasd\"]', '[\"Normarl\"]', 'sd', '[\"\"]', '2017-07-31 06:29:11', '2017-07-31 06:29:11'),
(13, 240, 254, '', '', '', '[\"30480\",\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-31 12:34:00', '[\"\",\"\"]', '[\"Normarl\",\"Normarl\"]', '', '[\"\",\"\"]', '2017-07-31 07:04:17', '2017-07-31 07:04:17'),
(14, 279, 362, 'test', 'test', 'test', '[\"30480\",\"30480\",\"30480\",\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-07-31 14:53:00', '[\"123\",\"123\",\"123\",\"123\"]', '[\"Reefer\",\"Reefer\",\"Normarl\",\"Normarl\"]', 'N/A', '[\"12\",\"12\",\"12\",\"12\"]', '2017-07-31 09:24:30', '2017-07-31 09:24:30'),
(15, 280, 369, 'test', 'test', '1122334455', '[\"30480\",\"30480\"]', '[\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\",\"METHOD-1 \\/ NET 17100+ TARE 2220 + PACKING 840 = 20160\"]', NULL, NULL, NULL, NULL, '2017-08-03 11:29:00', '[\"123\",\"123\"]', '[\"Normarl\",\"Hazardous\"]', 'N/A', '[\"NA\",\"NA\"]', '2017-08-03 05:59:56', '2017-08-03 05:59:56'),
(27, 274, 352, 'Lalitbhai', '', '9714613331', '[\"30480\",\"30480\",\"30480\",\"30480\"]', '', '[\"Method-1\",\"Method-1\",\"Method-1\",\"Method-1\"]', '[\"1\",\"1\",\"1\",\"1\"]', '[\"2\",\"2\",\"2\",\"2\"]', '[\"3\",\"3\",\"3\",\"3\"]', '0000-00-00 00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"Normal\",\"Normal\",\"Normal\",\"Normal\"]', '', '[\"\",\"\",\"\",\"\"]', '2017-08-11 06:20:25', '2017-08-11 06:20:25'),
(28, 282, 371, 'Lalitbhai', '', '9714613331', '[\"30480\",\"30480\",\"30480\",\"30480\"]', NULL, '[\"Method-1\",\"Method-1\",\"Method-1\",\"Method-1\"]', '[\"10\",\"10\",\"10\",\"10\"]', '[\"20\",\"20\",\"20\",\"20\"]', '[\"30\",\"30\",\"30\",\"30\"]', '11-08-2017 01:00:00:00', '[\"\",\"\",\"\",\"\"]', '[\"Normal\",\"Normal\",\"Normal\",\"Normal\"]', 'N/A', '[\"\",\"\",\"\",\"\"]', '2017-08-11 08:09:24', '2017-08-11 08:09:24'),
(29, 282, 374, 'Lalitbhai', 'Head', '9714613331', '[\"30480\",\"30480\",\"30480\",\"30480\"]', NULL, '[\"Method-1\",\"Method-1\",\"Method-1\",\"Method-1\"]', '[\"12\",\"13\",\"14\",\"14\"]', '[\"12\",\"13\",\"14\",\"14\"]', '[\"12\",\"13\",\"14\",\"14\"]', '2017-08-11 15:31:00:00:00', '[\"123\",\"234\",\"345\",\"456\"]', '[\"Normal\",\"Normal\",\"Normal\",\"Normal\"]', 'N/A', '[\"\",\"\",\"\",\"\"]', '2017-08-11 10:07:22', '2017-08-11 10:07:22'),
(30, 282, 373, 'Lalitbhai', 'Head', '9714613331', '[\"30480\",\"30480\",\"30480\",\"30480\"]', NULL, '[\"Method-1\",\"Method-1\",\"Method-1\",\"Method-1\"]', '[\"11\",\"44\",\"77\",\"111\"]', '[\"22\",\"55\",\"88\",\"112\"]', '[\"33\",\"66\",\"97\",\"113\"]', '11-08-2017 15:52:00:00', '[\"\",\"\",\"\",\"\"]', '[\"Normal\",\"Normal\",\"Normal\",\"Normal\"]', 'N/A', '[\"\",\"\",\"\",\"\"]', '2017-08-11 10:23:59', '2017-08-11 10:23:59'),
(31, 192, 194, 'Lalitbhai', 'Head', '9714613331', '[\"30480\",\"324324\",\"32423\"]', NULL, '[\"Method-1\",\"Method-1\",\"Method-1\"]', '[\"423\",\"34\",\"234\"]', '[\"342\",\"32\",\"234\"]', '[\"23\",\"324\",\"234\"]', '2017-08-12 3:02:00', '[\"123\",\"123\",\"123\"]', '[\"Normal\",\"Normal\",\"Normal\"]', '', '[\"NO\",\"NO\",\"NO\"]', '2017-08-12 09:34:07', '2017-08-12 09:34:07'),
(32, 275, 333, 'Lalitbhai', '', '9714613331', '[\"30480\",\"30480\",\"30480\",\"30480\"]', NULL, '[\"Method-1\",\"Method-1\",\"Method-2\",\"Method-2\"]', '[\"12\",\"12\",\"12\",\"12\"]', '[\"12\",\"12\",\"12\",\"12\"]', '[\"12\",\"2\",\"12\",\"12\"]', '2017-08-16 10:59:00', '[\"23213213\",\"123123\",\"123123\",\"123123\"]', '[\"Normal\",\"Normal\",\"Normal\",\"Normal\"]', '', '[\"\",\"\",\"\",\"\"]', '2017-08-16 05:29:22', '2017-08-16 05:29:22'),
(33, 275, 334, 'Lalitbhai', '', '9714613331', '[\"30480\",\"30480\"]', NULL, '[\"Method-1\",\"Method-1\"]', '[\"1\",\"1\"]', '[\"2\",\"2\"]', '[\"3\",\"3\"]', '2017-08-16 12:30:00', '[\"\",\"\"]', '[\"Normal\",\"Normal\"]', '', '[\"\",\"\"]', '2017-08-16 09:17:28', '2017-08-16 09:17:28'),
(34, 277, 340, 'Lalitbhai', 'Head', '9714613331', '[\"30480\",\"30480\"]', NULL, '[\"Method-1\",\"Method-1\"]', '[\"123\",\"123\"]', '[\"123\",\"123\"]', '[\"123\",\"123\"]', '2017-08-18 10:19:00', '[\"123\",\"123\"]', '[\"Normal\",\"Normal\"]', 'N/A', '[\"123\",\"123\"]', '2017-08-18 04:50:20', '2017-08-18 04:50:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `contracts_do`
--
ALTER TABLE `contracts_do`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `contracts_docs`
--
ALTER TABLE `contracts_docs`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `contracts_product`
--
ALTER TABLE `contracts_product`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `contracts_rawmaterial`
--
ALTER TABLE `contracts_rawmaterial`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `contracts_scheme`
--
ALTER TABLE `contracts_scheme`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `contracts_shipment`
--
ALTER TABLE `contracts_shipment`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `delivery_terms`
--
ALTER TABLE `delivery_terms`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `discharge_ports`
--
ALTER TABLE `discharge_ports`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `final_destination`
--
ALTER TABLE `final_destination`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `forwarders`
--
ALTER TABLE `forwarders`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `importers`
--
ALTER TABLE `importers`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `payment_terms`
--
ALTER TABLE `payment_terms`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ports`
--
ALTER TABLE `ports`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `schemes`
--
ALTER TABLE `schemes`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `stocks_product`
--
ALTER TABLE `stocks_product`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `stocks_vendor`
--
ALTER TABLE `stocks_vendor`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `surveyors`
--
ALTER TABLE `surveyors`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `vgm_details`
--
ALTER TABLE `vgm_details`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;
--
-- AUTO_INCREMENT for table `contracts_do`
--
ALTER TABLE `contracts_do`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=440;
--
-- AUTO_INCREMENT for table `contracts_docs`
--
ALTER TABLE `contracts_docs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `contracts_product`
--
ALTER TABLE `contracts_product`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=580;
--
-- AUTO_INCREMENT for table `contracts_rawmaterial`
--
ALTER TABLE `contracts_rawmaterial`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=396;
--
-- AUTO_INCREMENT for table `contracts_scheme`
--
ALTER TABLE `contracts_scheme`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=402;
--
-- AUTO_INCREMENT for table `contracts_shipment`
--
ALTER TABLE `contracts_shipment`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=396;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `Id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;
--
-- AUTO_INCREMENT for table `delivery_terms`
--
ALTER TABLE `delivery_terms`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `discharge_ports`
--
ALTER TABLE `discharge_ports`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `final_destination`
--
ALTER TABLE `final_destination`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `forwarders`
--
ALTER TABLE `forwarders`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `importers`
--
ALTER TABLE `importers`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `Id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `payment_terms`
--
ALTER TABLE `payment_terms`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `ports`
--
ALTER TABLE `ports`
  MODIFY `Id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `schemes`
--
ALTER TABLE `schemes`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `stocks_product`
--
ALTER TABLE `stocks_product`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `stocks_vendor`
--
ALTER TABLE `stocks_vendor`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `surveyors`
--
ALTER TABLE `surveyors`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vgm_details`
--
ALTER TABLE `vgm_details`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
