<?php

class PaymentTermController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "PaymentTerm's Register",
            'PaymentTerms' => API::get('paymentterms')
        ];
        $this->render('paymentterms/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New PaymentTerm"
        ];
        $this->render('paymentterms/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('paymentterm', Request::all());
        $this->redirect('paymentterms');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit PaymentTerm",
            'PaymentTerm' => API::get('paymentterm/' . $id)
        ];
        $this->render('paymentterms/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("paymentterm/$id", Request::all());
        $this->redirect('paymentterms');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('paymentterm/' . $id);
        $this->redirect('paymentterms');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('paymentterms/quickAdd');
    }
    public function quickCreate() {
        echo API::post('paymentterm', Request::all(), true);
        exit();
    }
}