<?php

class AppController extends Controller {

    public function index() {
        $data = [
            'Title' => 'Application Settings'
        ];
        $this->render('app/index', $data);
    }

}