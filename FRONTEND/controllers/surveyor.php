<?php

class SurveyorController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "Surveyor's Register",
            'Surveyors' => API::get('surveyors')
        ];
        $this->render('surveyors/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New Surveyor"
        ];
        $this->render('surveyors/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('surveyor', Request::all());
        $this->redirect('surveyors');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit Surveyor",
            'Surveyor' => API::get('surveyor/' . $id)
        ];
        $this->render('surveyors/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("surveyor/$id", Request::all());
        $this->redirect('surveyors');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('surveyor/' . $id);
        $this->redirect('surveyors');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('surveyors/quickAdd');
    }
    public function quickCreate() {
        echo API::post('surveyor', Request::all(), true);
        exit();
    }
}