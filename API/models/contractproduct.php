<?php

class ContractProduct extends Model {

    protected $table = 'contracts_product';     
    protected $primaryKey = 'Id';

    public function product() {
        return $this->hasOne('Product', 'Id', 'product_id');
    }
    

}