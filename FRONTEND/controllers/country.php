<?php

class CountryController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "Country's Register",
            'Countrys' => API::get('countrys')
        ];
        $this->render('countrys/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New Country"
        ];
        $this->render('countrys/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('country', Request::all());
        $this->redirect('countrys');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit Country",
            'Country' => API::get('country/' . $id)
        ];
        $this->render('countrys/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("country/$id", Request::all());
        $this->redirect('countrys');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('country/' . $id);
        $this->redirect('countrys');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('countrys/quickAdd');
    }
    public function quickCreate() {
        echo API::post('country', Request::all(), true);
        exit();
    }
}