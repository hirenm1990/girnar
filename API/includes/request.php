<?php

class Request {

	# Get parameters by key
	public static function get( $key ) {
		$request = json_decode(base64_decode($_REQUEST['encrypted']), true);
		$request = array_merge($request, $_REQUEST);
		return (isset($request[$key])) ? $request[$key] : false;
	}

	# Get all parameters
	public static function all() {
		$request = ( isset($_REQUEST['encrypted']) ) ? json_decode(base64_decode($_REQUEST['encrypted']), true) : [];
		$request = array_merge($request, $_REQUEST);
		return $request;
	}

	# Get request method
	public static function method() {
		return (isset($_REQUEST['_method'])) ? strtoupper($_REQUEST['_method']) : $_SERVER['REQUEST_METHOD'];
	}

}