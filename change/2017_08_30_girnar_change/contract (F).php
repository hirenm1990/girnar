<?php

class ContractController extends Controller {

    public function index() {
        $data = [
            'Title' => 'Contract Register',
            'Contracts' => API::get('contracts')
        ];
        $this->render('contracts/index', $data);
    }

    public function index2() {
        if( !Request::get('filter') ) {
            $_REQUEST['filter'] = 'incomplete';
        }

        $data = [
            'Title' => 'Contract Register',
            'Contracts' => API::get('contracts/shipments', Request::all()),
            'Filter' => Request::get('filter'),
            '_js' => [ RESOURCES . '/js/jquery.dataTables.min.js', RESOURCES . '/js/contracts2.js' ],
            '_css' => [ RESOURCES . '/css/jquery.dataTables.css' ]
        ];
        $this->render('contracts/index2', $data);
    }

	public function _new() {
		$data = [
			'Title' => 'Girnar Industries',
            'Surveyors' => API::get('surveyors'),
            'Importers' => API::get('importers'),
            'Ports' => API::get('ports'),
            'Countries' => API::get('countrys'),
            'PaymentTerms' => API::get('paymentterms'),
            'DeliveryTerms' => API::get('deliveryterms'),
            'DischargePorts' => API::get('dischargeports'),
            'FinalDestinations' => API::get('finaldestinations'),
            'Products' => API::get('products'),
            'ContainerSizes' => API::get('containersizes'),
            'Packages' => API::get('packages')
		];
		$this->render('contracts/new', $data);
	}

	public function duplicate( $id ) {
        $data = [
            'Title' => 'Girnar Industries',
            'Surveyors' => API::get('surveyors'),
            'Importers' => API::get('importers'),
            'Ports' => API::get('ports'),
            'Countries' => API::get('countrys'),
            'PaymentTerms' => API::get('paymentterms'),
            'DeliveryTerms' => API::get('deliveryterms'),
            'DischargePorts' => API::get('dischargeports'),
            'FinalDestinations' => API::get('finaldestinations'),
            'Products' => API::get('products'),
            'ContainerSizes' => API::get('containersizes'),
            'Packages' => API::get('packages'),
            'Contract' => API::get('contract/' . $id)
        ];
        $this->render('contracts/new-duplicate', $data);
    }

    public function create() {
        $contract = API::post('contract', Request::all());
        $id = $contract['Id'];

        // var_dump($contract);
        // exit();
        require_once ROOT . "controllers/document.php";
        $docController = new documentController;
        $docController->upload_multiple( $id );

        $this->redirect('contract/edit/' . $contract['shipment_id']);
    }

    public function newshipment( $contract_id ) {
        $data = [
            'Title' => 'Add New Shipment',
            'Contract' => API::get('contract/c/' . $contract_id ),
            'Products' => API::get('products'),
            'ContainerSizes' => API::get('containersizes'),
            'Packages' => API::get('packages')
        ];

        $data['dischargePorts'] = [
            $data['Contract']['port_discharge'],
            $data['Contract']['port_discharge2'],
            $data['Contract']['port_discharge3'],
        ];

        $this->render('contracts/new-shipment', $data);
    }

    public function createshipment() {
        $id = API::post('contract/create/shipment', Request::all());
        $this->redirect('contract/edit/' . $id);
    }

	public function edit( $id ) {
        $contract = API::get('contract/' . $id);
        $data = [
            'Title' => "Edit Contract",
            'Contract' => $contract,
            'Shipments' => API::get('contract/shipmentlist/' .$contract['Id']),
            'Surveyors' => API::get('surveyors'),
            'Importers' => API::get('importers'),
            'Ports' => API::get('ports'),
            'Countries' => API::get('countrys'),
            'Products' => API::get('products'),
            'Forwarders' => API::get('forwarders'),
            'Schemes' => API::get('schemes'),
            'PaymentTerms' => API::get('paymentterms'),
            'DeliveryTerms' => API::get('deliveryterms'),
            'DischargePorts' => API::get('dischargeports'),
            'FinalDestinations' => API::get('finaldestinations'),
            //'Products' => API::get('products'),
            'ContainerSizes' => API::get('containersizes'),
            'Packages' => API::get('packages'),
            'Stock' => API::get('stock'),
            'Company' => API::get('company/' . Cache::get('companyId')),
            // 'vgm' => API::get('shipment/vgm/' .$id),
        ];

        $data['productDischargePorts'] = [
            $data['Contract']['port_discharge'] . ", " . $data['Contract']['final_destination'],
            $data['Contract']['port_discharge2'] . ", " . $data['Contract']['final_destination2'],
            $data['Contract']['port_discharge3'] . ", " . $data['Contract']['final_destination3'],
        ];
        
        // var_dump($data['Contract']['shipment']['vgms']); exit();
        // return $data; exit();
		$this->render('contracts/edit', $data);
	}

    public function update( $id ) {
        API::put("contract/" . $id, Request::all());
        $this->redirect('contract/edit/' . Request::get('shipment_id'));
    }

    public function updateshipment( $id ) {
        API::put("shipment/" . $id, Request::all());
        $this->redirect('contract/edit/' . $id . '#packagingTab');
    }

    public function updatedo( $shipment_id ) {
        API::put('contract/do/' . $shipment_id, Request::all());
        $this->redirect('contract/edit/' . $shipment_id . '#doTab');
    }

    public function updaterm( $shipment_id ) {
        API::put('contract/rm/' . $shipment_id, Request::all());
        $this->redirect('contract/edit/' . $shipment_id . '#rawMaterialTab');
    }

    public function updatescheme( $shipment_id ) {
        API::put('contract/scheme/' . $shipment_id, Request::all());
        $this->redirect('contract/edit/' . $shipment_id . '#schemesTab');
    }

    public function remove( $contract_id ) {
        API::delete('contract/remove/' . $contract_id);
        $this->redirect('contracts');
    }

    public function removeshipment( $shipment_id ) {
        API::delete('shipment/remove/' . $shipment_id);
        $this->redirect('contracts');
    }

    public function updatestuffing( $shipment_id ) {
        API::put('shipment/stuffing/' . $shipment_id, Request::all());
        $this->redirect('contract/edit/' . $shipment_id . '#stuffingTab');
    }

    public function updatecomminvoice( $shipment_id ) {
        API::put('shipment/comminvoice/' . $shipment_id, Request::all());
        $this->redirect('contract/edit/' . $shipment_id . '#paymentTab');
    }

    public function updatevgm( $shipment_id ) {
        API::post('shipment/vgm/' .$shipment_id, Request::all());
        // return Request::all();
        $this->redirect('contract/edit/' . $shipment_id . '#vgm');
    }
}
