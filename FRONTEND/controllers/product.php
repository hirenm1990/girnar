<?php

class ProductController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "Product's Register",
            'Products' => API::get('products')
        ];
        $this->render('products/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New Product"
        ];
        $this->render('products/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('product', Request::all());
        $this->redirect('products');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit Product",
            'Product' => API::get('product/' . $id)
        ];
        $this->render('products/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("product/$id", Request::all());
        $this->redirect('products');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('product/' . $id);
        $this->redirect('products');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('products/quickAdd');
    }
    public function quickCreate() {
        echo API::post('product', Request::all(), true);
        exit();
    }
}