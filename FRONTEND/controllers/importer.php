<?php

class ImporterController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "Importer's Register",
            'Importers' => API::get('importers')
        ];
        $this->render('importers/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New Importer"
        ];
        $this->render('importers/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('importer', Request::all());
        $this->redirect('importers');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit Importer",
            'Importer' => API::get('importer/' . $id)
        ];
        $this->render('importers/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("importer/$id", Request::all());
        $this->redirect('importers');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('importer/' . $id);
        $this->redirect('importers');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('importers/quickAdd');
    }
    public function quickCreate() {
        echo API::post('importer', Request::all(), true);
        exit();
    }
}