<?php

class LoginController extends Controller {

	public function index() {
		
		if($this->auth()) {
			$this->redirect('/');
		}

		$data = [
			'Title' => 'Girnar Industries'
		];
		$this->render('login', $data);
	}

	public function post() {
		if(!isset($_POST['username'], $_POST['password']) || empty($_POST['username']) || empty($_POST['password'])) {
			return $this->error("Username &amp; password required.");
		}

		$data = [
			'username' => $_POST['username'],
			'password' => $_POST['password']
		];

		$result = API::get('user/login', $data);

		if($result['success']) {
			Cache::set('userid', $result['userid']);
			Cache::set('token', $result['token']);
			Cache::set('companyId', 1);

			$this->success(['redirect' => SITE_URL]);
		} else {
			$this->error( $result['error'] );
		}
	}

	public function logout() {
		Cache::clear();
		$this->redirect('login');
	}

}