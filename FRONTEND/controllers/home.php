<?php

class HomeController extends Controller {

	public function index() {
        $data = [
            'Title' => 'Dashboard',
            'Dashboard' => API::get('dashboard/statistics'),
        ];
		$this->render('dashboard/index', $data);
	}

    public function api( $any ) {
        echo API::get( $any, Request::all(), true );
    }

}