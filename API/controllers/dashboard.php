<?php

class DashboardController extends Controller {

    public function statistics() {
        $statistics = [
            'contracts' => Contract::count(),
            'shipments' => Shipment::count(),
            'importers' => Importer::count(),
            'products' => Product::count(),
            'countries' => Country::count()
        ];

        return $statistics;
    }

}