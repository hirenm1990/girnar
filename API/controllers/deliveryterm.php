<?php

class DeliveryTermController extends Controller {

    public function get() {
        return DeliveryTerm::orderBy('name')->get();
    }

    public function getById( $id ) {
        return DeliveryTerm::find( $id );
    }

    public function create() {
        $request = Request::all();

        $deliveryterm = new DeliveryTerm;
        $deliveryterm->name = $request['name'];
        $id = $deliveryterm->save();
        return $deliveryterm;
    }

    public function update( $id ) {
        $request = Request::all();

        $deliveryterm = DeliveryTerm::find( $id );
        $deliveryterm->name = $request['name'];

        $deliveryterm->save();
    }

    public function remove( $id ) {
        DeliveryTerm::destroy( $id );
    }

}