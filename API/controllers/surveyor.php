<?php

class SurveyorController extends Controller {

    public function get() {
        return Surveyor::where('is_active', 1)->orderBy('name')->get();
    }

    public function getById( $id ) {
        return Surveyor::find( $id );
    }

    public function create() {
        $request = Request::all();

        $surveyor = new Surveyor;
        $surveyor->name = $request['name'];
        $id = $surveyor->save();
        return $surveyor;
    }

    public function update( $id ) {
        $request = Request::all();

        $surveyor = Surveyor::find( $id );
        $surveyor->name = $request['name'];

        $surveyor->save();
    }

    public function remove( $id ) {
        $surveyor = Surveyor::find( $id );
        $surveyor->is_active = 0;
        $surveyor->save();

        // Surveyor::destroy( $id );
    }

}