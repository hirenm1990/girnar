<?php

class Router extends Controller {

	private static $routes = [];
	private static $path_prefix = "";
	private static $middleware_prefix = "";

	private static function route( $pattern, $callback, $method = "GET", $middleware = "" ) {
		// Prefix pattern
		$pattern = trim(self::$path_prefix, '/') . '/' . trim($pattern, '/');
		$pattern = trim($pattern, '/');

		// Prepare regex pattern
		$pattern = str_replace("{:any}", "(.*)", $pattern);
		$pattern = preg_replace("/[{].*[}]/U", "([^/]+)", $pattern);
		$pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';

		// Save route
		self::$routes[] = [
			'pattern' => $pattern,
			'callback' => $callback,
			'methods' => explode("|", $method),
			'middleware' => array_filter(explode(",", self::$middleware_prefix . $middleware))
		];
	}

	private static function initialize() {
		// Get url
		$url = (isset($_GET['path'])) ? $_GET['path'] : '';
		$url = rtrim($url, '/');

		// Detect request method
		$method = (isset($_REQUEST['_method'])) ? $_REQUEST['_method'] : $_SERVER['REQUEST_METHOD'];

		// Pick relevant route
		foreach(self::$routes as $route) {
			if(in_array($method, $route['methods']) && preg_match($route['pattern'], $url, $params)) {

				// Load middleware if any
				foreach($route['middleware'] as $middleware) {
					$middleware_result = call_user_func_array([new $middleware, 'index'], [$route, $params]);
					if($middleware_result !== true) {
						return $middleware_result;
					}
				}

				// Clean params
				array_shift($params);

				// Detect if callback is function or controller@action
				if(is_callable($route['callback'])) {
					call_user_func_array($route['callback'], array_values($params));
					return true; // Return true for GUI frontend
				} else {
					$parsed = explode("@", $route['callback']);

					// load controller file
					require_once ('controllers/' . $parsed[0] . '.php');

					// controller naming convention
					$controller = $parsed[0] . 'Controller';

					// call controller->action
					$result = call_user_func_array([new $controller, $parsed[1]], $params);

					if(is_array($result)) {
						echo json_encode($result);
					}
					return true; // Return true for GUI frontend
				}
			}
		}

		return false;
	}

	public static function group($option, $callback) {
		# Set path prefix
		if(isset($option['path'])) {
			self::$path_prefix = $option['path'];
		}

		# Set middleware prefix
		if(isset($option['middleware'])) {
			self::$middleware_prefix = $option['middleware'] . ",";
		}

		# Call sub functions
		call_user_func_array($callback, array());

		# Clear prefixes
		self::$path_prefix =  "";
		self::$middleware_prefix = "";
	}

	public static function get( $pattern, $callback, $middleware = "" ) {
		self::route( $pattern, $callback, "GET", $middleware );
	}

	public static function post( $pattern, $callback, $middleware = "" ) {
		self::route( $pattern, $callback, "POST", $middleware );
	}

	public static function put( $pattern, $callback, $middleware = "" ) {
		self::route( $pattern, $callback, "PUT", $middleware );
	}

	public static function delete( $pattern, $callback, $middleware = "" ) {
		self::route( $pattern, $callback, "DELETE", $middleware );
	}

	public static function auto( $pattern, $controller ) {
		self::get($pattern . 's', $controller . '@index');
		self::get($pattern . '/new', $controller . '@_new');
	    self::post($pattern, $controller . '@create');
	    self::get($pattern . '/edit/{id}', $controller . '@edit');
	    self::post($pattern . '/edit/{id}', $controller . '@update');
	    self::get($pattern . '/remove/{id}', $controller . '@remove');

	    # Quick Data Adding Routes
	    Router::get('quick/add/' . $pattern, $controller . '@quickAdd');
	    Router::post('quick/add/' . $pattern, $controller . '@quickCreate');
	}

	public static function run() {
		if(!self::initialize()) {
			self::render('404');
			exit();
		}
	}
}