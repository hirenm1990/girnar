<?php

class PackageController extends Controller {

    # Page request - Show all items on page
    public function index() {
        $data = [
            'Title' => "Package's Register",
            'Packages' => API::get('packages')
        ];
        $this->render('packages/index', $data);
    }

    # Page request - Show new item page
    public function _new() {
        $data = [
            'Title' => "Add New Package"
        ];
        $this->render('packages/new', $data);
    }

    # Database operation - Add data to database
    public function create() {
        API::post('package', Request::all());
        $this->redirect('packages');
    }

    # Page request - Show item edit page
    public function edit( $id ) {
        $data = [
            'Title' => "Edit Package",
            'Package' => API::get('package/' . $id)
        ];
        $this->render('packages/edit', $data);
    }

    # Database request - Upadte item in database
    public function update( $id ) {
        API::put("package/$id", Request::all());
        $this->redirect('packages');
    }

    # Database request - Remove item from database
    public function remove( $id ) {
        API::delete('package/' . $id);
        $this->redirect('packages');
    }

    # Quick Add
    public function quickAdd() {
        $this->render('packages/quickAdd');
    }
    public function quickCreate() {
        echo API::post('package', Request::all(), true);
        exit();
    }
}