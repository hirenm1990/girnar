<?php

class API {

	private static function curl( $url, $data = [], $method, $raw_output = false ) {
		// prepare url
		$url = API_URL . $url;

		// prepare post data
		$post_data = [
			'_method' => $method,
			'app_id' => API_APPID,
			'encrypted' => base64_encode(json_encode($data))
		];

		// add userid & token in request if available
		if(Cache::_isset('token') && Cache::_isset('userid') && Cache::_isset('companyId')) {
			$post_data['_user'] = Cache::get('userid');
			$post_data['_token'] = Cache::get('token');
			$post_data['_companyid'] = Cache::get('companyId');
		}

		# Prepare header
		$header = [
			'Content-Type: multipart/form-data',
		];

		// do curl thing
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_POST, count($data));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
		$result = curl_exec($curl);
		curl_close($curl);

		# return raw output
		if($raw_output) {
			return $result;
		}

		# return resulted json
		return json_decode($result, true);
	}

	public static function get( $url, $data = [], $raw_output = false ) {
		return self::curl($url, $data, 'GET', $raw_output);
	}

	public static function post( $url, $data = [], $raw_output = false ) {
		return self::curl($url, $data, 'POST', $raw_output);
	}

	public static function put( $url, $data = [], $raw_output = false ) {
		return self::curl($url, $data, 'PUT', $raw_output);
	}

	public static function delete( $url, $data = [], $raw_output = false ) {
		return self::curl($url, $data, 'DELETE', $raw_output);
	}

}