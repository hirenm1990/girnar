<?php

class StockvendorController extends Controller {

    public function get() {
        return Stockvendor::orderBy('name')->get();
    }

    public function getById( $id ) {
        return Stockvendor::find( $id );
    }

    public function create() {
        $request = Request::all();

        $surveyor = new Stockvendor;
        $surveyor->name = $request['name'];
        $id = $surveyor->save();
        return $surveyor;
    }

    public function update( $id ) {
        $request = Request::all();

        $surveyor = Stockvendor::find( $id );
        $surveyor->name = $request['name'];

        $surveyor->save();
    }

    public function remove( $id ) {
        Stockvendor::destroy( $id );
    }

}