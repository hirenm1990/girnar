<?php

class Purchase extends Model {

    protected $table = 'stocks';
    protected $primaryKey = 'Id';

    public function vendor() {
        return $this->hasOne('Stockvendor', 'Id', 'vendor_id');
    }

    public function product() {
        return $this->hasOne('Stockproduct', 'Id', 'product_id');
    }
}