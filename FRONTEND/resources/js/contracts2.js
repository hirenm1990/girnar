$(function() {

    var table = $("#datatable").DataTable({
        "pageLength": 100,
        "bSortCellsTop": true,
        "order": [
            [2, "desc"]
        ]
    });

    // table.columns().every( function () {
    //     var that = this;
    //
    //     $( 'input', this.footer() ).on( 'keyup change', function () {
    //         console.log($(this).val());
    //         if ( that.search() !== this.value ) {
    //             that
    //                 .search( this.value )
    //                 .draw();
    //         }
    //     } );
    // } );

    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input.hSearch' ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );

});