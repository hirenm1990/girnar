/**
 * General JS library for basic functionality
 * e.g. SelectBox, PopUp, Dropdown, TabBar etc.
 *
 * @author: Hiren Modhvadia <modhvadia.hiren@gmail.com>
 * @date: 22nd August 2016, 09:47 AM
 */

$(function() {

    /**
     * Header bar box-shadow on scroll
     */
    $('.container').scroll(function() {
        var scroll = $('.container').scrollTop();
        if (scroll > 0) {
            $(".header").addClass("shadow");
        } else {
            $(".header").removeClass("shadow");
        }
    });

    /**
     * Datepicker
     */
    $("input.datepick").datepick({
        dateFormat: "dd-mm-yyyy",
        showAnim: "",
        //monthsToShow: [2, 3],
        calculateWeek: true,
        onSelect: function() {
            $(this).trigger("change");
        }
    });

    /**
     * Datetime Picker
     */
    $("input.datetimepick").datetimepicker({
        format: "d-m-Y H:i",
        onChangeDateTime: function() {
            $(this).trigger("change");
        }
    });

    /**
     * TabBar
     */
    $(document).on("click", ".tabs li", function() {
        var me = $(this);

        if(!me.data("tab")) return;

        var tabs = me.parents(".tabs");
        var tabbar = tabs.parent(".tabbar");
        tabs.find("li").removeClass("active");
        me.addClass("active");

        tabbar.find(".tab").hide();
        tabbar.find(me.data("tab")).show();
    });

    /**
     * Dropdown
     */
    $(document).on("click", ".dropdown a", function(e) {
        e.stopPropagation();
        $('.dropdown ul').hide();
        $(this).parent(".dropdown").find("ul").show();
    });
    $(document).on("click", function(e) {
        if($(".dropdown").has(e.target).length === 0) {
            $(".dropdown ul").hide();
        }
    });

    /**
     * Invoke SelectBox Plugin
     */
    $("select").select();


    /**
     * Testing Confirm Plugin
     * Working Perfectly Fine
     * CSS stlyes are completely depending on plugin.popup
     */
    $(document).on("click", ".trash", function() {
        $.confirm({
            "title" : "Remove Contract",
            "message" : "You are about to remove contract `<em>forever</em>`. You will not be able to recover afterwise. Are you triple sure on your action?<br>",
            "buttons" : {
                "Yes" : {
                    class: "btn-red",
                    action: function() {
                        alert("contract deleted");
                    }
                },
                "No" : {
                    class: "btn-white"
                }
            }
        });
    });


    /**
     * Testing Notification/Alert Plugin
     * Working Perfectly Fine
     */
    //$.alert("Application started..", "pink");


    /**
     * File Upload
     */
    $(document).on("change", ".fileUploadBtn", function(e) {
        var files = $(this)[0].files;
        var contract_id = $(this).data('contractid');

        var list = "";
        $.each(files, function(i, v) {
            filename = v.name;
            filesize = Math.round(v.size/1000) + " Kilobytes";

            list = [
                '<li>',
                    '<div class="fileUploadName"><span>File Name</span> ', filename ,'</div>',
                    '<div class="fileUploadSize"><span>File Size</span> ', filesize ,'</div>',
                    '<progress value="0" max="100"></progress>',
                '</li>'
            ].join("");

            $(".fileUploadProgressWrap").append( list );

            var data = new FormData();
            data.append('file', v);

            var progress = $(".fileUploadProgressWrap li:last-child progress");

            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener("progress", function(event) {
                        if(event.lengthComputable) {
                            var percentComplete = event.loaded / event.total;
                            percentComplete = parseInt(percentComplete * 100);
                            progress.val( percentComplete );


                            if(percentComplete === 100) {

                            }
                        }
                    }, false);

                    return xhr;
                },
                url: config.url + "upload/" + contract_id,
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function( result ) {
                    progress.addClass("success");
                }
            });
        });
    });


    /**
     * Print Preview & Print Sample
     */
    $(document).on("click", ".printBtn", function() {
        var url = $(this).data("url");
        $(".modal.printPreviewModal").data("url", url);
        $(".modal.printPreviewModal #printPreviewIframe").attr("src", url);
        $(".modal.printPreviewModal").show();
    });
    $(document).on("click", ".modal.printPreviewModal .printPreviewPrintBtn", function() {
        var tempTitle = document.title;
        document.title = document.getElementById("printPreviewIframe").contentDocument.title;
        var frm = document.getElementById("printPreviewIframe").contentWindow;
        frm.focus();
        frm.print();
        document.title = tempTitle;
    });


    /**
     * Form Validation & Live Input Update
     */
    function validate(input) {

        // return if no validation rules applied
        if(!input.data("validate")) {
            return true;
        }

        var value = input.val();
        var rules = input.data("validate");
        var error = true;
        rules = rules.split("|");

        $.each($(rules), function(i, v) {
            console.log(v + " ==> " + $.trim(value).length);
            if(v == 'required' && $.trim(value).length === 0) {
                error = ["required", false];
                return;
            }

            if(v == 'max') {
                if(input.data('validate-max') && value.length > input.data('validate-max')) {
                    return ["max", false];
                }
            }

            if(v == 'min') {
                if(input.data('validate-min') && value.length < input.data('validate-min')) {
                    return ["min", false];
                }
            }

            if(v == 'unique') {
                if(input.data('validate-unique')) {
                    $.post(
                        input.data('validate-unique'),
                        { name: input.attr('name'), value: value },
                        function( response ) {
                            if(!response.success) {
                                return ["unique", false];
                            }
                        },
                        'json'
                    );
                }
            }
        });

        return error;
    }
    $(document).on("change", ".liveupdate", function() {
        console.log( validate($(this)) );
    });


    /**
     * Open Add New Entry Form On Pressing `+` on Combobox
     */
    $(document).on('keydown', '.selectWrap button', function(e) {
        if(e.key == "+" && $(this).attr("data-add-url")) {
            $('.quickActive').removeClass('quickActive');
            $(this).addClass('quickActive');

            var modal = $("<div class='modal quickAddModal'><div class='modal-title'>Quick Add New</div><div class='modal-close'><i class='ti-close'></i></div><div class='modal-content' style='max-height:inherit !important;margin-bottom: 0 !important;'> Loading... <style>.modal-content .form {padding:0}</style></div></div>");
            modal.appendTo('body').show();
            modal.find('.modal-content').load( $(this).data('add-url'), function() {
                $(".quickAddModal .quickFocus").focus();
            });
        }
    });
    /**
     * Open Add New Entry Form On Pressing AddNew button
     */
    $(document).on("click", '.selectBoxAddNew', function() {
        var event = $.Event("keydown");
        event.key = "+";
        $(this).parents(".selectWrap").find("button").focus().trigger(event);
    });

    /**
     * QUICK ADD SCRIPT
     */
    $(document).on("click", '.quickForm .quickAdd', function() {
        var modal = $(this).parents(".quickAddModal");
        var form = $(this).parents("form.quickForm");
        var target = $('.quickActive');
        var options = target.find(".optionsWrap ul");
        var actualSelect = $("." + target.parent(".selectWrap").data('id'));

        if(actualSelect.data('name-as-value')) {
            var nameAsValue = true;
        } else if(actualSelect.data('buyer')) {
            var buyerSelect = true;
        }

        $.post(
            form.attr("action"),
            form.serialize(),
            function( response ) {
                modal.remove();

                // if(nameAsValue) {
                //     actualSelect.append("<option value='"+response.name+"'>"+response.name+"</option>");
                //     options.append("<li class='quickAdded' data-value='"+response.name+"'>"+response.name+"</li>");
                // } else {
                //     actualSelect.append("<option value='"+response.Id+"'>"+response.name+"</option>");
                //     options.append("<li class='quickAdded' data-value='"+response.Id+"'>"+response.name+"</li>");
                // }

                $.each($("select[name='"+actualSelect.attr('name')+"']"), function(i, v) {
                    if(nameAsValue) {
                        $(this).append("<option value='"+response.name+"'>"+response.name+"</option>");
                        $(this).next(".selectWrap").find(".optionsWrap ul").append("<li class='quickAdded' data-value='"+response.name+"'>"+response.name+"</li>");
                    } else if(buyerSelect) {
                        $(this).append("<option value='"+response.Id+"' data-address='"+response.address+"'>"+response.name+"</option>");
                        $(this).next(".selectWrap").find(".optionsWrap ul").append("<li class='quickAdded' data-value='"+response.Id+"'>"+response.name+"</li>");
                    } else {
                        $(this).append("<option value='"+response.Id+"'>"+response.name+"</option>");
                        $(this).next(".selectWrap").find(".optionsWrap ul").append("<li class='quickAdded' data-value='"+response.Id+"'>"+response.name+"</li>");
                    }
                });

                // For selectbox inside template
                // var $template = $('template');
                // var node = $template.prop('content');
                // var $content = $(node).find("select[name='"+actualSelect.attr('name')+"']");
                // console.log($content);

                // $content.each( function() {
                //     if(nameAsValue) {
                //         $(this).append("<option value='"+response.name+"'>"+response.name+"</option>");
                //     } else {
                //         $(this).append("<option value='"+response.Id+"'>"+response.name+"</option>");
                //     }
                // });


                options.find('.quickAdded').click();
                options.find('.quickAdded').removeClass('quickAdded');
                $(".quickActive").focus();
                target.removeClass("quickActive");
            },
            'json'
        );
    });

});

/**
 * SelectBox Plugin
 * @author: Hiren Modhvadia <modhvadia.hiren@gmail.com>
 */
(function($) {
    $.fn.select = function() {
        return this.each(function(item) {

            // Unique id to select start
            if($('body').data('select_i') == undefined) {
                $('body').data('select_i', 1);
            } else {
                $('body').data('select_i', $('body').data('select_i') + 1);
            }
            var i = $('body').data('select_i');
            // Unique id to select End

            var me = $(this);

            // Add data attribute if exists, useful for + key integration
            if(me.attr('data-add-url')) {
                var html = "<div class='selectWrap' data-id='selectWrap"+i+"'>"
                        +"<button type='button' style='display:inline-block;'>"
                            +"<span></span>"
                            +"<i class='ti-angle-down'></i>"
                            +"<div class='optionsWrap'>"
                                +"<input type='text' disabled='disabled' placeholder='Type To Search...'>"
                                +"<ul></ul>"
                                +"<a class='selectBoxAddNew'><i class='ti-plus'></i> Add New Item</a>"
                            +"</div>"
                        +"</button>"
                    +"</div>";
                var inputBox = $(html);

                inputBox.find('button').attr('data-add-url', me.attr('data-add-url'));
            } else {
                var inputBox = $("<div class='selectWrap' data-id='selectWrap"+i+"'><button type='button' style='display:inline-block;'><span></span><i class='ti-angle-down'></i><div class='optionsWrap'><input type='text' disabled='disabled' placeholder='Type To Search...'><ul></ul></div></button></div>");
            }

            // hide default select input
            me.hide();

            me.addClass("selectWrap"+i);

            var list = "";
            var selectedValue = "";
            var selectedText = "";
            $.map(me.find("option"), function(i) {
                if(me.val() == i.value) {
                    selectedText = i.text;
                    selectedValue = i.value;
                    list += "<li class='active' data-value='"+i.value+"'>"+i.text+"</li>";
                } else {
                    list += "<li data-value='"+i.value+"'>"+i.text+"</li>";
                }
            });

            inputBox.find(".optionsWrap ul").html( list );

            // append new inputbox
            me.after( inputBox );

            // select default value
            inputBox.find("button span").html( selectedText );
        });
    }

    // On clicking/focusing selectbox
    $(document).on("click, focus", ".selectWrap button", function() {
        $(".optionsWrap").hide();
        $(this).addClass("activeSelect");
        $(this).find(".optionsWrap input").val("");
        selectOptionFilter();
        $(this).find(".optionsWrap").show();

        // Scroll to selected option
        var options = $(this).find(".optionsWrap ul");
        options.scrollTop(0).scrollTop(options.find(".active").position().top - 100);
    });

    // Hide on clicking outside selectbox
    $(document).on("click", function(e) {
        if($(".selectWrap").has(e.target).length === 0) {
            $(".selectWrap .optionsWrap").hide();
            $(".activeSelect").removeClass("activeSelect");
        }
    });

    // Option click event
    $(document).on("click", ".selectWrap .optionsWrap li", function(e) {
        e.stopPropagation();

        // change value in selectbox
        var select = $(this).parents(".selectWrap");
        select.find("button span").html( $(this).text() );

        var options = $(this).parent("ul");
        options.find("li").removeClass("active");
        $(this).addClass("active");

        select.find("button").blur();

        // change value in original select input
        var target = $("." + select.data("id"));
        target.val($(this).data("value")).trigger("change");
    });

    // Select blur/focusout event
    $(document).on("blur", ".selectWrap button", function(e) {
        $(".activeSelect").removeClass("activeSelect");
        $(".selectWrap .optionsWrap").hide();
    });

    // Keyboard navigation
    $(document).on("keydown", ".activeSelect", function(e) {
        var target = $("." + $(this).parent(".selectWrap").data("id"));
        var options = $(this).find(".optionsWrap ul");
        var search = $(this).find(".optionsWrap input");

        var active = options.find(".active");

        if(e.keyCode == 38) { // UP
            e.preventDefault();

            if(active.prevAll("li:visible:first").length > 0) {
                active.removeClass("active");
                active.prevAll("li:visible:first").addClass("active");

                // Scroll to active option
                options.scrollTop(0).scrollTop(options.find(".active").position().top - 100);

                var newActive = options.find(".active");
                $(this).find("span").html( newActive.text() );
                target.val( newActive.data("value") ).trigger("change");
            } else {
                if(options.find("li.active:visible").length === 0) {
                    options.find(".active").removeClass("active");
                    options.find("li:visible:first").addClass("active");
                    var newActive = options.find(".active");
                    $(this).find("span").html( newActive.text() );
                    target.val( newActive.data("value") ).trigger("change");
                }
            }
        } else if(e.keyCode == 40) { // DOWN
            e.preventDefault();

            if(active.nextAll("li:visible:first").length > 0) {
                active.removeClass("active");
                active.nextAll("li:visible:first").addClass("active");

                // Scroll to active option
                options.scrollTop(0).scrollTop(options.find(".active").position().top - 100);

                var newActive = options.find(".active");
                $(this).find("span").html( newActive.text() );
                target.val( newActive.data("value") ).trigger("change");
            } else {
                if(options.find("li.active:visible").length === 0) {
                    options.find(".active").removeClass("active");
                    options.find("li:visible:first").addClass("active");
                    var newActive = options.find(".active");
                    $(this).find("span").html( newActive.text() );
                    target.val( newActive.data("value") ).trigger("change");
                }
            }
        } else if(e.keyCode == 13) { // ENTER KEY
            e.preventDefault();
            options.find(".active").click();
        } else if(e.keyCode >= 65 && e.keyCode <= 90) { // CHARACTERS A-Z
            e.preventDefault();
            search.val(search.val() + e.key);
            selectOptionFilter();
        } else if(e.keyCode >= 96 && e.keyCode <= 105) { // NUMBER 0-0 FROM NUMPAD
            e.preventDefault();
            search.val(search.val() + e.key);
            selectOptionFilter();
        } else if(e.keyCode >= 48 && e.keyCode <= 57) { // NUMBER 0-9 FROM TOP NUMPAD
            e.preventDefault();
            search.val(search.val() + e.key);
            selectOptionFilter();
        } else if(e.keyCode == 8) { // BACKSPACE
            e.preventDefault();
            search.val(search.val().slice(0, -1));
            selectOptionFilter();
        } else {
            //console.log(e.key + " ==> " + e.keyCode);
        }
    });

    function selectOptionFilter() {
        var searchText = $(".activeSelect .optionsWrap input").val();
        searchText = searchText.toLowerCase();
        searchText = searchText.replace(/\s+/g, '');

        $(".activeSelect .optionsWrap ul li").each(function() {
            var currentLiText = $(this).text(),
            showCurrentLi = ((currentLiText.toLowerCase()).replace(/\s+/g, '')).indexOf(searchText) !== -1;

            if(showCurrentLi) {
                $(this).html( $(this).text().replace(new RegExp("("+searchText+")", "ig"), "<u>$1</u>") );
            }
            $(this).toggle(showCurrentLi);
        });
    }
}(jQuery));


/**
 * Popup/Modal Plugin
 */
(function($) {
    $.fn.modal = function() {
        return this.each(function(i) {

        });
    };

    $(document).on("click", ".modal .modal-close", function() {
        $(this).parents(".modal").hide();
    });

    $(document).on("click", "[data-modal]", function() {
        $(".modal."+$(this).data("modal")).show();
    });

    $(document).on("keyup", function(e) {
        if(e.key == "Escape") {
            $(".modal:visible").hide();
            $(".quickActive").focus();
            $(".quickActive").removeClass("quickActive");
        }
    });
}(jQuery));
$.fn.modal(); // Invoking data dependent modals


/**
 * Alert/Notification Plugin
 * Autohide after 8 Seconds,
 * Colors Available : red, green, yellow, blue, orange, pink, purple, teal
 */
(function($) {
    $.alert = function(message, color) {
        if($(".alerts-wrap").length === 0) { $("body").append("<div class='alerts-wrap'></div>"); }
        if(!color) { color = "blue"; }

        var alertHtml = [
            "<div class='alert alert-",color,"'>",
                "<div class='alert-close'><i class='ti-close'></i></div>",
                "<div class='alert-content'>",message,"</div>",
            "</div>"
        ].join("");

        $(alertHtml).hide().appendTo(".alerts-wrap").fadeIn("fast");

        setTimeout(function() {
            var target = $(".alerts-wrap .alert:not(.pipelined):first");
            target.addClass("pipelined");
            target.fadeOut("fast", function() {
                target.remove();
            });
        }, 8000);
    };

    $(document).on("click", ".alert .alert-close", function() {
        var target = $(this).parents(".alert");
        target.addClass("pipelined");
        target.fadeOut("fast", function() {
            target.remove();
        });
    });
}(jQuery));