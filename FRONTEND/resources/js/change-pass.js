$(function() {

    $(document).on('click', '#updatePass', function() {
        if($('.old_pass').val() == "" || $('.new_pass').val() == "") {
            $('.errormsg').html("Both passwords are required.");
            return;
        }

        $.post(
            config.url + "user/changepass",
            {
                old_pass : $('.old_pass').val(),
                new_pass : $('.new_pass').val()
            },
            function( response ) {
                if(response.success) {
                    window.location.replace( config.url + "logout" );
                } else {
                    $('.errormsg').html( response.message );
                }
            },
            'JSON'
        );
    });

});