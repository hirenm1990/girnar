<?php

class DocumentController extends Controller {

    public function upload( $contract_id ) {
        if(isset($_FILES) && !empty($_FILES)) {
            $name = 'C'.$contract_id .' ('. uniqid() . ') ' . $_FILES["file"]["name"];
            $tmp_name = $_FILES["file"]["tmp_name"];
            $type = $_FILES["file"]["type"];
            $size = $_FILES["file"]["size"];

            $pathinfo = pathinfo($_FILES["file"]["name"]);
            $ext = $pathinfo["extension"];
            $filename = $pathinfo["filename"];
            $basename = $pathinfo["basename"];

            # Prepare upload path, [ directory will be, "uploads/2016/08/*.*" ]
            $upload = date('Y') . DS . date('m') . DS;
            if(!file_exists(ROOT . 'uploads' . DS . $upload)) {
                mkdir(ROOT . 'uploads' . DS . $upload, 0777, true);
            }
            $upload = $upload . $name;

            if(move_uploaded_file($tmp_name, ROOT . 'uploads' . DS . $upload)) {
                $data = [
                    'contract_id' => $contract_id,
                    'name' => $_FILES["file"]["name"],
                    'location' => $upload,
                    'size' => $size,
                    'type' => $type,
                    'extension' => $ext
                ];
                API::post('contract/document', $data);
                echo "success";
            } else {
                echo "failed";
            }
        }
    }

    public function upload_multiple( $contract_id ) {
        if(isset($_FILES) && !empty($_FILES)) {
            foreach($_FILES["file"]["name"] as $k => $v) {
                $name = 'C'.$contract_id .' ('. uniqid() . ') ' . $_FILES["file"]["name"][$k];
                $tmp_name = $_FILES["file"]["tmp_name"][$k];
                $type = $_FILES["file"]["type"][$k];
                $size = $_FILES["file"]["size"][$k];

                $pathinfo = pathinfo($_FILES["file"]["name"][$k]);
                $ext = $pathinfo["extension"];
                $filename = $pathinfo["filename"];
                $basename = $pathinfo["basename"];

                # Prepare upload path, [ directory will be, "uploads/2016/08/*.*" ]
                $upload = date('Y') . DS . date('m') . DS;
                if(!file_exists(ROOT . 'uploads' . DS . $upload)) {
                    mkdir(ROOT . 'uploads' . DS . $upload, 0777, true);
                }
                $upload = $upload . $name;

                if(move_uploaded_file($tmp_name, ROOT . 'uploads' . DS . $upload)) {
                    $data = [
                        'contract_id' => $contract_id,
                        'name' => $_FILES["file"]["name"][$k],
                        'location' => $upload,
                        'size' => $size,
                        'type' => $type,
                        'extension' => $ext
                    ];
                    API::post('contract/document', $data);
                    echo "success";
                } else {
                    echo "failed";
                }
            }
        }
    }

    public function remove( $doc_id ) {
        $doc = API::get('document/' . $doc_id);
        $file = ROOT . 'uploads/' . $doc['location'];

        if(file_exists($file)) {
            unlink($file);
        }

        API::delete('contract/document/' . $doc_id);
        $this->redirect( $_SERVER['HTTP_REFERER'] . '#documentsTab', false );
    }

}