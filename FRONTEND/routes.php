<?php

# API testing page - Remove it in final product
Router::get('api', function() {
    if(isset($_GET['url'])) {
        $result = API::get($_GET['url'], array(), true);
        echo $result;
    } else {
        echo "<form method='get' action=''><input type='text' name='url'><button type='submit'>Get Result</button></form>";
    }
});

# Pages without need of auth
# will be redirected to dashboard if authorized
Router::get('login', 'login@index');
Router::post('login', 'login@post');
Router::get('logout', 'login@logout');

# Pages with auth needed
# will be redirected to login page if not authorized

Router::group(['middleware' => 'auth'], function() {

    # Dashboard / Homepage
    Router::get('/', 'home@index');

    # Contract related routes
    Router::get('contracts', 'contract@index'); # Contracts Register
	Router::get('contracts2', 'contract@index2'); # Contracts Register 2nd

	Router::get('contract/new', 'contract@_new'); # Create new contract page
	Router::get('contract/duplicate/{contract-id}', 'contract@duplicate'); # Duplicate existing contract
    Router::post('contract', 'contract@create'); # New Contract Post request
    Router::get('contract/new/shipment/{contract-id}', 'contract@newshipment'); # Add new shipment page
    Router::post('contract/new/shipment', 'contract@createshipment'); # Add new shipment page

	Router::get('contract/edit/{id}', 'contract@edit'); # Edit contract page
    Router::post('contract/edit/{id}', 'contract@update'); # Update Contract Post request
    Router::post('shipment/edit/{id}', 'contract@updateshipment'); # Update Contract Post request

    // Router::get('contract/vgm/{shipment_id}', 'contract@getvgm');
    Router::post('contract/vgm/{shipment_id}', 'contract@updatevgm');

    Router::post('upload/{contract_id}', 'document@upload'); # Upload document to contract
    Router::get('document/remove/{document_id}', 'document@remove'); # Remove document from contract

    Router::post('contract/do/{shipment_id}', 'contract@updatedo'); # Update Delivery Order Tab
    Router::post('contract/rm/{shipment_id}', 'contract@updaterm'); # Update Raw Material Tab
    Router::post('contract/scheme/{shipment_id}', 'contract@updatescheme'); # Update Contract Schemes Tab



    Router::get('contract/remove/{contract_id}', 'contract@remove'); # Remove whole contract
    Router::get('shipment/remove/{shipment_id}', 'contract@removeshipment'); # Remove selected shipment

    Router::post('shipment/stuffing/{shipment_id}', 'contract@updateStuffing'); # Update Stuffing Tab Container Details
    Router::post('shipment/comminvoice/{shipment_id}', 'contract@updateCommInvoice'); # Update Comm. Invoice Details

    # Quotation Sheet
    Router::get('quotation', 'quotation@index');
    Router::get('quotationPartial', 'quotation@partial');
    Router::post('quotation', 'quotation@store');


    # Purchase
    Router::get('purchase', 'purchase@index');
    Router::post('purchase', 'purchase@store');
    Router::get('purchase/remove/{id}', 'purchase@remove');
    
    Router::get('purchase/edit/{id}', 'purchase@edit');
    Router::post('purchase/update/{id}', 'purchase@update');

    # Stock
    Router::get('purchase/stock', 'purchase@stock');

    # Autogenerate routes for basic CRUD operations
    Router::auto('importer', 'importer');
    Router::auto('product', 'product');
    Router::auto('scheme', 'scheme');
    Router::auto('surveyor', 'surveyor');
    Router::auto('country', 'country');
    Router::auto('port', 'port');
    Router::auto('forwarder', 'forwarder');
    Router::auto('paymentterm', 'paymentterm');
    Router::auto('deliveryterm', 'deliveryterm');
    Router::auto('dischargeport', 'dischargeport');
    Router::auto('finaldestination', 'finaldestination');
    Router::auto('package', 'package');
    Router::auto('stockproduct', 'stockproduct');
    Router::auto('stockvendor', 'stockvendor');
    

    # Company profile
    Router::get('company', 'company@index');
    Router::post('company', 'company@update');


    # Reports page
    Router::get('reports', 'report@index'); # Dev. report
    Router::get('reports/contracts/weekly-sheet', 'report@contractWeeklySheet');
    Router::get('reports/contracts/girnar-master', 'report@contractGirnarMaster');
    Router::get('reports/weeklycontracts', 'report@weeklycontractsdone');
    Router::post('reports/weeklycontracts', 'report@weeklycontractsdonePOST');
    
    Router::get('reports/weeklydocument', 'report@weeklydocument');
    Router::post('reports/weeklydocument', 'report@weeklydocumentPOST');
    
    Router::get('reports/weeklystuffing', 'report@weeklystuffingReport');
    Router::post('reports/weeklystuffing', 'report@weeklystuffingReportPOST');
   
    Router::get('reports/weeklyfreight', 'report@weeklyfreight');
    Router::post('reports/weeklyfreight', 'report@weeklyfreightPOST');
    
    Router::get('reports/weeklyinvoice', 'report@weeklyinvoiceReport');
    Router::post('reports/weeklyinvoice', 'report@weeklyinvoiceReportPOST');

    #report format 

     Router::get('reports/format', 'report@reportformat');
     Router::get('reports/bookingdetail', 'report@bookingdetail');
     Router::get('reports/shippingschedule', 'report@shippingschedule');
     Router::get('reports/exportstuffingschedule', 'report@exportstuffingschedule');
     Router::get('reports/accountmaster/{shipment_id}', 'report@accountmaster');

    # User profile
    Router::get('user/profile', 'user@profile');
    Router::post('user/profile', 'user@update');

    # Change password
    Router::get('user/changepass', 'user@changepass');
    Router::post('user/changepass', 'user@updatepass');

    # User Management
    Router::get('user/index', 'user@index');
    Router::get('user/new', 'user@_new');
    Router::post('user', 'user@create');
    Router::get('user/edit/{id}', 'user@edit');
    Router::post('user/edit/{id}', 'user@updateRestrictedArea');
    Router::get('user/remove/{id}', 'user@remove');

    # App settings
    Router::get('app/settings', 'app@index');


    # Print previews
    Router::get('print/{target}/{id}', 'print@index');
    Router::get('print/{target}/{id}/{id2}', 'print@index2');


    # User Log
    Router::get('log', 'log@index');


    # Direct API Calling (only GET method)
    Router::post('api/{:any}', 'home@api');


});