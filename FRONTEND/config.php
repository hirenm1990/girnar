<?php

# Start session
session_start();

# Disable error reporting in final
error_reporting(0);
// error_reporting(E_ALL);

# Directory root
define ('DS', DIRECTORY_SEPARATOR);
define ('ROOT', dirname(__FILE__) . DS);

# API configuration
define ('API_URL', 'http://localhost/girnar/API/');
define ('API_APPID', '1-DESKTOP');
define ('API_KEY', '123456789');

# Site configuration
define ('SITE_URL', 'http://localhost/girnar/FRONTEND');
define ('RESOURCES', 'http://localhost/girnar/FRONTEND/resources');

# Load required files
require_once "includes/controller.php";
require_once "includes/router.php";
require_once "includes/cache.php";
require_once "includes/request.php";
require_once "includes/api.php";
require_once "includes/functions.php";
require_once "includes/PHPExcel/PHPExcel.php";

# Autoload middlewares
function autoload_middleware( $class ) {
	if(file_exists(ROOT . 'middlewares/' . strtolower($class) . '.php')) {
		require_once (ROOT . 'middlewares/' . strtolower($class) . '.php');
	}
}
spl_autoload_register( 'autoload_middleware' );

# Load twig templating engine
require_once "includes/Twig/Autoloader.php";
Twig_Autoloader::register();

# Load routes file
require_once "routes.php";