<?php

class Controller {

    public function __construct() {
    }

    /**
     * Convert d-m-Y date format to Y-m-d format
     */
    public function ymd( $date ) {
        if(empty($date)) {
            return "0000-00-00";
        }

        $save = $date;
        $date = explode("-", $date);
        if(count($date) === 3) {
            return $date[2] . '-' . $date[1] . '-' . $date[0];
        } else {
            return "0000-00-00";
        }
    }

    /**
     * Convert d-m-Y H:i format to Y-m-d H:i:s format
     */
    public function ymdt( $date ) {
        if(empty($date)) {
            return "0000-00-00 00:00:00";
        }

        $date = explode("-", $date);
        if(count($date) === 3) {
            $parsed = array_filter(explode(" ", $date[2]));
            $year = $parsed[0];
            $time = $parsed[1] . ':00';

            return $year . '-' . $date[1] . '-' . $date[0] . ' ' . $time;
        } else {
            return "0000-00-00 00:00:00";
        }
    }

    /**
     * Add Entry To MySQL `LOG` table for given parameters
     */
    public function log( $table_name, $table_id, $operation, $log ) {
        $request = Request::all();
        $log = new Log;
        $log->company_id = $request['_companyid'];
        $log->user_id = $request['_user'];
        $log->table_name = $table_name;
        $log->table_id = $table_id;
        $log->operation = $operation;
        $log->log = $log;
    }

}