<?php

class PrintController extends Controller {

    public function index( $target, $id ) {
        $func = '__' . $target;
        $this->$func( $id );
    }

    public function index2( $target, $id, $id2 ) {
        $func = '__' . $target;
        $this->$func( $id, $id2 );
    }

    public function __proforma_invoice( $id ) {
        $data = [
            'Title' => 'Proforma Invoice - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/c/' . $id)
        ];
        $this->render('print/proforma_invoice', $data);
    }

    public function __proforma_invoice_2( $shipment_id ) {
        $data = [
            'Title' => 'Proforma Invoice',
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $shipment_id)
        ];
        $this->render('print/proforma_invoice_2', $data);
    }

    public function __sales_order( $id ) {
        $data = [
            'Title' => 'Proforma Invoice - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/c/' . $id)
        ];
        $this->render('print/sales_order', $data);
    }

    public function __appendix_1( $id ) {
        $data = [
            'Title' => 'appendix-1 - '.$id,
            'Contract' => API::get('contract/' . $id)
        ];
        $this->render('print/appendix_1',$data);
    }
    public function __appendix_iii( $id ) {
        $data = [
            'Title' => 'appendix-III - '.$id,
        ];
        $this->render('print/appendix_iii',$data);
    }

    public function __invoice_cum_packing( $id, $id2 = "" )
    {
        $data = [
            'Title' => 'Invoice Cum Packing List - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        /**
         * Trim data if multiple invoice is selected
         */
        if($data['Contract']['shipment']['multiple_invoice'] == 1) {
            $multi_invoice_data = json_decode($data['Contract']['shipment']['multiple_invoice_data'], true);
            if(isset($multi_invoice_data[$id2])) {
                $temp = $multi_invoice_data[$id2];
                $data['Contract']['shipment']['invoice_no'] = $temp['invoice_no'];
                $data['Contract']['shipment']['export_invoice_date'] = $temp['invoice_date'];
            }

            $start = 0;
            $end = 0;
            $start40 = 0;
            $end40 = 0;
            $resume = 1;
            foreach($multi_invoice_data as $k => $row) {

                if($resume == 1) {
                    $start = $end;
                    $start40 = $end40;

                    $end = $row['uptotwenty'] + $start;
                    $end40 = $row['uptoforty'] + $start40;
                }

                if($k == $id2) {
                    $resume = 0;

                    // Trim container products & replace quantity
                    foreach($data['Contract']['shipment']['products'] as $x => $product) {
                        if($row['product_' . $x] != 0 && $row['product_' . $x] != "") {
                            $data['Contract']['shipment']['products'][$x]['quantity'] = $row['product_' . $x];
                            $data['Contract']['shipment']['products'][$x]['total_amount'] = $row['product_' . $x] * $data['Contract']['shipment']['products'][$x]['rate'];
                        } else {
                            unset( $data['Contract']['shipment']['products'][$x] );
                        }
                    }
                    // Trim container products ends
                }
            }

            $final_containers = [];
            $twenty_containers = array_slice($containers, 0, $data['Contract']['shipment']['container_size'][0]);
            $forty_containers = array_slice($containers, $data['Contract']['shipment']['container_size'][0], $data['Contract']['shipment']['container_size'][1]);

            $i = 1;
            $container_size_twenty = 0;
            $container_size_forty = 0;
            foreach($twenty_containers as $container) {
                if($start < $i && $end >= $i) {
                    $final_containers[] = $container;
                    $container_size_twenty++;
                }

                $i++;
            }

            $i = 1;
            foreach($forty_containers as $container) {
                if($start40 < $i && $end40 >= $i) {
                    $final_containers[] = $container;
                    $container_size_forty++;
                }

                $i++;
            }

            $data['Contract']['shipment']['container_size'][0] = $container_size_twenty;
            $data['Contract']['shipment']['container_size'][1] = $container_size_forty;


            $final_container_data = [];
            $i = 1;
            foreach($container_data['twentyContainer'] as $k => $v) {
                if($start < $i && $end >= $i) {
                    $final_container_data['twentyContainer'][] = $v;
                    $final_container_data['twentySelfSeal'][] = $container_data['twentySelfSeal'][$k];
                    $final_container_data['twentyLineSeal'][] = $container_data['twentyLineSeal'][$k];
                    $final_container_data['twentyPkg'][] = $container_data['twentyPkg'][$k];
                    $final_container_data['twentyGw'][] = $container_data['twentyGw'][$k];
                    $final_container_data['twentyNw'][] = $container_data['twentyNw'][$k];
                }
                $i++;
            }

            $i = 1;
            foreach($container_data['fortyContainer'] as $k => $v) {
                if($start40 < $i && $end40 >= $i) {
                    $final_container_data['fortyContainer'][] = $v;
                    $final_container_data['fortySelfSeal'][] = $container_data['fortySelfSeal'][$k];
                    $final_container_data['fortyLineSeal'][] = $container_data['fortyLineSeal'][$k];
                    $final_container_data['fortyPkg'][] = $container_data['fortyPkg'][$k];
                    $final_container_data['fortyGw'][] = $container_data['fortyGw'][$k];
                    $final_container_data['fortyNw'][] = $container_data['fortyNw'][$k];
                }
                $i++;
            }

            $containers = $final_containers;
            $container_data = $final_container_data;
        }
        // Trimming ends...

        $data['Contract']['shipments'][] = $data['Contract']['shipment'];

        ## Prepare marks column's data
        /*$marks_data = [];
        foreach($containers as $container) {
            foreach($data['Contract']['shipment']['products'] as $index => $product) {
                if($container['np'][$index] != "" && $container['np'][$index] != 0) {
                    $marks_data[] = "{$container['pn'][$index]}<br>" .
                        "Lot No : {$container['bn'][$index]}<br>" .
                        "Net Weight : {$container['nw'][$index]}<br>" .
                        "Gross Weight : {$container['gw'][$index]}<br>" .
                        "MADE IN INDIA";
                }
            }
        }
        $data['marks_data'] = implode("<br><br>", $marks_data);*/
        $data['marks_data'] = nl2br($data['Contract']['shipment']['markings']);


        ## Prepare total net weight & gross weight
        $gross_weight = 0;
        $net_weight = 0;
        foreach($container_data['twentyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['fortyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['twentyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        foreach($container_data['fortyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        $data['gross_weight'] = $gross_weight;
        $data['net_weight'] = $net_weight;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        // $data['total_package'] = $totalPkg . " " . $pkgType;
        if($data['Contract']['shipment']['flexitank'] == 0){
            $data['total_package'] = $totalPkg . " " . $pkgType;
        }else{
            $data['total_package'] = $pkgType;
        } 
        // Calculate total packages used & get it's general package type too ends...

        // Check if flexi
        if($data['Contract']['shipment']['flexitank']) {
            $data['total_package'] = $data['Contract']['shipment']['container_size'][0] + $data['Contract']['shipment']['container_size'][1] . " Flexi";
        }
        // echo json_encode($data['Contract']['shipment']);
        // exit();


        // Prepare DBK Sr. No. based on products used
        $uniq_dbk = [];
        foreach($data['Contract']['shipment']['products'] as $product) {
            $uniq_dbk[] = mb_substr($product['product']['hs_code'], 0, 4).'B';
        }
        $data['dbk_sr_no'] = implode(" & ", array_unique(array_filter($uniq_dbk)));
        // Prepare DBK Sr. No. based on products used ends...


        // $containers_detail2 = json_decode($data['Contract']['shipment']['containers_detail'], true);
        // echo json_encode($containers_detail2);
        // exit();

        $this->render('print/invoice_cum_packing', $data);
    }

    public function __self_sealing( $id, $id2 = "" )
    {
        $data = [
            'Title' => 'Self Sealing - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];


        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        /**
         * Trim data if multiple invoice is selected
         */
        if($data['Contract']['shipment']['multiple_invoice'] == 1) {
            $multi_invoice_data = json_decode($data['Contract']['shipment']['multiple_invoice_data'], true);
            if(isset($multi_invoice_data[$id2])) {
                $temp = $multi_invoice_data[$id2];
                $data['Contract']['shipment']['invoice_no'] = $temp['invoice_no'];
                $data['Contract']['shipment']['export_invoice_date'] = $temp['invoice_date'];
            }

            $start = 0;
            $end = 0;
            $start40 = 0;
            $end40 = 0;
            $resume = 1;
            foreach($multi_invoice_data as $k => $row) {

                if($resume == 1) {
                    $start = $end;
                    $start40 = $end40;

                    $end = $row['uptotwenty'] + $start;
                    $end40 = $row['uptoforty'] + $start40;
                }

                if($k == $id2) {
                    $resume = 0;

                    // Trim container products & replace quantity
                    foreach($data['Contract']['shipment']['products'] as $x => $product) {
                        if($row['product_' . $x] != 0 && $row['product_' . $x] != "") {
                            $data['Contract']['shipment']['products'][$x]['quantity'] = $row['product_' . $x];
                            $data['Contract']['shipment']['products'][$x]['total_amount'] = $row['product_' . $x] * $data['Contract']['shipment']['products'][$x]['rate'];
                        } else {
                            unset( $data['Contract']['shipment']['products'][$x] );
                        }
                    }
                    // Trim container products ends
                }
            }

            $final_containers = [];
            $twenty_containers = array_slice($containers, 0, $data['Contract']['shipment']['container_size'][0]);
            $forty_containers = array_slice($containers, $data['Contract']['shipment']['container_size'][0], $data['Contract']['shipment']['container_size'][1]);

            $i = 1;
            foreach($twenty_containers as $container) {
                if($start < $i && $end >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }

            $i = 1;
            foreach($forty_containers as $container) {
                if($start40 < $i && $end40 >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }


            $final_container_data = [];
            $i = 1;
            foreach($container_data['twentyContainer'] as $k => $v) {
                if($start < $i && $end >= $i) {
                    $final_container_data['twentyContainer'][] = $v;
                    $final_container_data['twentySelfSeal'][] = $container_data['twentySelfSeal'][$k];
                    $final_container_data['twentyLineSeal'][] = $container_data['twentyLineSeal'][$k];
                    $final_container_data['twentyPkg'][] = $container_data['twentyPkg'][$k];
                    $final_container_data['twentyGw'][] = $container_data['twentyGw'][$k];
                    $final_container_data['twentyNw'][] = $container_data['twentyNw'][$k];
                }
                $i++;
            }

            $i = 1;
            foreach($container_data['fortyContainer'] as $k => $v) {
                if($start40 < $i && $end40 >= $i) {
                    $final_container_data['fortyContainer'][] = $v;
                    $final_container_data['fortySelfSeal'][] = $container_data['fortySelfSeal'][$k];
                    $final_container_data['fortyLineSeal'][] = $container_data['fortyLineSeal'][$k];
                    $final_container_data['fortyPkg'][] = $container_data['fortyPkg'][$k];
                    $final_container_data['fortyGw'][] = $container_data['fortyGw'][$k];
                    $final_container_data['fortyNw'][] = $container_data['fortyNw'][$k];
                }
                $i++;
            }

            $containers = $final_containers;
            $container_data = $final_container_data;
        }
        // Trimming ends...


        $data['Info'] = $data['Contract']['shipment'];
        // $data['Info'] = $data['Contract']['contractProduct']['product'];

        //$data['Containers'] = json_decode($data['Contract']['shipment']['containers_detail'], true);
        $data['Containers'] = $container_data;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        $data['total_package'] = $totalPkg . " " . $pkgType;
        $data['total_containers'] = count($containers);

        $data['containers'] = $containers;

        $this->render('print/self_sealing', $data);
    }

    public function __excise_sealing( $id, $id2 = "" )
    {
        $data = [
            'Title' => 'Excise Sealing - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];


        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        /**
         * Trim data if multiple invoice is selected
         */

        if($data['Contract']['shipment']['multiple_invoice'] == 1) {
            $multi_invoice_data = json_decode($data['Contract']['shipment']['multiple_invoice_data'], true);
            if(isset($multi_invoice_data[$id2])) {
                $temp = $multi_invoice_data[$id2];
                $data['Contract']['shipment']['invoice_no'] = $temp['invoice_no'];
                $data['Contract']['shipment']['export_invoice_date'] = $temp['invoice_date'];
            }

            $start = 0;
            $end = 0;
            $start40 = 0;
            $end40 = 0;
            $resume = 1;
            foreach($multi_invoice_data as $k => $row) {

                if($resume == 1) {
                    $start = $end;
                    $start40 = $end40;

                    $end = $row['uptotwenty'] + $start;
                    $end40 = $row['uptoforty'] + $start40;
                }

                if($k == $id2) {
                    $resume = 0;
                }
            }

            $final_containers = [];
            $twenty_containers = array_slice($containers, 0, $data['Contract']['shipment']['container_size'][0]);
            $forty_containers = array_slice($containers, $data['Contract']['shipment']['container_size'][0], $data['Contract']['shipment']['container_size'][1]);

            $i = 1;
            foreach($twenty_containers as $container) {
                if($start < $i && $end >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }

            $i = 1;
            foreach($forty_containers as $container) {
                if($start40 < $i && $end40 >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }


            $final_container_data = [];
            $i = 1;
            foreach($container_data['twentyContainer'] as $k => $v) {
                if($start < $i && $end >= $i) {
                    $final_container_data['twentyContainer'][] = $v;
                    $final_container_data['twentySelfSeal'][] = $container_data['twentySelfSeal'][$k];
                    $final_container_data['twentyLineSeal'][] = $container_data['twentyLineSeal'][$k];
                    $final_container_data['twentyPkg'][] = $container_data['twentyPkg'][$k];
                    $final_container_data['twentyGw'][] = $container_data['twentyGw'][$k];
                    $final_container_data['twentyNw'][] = $container_data['twentyNw'][$k];
                }
                $i++;
            }

            $i = 1;
            foreach($container_data['fortyContainer'] as $k => $v) {
                if($start40 < $i && $end40 >= $i) {
                    $final_container_data['fortyContainer'][] = $v;
                    $final_container_data['fortySelfSeal'][] = $container_data['fortySelfSeal'][$k];
                    $final_container_data['fortyLineSeal'][] = $container_data['fortyLineSeal'][$k];
                    $final_container_data['fortyPkg'][] = $container_data['fortyPkg'][$k];
                    $final_container_data['fortyGw'][] = $container_data['fortyGw'][$k];
                    $final_container_data['fortyNw'][] = $container_data['fortyNw'][$k];
                }
                $i++;
            }

            $containers = $final_containers;
            $container_data = $final_container_data;
        }
        // Trimming ends...


        $data['Info'] = $data['Contract']['shipment'];

        //$data['Containers'] = json_decode($data['Contract']['shipment']['containers_detail'], true);
        $data['Containers'] = $container_data;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        $data['total_package'] = $totalPkg . " " . $pkgType;
        $data['total_containers'] = count($containers);

        $data['containers'] = $containers;
        // echo json_encode($data['containers']);
        // exit();

        $this->render('print/excise_sealing', $data);
    }







    public function __declaration( $id, $id2 = "" ) {
        $data = [
            'Title' => 'Self Sealing - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        /**
         * Trim data if multiple invoice is selected
         */
        if($data['Contract']['shipment']['multiple_invoice'] == 1) {
            $multi_invoice_data = json_decode($data['Contract']['shipment']['multiple_invoice_data'], true);
            if(isset($multi_invoice_data[$id2])) {
                $temp = $multi_invoice_data[$id2];
                $data['Contract']['shipment']['invoice_no'] = $temp['invoice_no'];
                $data['Contract']['shipment']['export_invoice_date'] = $temp['invoice_date'];
            }

            $start = 0;
            $end = 0;
            $start40 = 0;
            $end40 = 0;
            $resume = 1;
            foreach($multi_invoice_data as $k => $row) {

                if($resume == 1) {
                    $start = $end;
                    $start40 = $end40;

                    $end = $row['uptotwenty'] + $start;
                    $end40 = $row['uptoforty'] + $start40;
                }

                if($k == $id2) {
                    $resume = 0;

                    // Trim container products & replace quantity
                    foreach($data['Contract']['shipment']['products'] as $x => $product) {
                        if($row['product_' . $x] != 0 && $row['product_' . $x] != "") {
                            $data['Contract']['shipment']['products'][$x]['quantity'] = $row['product_' . $x];
                            $data['Contract']['shipment']['products'][$x]['total_amount'] = $row['product_' . $x] * $data['Contract']['shipment']['products'][$x]['rate'];
                        } else {
                            unset( $data['Contract']['shipment']['products'][$x] );
                        }
                    }
                    // Trim container products ends
                }
            }

            $final_containers = [];
            $twenty_containers = array_slice($containers, 0, $data['Contract']['shipment']['container_size'][0]);
            $forty_containers = array_slice($containers, $data['Contract']['shipment']['container_size'][0], $data['Contract']['shipment']['container_size'][1]);

            $i = 1;
            foreach($twenty_containers as $container) {
                if($start < $i && $end >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }

            $i = 1;
            foreach($forty_containers as $container) {
                if($start40 < $i && $end40 >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }


            $final_container_data = [];
            $i = 1;
            foreach($container_data['twentyContainer'] as $k => $v) {
                if($start < $i && $end >= $i) {
                    $final_container_data['twentyContainer'][] = $v;
                    $final_container_data['twentySelfSeal'][] = $container_data['twentySelfSeal'][$k];
                    $final_container_data['twentyLineSeal'][] = $container_data['twentyLineSeal'][$k];
                    $final_container_data['twentyPkg'][] = $container_data['twentyPkg'][$k];
                    $final_container_data['twentyGw'][] = $container_data['twentyGw'][$k];
                    $final_container_data['twentyNw'][] = $container_data['twentyNw'][$k];
                }
                $i++;
            }

            $i = 1;
            foreach($container_data['fortyContainer'] as $k => $v) {
                if($start40 < $i && $end40 >= $i) {
                    $final_container_data['fortyContainer'][] = $v;
                    $final_container_data['fortySelfSeal'][] = $container_data['fortySelfSeal'][$k];
                    $final_container_data['fortyLineSeal'][] = $container_data['fortyLineSeal'][$k];
                    $final_container_data['fortyPkg'][] = $container_data['fortyPkg'][$k];
                    $final_container_data['fortyGw'][] = $container_data['fortyGw'][$k];
                    $final_container_data['fortyNw'][] = $container_data['fortyNw'][$k];
                }
                $i++;
            }

            $containers = $final_containers;
            $container_data = $final_container_data;
        }
        // Trimming ends...


        $data['Info'] = $data['Contract']['shipment'];
        //$data['Containers'] = json_decode($data['Contract']['shipment']['containers_detail'], true);
        $data['Containers'] = $container_data;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        $data['total_package'] = $totalPkg . " " . $pkgType;
        $data['pkgType'] = $pkgType;
        $data['total_containers'] = count($containers);

        $this->render('print/declaration', $data);
    }

    public function __self_declaration( $id, $id2 = "" ) {
        $data = [
            'Title' => 'Self Sealing - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        /**
         * Trim data if multiple invoice is selected
         */
        if($data['Contract']['shipment']['multiple_invoice'] == 1) {
            $multi_invoice_data = json_decode($data['Contract']['shipment']['multiple_invoice_data'], true);
            if(isset($multi_invoice_data[$id2])) {
                $temp = $multi_invoice_data[$id2];
                $data['Contract']['shipment']['invoice_no'] = $temp['invoice_no'];
                $data['Contract']['shipment']['export_invoice_date'] = $temp['invoice_date'];
            }

            $start = 0;
            $end = 0;
            $start40 = 0;
            $end40 = 0;
            $resume = 1;
            foreach($multi_invoice_data as $k => $row) {

                if($resume == 1) {
                    $start = $end;
                    $start40 = $end40;

                    $end = $row['uptotwenty'] + $start;
                    $end40 = $row['uptoforty'] + $start40;
                }

                if($k == $id2) {
                    $resume = 0;

                    // Trim container products & replace quantity
                    foreach($data['Contract']['shipment']['products'] as $x => $product) {
                        if($row['product_' . $x] != 0 && $row['product_' . $x] != "") {
                            $data['Contract']['shipment']['products'][$x]['quantity'] = $row['product_' . $x];
                            $data['Contract']['shipment']['products'][$x]['total_amount'] = $row['product_' . $x] * $data['Contract']['shipment']['products'][$x]['rate'];
                        } else {
                            unset( $data['Contract']['shipment']['products'][$x] );
                        }
                    }
                    // Trim container products ends
                }
            }

            $final_containers = [];
            $twenty_containers = array_slice($containers, 0, $data['Contract']['shipment']['container_size'][0]);
            $forty_containers = array_slice($containers, $data['Contract']['shipment']['container_size'][0], $data['Contract']['shipment']['container_size'][1]);

            $i = 1;
            foreach($twenty_containers as $container) {
                if($start < $i && $end >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }

            $i = 1;
            foreach($forty_containers as $container) {
                if($start40 < $i && $end40 >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }


            $final_container_data = [];
            $i = 1;
            foreach($container_data['twentyContainer'] as $k => $v) {
                if($start < $i && $end >= $i) {
                    $final_container_data['twentyContainer'][] = $v;
                    $final_container_data['twentySelfSeal'][] = $container_data['twentySelfSeal'][$k];
                    $final_container_data['twentyLineSeal'][] = $container_data['twentyLineSeal'][$k];
                    $final_container_data['twentyPkg'][] = $container_data['twentyPkg'][$k];
                    $final_container_data['twentyGw'][] = $container_data['twentyGw'][$k];
                    $final_container_data['twentyNw'][] = $container_data['twentyNw'][$k];
                }
                $i++;
            }

            $i = 1;
            foreach($container_data['fortyContainer'] as $k => $v) {
                if($start40 < $i && $end40 >= $i) {
                    $final_container_data['fortyContainer'][] = $v;
                    $final_container_data['fortySelfSeal'][] = $container_data['fortySelfSeal'][$k];
                    $final_container_data['fortyLineSeal'][] = $container_data['fortyLineSeal'][$k];
                    $final_container_data['fortyPkg'][] = $container_data['fortyPkg'][$k];
                    $final_container_data['fortyGw'][] = $container_data['fortyGw'][$k];
                    $final_container_data['fortyNw'][] = $container_data['fortyNw'][$k];
                }
                $i++;
            }

            $containers = $final_containers;
            $container_data = $final_container_data;
        }
        // Trimming ends...


        $data['Info'] = $data['Contract']['shipment'];
        
        //$data['Containers'] = json_decode($data['Contract']['shipment']['containers_detail'], true);
        $data['Containers'] = $container_data;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        $data['total_package'] = $totalPkg . " " . $pkgType;
        $data['pkgType'] = $pkgType;

        $this->render('print/self_declaration', $data);
    }

    public function __office_superintendent( $id, $id2 = "" ) {
        $data = [
            'Title' => 'Self Sealing - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        /**
         * Trim data if multiple invoice is selected
         */
        if($data['Contract']['shipment']['multiple_invoice'] == 1) {
            $multi_invoice_data = json_decode($data['Contract']['shipment']['multiple_invoice_data'], true);
            if(isset($multi_invoice_data[$id2])) {
                $temp = $multi_invoice_data[$id2];
                $data['Contract']['shipment']['invoice_no'] = $temp['invoice_no'];
                $data['Contract']['shipment']['export_invoice_date'] = $temp['invoice_date'];
            }

            $start = 0;
            $end = 0;
            $start40 = 0;
            $end40 = 0;
            $resume = 1;
            foreach($multi_invoice_data as $k => $row) {

                if($resume == 1) {
                    $start = $end;
                    $start40 = $end40;

                    $end = $row['uptotwenty'] + $start;
                    $end40 = $row['uptoforty'] + $start40;
                }

                if($k == $id2) {
                    $resume = 0;

                    // Trim container products & replace quantity
                    foreach($data['Contract']['shipment']['products'] as $x => $product) {
                        if($row['product_' . $x] != 0 && $row['product_' . $x] != "") {
                            $data['Contract']['shipment']['products'][$x]['quantity'] = $row['product_' . $x];
                            $data['Contract']['shipment']['products'][$x]['total_amount'] = $row['product_' . $x] * $data['Contract']['shipment']['products'][$x]['rate'];
                        } else {
                            unset( $data['Contract']['shipment']['products'][$x] );
                        }
                    }
                    // Trim container products ends
                }
            }

            $final_containers = [];
            $twenty_containers = array_slice($containers, 0, $data['Contract']['shipment']['container_size'][0]);
            $forty_containers = array_slice($containers, $data['Contract']['shipment']['container_size'][0], $data['Contract']['shipment']['container_size'][1]);

            $i = 1;
            foreach($twenty_containers as $container) {
                if($start < $i && $end >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }

            $i = 1;
            foreach($forty_containers as $container) {
                if($start40 < $i && $end40 >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }


            $final_container_data = [];
            $i = 1;
            foreach($container_data['twentyContainer'] as $k => $v) {
                if($start < $i && $end >= $i) {
                    $final_container_data['twentyContainer'][] = $v;
                    $final_container_data['twentySelfSeal'][] = $container_data['twentySelfSeal'][$k];
                    $final_container_data['twentyLineSeal'][] = $container_data['twentyLineSeal'][$k];
                    $final_container_data['twentyPkg'][] = $container_data['twentyPkg'][$k];
                    $final_container_data['twentyGw'][] = $container_data['twentyGw'][$k];
                    $final_container_data['twentyNw'][] = $container_data['twentyNw'][$k];
                }
                $i++;
            }

            $i = 1;
            foreach($container_data['fortyContainer'] as $k => $v) {
                if($start40 < $i && $end40 >= $i) {
                    $final_container_data['fortyContainer'][] = $v;
                    $final_container_data['fortySelfSeal'][] = $container_data['fortySelfSeal'][$k];
                    $final_container_data['fortyLineSeal'][] = $container_data['fortyLineSeal'][$k];
                    $final_container_data['fortyPkg'][] = $container_data['fortyPkg'][$k];
                    $final_container_data['fortyGw'][] = $container_data['fortyGw'][$k];
                    $final_container_data['fortyNw'][] = $container_data['fortyNw'][$k];
                }
                $i++;
            }

            $containers = $final_containers;
            $container_data = $final_container_data;
        }
        // Trimming ends...


        $data['Info'] = $data['Contract']['shipment'];
        //$data['Containers'] = json_decode($data['Contract']['shipment']['containers_detail'], true);
        $data['Containers'] = $container_data;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        $data['total_package'] = $totalPkg . " " . $pkgType;
        $data['pkgType'] = $pkgType;
        $data['total_containers'] = count($containers);

        $this->render('print/office_superintendent', $data);
    }

    public function __annexure_a( $id, $id2 = "" ) {
        $data = [
            'Title' => 'Self Sealing - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        $data = [
            'Title' => 'Self Sealing - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        /**
         * Trim data if multiple invoice is selected
         */
        if($data['Contract']['shipment']['multiple_invoice'] == 1) {
            $multi_invoice_data = json_decode($data['Contract']['shipment']['multiple_invoice_data'], true);
            if(isset($multi_invoice_data[$id2])) {
                $temp = $multi_invoice_data[$id2];
                $data['Contract']['shipment']['invoice_no'] = $temp['invoice_no'];
                $data['Contract']['shipment']['export_invoice_date'] = $temp['invoice_date'];
            }

            $start = 0;
            $end = 0;
            $start40 = 0;
            $end40 = 0;
            $resume = 1;
            foreach($multi_invoice_data as $k => $row) {

                if($resume == 1) {
                    $start = $end;
                    $start40 = $end40;

                    $end = $row['uptotwenty'] + $start;
                    $end40 = $row['uptoforty'] + $start40;
                }

                if($k == $id2) {
                    $resume = 0;

                    // Trim container products & replace quantity
                    foreach($data['Contract']['shipment']['products'] as $x => $product) {
                        if($row['product_' . $x] != 0 && $row['product_' . $x] != "") {
                            $data['Contract']['shipment']['products'][$x]['quantity'] = $row['product_' . $x];
                            $data['Contract']['shipment']['products'][$x]['total_amount'] = $row['product_' . $x] * $data['Contract']['shipment']['products'][$x]['rate'];
                        } else {
                            unset( $data['Contract']['shipment']['products'][$x] );
                        }
                    }
                    // Trim container products ends
                }
            }

            $final_containers = [];
            $twenty_containers = array_slice($containers, 0, $data['Contract']['shipment']['container_size'][0]);
            $forty_containers = array_slice($containers, $data['Contract']['shipment']['container_size'][0], $data['Contract']['shipment']['container_size'][1]);

            $i = 1;
            foreach($twenty_containers as $container) {
                if($start < $i && $end >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }

            $i = 1;
            foreach($forty_containers as $container) {
                if($start40 < $i && $end40 >= $i) {
                    $final_containers[] = $container;
                }

                $i++;
            }


            $final_container_data = [];
            $i = 1;
            foreach($container_data['twentyContainer'] as $k => $v) {
                if($start < $i && $end >= $i) {
                    $final_container_data['twentyContainer'][] = $v;
                    $final_container_data['twentySelfSeal'][] = $container_data['twentySelfSeal'][$k];
                    $final_container_data['twentyLineSeal'][] = $container_data['twentyLineSeal'][$k];
                    $final_container_data['twentyPkg'][] = $container_data['twentyPkg'][$k];
                    $final_container_data['twentyGw'][] = $container_data['twentyGw'][$k];
                    $final_container_data['twentyNw'][] = $container_data['twentyNw'][$k];
                }
                $i++;
            }

            $i = 1;
            foreach($container_data['fortyContainer'] as $k => $v) {
                if($start40 < $i && $end40 >= $i) {
                    $final_container_data['fortyContainer'][] = $v;
                    $final_container_data['fortySelfSeal'][] = $container_data['fortySelfSeal'][$k];
                    $final_container_data['fortyLineSeal'][] = $container_data['fortyLineSeal'][$k];
                    $final_container_data['fortyPkg'][] = $container_data['fortyPkg'][$k];
                    $final_container_data['fortyGw'][] = $container_data['fortyGw'][$k];
                    $final_container_data['fortyNw'][] = $container_data['fortyNw'][$k];
                }
                $i++;
            }

            $containers = $final_containers;
            $container_data = $final_container_data;
        }
        // Trimming ends...


        $data['Info'] = $data['Contract']['shipment'];
        //$data['Containers'] = json_decode($data['Contract']['shipment']['containers_detail'], true);
        $data['Containers'] = $container_data;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        $data['total_package'] = $totalPkg . " " . $pkgType;
        $data['pkgType'] = $pkgType;

        $this->render('print/annexure_a', $data);
    }

    public function __packing_list( $id ) {
        $data = [
            'Title' => 'Packing List - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        $data['Contract']['shipments'][] = $data['Contract']['shipment'];
        $data['marks_data'] = nl2br($data['Contract']['shipment']['markings']);

        ## Prepare total net weight & gross weight
        $gross_weight = 0;
        $net_weight = 0;
        foreach($container_data['twentyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['fortyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['twentyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        foreach($container_data['fortyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        $data['gross_weight'] = $gross_weight;
        $data['net_weight'] = $net_weight;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        if($data['Contract']['shipment']['flexitank'] == 0){
            $data['total_package'] = $totalPkg . " " . $pkgType;
        }else{
            $data['total_package'] = $pkgType;
        }   
        // Calculate total packages used & get it's general package type too ends...


        // Prepare DBK Sr. No. based on products used
        $uniq_dbk = [];
        foreach($data['Contract']['shipment']['products'] as $product) {
            $uniq_dbk[] = $product['product']['dbk_scheme_no'];
        }
        $data['dbk_sr_no'] = implode(" & ", array_unique(array_filter($uniq_dbk)));
        // Prepare DBK Sr. No. based on products used ends...

        $data['Containers'] = json_decode($data['Contract']['shipment']['containers_detail'], true);

        $this->render('print/packing_list', $data);
    }

    public function __packing_list2( $id ) {
        $data = [
            'Title' => 'Packing List - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        $data['Contract']['shipments'][] = $data['Contract']['shipment'];
        $data['marks_data'] = nl2br($data['Contract']['shipment']['markings']);

        ## Prepare total net weight & gross weight
        $gross_weight = 0;
        $net_weight = 0;
        foreach($container_data['twentyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['fortyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['twentyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        foreach($container_data['fortyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        $data['gross_weight'] = $gross_weight;
        $data['net_weight'] = $net_weight;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        // $data['total_package'] = $totalPkg . " " . $pkgType;
        if($data['Contract']['shipment']['flexitank'] == 0){
            $data['total_package'] = $totalPkg . " " . $pkgType;
        }else{
            $data['total_package'] = $pkgType;
        } 
        // Calculate total packages used & get it's general package type too ends...


        // Prepare DBK Sr. No. based on products used
        $uniq_dbk = [];
        foreach($data['Contract']['shipment']['products'] as $product) {
            $uniq_dbk[] = $product['product']['dbk_scheme_no'];
        }
        $data['dbk_sr_no'] = implode(" & ", array_unique(array_filter($uniq_dbk)));
        // Prepare DBK Sr. No. based on products used ends...
        $data['Containers'] = json_decode($data['Contract']['shipment']['containers_detail'], true);

        $this->render('print/packing_list2', $data);
    }


    public function __commercial_invoice( $id ) {
        $data = [
            'Title' => 'Commercial Invoice - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        $data['Contract']['shipments'][] = $data['Contract']['shipment'];
        $data['marks_data'] = nl2br($data['Contract']['shipment']['markings']);

        ## Prepare total net weight & gross weight
        $gross_weight = 0;
        $net_weight = 0;
        foreach($container_data['twentyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['fortyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['twentyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        foreach($container_data['fortyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        $data['gross_weight'] = $gross_weight;
        $data['net_weight'] = $net_weight;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        
        if($data['Contract']['shipment']['flexitank'] == 0){
            $data['total_package'] = $totalPkg . " " . $pkgType;
            // Calculate total packages used & get it's general package type too ends...
        }else{
            // $data['total_package'] ="FLEXITANK";
            $data['total_package'] = $pkgType;
        }

        // Prepare DBK Sr. No. based on products used
        $uniq_dbk = [];
        foreach($data['Contract']['shipment']['products'] as $product) {
            $uniq_dbk[] = $product['product']['dbk_scheme_no'];
        }
        $data['dbk_sr_no'] = implode(" & ", array_unique(array_filter($uniq_dbk)));
        // Prepare DBK Sr. No. based on products used ends...

        $this->render('print/commercial_invoice', $data);
    }

    public function __commercial_invoice2( $id ) {
        $data = [
            'Title' => 'Commercial Invoice - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        $data['Contract']['shipments'][] = $data['Contract']['shipment'];
        $data['marks_data'] = nl2br($data['Contract']['shipment']['markings']);

        ## Prepare total net weight & gross weight
        $gross_weight = 0;
        $net_weight = 0;
        foreach($container_data['twentyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['fortyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['twentyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        foreach($container_data['fortyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        $data['gross_weight'] = $gross_weight;
        $data['net_weight'] = $net_weight;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        // $flexitank = 0;
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
            // $flexitank += $k;
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
            // $flexitank += $k;
        }
        
        if($data['Contract']['shipment']['flexitank'] == 0){
            $data['total_package'] = $totalPkg . " " . $pkgType;    
        }else{
            $data['total_package'] = $pkgType;
        }
        
        // Calculate total packages used & get it's general package type too ends...


        // Prepare DBK Sr. No. based on products used
        $uniq_dbk = [];
        foreach($data['Contract']['shipment']['products'] as $product) {
            $uniq_dbk[] = $product['product']['dbk_scheme_no'];
        }
        $data['dbk_sr_no'] = implode(" & ", array_unique(array_filter($uniq_dbk)));
        // Prepare DBK Sr. No. based on products used ends...

        $this->render('print/commercial_invoice2', $data);
    }

    public function __bill_of_exchange( $id ) {
        $data = [
            'Title' => 'Bill Of Exchange - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        $data['Contract']['shipments'][] = $data['Contract']['shipment'];
        $data['marks_data'] = nl2br($data['Contract']['shipment']['markings']);

        ## Prepare total net weight & gross weight
        $gross_weight = 0;
        $net_weight = 0;
        foreach($container_data['twentyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['fortyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['twentyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        foreach($container_data['fortyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        $data['gross_weight'] = $gross_weight;
        $data['net_weight'] = $net_weight;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        $data['total_package'] = $totalPkg . " " . $pkgType;
        // Calculate total packages used & get it's general package type too ends...


        // Prepare DBK Sr. No. based on products used
        $uniq_dbk = [];
        foreach($data['Contract']['shipment']['products'] as $product) {
            $uniq_dbk[] = $product['product']['dbk_scheme_no'];
        }
        $data['dbk_sr_no'] = implode(" & ", array_unique(array_filter($uniq_dbk)));
        // Prepare DBK Sr. No. based on products used ends...

        $this->render('print/bill_of_exchange', $data);
    }

     public function __annexure_vii( $id ) {
        $data = [
            'Title' => 'Annexure VII - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        /**
         * Get containers & it's data
         */
        $containers = json_decode($data['Contract']['shipment']['containers_detail2'], true);
        $container_data = json_decode($data['Contract']['shipment']['containers_detail'], true);

        $data['Contract']['shipments'][] = $data['Contract']['shipment'];
        $data['marks_data'] = nl2br($data['Contract']['shipment']['markings']);

        ## Prepare total net weight & gross weight
        $gross_weight = 0;
        $net_weight = 0;
        foreach($container_data['twentyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['fortyGw'] as $gw) {
            if(is_numeric($gw)) { $gross_weight += $gw; }
        }
        foreach($container_data['twentyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        foreach($container_data['fortyNw'] as $nw) {
            if(is_numeric($nw)) { $net_weight += $nw; }
        }
        $data['gross_weight'] = $gross_weight;
        $data['net_weight'] = $net_weight;

        // Calculate total packages used & get it's general package type too
        $totalPkg = 0;
        $pkgType = "";
        foreach($container_data['twentyGw'] as $k => $item) {
            $totalPkg += $container_data['twentyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['twentyPkg'][$k], ' ') != 0) ? substr($container_data['twentyPkg'][$k], strpos($container_data['twentyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        foreach($container_data['fortyGw'] as $k => $item) {
            $totalPkg += $container_data['fortyPkg'][$k];
            $pkgTypeLocal = (strpos($container_data['fortyPkg'][$k], ' ') != 0) ? substr($container_data['fortyPkg'][$k], strpos($container_data['fortyPkg'][$k], ' ')) : "Packages";
            if($pkgType == "") {
                $pkgType = $pkgTypeLocal;
            } elseif($pkgType != $pkgTypeLocal) {
                $pkgType = "Packages";
            }
        }
        $data['total_package'] = $totalPkg . " " . $pkgType;
        // Calculate total packages used & get it's general package type too ends...


        // Prepare DBK Sr. No. based on products used
        $uniq_dbk = [];
        foreach($data['Contract']['shipment']['products'] as $product) {
            $uniq_dbk[] = $product['product']['dbk_scheme_no'];
        }
        $data['dbk_sr_no'] = implode(" & ", array_unique(array_filter($uniq_dbk)));
        // Prepare DBK Sr. No. based on products used ends...

        $this->render('print/annexure_vii', $data);
    }

    public function __vgmcontainer( $id )
    {
        $data = [
            'Title' => 'Verified Gross Mass Of Container - ' . $id,
            'Company' => API::get('company/' . Cache::get('companyId')),
            'Contract' => API::get('contract/' . $id)
        ];

        // echo API::get('company'); exit();
        // var_dump($data);exit();
        // return $data;
        $this->render('print/vgmcontainer',$data);
    }



}
