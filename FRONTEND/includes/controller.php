<?php

class Controller {

    public function __construct() {
    }

    /**
     * Redirect function
     */
    public function redirect( $url = '', $prepend_site_url = true ) {
        if ($prepend_site_url) {
            header("Location: " . SITE_URL . '/' . ltrim($url, '/'));
        } else {
            header("Location: " . $url);
        }
    }

    /**
     * Display json success
     */
    public function success( $data ) {
        $data['success'] = true;
        echo json_encode( $data );
    }

    /**
     * Display json error
     */
    public function error( $msg ) {
        echo json_encode( ['success' => false, 'message' => $msg] );
    }

    /**
     * Render template using Twig template engine
     */
    public function render( $template, $data = [] ) {
        $loader = new Twig_Loader_Filesystem( ROOT . 'views' . DS );
        $twig = new Twig_Environment($loader, array(
            # 'cache' => ROOT . 'cache' . DS,
            'cache' => false,
        ));

        # Add asset() function to Twig
        $twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) {
            return sprintf( RESOURCES .'/%s', ltrim($asset, '/') );
        }));

        # Add url() function to Twig
        $twig->addFunction(new \Twig_SimpleFunction('url', function ($url = '') {
            return sprintf( SITE_URL .'/%s', ltrim($url, '/') );
        }));

        # Add json_decode() function to Twig
        $twig->addFunction(new \Twig_SimpleFunction('json_decode', function ($data = '') {
            return json_decode( $data, true );
        }));

        # Add json_encode() function to Twig
        $twig->addFunction(new \Twig_SimpleFunction('json_encode', function ($data = '') {
            return json_encode( $data, true );
        }));

        # Filter : Convert number to word
        $twig->addFilter(new Twig_SimpleFilter('to_word', 'convert_number_to_words'));

        # Filter : Convert bytes to KB, MB, GB
        $twig->addFilter(new Twig_SimpleFilter('filesize', 'formatSizeUnits'));

        # Filter : Get year_boundry from Y-m-d formatted date
        $twig->addFilter(new Twig_SimpleFilter('year_boundry', 'year_boundry'));

        # Currency Filter (Automatic)
        $twig->addFilter(new Twig_SimpleFilter('custom_currency', 'custom_currency'));

        # Currency Symbol (Automatic)
        $twig->addFilter(new Twig_SimpleFilter('currency_symbol', 'currency_symbol'));

        $template = $twig->loadTemplate( $template . '.html.twig' );
        echo $template->render( $data );
    }

    /**
     * Authorization Check | Return true if authorized
     */
    public function auth() {
        if(Cache::_isset('token') && Cache::_isset('userid')) {
            return true;
        }

        return false;
    }

}
