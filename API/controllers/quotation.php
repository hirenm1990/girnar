<?php

class QuotationController extends Controller {

    public function get() {
        return Company::find( Request::get('_companyid') );
    }

    public function update() {
        $request = Request::all();

        $company = Company::find( $request['_companyid'] );
        $company->quotation_data = json_encode($request['json_data']);

        $company->save();
    }

}