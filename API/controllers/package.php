<?php

class PackageController extends Controller {

    public function get() {
        return Package::orderBy('name')->get();
    }

    public function getById( $id ) {
        return Package::find( $id );
    }

    public function create() {
        $request = Request::all();

        $package = new Package;
        $package->name = $request['name'];
        $id = $package->save();
        return $package;
    }

    public function update( $id ) {
        $request = Request::all();

        $package = Package::find( $id );
        $package->name = $request['name'];

        $package->save();
    }

    public function remove( $id ) {
        Package::destroy( $id );
    }

}