<?php

class ContractController extends Controller {

    public function get( $type = '' ) {
        return Contract::with('shipments.products.product', 'importer')->orderBy('contract_no', 'DESC')->get();
    }

    public function getshipments() {
        $filter = (Request::get('filter')) ? Request::get('filter') : 'incomplete';
        $filter = ($filter == 'incomplete') ? ['contract', 'delivery', 'rawmaterial', 'stuffing'] : [ $filter ];

        return Shipment::with('contract.importer','deliveryorder', 'rawmaterial', 'products.product')->whereIn('shipment_status', $filter)->orderBy('created_at', 'DESC')->get();
    }

    public function getshipmentlist( $contract_id ) {
        return Shipment::where('contract_id', $contract_id)->with('port')->orderby('shipment_code')->get();
    }

    public function getContractById( $contract_id ) {
        return Contract::with('shipments.products.product', 'importer', 'port', 'surveyor', 'country')->find( $contract_id );
    }

    public function getById( $shipment_id ) {
        # Get Shipment
        $shipment = Shipment::with('products.product','deliveryorder','port','country','rawmaterial', 'schemes','vgms')->find( $shipment_id );

        # Get relevant Contract
        $contract = Contract::with('documents','port', 'country', 'importer', 'surveyor')->find( $shipment->contract_id );

        # Rearrange Array
        $contract['shipment'] = $shipment;
        $contract['do'] = $shipment['deliveryorder'];
        $contract['rm'] = $shipment['rawmaterial'];
        $contract['scheme'] = $shipment['schemes'];

        # Remove unnecessary items from Array
        unset( $shipment['deliveryorder'], $shipment['rawmaterial'], $shipment['schemes'] );

        # Return Data
        return $contract;
    }

    public function create() {
        $request = Request::all();
        $contract = new Contract;
        $contract->user_id = $request['_user'];
        $contract->contract_no = $request['contract_no'];
        $contract->contract_date = $this->ymd($request['contract_date']);
        $contract->importer_id = $request['importer_id'];
        $contract->importer_address = $request['importer_address'];
        $contract->purchase_order_no = $request['purchase_order_no'];
        $contract->surveyor_id = $request['surveyor_id'];
        $contract->port_loading = $request['port_loading'];
        $contract->port_discharge = $request['port_discharge'];
        $contract->port_discharge2 = $request['port_discharge2'];
        $contract->port_discharge3 = $request['port_discharge3'];
        $contract->final_destination_country = $request['final_destination_country'];
        $contract->final_destination = $request['final_destination'];
        $contract->final_destination2 = $request['final_destination2'];
        $contract->final_destination3 = $request['final_destination3'];
        $contract->delivery_terms = $request['delivery_terms'];
        $contract->payment_terms = $request['payment_terms'];
        // $contract->quotation_freight = json_encode($request['quotation_freight']);
        $contract->dollor_exchange_rate = $request['dollor_exchange_rate'];
        $contract->currency = $request['currency'];
        
        if(!empty($request['etaB'])){
            $contract->contract_type = 'multiple';    
        }else{
            $contract->contract_type = 'single';
        }

        # Get importer detail to be used in notifier & consignee party's address
        $importer = Importer::find($request['importer_id']);

        $contract->notifier_party = ( isset($request['notifier_party']) ) ? $importer->name . PHP_EOL . $request['importer_address'] : $request['notifier_party_address'];
        $contract->consignee_party = ( isset($request['consignee_party']) ) ? $importer->name . PHP_EOL . $request['importer_address'] : $request['consignee_party_address'];

        $contract->save();
        $return = ['Id' => $contract->Id];


        if(!empty($request['etaA'])){

            $shipment = new Shipment;
            $shipment->contract_id = $contract->Id;
            $shipment->shipment_code = $request['shipment_codeA'];
            $shipment->eta = $request['etaA'];
            $shipment->container_size = json_encode($request['container_sizeA']);
            $shipment->discharge_port = $request['discharge_portA'];
            $shipment->quotation_freight = json_encode($request['quotation_freightA']);
            $shipment->notes = $request['notesA'];
            $shipment->save();
            $shipment_id_A = $shipment->Id;

            # Add Products To Shipment
            
            foreach($request['shipment_code'] as $k => $v) {
                if($v == 'A'){
                    if(!empty($request['quantity'][$k]) && !empty($request['rate'][$k])) {
                        $product = new ContractProduct;
                        $product->contract_id = $contract->Id;
                        $product->shipment_id = $shipment_id_A;
                        $product->product_id = $request['product_id'][$k];
                        $product->package_type = $request['package'][$k];
                        $product->specification = $request['specification'][$k];
                        $product->quantity = $request['quantity'][$k];
                        $product->rate = $request['rate'][$k];
                        $product->total_amount = $request['total_amount'][$k];
                        $product->discharge_port = $request['product_discharge_port'][$k];
                        $product_id = $product->save();
                    }
                }
            }
            

            # Create Empty Delivery Order Entry
            $do = new DeliveryOrder;
            $do->contract_id = $contract->Id;
            $do->shipment_id = $shipment_id_A;
            $do->save();

            # Create Empty Raw Material Entry
            /*$rm = new RawMaterial;
            $rm->contract_id = $contract->Id;
            $rm->shipment_id = $shipment_id;
            $rm->save();*/

            # Create Empty Schemes Entry
            $scheme = new ContractScheme;
            $scheme->contract_id = $contract->Id;
            $scheme->shipment_id = $shipment_id_A;
            $scheme->schemes = "";
            $scheme->save();
        }

        if(!empty($request['etaB'])){

            $shipment = new Shipment;
            $shipment->contract_id = $contract->Id;
            $shipment->shipment_code = $request['shipment_codeB'];
            $shipment->eta = $request['etaB'];
            $shipment->container_size = json_encode($request['container_sizeB']);
            $shipment->discharge_port = $request['discharge_portB'];
            $shipment->quotation_freight = json_encode($request['quotation_freightB']);
            $shipment->notes = $request['notesC'];
            $shipment->save();
            $shipment_id_B = $shipment->Id;

            # Add Products To Shipment
            
            foreach($request['shipment_code'] as $k => $v) {
                if($v == 'B'){
                    if(!empty($request['quantity'][$k]) && !empty($request['rate'][$k])) {
                        $product = new ContractProduct;
                        $product->contract_id = $contract->Id;
                        $product->shipment_id = $shipment_id_B;
                        $product->product_id = $request['product_id'][$k];
                        $product->package_type = $request['package'][$k];
                        $product->specification = $request['specification'][$k];
                        $product->quantity = $request['quantity'][$k];
                        $product->rate = $request['rate'][$k];
                        $product->total_amount = $request['total_amount'][$k];
                        $product->discharge_port = $request['product_discharge_port'][$k];
                        $product_id = $product->save();
                    }
                }
            }

            # Create Empty Delivery Order Entry
            $do = new DeliveryOrder;
            $do->contract_id = $contract->Id;
            $do->shipment_id = $shipment_id_B;
            $do->save();

            # Create Empty Raw Material Entry
            /*$rm = new RawMaterial;
            $rm->contract_id = $contract->Id;
            $rm->shipment_id = $shipment_id;
            $rm->save();*/

            # Create Empty Schemes Entry
            $scheme = new ContractScheme;
            $scheme->contract_id = $contract->Id;
            $scheme->shipment_id = $shipment_id_B;
            $scheme->schemes = "";
            $scheme->save();
        }

        if(!empty($request['etaC'])){

            $shipment = new Shipment;
            $shipment->contract_id = $contract->Id;
            $shipment->shipment_code = $request['shipment_codeC'];
            $shipment->eta = $request['etaC'];
            $shipment->container_size = json_encode($request['container_sizeC']);
            $shipment->discharge_port = $request['discharge_portC'];
            $shipment->quotation_freight = json_encode($request['quotation_freightC']);
            $shipment->notes = $request['notesC'];
            $shipment->save();
            $shipment_id_C = $shipment->Id;

            # Add Products To Shipment
            
            foreach($request['shipment_code'] as $k => $v) {
                if($v == 'C'){
                    if(!empty($request['quantity'][$k]) && !empty($request['rate'][$k])) {
                        $product = new ContractProduct;
                        $product->contract_id = $contract->Id;
                        $product->shipment_id = $shipment_id_C;
                        $product->product_id = $request['product_id'][$k];
                        $product->package_type = $request['package'][$k];
                        $product->specification = $request['specification'][$k];
                        $product->quantity = $request['quantity'][$k];
                        $product->rate = $request['rate'][$k];
                        $product->total_amount = $request['total_amount'][$k];
                        $product->discharge_port = $request['product_discharge_port'][$k];
                        $product_id = $product->save();
                    }
                }
            }

            # Create Empty Delivery Order Entry
            $do = new DeliveryOrder;
            $do->contract_id = $contract->Id;
            $do->shipment_id = $shipment_id_C;
            $do->save();

            # Create Empty Raw Material Entry
            /*$rm = new RawMaterial;
            $rm->contract_id = $contract->Id;
            $rm->shipment_id = $shipment_id;
            $rm->save();*/

            # Create Empty Schemes Entry
            $scheme = new ContractScheme;
            $scheme->contract_id = $contract->Id;
            $scheme->shipment_id = $shipment_id_C;
            $scheme->schemes = "";
            $scheme->save();
        }

        if(!empty($request['etaD'])){

            $shipment = new Shipment;
            $shipment->contract_id = $contract->Id;
            $shipment->shipment_code = $request['shipment_codeD'];
            $shipment->eta = $request['etaD'];
            $shipment->container_size = json_encode($request['container_sizeD']);
            $shipment->discharge_port = $request['discharge_portD'];
            $shipment->quotation_freight = json_encode($request['quotation_freightD']);
            $shipment->notes = $request['notesD'];
            $shipment->save();
            $shipment_id_D = $shipment->Id;

            # Add Products To Shipment
            
            foreach($request['shipment_code'] as $k => $v) {
                if($v == 'D'){
                    if(!empty($request['quantity'][$k]) && !empty($request['rate'][$k])) {
                        $product = new ContractProduct;
                        $product->contract_id = $contract->Id;
                        $product->shipment_id = $shipment_id_D;
                        $product->product_id = $request['product_id'][$k];
                        $product->package_type = $request['package'][$k];
                        $product->specification = $request['specification'][$k];
                        $product->quantity = $request['quantity'][$k];
                        $product->rate = $request['rate'][$k];
                        $product->total_amount = $request['total_amount'][$k];
                        $product->discharge_port = $request['product_discharge_port'][$k];
                        $product_id = $product->save();
                    }
                }
            }

            # Create Empty Delivery Order Entry
            $do = new DeliveryOrder;
            $do->contract_id = $contract->Id;
            $do->shipment_id = $shipment_id_D;
            $do->save();

            # Create Empty Raw Material Entry
            /*$rm = new RawMaterial;
            $rm->contract_id = $contract->Id;
            $rm->shipment_id = $shipment_id;
            $rm->save();*/

            # Create Empty Schemes Entry
            $scheme = new ContractScheme;
            $scheme->contract_id = $contract->Id;
            $scheme->shipment_id = $shipment_id_D;
            $scheme->schemes = "";
            $scheme->save();
        }
            


        $return['shipment_id'] = $shipment_id_A;
        return $return;
    }

    public function createshipment() {
        $request = Request::all();

        $shipment = new Shipment;
        $shipment->contract_id = $request['contract_id'];
        $shipment->shipment_code = $request['shipment_code'];
        $shipment->eta = $request['eta'];
        $shipment->container_size = json_encode($request['container_size']);
        $shipment->notes = $request['notes'];

        $shipment->save();
        $shipment_id = $shipment->Id;

        # Add Products To Shipment
        foreach($request['product_id'] as $k => $v) {
            if(!empty($request['quantity'][$k]) && !empty($request['rate'][$k])) {
                $product = new ContractProduct;
                $product->contract_id = $request['contract_id'];
                $product->shipment_id = $shipment_id;
                $product->product_id = $v;
                $product->package_type = $request['package'][$k];
                $product->specification = $request['specification'][$k];
                $product->quantity = $request['quantity'][$k];
                $product->rate = $request['rate'][$k];
                $product->total_amount = $request['total_amount'][$k];
                $product->discharge_port = $request['product_discharge_port'][$k];

                $product_id = $product->save();
            }
        }

        # Create Empty Delivery Order Entry
        $do = new DeliveryOrder;
        $do->contract_id = $request['contract_id'];
        $do->shipment_id = $shipment_id;
        $do->save();

        # Create Empty Raw Material Entry
        /*$rm = new RawMaterial;
        $rm->contract_id = $request['contract_id'];
        $rm->shipment_id = $shipment_id;
        $rm->save();*/

        # Create Empty Schemes Entry
        $scheme = new ContractScheme;
        $scheme->contract_id = $request['contract_id'];
        $scheme->shipment_id = $shipment_id;
        $scheme->schemes = "";
        $scheme->save();

        # Reset contract type
        if ( Shipment::where('contract_id', $request['contract_id'])->get()->count() > 1 ) {
            $contract = Contract::find( $request['contract_id'] );
            $contract->contract_type = "multiple";
            $contract->save();
        } else {
            $contract = Contract::find( $request['contract_id'] );
            $contract->contract_type = "single";
            $contract->save();
        }

        return $shipment_id;
    }

    public function update( $id ) {
        $request = Request::all();

        $contract = Contract::find( $id );
        $contract->contract_no = $request['contract_no'];
        $contract->contract_date = $this->ymd($request['contract_date']);
        $contract->importer_id = $request['importer_id'];
        $contract->importer_address = $request['importer_address'];
        $contract->purchase_order_no = $request['purchase_order_no'];
        $contract->surveyor_id = $request['surveyor_id'];
        $contract->port_loading = $request['port_loading'];
        $contract->port_discharge = $request['port_discharge'];
        $contract->port_discharge2 = $request['port_discharge2'];
        $contract->port_discharge3 = $request['port_discharge3'];
        $contract->final_destination_country = $request['final_destination_country'];
        $contract->final_destination = $request['final_destination'];
        $contract->final_destination2 = $request['final_destination2'];
        $contract->final_destination3 = $request['final_destination3'];
        $contract->delivery_terms = $request['delivery_terms'];
        $contract->payment_terms = $request['payment_terms'];
        // $contract->quotation_freight = json_encode($request['quotation_freight']);
        $contract->dollor_exchange_rate = $request['dollor_exchange_rate'];
        $contract->currency = $request['currency'];
        $contract->notifier_party = $request['notifier_party'];
        $contract->consignee_party = $request['consignee_party'];

        $contract->save();
    }

    public function updateshipment( $shipment_id ) {
        $request = Request::all();

        $shipment = Shipment::find( $shipment_id );
        $shipment->shipment_code = $request['shipment_code'];
        $shipment->eta = $request['eta'];
        $shipment->container_size = json_encode($request['container_size']);
        $shipment->quotation_freight = json_encode($request['quotation_freight']);
        $shipment->notes = $request['notes'];

        $shipment->save();

        # Remove all products under shipment first
        ContractProduct::where('shipment_id', $shipment_id)->delete();

        # Add updated products under shipment again
        foreach($request['product_id'] as $k => $v) {
            if(!empty($request['quantity'][$k]) && !empty($request['rate'][$k])) {
                $product = new ContractProduct;
                $product->contract_id = $request['contract_id'];
                $product->shipment_id = $shipment_id;
                $product->product_id = $v;
                $product->package_type = $request['package'][$k];
                $product->specification = $request['specification'][$k];
                $product->quantity = $request['quantity'][$k];
                $product->rate = $request['rate'][$k];
                $product->total_amount = $request['total_amount'][$k];
                $product->discharge_port = $request['product_discharge_port'][$k];

                $product->save();
            }
        }
    }

    public function updatedo( $shipment_id ) {
        $request = Request::all();

        $do = DeliveryOrder::find( $request['do_id'] );
        $do->do_no = $request['do_no'];
        $do->do_date = $this->ymd($request['do_date']);
        $do->do_exp_date = $this->ymd($request['do_exp_date']);
        $do->forwarder_id = $request['forwarder_id'];
        $do->shipment_line = $request['shipment_line'];
        $do->freight = $request['freight'];
        $do->total_freight = $request['total_freight'];
        $do->thc = $request['thc'];
        $do->blc = $request['blc'];
        $do->vessel_name = $request['vessel_name'];
        $do->voyage_no = $request['voyage_no'];
        $do->eta_origin = $this->ymd($request['eta_origin']);
        $do->etd_origin = $this->ymd($request['etd_origin']);
        $do->eta_destination = $this->ymd($request['eta_destination']);
        $do->eta_date = $this->ymd($request['eta_date']);
        $do->total_transit = $request['total_transit'];
        $do->vgm_cutoff = $this->ymdt($request['vgm_cutoff']);
        $do->container_gate_open = $this->ymdt($request['container_gate_open']);
        $do->gate_cutoff = $this->ymdt($request['gate_cutoff']);
        $do->document_cutoff = $this->ymdt($request['document_cutoff']);
        $do->si_cutoff = $this->ymdt($request['si_cutoff']);

        $do->trans_port = json_encode($request['trans_port']);
        $do->trans_vessel_name = json_encode($request['trans_vessel_name']);
        $do->trans_eta = json_encode($request['trans_eta']);
        $do->trans_etd = json_encode($request['trans_etd']);

        $do->stuffing_from = $this->ymd($request['stuffing_from']);
        $do->stuffing_to = $this->ymd($request['stuffing_to']);

        // $do->do_port_loading = $request['do_port_loading'];
        // $do->do_port_discharge = $request['do_port_discharge'];
        // $do->do_quotation_freight = json_encode($request['do_quotation_freight']);
        // $do->do_container_size = json_encode($request['do_container_size']);
        $do->total_detention_days = $request['total_detention_days'];
        $do->total_demurrage_days = $request['total_demurrage_days'];
        $do->change_eta_destination_port = $this->ymd($request['change_eta_destination_port']);
        $do->change_total_transit_time = $request['change_total_transit_time'];

        $do->save();
    }

    public function updaterm( $shipment_id ) {
        $request = Request::all();

        foreach($request['products'] as  $key => $product) {
            $rm = RawMaterial::firstOrCreate([
                'contract_id' => $request['contract_id'],
                'shipment_id' => $request['shipment_id'],
                'product_id' => $product
            ]);
            $rm->qty = $request['values'][$key];
            $rm->save();
        }

        //$rm = RawMaterial::find( $request['rawmaterial_id'] );
        //$rm->order_date = $this->ymd($request['order_date']);
        //$rm->delivery_date = $this->ymd($request['delivery_date']);

        //$rm->save();
    }

    public function updatescheme( $shipment_id ) {
        $request = Request::all();

        $scheme = ContractScheme::find( $request['scheme_id'] );
        $scheme->schemes = implode(',', $request['scheme']);
        $scheme->save();
    }

    public function remove( $contract_id ) {
        DeliveryOrder::where('contract_id', $contract_id)->delete();
        ContractProduct::where('contract_id', $contract_id)->delete();
        ContractScheme::where('contract_id', $contract_id)->delete();
        RawMaterial::where('contract_id', $contract_id)->delete();
        Shipment::where('contract_id', $contract_id)->delete();
        Document::where('contract_id', $contract_id)->delete();
        Contract::destroy( $contract_id );
    }

    public function removeshipment( $shipment_id ) {
        $shipment = Shipment::find( $shipment_id );

        DeliveryOrder::where('shipment_id', $shipment_id)->delete();
        ContractProduct::where('shipment_id', $shipment_id)->delete();
        ContractScheme::where('shipment_id', $shipment_id)->delete();
        RawMaterial::where('shipment_id', $shipment_id)->delete();
        Shipment::destroy( $shipment_id );

        # Reset contract type
        $count = Shipment::where('contract_id', $shipment['contract_id'])->get()->count();
        if (  $count == 0 ) {
            $this->remove( $shipment['contract_id'] );
        } elseif( $count == 1) {
            $contract = Contract::find( $shipment['contract_id'] );
            $contract->contract_type = "single";
            $contract->save();
        } else {
            $contract = Contract::find( $shipment['contract_id'] );
            $contract->contract_type = "multiple";
            $contract->save();
        }
    }

    public function updatestuffing( $shipment_id ) {
        $request = Request::all();
        $data = [
            'twentyContainer' => (isset($request['twentyContainer'])) ? $request['twentyContainer'] : [],
            'twentySelfSeal' => (isset($request['twentySelfSeal'])) ? $request['twentySelfSeal'] : [],
            'twentyLineSeal' => (isset($request['twentyLineSeal'])) ? $request['twentyLineSeal'] : [],
            'twentyPkg' => (isset($request['twentyPkg'])) ? $request['twentyPkg'] : [],
            'twentyGw' => (isset($request['twentyGw'])) ? $request['twentyGw'] : [],
            'twentyNw' => (isset($request['twentyNw'])) ? $request['twentyNw'] : [],

            'fortyContainer' => (isset($request['fortyContainer'])) ? $request['fortyContainer'] : [],
            'fortySelfSeal' => (isset($request['fortySelfSeal'])) ? $request['fortySelfSeal'] : [],
            'fortyLineSeal' => (isset($request['fortyLineSeal'])) ? $request['fortyLineSeal'] : [],
            'fortyPkg' => (isset($request['fortyPkg'])) ? $request['fortyPkg'] : [],
            'fortyGw' => (isset($request['fortyGw'])) ? $request['fortyGw'] : [],
            'fortyNw' => (isset($request['fortyNw'])) ? $request['fortyNw'] : []
        ];

        $data2 = [];
        $product_gw_data = [];
        $product_nw_data = [];
        $product_np_data = [];
        for($i = 1; $i < 100; $i++) {
            if(isset($request['np_' . $i])) {
                $data2[$i] = [
                    'pn' => $request['pn_' . $i],
                    //'bn' => $request['bn_' . $i],
                    'np' => $request['np_' . $i],
                    'gw' => $request['gw_' . $i],
                    'nw' => $request['nw_' . $i],
                ];

                foreach($request['contract_product_id_' . $i] as $k => $product_id) {
                    if(!isset($product_gw_data[ $product_id ])) {
                        $product_gw_data[ $product_id ] = 0;
                    }

                    if(!isset($product_nw_data[ $product_id ])) {
                        $product_nw_data[ $product_id ] = 0;
                    }

                    $product_gw_data[ $product_id ] += $request['gw_' . $i][ $k ];
                    $product_nw_data[ $product_id ] += $request['nw_' . $i][ $k ];

                    $parse = explode(" ", $request['np_' . $i][ $k ]);
                    $parse_no = $parse[0];
                    $parse_type = $parse[1];

                    if(!isset($product_np_data[ $product_id ])) {
                        $product_np_data[ $product_id ]['no'] = 0;
                        $product_np_data[ $product_id ]['type'] = $parse_type;
                    }

                    if($product_np_data[ $product_id ]['type'] != $parse_type) {
                        $product_np_data[ $product_id ]['type'] = "Packages";
                    }

                    $product_np_data[ $product_id ]['no'] += $parse_no;
                }
            }
        }

        foreach($product_gw_data as $k => $gw) {
            $p = ContractProduct::find( $k );
            $p->total_gw = $gw / 1000;
            $p->save();
        }

        foreach($product_nw_data as $k => $nw) {
            $p = ContractProduct::find( $k );
            $p->total_nw = $nw / 1000;
            $p->save();
        }

        foreach($product_np_data as $k => $product) {
            $p = ContractProduct::find( $k );
            $p->total_packages = $product['no'] . ' ' . $product['type'];
            $p->save();
        }

        $shipment = Shipment::find( $shipment_id );
        $shipment->containers_detail = json_encode( $data, true );
        $shipment->containers_detail2 = json_encode( $data2, true );

        $shipment->invoice_no = $request['invoice_no'];

        //$shipment->dbk_scheme_no = $request['dbk_scheme_no'];
        //$shipment->dbk_scheme = ($request['dbk_scheme_no'] != "") ? "yes" : "no";
        $shipment->dbk_scheme = "yes";

        $shipment->examination_date = $this->ymd($request['export_invoice_date']);
        $shipment->export_invoice_date = $this->ymd($request['export_invoice_date']);
        $shipment->are_no = $request['are_no'];
        $shipment->are_date = $this->ymd($request['are_date']);

        //$shipment->dbk_tariff_item_no = $request['dbk_tariff_item_no'];
        $shipment->dbk_tariff_item_no = "";

        $shipment->dbk_rate = $request['dbk_rate'];
        $shipment->examining_officer = $request['examining_officer'];
        $shipment->supervision_officer = $request['supervision_officer'];
        $shipment->file_no = $request['file_no'];
        $shipment->examined = $request['examined'];
        $shipment->markings = $request['markings'];
        $shipment->invoice_buyer_address = $request['invoice_buyer_address'];
        $shipment->invoice_notifier_address = $request['invoice_notifier_address'];
        $shipment->comm_freight = $request['freight'];
        $shipment->extra_charge_name = $request['extra_charge_name'];
        $shipment->extra_charge = $request['extra_charge'];

        // Flexi tank
        $shipment->flexitank = (isset($request['flexitank']) && $request['flexitank'] == true) ? 1 : 0;

        // Multiple invoice entry
        if(isset($request['multiple_invoice']) && $request['multiple_invoice'] == true ) {
            $multiple_invoice_data = [];    
            foreach($request['m_invoice_no'] as $k => $v) {
                $temp = [
                    'invoice_no' => $v,
                    'invoice_date' => $this->ymd($request['m_invoice_date'][$k]),
                    'uptotwenty' => $request['m_uptotwenty'][$k],
                    'uptoforty' => $request['m_uptoforty'][$k],
                ];

                for($i = 0; $i < 100; $i++) {
                    if(isset($request['product_' . $i][$k])) {
                        $temp[ 'product_' . $i ] = $request['product_' . $i][$k];
                    }
                }

                $multiple_invoice_data[] = $temp;
            }

            $shipment->multiple_invoice = 1;
            $shipment->multiple_invoice_data = json_encode($multiple_invoice_data);
        } else {
            $shipment->multiple_invoice = 0;
        }

        if(isset($request['stuffing_phase']) && $request['stuffing_phase'] == true) {
            $shipment->stuffing_phase = 1;
        }

        $shipment->save();
    }


    public function updateCommInvoice( $shipment_id ) {
        $request = Request::all();
        $shipment = Shipment::find( $shipment_id );
        $shipment->invoice_consignee_address = $request['invoice_consignee_address'];
        $shipment->comm_invoice_no = $request['comm_invoice_no'];
        $shipment->company_detail = $request['company_detail'];
        $shipment->comm_invoice_date = $this->ymd($request['comm_invoice_date']);
        $shipment->lc_no = $request['lc_no'];
        $shipment->lc_date = $this->ymd($request['lc_date']);
        $shipment->bl_no = $request['bl_no'];
        $shipment->bl_date = $this->ymd($request['bl_date']);
        $shipment->drawn_under = $request['drawn_under'];
        $shipment->specification = $request['specification'];
        
        $shipment->document_ready_on = $this->ymd($request['document_ready_on']);
        $shipment->document_status = $request['document_status'];

        $shipment->comm_invoice_note = $request['comm_invoice_note'];
        $shipment->comm_invoice_note2 = $request['comm_invoice_note2'];
        $shipment->comm_bank_no = $request['comm_bank_no'];
        $shipment->comm_bank_detail = $request['comm_bank_detail'];
        $shipment->comm_eta = $request['comm_eta'];
        $shipment->comm_invoice_discount = $request['comm_invoice_discount'];

        $shipment->company_detail = $request['company_detail'];
        $shipment->beneficiary_bank_detail = $request['beneficiary_bank_detail'];
        $shipment->importer_address = $request['importer_address'];
        $shipment->notifier_party = $request['notifier_party'];
        $shipment->voyage_no = $request['voyage_no'];
        $shipment->port_loading = $request['port_loading'];
        
        $shipment->discharge_port = $request['discharge_port'];
        $shipment->discharge_port2 = $request['discharge_port2'];
        $shipment->discharge_port3 = $request['discharge_port3'];
        
        $shipment->final_destination = $request['final_destination'];
        $shipment->final_destination2 = $request['final_destination2'];
        $shipment->final_destination3 = $request['final_destination3'];

        $shipment->final_destination_country = $request['final_destination_country'];
        $shipment->delivery_terms = $request['delivery_terms'];
        $shipment->payment_terms = $request['payment_terms'];
        $shipment->save();

        // foreach ( as $key => $value) {
        //     $product = ContractProduct::find( $request['cpid'][$k] );
        //     $product->comm_invoice_name = $value;
        //     $product->save();
        //     // if(empty($product->comm_invoice_name)){
                
        //     // }
        // }
        
        foreach($request['gross_quantity'] as $k => $v) {
            $product = ContractProduct::find( $request['cpid'][$k] );
            $product->gross_quantity = $v;
            $product->comm_invoice_name = $request['comm_invoice_name'][$k];
            $product->save();
        }
    }

    public function updatevgm( $shipment_id )
    {
        $request = Request::all();
         
        $vgm = Vgm::where('shipment_id',$shipment_id)->first();
        if($vgm->shipment_id == $shipment_id){
            $vgm->contract_id = $request['contract_id'];
            $vgm->shipment_id = $request['shipment_id'];
            
            $vgm->name_authorized = $request['name_authorized'];
            $vgm->designation_authorized = $request['designation_authorized'];
            $vgm->contact_detail = $request['contact_detail'];
            $vgm->date_time = $this->ymdt($request['date_time']);
            $vgm->hazardous = $request['hazardous'];
            $vgm->max_weight_csc = json_encode($request['max_weight_csc']);

            // $vgm->gross_mass = $gross_mass;
            
            $vgm->gross_method = json_encode($request['gm_method']);
            $vgm->gross_net = json_encode($request['gm_net']);
            $vgm->gross_tare = json_encode($request['gm_tare']);
            $vgm->gross_packing = json_encode($request['gm_packing']);

            $vgm->weight_slip_no = json_encode($request['weight_slip_no']);
            $vgm->type = json_encode($request['type']);
            $vgm->booking = json_encode($request['booking']);
            $vgm->save();
        } else {
            $vgm = new Vgm;
            $vgm->contract_id = $request['contract_id'];
            $vgm->shipment_id = $request['shipment_id'];
            
            $vgm->name_authorized = $request['name_authorized'];
            $vgm->designation_authorized = $request['designation_authorized'];
            $vgm->contact_detail = $request['contact_detail'];
            $vgm->date_time = $this->ymdt($request['date_time']);
            $vgm->hazardous = $request['hazardous'];
            $vgm->max_weight_csc = json_encode($request['max_weight_csc']);
            
            $vgm->gross_method = json_encode($request['gm_method']);
            $vgm->gross_net = json_encode($request['gm_net']);
            $vgm->gross_tare = json_encode($request['gm_tare']);
            $vgm->gross_packing = json_encode($request['gm_packing']);

            // $vgm->gross_mass = json_encode($request['gross_mass']);
            
            $vgm->weight_slip_no = json_encode($request['weight_slip_no']);
            $vgm->type = json_encode($request['type']);
            $vgm->booking = json_encode($request['booking']);
            $vgm->save();
        }
    }

}
