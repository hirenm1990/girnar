<?php

class DeliveryOrder extends Model {

    protected $table = 'contracts_do';
    protected $primaryKey = 'Id';

    public function getTransPortAttribute( $value ) {
        return json_decode( $value );
    }

    public function getTransVesselNameAttribute( $value ) {
        return json_decode( $value );
    }

    public function getTransEtaAttribute( $value ) {
        return json_decode( $value );
    }

    public function getTransEtdAttribute( $value ) {
        return json_decode( $value );
    }

    public function getDoQuotationFreightAttribute( $value ) {
        return json_decode( $value );
    }

    public function getDoContainerSizeAttribute( $value ) {
        return json_decode( $value );
    }
    // 
   


}
