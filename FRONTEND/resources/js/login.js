$(function() {

    var engine = new RainyDay({
        image: document.getElementById('loginImage'),
        parentElement: document.getElementById('loginImageWrap')
    });
    engine.rain([ [0, 2, 200], [3, 3, 1] ], 100);

    // Login button click
    $(document).on('click', '#loginBtn', function() {
        var data = {
                username : $('.username').val(),
                password : $('.password').val()
            };

        post('login', data, function( response ) {
            if( response.success ) {
                window.location.replace( response.redirect );
            } else {
                $('.errormsg').html( response.message ).fadeIn();
            }
        });

        return false;
    });

});

function post( url, data, callback ) {
    $.post(
        config.url + url,
        data,
        callback,
        'JSON'
    );
}