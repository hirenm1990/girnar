<?php

class Auth {

	public function __construct() {
		return $this->user();
	}

	public function user() {
		return true;

		# Check if all required parameters are there or not
		$request = $_REQUEST;
		if(isset($request['_token'], $request['_user'])) {
			$data = User::find( $request['_user'] );
			$token = sha1( $data['email'] . $data['username'] . $data['created_at'] );

			if($request['_token'] == $token) {
				return true;
			}
		}

		return ['error' => 'Authentication failed!'];
	}

	public static function login() {
		$request = Request::all();

		if(isset($request['username'], $request['password'])) {
			$data = User::where(['username' => $request['username'], 'password' => $request['password']])->get();

			if(!empty($data) && count($data) == 1) {
				$data = $data[0];
				return [
					'success' => true,
					'token' => sha1( $data['email'] . $data['username'] . $data['created_at'] ),
					'userid' => $data['Id']
				];
			}
		}

		return ['success' => false, 'error' => 'Invalid username or password!'];
	}
}